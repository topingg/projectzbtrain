----------------------------------------------------------------
DefineManager

You can define and enable/disable symbols easily, with GUI interface.


more informations:
http://avoex.com
https://www.facebook.com/avoexgames

----------------------------------------------------------------
How to use DefineManager

 1. Open DefineManager. (Window > Define Manager)
 2. input symbol.
 3. click 'Add define' button.
 4. click 'Apply' button.

watch video tutorial.
https://youtu.be/oJxIVQaP4sE

----------------------------------------------------------------
Attention

 DefineManager generate DefineSettings.cs and rsp files on /Assets folder.

 DO NOT EDIT OR DELETE '/Assets/DefineSettings.cs', '/Assets/gmcs.rsp', '/Assets/smcs.rsp', '/Assets/us.rsp' files


----------------------------------------------------------------
Trouble shooting

 if you use 'Microsoft Visual Studio tool for Unity', install latest version.


----------------------------------------------------------------
version history

1.0.8
 - moved menu item 'AvoEx/Define Manager' to 'Window/Define Manager', 'Tools/AvoEx/Define Manager', 'Edit/Project Settings/Define'
 - rename Util/Utility.cs to Util/UtilEvent.cs

1.0.5
 - added enable/disable all button.

1.0.4
 - added readme.txt
 - fixed refreshing automatically generated files.

1.0.3
 - added default EditorWindow size.