﻿using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
using System.IO;
#endif

/* See the "http://avoex.com/avoex/default-license" for the full license governing this code. */

namespace AvoEx
{

    public partial class BundleManager
    {

        public partial class BundleInfo
        {
            [LitJson.JsonIgnore]
            public static string FILE_NAME
            {
                get { return "BundleInfo.txt"; }
            }

            public string bundleName { get; private set; }
            public bool isPreDownload { get; private set; }
            public long fileSize { get; private set; } //  outputs only.
            public uint crc { get; private set; }
        }

#if UNITY_EDITOR
        public static string EDITING_INFOS_PATH
        {
            get
            {
                return Path.Combine(BundleEditorSettings.GetSettingDataPath(), "Editor");
            }
        }

        public partial class BundleInfo
        {
            [LitJson.JsonIgnore]
            public bool isJustAdded { get; private set; }
            [LitJson.JsonIgnore]
            public bool isChanged { get; private set; }
            [LitJson.JsonIgnore]
            public bool isEdited { get; private set; }

            [LitJson.JsonIgnore]
            static Dictionary<string, BundleInfo> s_editingBundleInfos = null;
            [LitJson.JsonIgnore]
            static Dictionary<string, BundleInfo> s_savedBundleInfos = null;

            public static Dictionary<string, BundleInfo> LoadBundleInfos(string path)
            {
                Dictionary<string, BundleInfo> loadedAssetInfos = UtilFile.LoadJsonToObject<Dictionary<string, BundleInfo>>(System.IO.Path.Combine(path, FILE_NAME));
                if (loadedAssetInfos == null)
                    loadedAssetInfos = new Dictionary<string, BundleInfo>();
                return loadedAssetInfos;
            }

            public static bool IsEditingBundle(string name)
            {
                return s_editingBundleInfos.ContainsKey(name);
            }

            static BundleInfo()
            {
                s_editingBundleInfos = LoadBundleInfos(EDITING_INFOS_PATH); // refresh editing bundle info.
                Refresh(); // always needed editing info on editor.
            }

            public static bool IsEdited()
            {
                if (s_savedBundleInfos == null && s_editingBundleInfos == null)
                    return false;
                if (s_savedBundleInfos == null || s_editingBundleInfos == null)
                    return true;
                if (s_savedBundleInfos.Count != s_editingBundleInfos.Count)
                    return true;

                foreach (KeyValuePair<string, BundleInfo> savedInfo in s_savedBundleInfos)
                {
                    BundleInfo editingInfo = null;
                    s_editingBundleInfos.TryGetValue(savedInfo.Key, out editingInfo);
                    if (editingInfo == null || editingInfo.isEdited)
                        return true;
                }

                return false;
            }

            public static BundleInfo AddEditingInfo(string bundleName, bool isPreDownload = true)
            {
                BundleInfo addedInfo = new BundleInfo();
                addedInfo.bundleName = bundleName;
                addedInfo.isPreDownload = isPreDownload;
                addedInfo.isJustAdded = true;
                addedInfo.isChanged = false;
                addedInfo.isEdited = false;
                addedInfo.crc = 0;
                s_editingBundleInfos[bundleName] = addedInfo; // s_editingAssetInfos must be not null. initialized at static creator

                return addedInfo;
            }

            public static BundleInfo GetEditingInfo(string bundleName)
            {
                BundleInfo editingInfo = null;
                s_editingBundleInfos.TryGetValue(bundleName, out editingInfo); // s_editingAssetInfos must be not null. initialized at static creator
                if (editingInfo == null)
                    editingInfo = AddEditingInfo(bundleName);

                return editingInfo;
            }

            /// <summary>
            /// update BundleInfo from AssetDataBase.
            /// and check is edited
            /// </summary>
            public static void Refresh()
            {
                //Debug.Log("BundleInfo.Refresh()");

                s_savedBundleInfos = LoadBundleInfos(EDITING_INFOS_PATH);

                List<string> listRemove = new List<string>(s_editingBundleInfos.Keys);

                // update editing BundleInfo from AssetDataBase.
                foreach (string bundleName in AssetDatabase.GetAllAssetBundleNames())
                {
                    if (bundleName == AssetManager.AssetInfo.BUNDLE_NAME)
                        continue;

                    BundleInfo editingInfo = null;
                    s_editingBundleInfos.TryGetValue(bundleName, out editingInfo);
                    if (editingInfo == null)
                    {
                        AddEditingInfo(bundleName);
                    }

                    listRemove.Remove(bundleName);
                }

                // del editing BundleInfo, not exist in AssetDataBase.
                foreach (string GUID in listRemove)
                {
                    s_editingBundleInfos.Remove(GUID);
                }

                // check edited with saved data.
                foreach (KeyValuePair<string, BundleInfo> editingInfo in s_editingBundleInfos)
                {
                    editingInfo.Value.CheckEditing();
                }
            }

            /// <summary>
            /// load saved BundleInfo from target output directory
            /// and check is changed with output data.
            /// </summary>
            /// <param name="settings">BundleEditorSettings included information of exporting target platform</param>
            /// <returns>BundleInfo from output directory</returns>
            public static Dictionary<string, BundleInfo> LoadTargetBundleInfo(BundleEditorSettings settings = null)
            {
                //Debug.Log("BundleInfo.LoadTargetBundleInfo()");

                Dictionary<string, BundleInfo> loadedBundleInfo = null;
                if (settings == null)
                {
                    loadedBundleInfo = new Dictionary<string, BundleInfo>();
                }
                else
                {
                    loadedBundleInfo = UtilFile.LoadJsonToObject<Dictionary<string, BundleInfo>>(System.IO.Path.Combine(settings.TargetOutputPath, FILE_NAME));
                    if (loadedBundleInfo == null)
                        loadedBundleInfo = new Dictionary<string, BundleInfo>();

                    // update status of editing BundleInfo from saved BundleInfo.
                    foreach (KeyValuePair<string, BundleInfo> editingInfo in s_editingBundleInfos)
                    {
                        BundleInfo targetInfo = null;
                        loadedBundleInfo.TryGetValue(editingInfo.Key, out targetInfo);
                        if (targetInfo == null)
                        {   // mark isJustAdded
                            editingInfo.Value.isJustAdded = true;
                            editingInfo.Value.isChanged = false;
                        }
                        else
                        {
                            editingInfo.Value.isJustAdded = false;
                            editingInfo.Value.isChanged = (editingInfo.Value.isPreDownload != targetInfo.isPreDownload);
                        }
                    }
                }

                return loadedBundleInfo;
            }

            /// <summary>
            /// save editing info.
            /// </summary>
            public static void Save()
            {
                UtilFile.SaveObjectToJson(s_editingBundleInfos, System.IO.Path.Combine(EDITING_INFOS_PATH, FILE_NAME));

                s_savedBundleInfos = LoadBundleInfos(EDITING_INFOS_PATH);
                Refresh();
            }

            public static void Revert()
            {
                foreach (KeyValuePair<string, BundleInfo> editingInfo in s_editingBundleInfos)
                {
                    if (editingInfo.Value.isEdited == false)
                        continue;

                    BundleInfo savedInfo = null;
                    s_savedBundleInfos.TryGetValue(editingInfo.Key, out savedInfo);
                    if (savedInfo == null)
                        continue;

                    editingInfo.Value.isPreDownload = savedInfo.isPreDownload; // revert predownload flag from saved info.
                    editingInfo.Value.isEdited = false;
                }
            }

            public static void Build(BundleEditorSettings buildSettings)
            {
                if (buildSettings == null)
                    return;

                Dictionary<string, BundleInfo> dictBuildBundles = new Dictionary<string, BundleInfo>(s_editingBundleInfos.Count);
                foreach (BundleInfo bundle in s_editingBundleInfos.Values)
                {
                    string[] assetNames = AssetDatabase.GetAssetPathsFromAssetBundle(bundle.bundleName);
                    if (assetNames.Length == 0)
                        continue; // if no assets in bundle.

                    string bundlePathFilename = System.IO.Path.Combine(buildSettings.TargetOutputPath, bundle.bundleName);

                    FileInfo bundleFileInfo = new FileInfo(bundlePathFilename);
                    bundle.fileSize = bundleFileInfo.Length;
                    bundle.crc = GetBundleCRC(bundlePathFilename);
                    dictBuildBundles[bundle.bundleName] = bundle;
                }

                UtilFile.SaveObjectToJson(dictBuildBundles, System.IO.Path.Combine(buildSettings.TargetOutputPath, FILE_NAME));
            }

            public bool IsChanged(BundleInfo otherInfo)
            {
                if (otherInfo == null)
                    return true;

                if (bundleName != otherInfo.bundleName ||
                    isPreDownload != otherInfo.isPreDownload)
                    return true;

                return false;
            }

            public void SetPreDownload(bool bPreDownload)
            {
                if (isPreDownload == bPreDownload)
                    return;
                isPreDownload = bPreDownload;
                CheckEditing();
            }

            public void CheckEditing()
            {
                BundleInfo savedInfo = null;
                s_savedBundleInfos.TryGetValue(bundleName, out savedInfo);
                if (savedInfo == null)
                {
                    isEdited = true;
                }
                else
                {
                    isEdited = (isPreDownload != savedInfo.isPreDownload);
                }
            }

            public static uint GetBundleCRC(string bundleFilePathname)
            {
                uint resultCRC = 0;
                BuildPipeline.GetCRCForAssetBundle(bundleFilePathname, out resultCRC);

                /*
                FileInfo fileInfo = new FileInfo(bundleFilePathname);
                byte[] buffer = new byte[1024 * 1024 * 5];

                using (FileStream fs = File.OpenRead(bundleFilePathname))
                {
                    using (CrcStream crcStream = new CrcStream(fs))
                    {
                        int n = 0;
                        while (n < fileInfo.Length)
                        {
                            n += crcStream.Read(buffer, 0, buffer.Length);
                        }

                        resultCRC = crcStream.ReadCrc;
                    }
                }
                */

                return resultCRC;
            }
        }
#endif

    }

#if UNITY_EDITOR
    public partial class BundleSettings
    {
        [LitJson.JsonIgnore]
        static BundleSettings s_bundleSettings = null;

        static BundleSettings()
        {
            Refresh();
        }

        [LitJson.JsonIgnore]
        public static BundleSettings SavedSettings
        {
            get
            {
                if (s_bundleSettings == null)
                    Refresh();

                return s_bundleSettings;
            }
        }

        public static void Refresh()
        {
            s_bundleSettings = Load();
        }

        public static void Save(BundleSettings newSettings)
        {
            string SAVE_PATH = Path.Combine(BundleEditorSettings.GetSettingDataPath(), "Resources");
            if (Directory.Exists(SAVE_PATH) == false)
                Directory.CreateDirectory(SAVE_PATH);
            SAVE_PATH = Path.Combine(SAVE_PATH, typeof(BundleSettings).ToString() + ".txt");
            UtilFile.SaveObjectToJson(newSettings, SAVE_PATH);
            AssetDatabase.ImportAsset(SAVE_PATH);
            Refresh();
        }

        public static bool IsChanged(BundleSettings newSettings)
        {
            if (s_bundleSettings == null && newSettings == null)
                return false;
            if (s_bundleSettings == null || newSettings == null)
                return true;
            if (s_bundleSettings.downloadBaseUrl != newSettings.downloadBaseUrl ||
                s_bundleSettings.preDownloadCapacity != newSettings.preDownloadCapacity ||
                s_bundleSettings.loadFromStreamingAssets != newSettings.loadFromStreamingAssets)
                return true;

            return false;
        }
    }

    // this editor class is used by BundleManager for test mode. so, defined here instead Editor folder.
    public class BundleEditorSettings
    {
        public enum TEST_MODE
        {
            DownloadUrl,
            OutputBundle,
            AssetDatabase,
            StreamingAssets,
        }

        private const int MAX_USED_DOWNLOAD_URL = 20;

        public string outputPath;
        public bool useActiveBuildTarget;
        public BundleManager.PLATFROM targetPlatform;
        public TEST_MODE testMode;
        public List<string> listUsedDownloadURLs = new List<string>();

        [LitJson.JsonIgnore]
        public string TargetOutputPath
        {
            get
            {
                if (testMode == TEST_MODE.StreamingAssets)
                    return string.Concat(Application.streamingAssetsPath, "/AvoEx/AssetBundles/", targetPlatform.ToString());

                return string.Concat(outputPath, "/", targetPlatform.ToString());
            }
        }

        public bool LastUsedDownloadURL(string szUrl)
        {
            if (string.IsNullOrEmpty(szUrl))
                return false;

            if (listUsedDownloadURLs.Count > 0 && listUsedDownloadURLs[0] == szUrl)
                return false; // already last used url.

            if (listUsedDownloadURLs.Contains(szUrl))
                listUsedDownloadURLs.Remove(szUrl);

            listUsedDownloadURLs.Insert(0, szUrl);
            if (listUsedDownloadURLs.Count > MAX_USED_DOWNLOAD_URL)
                listUsedDownloadURLs.RemoveAt(listUsedDownloadURLs.Count - 1);
            return true;
        }

        #region STATIC_INTERFACES
        [LitJson.JsonIgnore]
        static readonly string DEFAULT_OUTPUT_PATH = Application.dataPath + "/AssetBundles";
        [LitJson.JsonIgnore]
        const string BUNDLE_EDITOR_SETTINGS_PATH = "ProjectSettings/BundleEditorSettings.txt";

        [LitJson.JsonIgnore]
        static BundleEditorSettings s_bundleEditorSettings = null;

        static BundleEditorSettings()
        {
            Refresh();
        }

        // BundleEditorSettings
        [LitJson.JsonIgnore]
        public static BundleEditorSettings SavedSettings
        {
            get
            {
                if (s_bundleEditorSettings == null)
                    Refresh();

                return s_bundleEditorSettings;
            }
        }

        public static void Refresh()
        {
            s_bundleEditorSettings = Load();
        }

        public static BundleEditorSettings Load(bool defaultValue = false)
        {
            BundleEditorSettings loadedData = UtilFile.LoadJsonToObject<BundleEditorSettings>(BUNDLE_EDITOR_SETTINGS_PATH);
            if (loadedData == null)
            {
                loadedData = new BundleEditorSettings();
                if (defaultValue)
                {
                    loadedData.outputPath = DEFAULT_OUTPUT_PATH;
                    loadedData.useActiveBuildTarget = true;
                }
            }
            return loadedData;
        }

        public static void Save(BundleEditorSettings newSettings)
        {
            UtilFile.SaveObjectToJson<BundleEditorSettings>(newSettings, BUNDLE_EDITOR_SETTINGS_PATH);
            AssetDatabase.ImportAsset(BUNDLE_EDITOR_SETTINGS_PATH);
            Refresh();
        }

        public static bool IsChanged(BundleEditorSettings newSettings)
        {
            if (s_bundleEditorSettings == null && newSettings == null)
                return false;
            if (s_bundleEditorSettings == null || newSettings == null)
                return true;
            if (s_bundleEditorSettings.outputPath != newSettings.outputPath ||
                s_bundleEditorSettings.useActiveBuildTarget != newSettings.useActiveBuildTarget ||
                s_bundleEditorSettings.targetPlatform != newSettings.targetPlatform ||
                s_bundleEditorSettings.testMode != newSettings.testMode)
                return true;

            return false;
        }
        #endregion STATIC_INTERFACES

        //
        public static string GetSettingDataPath()
        {
            string[] GUIDs = AssetDatabase.FindAssets(typeof(BundleManager).Name);
            foreach (string GUID in GUIDs)
            {
                string path = AssetDatabase.GUIDToAssetPath(GUID);
                if (path.Contains("AvoEx"))
                    return Path.GetDirectoryName(path);
            }
            return null;
        }

    }
#endif

}