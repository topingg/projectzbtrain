﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

/* See the "http://avoex.com/avoex/default-license" for the full license governing this code. */

namespace AvoEx
{

    public partial class BundleManager
    {

        public class ManagedBundle
        {
            string m_bundleName;
            Hash128 m_hash;
            AssetBundle m_assetBundle;
            uint m_crc;
            int m_referenceCounter;
            string m_error;
            bool m_isLoadComplete;
            string m_downloadUrl;

#if USE_UNITYWEBREQUEST
            UnityWebRequest m_unityWebRequest;
            AsyncOperation m_unityWebAsyncOP;
#else
            WWW m_www;
#endif
            List<ManagedBundle> m_listDependencies = new List<ManagedBundle>();

            public AssetBundle assetBundle { get { return m_assetBundle; } }
            public string error
            {
                get { return m_error; }
                private set
                {
                    m_error = value;
#if DEBUG_BUNDLEMANAGER
                    if (string.IsNullOrEmpty(m_error) == false)
                        Debug.LogErrorFormat("ManagedBundle.{0}", m_error);
#endif
                }
            }
            public bool isLoadComplete
            {
                get { return m_isLoadComplete; }
            }
            public float downloadProgress
            {
                get
                {
                    if (assetBundle != null)
                        return 1f;

#if USE_UNITYWEBREQUEST
                    if (m_unityWebRequest == null)
                        return 0f;
                    return m_unityWebRequest.downloadProgress;
#else
                    if (m_www == null)
                        return 0f;
                    return m_www.progress;
#endif
                }
            }
            public List<ManagedBundle> dependencies
            {
                get { return m_listDependencies; }
            }
            public bool IsVersionCached
            {
                get
                {
                    if (Caching.IsVersionCached(m_downloadUrl, m_hash))
                    {
                        m_isLoadComplete = true; // if version cached, check load complete
                        return true;
                    }

                    return false;
                }
            }

            ManagedBundle(string bundleName, Hash128 hash)
            {
                m_bundleName = bundleName;
                m_hash = hash;
                m_crc = 0;
                m_assetBundle = null;
                m_referenceCounter = 0;
                m_isLoadComplete = false;
                m_downloadUrl = GetDownloadUrl(m_bundleName);

#if USE_UNITYWEBREQUEST
                m_unityWebRequest = null;
                m_unityWebAsyncOP = null;
#else
                m_www = null;
#endif
            }

            bool SetDependencies(string[] dependencies)
            {
                if (dependencies == null)
                    return true;

                for (int i = 0; i < dependencies.Length; ++i)
                {
                    ManagedBundle dependBundle = null;
                    s_dictManagedBundle.TryGetValue(dependencies[i], out dependBundle);
                    if (dependBundle == null) // depend bundle can not be null.
                    {
                        Debug.LogErrorFormat("SetDependencies of '{0}' is failed. '{1}' is not exist", m_bundleName, dependencies[i]);
                        return false;
                    }
                    m_listDependencies.Add(dependBundle);
                }

                return true;
            }

            void IncReferenceCounter()
            {
#if DEBUG_BUNDLEMANAGER
                Debug.LogFormat("ManagedBundle.IncReferenceCounter() : bundleName = '{0}', refCounter = '{1}'", m_bundleName, m_referenceCounter + 1);
#endif
                if (m_referenceCounter++ > 0)
                    return;

                 StartDownload();
            }

            void DecReferenceCounter(int amount = 1)
            {
                if (m_referenceCounter == 0)
                    return;
                m_referenceCounter -= amount;
#if DEBUG_BUNDLEMANAGER
                Debug.LogFormat("ManagedBundle.DecReferenceCounter() : bundleName = '{0}', refCounter = '{1}'", m_bundleName, m_referenceCounter);
#endif
                if (m_referenceCounter > 0)
                    return;
                if (m_referenceCounter < 0)
                    m_referenceCounter = 0;

                StopDownload();
                Unload();
            }

            void Unload()
            {
                m_referenceCounter = 0;

                if (m_assetBundle != null)
                {
                    m_assetBundle.Unload(false);
                    m_assetBundle = null;
#if DEBUG_BUNDLEMANAGER
                    Debug.LogFormat("ManagedBundle.Unload() : bundleName = '{0}'", m_bundleName);
#endif
                }
            }

            void StartDownload()
            {
                error = string.Empty;

                //m_www = WWW.LoadFromCacheOrDownload(m_downloadUrl, m_hash);

                //if (UpdateDownload() == false)
                //{
                //    m_isLoadComplete = false;
                //    s_listLoadingBundle.Add(this);
                //}
                if (IsVersionCached)
                {
#if DEBUG_BUNDLEMANAGER
                    Debug.LogFormat("ManagedBundle.StartDownload() - load from cache : bundleName = '{0}'", m_bundleName);
#endif

#if USE_UNITYWEBREQUEST
                    //m_unityWebRequest = UnityWebRequest.GetAssetBundle(m_downloadUrl, m_hash, m_crc);
                    //m_unityWebAsyncOP = m_unityWebRequest.Send();
                    //m_isLoadComplete = false;
                    //s_listLoadingBundle.Add(this);

                    WWW www = WWW.LoadFromCacheOrDownload(m_downloadUrl, m_hash);
                    m_assetBundle = www.assetBundle;
                    www.Dispose();
                    www = null;
#else
                    m_www = WWW.LoadFromCacheOrDownload(m_downloadUrl, m_hash);
                    m_assetBundle = m_www.assetBundle;
                    m_www.Dispose();
                    m_www = null;
#endif
                }
                else
                {
#if DEBUG_BUNDLEMANAGER
                    Debug.LogFormat("ManagedBundle.StartDownload() - download from url : bundleName = '<color=#ffff00ff>{0}</color>', url = '{1}'", m_bundleName, m_downloadUrl);
#endif

#if USE_UNITYWEBREQUEST
                    m_unityWebRequest = UnityWebRequest.GetAssetBundle(m_downloadUrl, m_hash, m_crc);
                    m_unityWebAsyncOP = m_unityWebRequest.Send();
#else
                    m_www = WWW.LoadFromCacheOrDownload(m_downloadUrl, m_hash);
#endif
                    m_isLoadComplete = false;
                    s_listLoadingBundle.Add(this);
                }
            }

            void StopDownload()
            {
                s_listLoadingBundle.Remove(this);

#if USE_UNITYWEBREQUEST
                if (m_unityWebRequest != null)
                {
                    m_unityWebRequest.Dispose();
                    m_unityWebRequest = null;
                    m_unityWebAsyncOP = null;
#else
                if (m_www != null)
                {
                    m_www.Dispose();
                    m_www = null;
#endif

#if DEBUG_BUNDLEMANAGER
                    Debug.LogFormat("ManagedBundle.StopDownload() : bundleName = '{0}', url = '{1}'", m_bundleName, m_downloadUrl);
#endif
                }
            }

            bool UpdateDownload()
            {
                //// (m_www.assetBundle == null) cause freeze on android device.
                //if (m_www == null || (m_www.assetBundle == null && m_www.isDone == false))
                //    return false;

#if USE_UNITYWEBREQUEST
                if (m_unityWebRequest == null || m_unityWebRequest.isDone == false)
                    return false;

                if (string.IsNullOrEmpty(m_unityWebRequest.error))
                {
                    m_assetBundle = DownloadHandlerAssetBundle.GetContent(m_unityWebRequest);
                    if (m_assetBundle == null)
                    {
                        error = string.Format("Unknown error : url = '{0}', hash = '{1}'", m_downloadUrl, m_hash);
                    }
                }
                else
                {
                    error = m_unityWebRequest.error;
                }

                m_unityWebRequest.Dispose();
                m_unityWebRequest = null;
#else
                if (m_www == null || m_www.isDone == false)
                    return false;

                if (string.IsNullOrEmpty(m_www.error))
                {
                    m_assetBundle = m_www.assetBundle;
                    if (m_assetBundle == null)
                    {
                        error = string.Format("Unknown error : url = '{0}', hash = '{1}'", m_downloadUrl, m_hash);
                    }
                }
                else
                {
                    error = m_www.error;
                }

                m_www.Dispose();
                m_www = null;
#endif
                m_isLoadComplete = true;

#if DEBUG_BUNDLEMANAGER
                Debug.LogFormat("ManagedBundle.UpdateDownload() is finished: bundleName = '<color=#00ff00ff>{0}</color>', url = '{1}'", m_bundleName, m_downloadUrl);
#endif
                return true;
            }
            
#region STATIC_INTERFACES
            static Dictionary<string, ManagedBundle> s_dictManagedBundle = new Dictionary<string, ManagedBundle>();
            static List<ManagedBundle> s_listLoadingBundle = new List<ManagedBundle>();
            static float s_downloadProgress = 0f;

            public static int downloadingCount
            {
                get { return s_listLoadingBundle.Count; }
            }

            public static float downloadingProgress
            {
                get { return s_downloadProgress; }
            }

            // update downloading bundles..
            public static void UpdateBundles()
            {
                float progress = 0f;
                for (int i = 0; i < s_listLoadingBundle.Count;)
                {
                    if (s_listLoadingBundle[i].UpdateDownload())
                    {
                        s_listLoadingBundle.RemoveAt(i);
                    }
                    else
                    {
#if USE_UNITYWEBREQUEST
                        progress += s_listLoadingBundle[i++].m_unityWebRequest.downloadProgress;
#else
                        progress += s_listLoadingBundle[i++].m_www.progress;
#endif
                    }
                }

                if (s_listLoadingBundle.Count > 0)
                    s_downloadProgress = progress / s_listLoadingBundle.Count;
                else
                    s_downloadProgress = 1f;
            }

            public static void ClearBundles()
            {
                while (s_listLoadingBundle.Count > 0)
                {
                    s_listLoadingBundle[0].StopDownload();
                }
                var itorBundles = s_dictManagedBundle.Values.GetEnumerator();
                while (itorBundles.MoveNext())
                {
                    itorBundles.Current.Unload();
                }
                s_dictManagedBundle.Clear();
                s_downloadProgress = 0f;
            }

            public static bool Init(AssetBundleManifest bundleManifest)
            {
                ClearBundles();
                if (bundleManifest == null)
                    return false;

                string[] bundleNames = bundleManifest.GetAllAssetBundles();
                for (int i = 0; i < bundleNames.Length; ++i)
                {
                    s_dictManagedBundle.Add(bundleNames[i], new ManagedBundle(bundleNames[i], bundleManifest.GetAssetBundleHash(bundleNames[i])));
                }

                // set dependencies between ManagedBundle.
                var itorBundles = s_dictManagedBundle.Values.GetEnumerator();
                while (itorBundles.MoveNext())
                {
                    if (itorBundles.Current.SetDependencies(bundleManifest.GetAllDependencies(itorBundles.Current.m_bundleName)) == false)
                    {
                        return false;
                    }
                }

                return true;
            }

            // increase reference counter, and download bundle
            public static ManagedBundle LoadBundleAsync(string bundleName)
            {
                ManagedBundle managedBundle = null;
                s_dictManagedBundle.TryGetValue(bundleName, out managedBundle);
                if (managedBundle == null)
                {
                    Debug.LogErrorFormat("LoadBundleAsync() is failed. '{0}' is not exist bundle.", bundleName);
                    return null;
                }

                // increase reference counter
                var itorDepends = managedBundle.m_listDependencies.GetEnumerator();
                while (itorDepends.MoveNext())
                {
                    itorDepends.Current.IncReferenceCounter(); // depend bundle can not be null.
                }
                managedBundle.IncReferenceCounter();

                return managedBundle;

            }

            // if bundle is downloaded, increase reference counter
            public static ManagedBundle LoadBundle(string bundleName)
            {
                // check bundle is already loaded.
                ManagedBundle managedBundle = null;
                s_dictManagedBundle.TryGetValue(bundleName, out managedBundle);
                if (managedBundle == null)
                {
                    Debug.LogErrorFormat("LoadBundle() is failed. '{0}' is not exist bundle.", bundleName);
                    return null;
                }
                else if (managedBundle.m_isLoadComplete == false)
                {
                    Debug.LogErrorFormat("LoadBundle() is failed. '{0}' is not loaded yet.", bundleName);
                    return null;
                }

                var itorDepends = managedBundle.m_listDependencies.GetEnumerator();
                while (itorDepends.MoveNext())
                {
                    if (itorDepends.Current.m_isLoadComplete == false) // depend bundle can not be null.
                    {
                        Debug.LogErrorFormat("LoadBundle() is failed. '{0}' is not loaded yet.", bundleName);
                        return null;
                    }
                }

                // increase reference counter
                itorDepends = managedBundle.m_listDependencies.GetEnumerator();
                while (itorDepends.MoveNext())
                {
                    itorDepends.Current.IncReferenceCounter(); // depend bundle can not be null.
                }
                managedBundle.IncReferenceCounter();

                return managedBundle;
            }

            // decrease reference counter
            public static void UnloadBundle(string bundleName, int counter = 1)
            {
                ManagedBundle managedBundle = null;
                s_dictManagedBundle.TryGetValue(bundleName, out managedBundle);
                if (managedBundle == null)
                    return;

                // increase reference counter
                var itorDepends = managedBundle.m_listDependencies.GetEnumerator();
                while (itorDepends.MoveNext())
                {
                    itorDepends.Current.DecReferenceCounter(counter); // depend bundle can not be null.
                }
                managedBundle.DecReferenceCounter(counter);
            }

            public static ManagedBundle GetManagedBundle(string bundleName)
            {
                ManagedBundle managedBundle = null;
                s_dictManagedBundle.TryGetValue(bundleName, out managedBundle);

                return managedBundle;
            }

            // download & load all bundles, then unload from memory.
            public static IEnumerator CoDownloadAllAsync(int iDownloadCapacity)
            {
                List<ManagedBundle> listDownloadBundles = new List<ManagedBundle>(iDownloadCapacity);

                var itorBundles = s_dictManagedBundle.Values.GetEnumerator();
                while (itorBundles.MoveNext())
                {
                    while (listDownloadBundles.Count >= iDownloadCapacity) // 다운로드 수가 설정된 수 이하로 떨어질때까지 대기.
                    {
                        for (int i = listDownloadBundles.Count - 1; i >= 0; --i)
                        {
                            if (listDownloadBundles[i].isLoadComplete) // 다운로드가 완료 됬다.
                            {
                                listDownloadBundles[i].DecReferenceCounter(); // 참조 카운터 내림 (참조가 아무도 없으면, 메모리에서 해제)
                                listDownloadBundles.RemoveAt(i); // 다운로드 관리 목록에서 해제.
                            }
                        }
                        
                        yield return null; 
                    }

                    if (itorBundles.Current.IsVersionCached) // 다른 경로를 통해, 다운로드가 진행 되었을수 있으므로, 시작 직전에 다운로드 확인.
                        continue; // 이미 다운로드 완료됨.

                    itorBundles.Current.IncReferenceCounter(); // 참조 카운터 올림 (다운로드 시작)
                    listDownloadBundles.Add(itorBundles.Current); // 다운로드 관리 목록에서 등록.
                }
            }
#endregion STATIC_INTERFACES
        }

    }

}