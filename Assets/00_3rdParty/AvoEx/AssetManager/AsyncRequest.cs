﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/* See the "http://avoex.com/avoex/default-license" for the full license governing this code. */

namespace AvoEx
{

    public class AsyncRequest : IEnumerator
    {
        protected string m_error;
        protected bool m_isDone;

        public string error
        {
            get { return m_error; }
            protected set
            {
                m_error = value;
#if DEBUG_ASSETMANAGER || DEBUG_BUNDLEMANAGER
                if (string.IsNullOrEmpty(m_error) == false)
                    Debug.LogErrorFormat("{0}.{1}", this, m_error);
#endif
            }
        }

        public bool isDone
        {
            get { return m_isDone; }
        }

        public virtual float progress { get; protected set; }

        #region IEnumerator
        public virtual object Current
        {
            get { return null; }
        }
        public virtual bool MoveNext()
        {
            return !m_isDone;
        }

        public virtual void Reset()
        {
        }
        #endregion IEnumerator
    }

    #region BUNDLE_REQUESTS
    public class AsyncBundleRequest : AsyncRequest
    {
        string m_bundleName = string.Empty;
        BundleManager.ManagedBundle m_managedBundle = null;

        public AsyncBundleRequest(string bundleName)
        {
            m_bundleName = bundleName;
#if UNITY_EDITOR
            if (BundleEditorSettings.SavedSettings.testMode == BundleEditorSettings.TEST_MODE.AssetDatabase)
            {
                m_isDone = true;
            }
            else
#endif
            if (BundleManager.isInitialized)
            {
                Update(); // update right now.
            }
            else if (BundleManager.isError)
            {
                error = string.Format("new AsyncBundleRequest() is failed. m_bundleName = '{0}', {1}", m_bundleName, BundleManager.error);
                m_isDone = true;
            }

            if (m_isDone)
                return; // request completed.

            s_listBundleRequests.Add(this);
        }

        public AssetBundle assetBundle
        {
            get { return m_managedBundle.assetBundle; }
        }

        /// <summary>
        /// assumes the BundleManager is initialized or error. Update() must called after the BundleManager is initialized.
        /// call once per frame.
        /// </summary>
        public void Update()
        {
            if (m_managedBundle == null)
            {   // this is first time Update() after the BundleManager is initialized.
                if (BundleManager.isError)
                {
                    error = string.Format("first Update() is failed. m_bundleName = '{0}', {1}", m_bundleName, BundleManager.error);
                    m_isDone = true;
                    return;
                }

                m_managedBundle = BundleManager.ManagedBundle.LoadBundleAsync(m_bundleName);
                if (m_managedBundle == null)
                {
                    error = string.Format("first Update() is failed. m_bundleName = '{0}'", m_bundleName);
                    m_isDone = true;
                    return;
                }
            }

            // check downloading
            if (string.IsNullOrEmpty(m_managedBundle.error) == false)
            {
                error = m_managedBundle.error;
                m_isDone = true; // error
            }
            else // check progress
            {
                float downloadProgress = 0f;
                bool isDownloadFinished = m_managedBundle.isLoadComplete;

                downloadProgress += m_managedBundle.downloadProgress;
                var itorDepends = m_managedBundle.dependencies.GetEnumerator();
                while (itorDepends.MoveNext())
                {
                    if (string.IsNullOrEmpty(itorDepends.Current.error) == false)
                    {
                        error = itorDepends.Current.error;
                        m_isDone = true; // error by depend bundle
                        isDownloadFinished = false;
                    }
                    else
                    {
                        downloadProgress += itorDepends.Current.downloadProgress;
                        if (itorDepends.Current.isLoadComplete == false)
                            isDownloadFinished = false;
                    }
                }

                progress = downloadProgress / (m_managedBundle.dependencies.Count + 1);
//#if DEBUG_BUNDLEMANAGER
//                Debug.LogFormat("AsyncBundleRequest.Update() : bundleName = {0}, progress = {1}", m_bundleName, progress);
//#endif

                if (isDownloadFinished) // all bundles are completed
                {
                    m_isDone = true;
                }
            }
        }

        public string name
        {
            get { return m_bundleName; }
        }

        #region STATIC_INTERFACES
        static List<AsyncBundleRequest> s_listBundleRequests = new List<AsyncBundleRequest>();

        /// <summary>
        /// assumes the BundleManager is initialized or error. Update() must called after the BundleManager is initialized.
        /// call once per frame.
        /// </summary>
        public static void UpdateRequests()
        {
            for (int i = 0; i < s_listBundleRequests.Count;)
            {
                s_listBundleRequests[i].Update();

                if (s_listBundleRequests[i].isDone)
                    s_listBundleRequests.RemoveAt(i);
                else
                    ++i;
            }
        }

        public static void ClearRequests()
        {
            s_listBundleRequests.Clear();
        }
        #endregion STATIC_INTERFACES
    }
    #endregion BUNDLE_REQUESTS

    #region ASSET_REQUESTS
    public abstract class AsyncAssetRequest : AsyncRequest
    {
        protected virtual void Update()
        {
            error = "Update() is failed. no asset";
            m_isDone = true;
        }

        #region STATIC_INTERFACES
        protected static List<AsyncAssetRequest> s_listAssetRequests = new List<AsyncAssetRequest>();

        /// <summary>
        /// assumes the AssetManager is initialized or error. Update() must called after the AssetManager is initialized.
        /// call once per frame.
        /// </summary>
        public static void UpdateRequests()
        {
            for (int i = 0; i < s_listAssetRequests.Count;)
            {
                s_listAssetRequests[i].Update();

                if (s_listAssetRequests[i].isDone)
                    s_listAssetRequests.RemoveAt(i);
                else
                    ++i;
            }
        }

        public static void ClearRequests()
        {
            s_listAssetRequests.Clear();
        }
        #endregion STATIC_INTERFACES
    }

    public class AssetRequest<T> : AsyncAssetRequest where T : Object
    {
        string reqAssetName = string.Empty;
        AssetManager.ManagedAsset managedAsset = null;

        public T asset
        {
            get { return managedAsset == null ? null : managedAsset.asset as T; }
        }

        public override float progress
        {
            get
            {
                return (managedAsset == null ? 0f : managedAsset.progress);
            }
        }

        public AssetRequest(string assetName)
        {
            reqAssetName = assetName;

            if (AssetManager.isInitialized)
            {
                Update(); // update right now.
            }
            else if (AssetManager.isError)
            {
                error = string.Format("new AssetRequest() is failed. reqAssetName = '{0}', {1}", reqAssetName, AssetManager.error);
                m_isDone = true;
            }

            if (m_isDone)
                return; // request completed.

            s_listAssetRequests.Add(this);
        }

        /// <summary>
        /// assumes the AssetManager is initialized or error. Update() must called after the AssetManager is initialized.
        /// call once per frame.
        /// </summary>
        protected override void Update()
        {
            if (managedAsset == null)
            {   // this is first time Update() after the AssetManager is initialized.

                if (AssetManager.isError)
                {
                    error = string.Format("first Update() is failed. reqAssetName = '{0}', {1}", reqAssetName, AssetManager.error);
                    m_isDone = true;
                    return;
                }

                managedAsset = AssetManager.ManagedAsset.GetManagedAsset(reqAssetName);
                if (managedAsset == null)
                {
                    error = string.Format("first Update() is failed. reqAssetName = '{0}'", reqAssetName);
                    m_isDone = true;
                    return;
                }

                managedAsset.LoadAsync<T>();
            }

//#if DEBUG_ASSETMANAGER
//                Debug.LogFormat("AssetRequest.Update() : assetName = {0}, progress = {1}", reqAssetName, managedAsset.progress);
//#endif

            if (string.IsNullOrEmpty(managedAsset.error) == false)
            {
                error = string.Format("Update() is failed. reqAssetName = '{0}', {1}", reqAssetName, managedAsset.error);
                m_isDone = true;
            }
            else if (managedAsset.asset != null)
            {
                m_isDone = true;
            }
        }
    }
    #endregion ASSET_REQUESTS
}