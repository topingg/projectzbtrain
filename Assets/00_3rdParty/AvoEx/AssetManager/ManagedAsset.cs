﻿using UnityEngine;
using System.Collections.Generic;

/* See the "http://avoex.com/avoex/default-license" for the full license governing this code. */

namespace AvoEx
{

    public partial class AssetManager : MonoSingleton<AssetManager>
    {
        public abstract class ManagedAsset : MemObject
        {
            public enum TYPE
            {
                RESOURCE,
                BUNDLE,
            }

            public TYPE type { get; private set; }
            public string name { get; private set; }
            public Object asset { get { return memoryObject; } }
            string m_error;
            protected float m_progress;

            public string error
            {
                get { return m_error; }
                protected set
                {
                    m_error = value;
#if DEBUG_ASSETMANAGER
                    if (string.IsNullOrEmpty(m_error) == false)
                        Debug.LogErrorFormat("{0}(name = '{1}').{2}", this, name, m_error);
#endif
                }
            }

            public float progress
            {
                get { return m_progress; }
            }

            public T Load<T>() where T : Object
            {
                if (asset == null)
                    Caching<T>();

                return asset as T;
            }
            public abstract void LoadAsync<T>() where T : Object;

            public ManagedAsset(TYPE managedType, string managedName)
            {
                type = managedType;
                name = managedName;
            }

            protected abstract void Caching<T>() where T : Object;

            #region STATIC_INTERFACES
            protected static Dictionary<string, ManagedAsset> s_dicManagedAsset = new Dictionary<string, ManagedAsset>();
            public static int Count { get { return s_dicManagedAsset.Count; } }

            public static string FindBundleName(string szKeyword)
            {
                ManagedAssetFromBundle findAsset = null;
                var itorManagedAssets = s_dicManagedAsset.GetEnumerator();
                while (itorManagedAssets.MoveNext())
                {
                    findAsset = itorManagedAssets.Current.Value as ManagedAssetFromBundle;
                    if (findAsset != null && itorManagedAssets.Current.Key.Contains(szKeyword))
                        return findAsset.bundleName;
                }

                return "";
            }

            public static ManagedAsset GetManagedAsset(string name)
            {
                ManagedAsset loadedAsset = null;
                s_dicManagedAsset.TryGetValue(name, out loadedAsset);
                if (loadedAsset == null)
                {
#if UNITY_EDITOR
                    if (BundleEditorSettings.SavedSettings.testMode == BundleEditorSettings.TEST_MODE.AssetDatabase)
                    {
                        string assetPath = "Assets/" + name;
                        string GUID = UnityEditor.AssetDatabase.AssetPathToGUID(assetPath);
                        if (string.IsNullOrEmpty(GUID) == false)
                        {
                            loadedAsset = new ManagedAssetFromBundle(name, "", assetPath);
                            s_dicManagedAsset.Add(name, loadedAsset);
                            return loadedAsset;
                        }
                    }
#endif
                    if (currentStatus == STATUS.INITIALIZED)
                    {   // not exist in bundle asset info. load from Resources
                        string resName = System.IO.Path.ChangeExtension(name, null);
#if DEBUG_ASSETMANAGER
                        Debug.LogFormat("'{0}' asset is not exist in bundle. trying from resource('{1}').", name, resName);
#endif
                        loadedAsset = ManagedAssetFromResource.AddManagedResource(resName, name);
                    }
                    else
                    {
                        Debug.LogErrorFormat("AssetManager is not initialized : current status is {0}.", currentStatus);
                    }
                }
                return loadedAsset;
            }

            public static void ClearManagedAssets()
            {
                ManagedAssetFromBundle.ClearRequests();
                ManagedAssetFromResource.ClearRequests();
                var itorAssets = s_dicManagedAsset.Values.GetEnumerator();
                while (itorAssets.MoveNext())
                    itorAssets.Current.Unload();
                s_dicManagedAsset.Clear();
            }

            public static void UpdateManagedAssets()
            {
                ManagedAssetFromBundle.UpdateRequests();
                ManagedAssetFromResource.UpdateRequests();
            }
            #endregion STATIC_INTERFACES
        }

        class ManagedAssetFromBundle : ManagedAsset
        {
            public string bundleName { get; private set; }
            public string assetPath { get; private set; }
            System.Type assetType;

            public ManagedAssetFromBundle(string managedName, string bundleName, string assetPath) : base(TYPE.BUNDLE, managedName)
            {
                this.bundleName = bundleName;
                this.assetPath = assetPath;
            }

            protected override void Caching<T>()
            {
#if UNITY_EDITOR
                if (BundleEditorSettings.SavedSettings.testMode == BundleEditorSettings.TEST_MODE.AssetDatabase)
                {
                    SetMemoryObject(UnityEditor.AssetDatabase.LoadAssetAtPath<T>(assetPath));
                    return;
                }
#endif
                AssetBundle loadedBundle = BundleManager.LoadBundle(bundleName); // get already loaded bundle and increase reference count.
                if (loadedBundle == null)
                {
                    Debug.LogErrorFormat("ManagedAssetFromBundle.Caching<{0}>() is failed. name = '{1}', assetPath = '{2}', bundleName = '{3}'", typeof(T), name, assetPath, bundleName);
                    return;
                }
#if DEBUG_ASSETMANAGER
                Debug.LogFormat("ManagedAssetFromBundle.Caching<{0}>() : name = '{1}', assetPath = '{2}', bundleName = '{3}'", typeof(T), name, assetPath, bundleName);
#endif
                SetMemoryObject(loadedBundle.LoadAsset<T>(assetPath)); // load asset from AssetBundle.
                                                                       // do not UnloadBundle() right after LoadBundle(). new AssetBundle will duplicate same asset.
                                                                       // increase only one bundle reference counter per asset, and caching asset.
                                                                       // so easily unload bundle, and prevent LoadFromCacheOrDownload frequently.
            }

            public override void LoadAsync<T>()
            {
#if UNITY_EDITOR
                if (BundleEditorSettings.SavedSettings.testMode == BundleEditorSettings.TEST_MODE.AssetDatabase)
                {
                    T loadedObject = UnityEditor.AssetDatabase.LoadAssetAtPath<T>(assetPath);
                    if (loadedObject == null)
                    {
                        error = string.Format("ManagedAssetFromBundle.LoadAsync<{0}>() - not exist asset : name = '{1}', assetPath = '{2}', bundleName = '{3}'", typeof(T), name, assetPath, bundleName);
                        Debug.LogError(error);
                    }
                    else
                    {
                        SetMemoryObject(loadedObject);
                    }
                    return;
                }
#endif
                if (asset != null || s_dictBundleRequests.ContainsKey(this) || s_dictAssetRequests.ContainsKey(this))
                    return;

#if DEBUG_ASSETMANAGER
                Debug.LogFormat("ManagedAssetFromBundle.LoadAsync<{0}>() : name = '{1}', assetPath = '{2}', bundleName = '{3}'", typeof(T), name, assetPath, bundleName);
#endif

                error = string.Empty;
                assetType = typeof(T);
                m_progress = 0f;

                // check loading on current frame.
                AsyncBundleRequest bundleRequest = BundleManager.LoadBundleAsync(bundleName);
                if (UpdateBundleRequest(bundleRequest) == false)
                {
                    s_dictBundleRequests[this] = bundleRequest; // bundleRequest.isDone is false. needs update
                    return;
                }
                else if (string.IsNullOrEmpty(error) == false)
                {
                    return; // error. load failed.
                }

                AssetBundleRequest assetRequest = bundleRequest.assetBundle.LoadAssetAsync<T>(assetPath); // assetBundle is not null. checked on UpdateBundleRequest().
                if (UpdateAssetRequest(assetRequest) == false)
                    s_dictAssetRequests[this] = assetRequest; // assetRequest.isDone is false. needs update
            }

            #region MEM_OBJECT
            /// <summary>
            /// called by MemObject.Unload()
            /// </summary>
            public override void Release()
            {
                BundleManager.UnloadBundle(bundleName);
                m_progress = 0f;

#if DEBUG_ASSETMANAGER
                Debug.LogFormat("ManagedAssetFromBundle.Release() : name = '{0}', bundleName = '{1}', assetPath = '{2}'", name, bundleName, assetPath);
#endif
            }
            #endregion MEM_OBJECT

            // if request is done, return true
            bool UpdateBundleRequest(AsyncBundleRequest bundleRequest)
            {
                //if (bundleRequest == null)
                //{
                //    error = "UpdateBundleRequest() - AsyncBundleRequest is null";
                //    return true;
                //}
                // bundleRequest must be not null.
                m_progress = bundleRequest.progress * 0.7f; // downloading progress is 0f to 0.7f.
                if (bundleRequest.isDone == false)
                    return false;

                if (string.IsNullOrEmpty(bundleRequest.error) == false)
                    error = bundleRequest.error;
                else if (bundleRequest.assetBundle == null)
                    error = string.Format("UpdateBundleRequest() - assetBundle is null. bundleName = '{0}', assetPath = '{1}'", bundleName, assetPath);

                return true;
            }

            // if request is done, return true
            bool UpdateAssetRequest(AssetBundleRequest assetRequest)
            {
                //if (assetRequest == null)
                //{
                //    error = "UpdateAssetRequest() - AssetBundleRequest is null";
                //    return true;
                //}
                // assetRequest must be not null.
                m_progress = 0.7f + (assetRequest.progress * 0.3f); // loading progress is 0.7f to 1.0f
                if (assetRequest.isDone == false)
                    return false;

                if (assetRequest.asset == null) // this creates some garbage..
                {
                    error = string.Format("UpdateBundleRequest() - asset is null. bundleName = '{0}', assetPath = '{1}'", bundleName, assetPath);
                }
                else
                {
#if DEBUG_ASSETMANAGER
                    Debug.LogFormat("ManagedAssetFromBundle.UpdateAssetRequest() - async loading is completed : name = '{0}', assetPath = '{1}', bundleName = '{2}'", name, assetPath, bundleName);
#endif
                    SetMemoryObject(assetRequest.asset);
                }

                return true;
            }

            #region STATIC_INTERFACES
            static Dictionary<ManagedAssetFromBundle, AsyncBundleRequest> s_dictBundleRequests = new Dictionary<ManagedAssetFromBundle, AsyncBundleRequest>();
            static Dictionary<ManagedAssetFromBundle, AssetBundleRequest> s_dictAssetRequests = new Dictionary<ManagedAssetFromBundle, AssetBundleRequest>();
            static List<ManagedAssetFromBundle> s_listRemoveRequests = new List<ManagedAssetFromBundle>(10); // 10 capacity is avoidance for garbage.

            public static void UpdateRequests()
            {
                // update AsyncBundleRequest.
                s_listRemoveRequests.Clear();

                var itorBundleRequest = s_dictBundleRequests.GetEnumerator();
                while (itorBundleRequest.MoveNext())
                {
                    if (itorBundleRequest.Current.Key.UpdateBundleRequest(itorBundleRequest.Current.Value) == false)
                        continue; // bundleRequest.isDone is false. needs update

                    s_listRemoveRequests.Add(itorBundleRequest.Current.Key); // request is done. remove from the request list.

                    if (string.IsNullOrEmpty(itorBundleRequest.Current.Key.error))
                        s_dictAssetRequests[itorBundleRequest.Current.Key] = itorBundleRequest.Current.Value.assetBundle.LoadAssetAsync(itorBundleRequest.Current.Key.assetPath, itorBundleRequest.Current.Key.assetType); // assetBundle is not null. checked on UpdateBundleRequest().
                }

                var itorRemoves = s_listRemoveRequests.GetEnumerator();
                while (itorRemoves.MoveNext())
                    s_dictBundleRequests.Remove(itorRemoves.Current);

                // update AssetBundleRequest.
                s_listRemoveRequests.Clear();

                var itorAssetRequest = s_dictAssetRequests.GetEnumerator();
                while (itorAssetRequest.MoveNext())
                {
                    if (itorAssetRequest.Current.Key.UpdateAssetRequest(itorAssetRequest.Current.Value) == false)
                        continue; // assetRequest.isDone is false. needs update

                    s_listRemoveRequests.Add(itorAssetRequest.Current.Key); // request is done. remove from the request list.
                }

                itorRemoves = s_listRemoveRequests.GetEnumerator();
                while (itorRemoves.MoveNext())
                    s_dictAssetRequests.Remove(itorRemoves.Current);
            }

            public static void ClearRequests()
            {
                s_dictBundleRequests.Clear();
                s_dictAssetRequests.Clear();
                s_listRemoveRequests.Clear();
            }

            public static void LoadFromJson(string json)
            {
                Dictionary<string, AssetInfo> dictAssetInfos = LitJson.JsonMapper.ToObject<Dictionary<string, AssetInfo>>(json);
                if (dictAssetInfos == null)
                    return;

                var itorAssetInfos = dictAssetInfos.GetEnumerator();
                while (itorAssetInfos.MoveNext())
                {
                    AssetInfo assetInfo = itorAssetInfos.Current.Value;
                    if (s_dicManagedAsset.ContainsKey(assetInfo.managedName))
                    {
                        Debug.LogErrorFormat("{0} is already exist asset name.", itorAssetInfos.Current.Value.managedName);
                    }
                    s_dicManagedAsset[assetInfo.managedName] = new ManagedAssetFromBundle(assetInfo.managedName, assetInfo.bundleName, assetInfo.assetPath);
                }
            }
            #endregion STATIC_INTERFACES
        }

        class ManagedAssetFromResource : ManagedAsset
        {
            public string resourceName { get; private set; }

            public ManagedAssetFromResource(string managedName, string resourceName) : base(TYPE.RESOURCE, managedName)
            {
                this.resourceName = resourceName;
            }

            protected override void Caching<T>()
            {
#if DEBUG_ASSETMANAGER
                Debug.LogFormat("ManagedAssetFromResource.Caching<{0}>() : name = '{1}', resourceName = '{2}'", typeof(T), name, resourceName);
#endif
                SetMemoryObject(Resources.Load<T>(resourceName));
                if (asset == null)
                {
                    error = string.Format("Caching<{0}>() is failed. '{1}' is not exist resource.", typeof(T), resourceName);
                    return;
                }
            }

            public override void LoadAsync<T>()
            {
                if (asset != null || s_dictResourceRequests.ContainsKey(this))
                    return;
#if DEBUG_ASSETMANAGER
                Debug.LogFormat("ManagedAssetFromResource.LoadAsync<{0}>() : name = '{1}', resourceName = '{2}'", typeof(T), name, resourceName);
#endif

                m_progress = 0f;
                s_dictResourceRequests[this] = Resources.LoadAsync<T>(resourceName);
            }

            #region MEM_OBJECT
            /// <summary>
            /// called by MemObject.Unload()
            /// </summary>
            public override void Release()
            {
                Resources.UnloadAsset(asset);
                m_progress = 0f;

#if DEBUG_ASSETMANAGER
                Debug.LogFormat("ManagedAssetFromResource.Release() : name = '{0}', resourceName = '{1}'", name, resourceName);
#endif
            }
            #endregion MEM_OBJECT

            #region STATIC_INTERFACES
            static Dictionary<ManagedAssetFromResource, ResourceRequest> s_dictResourceRequests = new Dictionary<ManagedAssetFromResource, ResourceRequest>();
            static List<ManagedAssetFromResource> s_listRemoveRequests = new List<ManagedAssetFromResource>();

            public static void UpdateRequests()
            {
                s_listRemoveRequests.Clear();

                var itorRequest = s_dictResourceRequests.GetEnumerator();
                while (itorRequest.MoveNext())
                {
                    itorRequest.Current.Key.m_progress = itorRequest.Current.Value.progress;
                    if (itorRequest.Current.Value.isDone == false)
                        continue;

                    itorRequest.Current.Key.SetMemoryObject(itorRequest.Current.Value.asset);
                    if (itorRequest.Current.Key.asset == null)
                        itorRequest.Current.Key.error = string.Format("UpdateRequests() is failed. '{0}' is not exist resource.", itorRequest.Current.Key);
#if DEBUG_ASSETMANAGER
                    else
                        Debug.LogFormat("ManagedAssetFromResource.UpdateRequests() - async loading is completed : name = '{0}', resourceName = '{1}'", itorRequest.Current.Key.name, itorRequest.Current.Key.resourceName);
#endif

                    s_listRemoveRequests.Add(itorRequest.Current.Key);
                }

                var itorRemoves = s_listRemoveRequests.GetEnumerator();
                while (itorRemoves.MoveNext())
                {
                    s_dictResourceRequests.Remove(itorRemoves.Current);
                }
            }

            public static void ClearRequests()
            {
                s_dictResourceRequests.Clear();
                s_listRemoveRequests.Clear();
            }

            public static ManagedAssetFromResource AddManagedResource(string resourceName, string managedName = null)
            {
                if (string.IsNullOrEmpty(resourceName))
                    return null;

                if (string.IsNullOrEmpty(managedName))
                    managedName = resourceName;

                if (s_dicManagedAsset.ContainsKey(managedName))
                {
                    Debug.LogErrorFormat("ManagedAssetFromResource.AddManagedResource('{0}') is failed. the key '{1}' is already exist.", resourceName, managedName);
                    return null;
                }

                ManagedAssetFromResource managedResource = null;
                managedResource = new ManagedAssetFromResource(managedName, resourceName); ;
                s_dicManagedAsset[managedName] = managedResource;

                return managedResource;
            }
            #endregion STATIC_INTERFACES
        }

    }

}