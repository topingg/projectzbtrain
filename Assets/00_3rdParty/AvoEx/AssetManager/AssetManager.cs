﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

/* See the "http://avoex.com/avoex/default-license" for the full license governing this code. */

namespace AvoEx
{

    public partial class AssetManager : MonoSingleton<AssetManager>
    {
        public enum STATUS
        {
            NONE,
            INITIALIZE_STARTED,
            DOWNLOAD_ASSET_INFO,
            LOAD_ASSET_INFO,
            INITIALIZED,
            ERROR,
        }

        static STATUS m_currentStatus = STATUS.NONE;
        public static STATUS currentStatus
        {
            get
            {
#if UNITY_EDITOR
                if (BundleEditorSettings.SavedSettings.testMode == BundleEditorSettings.TEST_MODE.AssetDatabase)
                    return STATUS.INITIALIZED;
#endif
                return m_currentStatus;
            }
            private set
            {
#if DEBUG_ASSETMANAGER
                Debug.LogFormat("AssetManager STATUS changed <color=#ffff00ff>{0}</color> from {1}", value, m_currentStatus);
#endif
                m_currentStatus = value;
            }
        }
        static string m_error;
        public static string error
        {
            get { return m_error; }
            private set
            {
                m_error = value;
#if DEBUG_ASSETMANAGER
                if (string.IsNullOrEmpty(m_error) == false)
                    Debug.LogErrorFormat("AssetManager.{0}", m_error);
#endif
            }
        }

        public static bool isInitialized
        {
            get { return (currentStatus == STATUS.INITIALIZED); }
        }

        public static bool isError
        {
            get { return (currentStatus == STATUS.ERROR); }
        }

        public static float downloadingAssetInfoProgress { get; private set; }

        // Use this for initialization
        void Start()
        {
            Initialize();
        }

        // Update is called once per frame
        void Update()
        {
            ManagedAsset.UpdateManagedAssets();
            if (isInitialized || isError)
                AsyncAssetRequest.UpdateRequests();
        }

        void Initialize()
        {
            if (currentStatus != STATUS.NONE)
                return; // Initialize() must called after Clear()
            currentStatus = STATUS.INITIALIZE_STARTED;
            StartCoroutine(CoInitialize());
        }

        void Clear()
        {
            StopAllCoroutines();

            currentStatus = STATUS.NONE;
            error = string.Empty;

            ManagedAsset.ClearManagedAssets();
            AsyncAssetRequest.ClearRequests();
        }

        IEnumerator CoInitialize()
        {
            downloadingAssetInfoProgress = 0f;
            currentStatus = STATUS.DOWNLOAD_ASSET_INFO;
            AsyncBundleRequest bundleAssetInfo = BundleManager.LoadBundleAsync(AssetInfo.BUNDLE_NAME);
            while (bundleAssetInfo.isDone == false)
            {
                downloadingAssetInfoProgress = bundleAssetInfo.progress;
                yield return null;
            }

            if (bundleAssetInfo.assetBundle == null || string.IsNullOrEmpty(bundleAssetInfo.error) == false)
            {   // If downloading fails.
                error = bundleAssetInfo.error;
                currentStatus = STATUS.ERROR;
                yield break;
            }
            else
            {   // If downloading succeeds.
                downloadingAssetInfoProgress = 1f;
                currentStatus = STATUS.LOAD_ASSET_INFO;

                AssetBundleRequest reqAssetInfo = bundleAssetInfo.assetBundle.LoadAssetAsync<TextAsset>(AssetInfo.FILE_NAME);
                yield return reqAssetInfo;

                TextAsset txtAsset = reqAssetInfo.asset as TextAsset;
                if (txtAsset == null)
                {
                    currentStatus = STATUS.ERROR;
                    yield break;
                }

                ManagedAssetFromBundle.LoadFromJson(txtAsset.text);

                bundleAssetInfo.assetBundle.Unload(false);
            }

            currentStatus = STATUS.INITIALIZED;
        }

        #region STATIC_INTERFACES
        public static void InitManager()
        {
            Instance.Initialize();
        }

        public static void ClearManager()
        {
            if (IsInstantiated)
                Instance.Clear();
        }

        public static GameObject CreateInstance(string name)
        {
            GameObject instance = LoadAsset<GameObject>(name);
            if (instance == null)
                return null;

            return Instantiate(instance);
        }

        public static T LoadAsset<T>(string name) where T : Object
        {
            if (string.IsNullOrEmpty(name))
                return null;

            ManagedAsset managed = ManagedAsset.GetManagedAsset(name);
            if (managed == null)
            {
                Debug.LogErrorFormat("AssetManager.LoadAsset<{0}>( <color=#ff0000ff>'{1}'</color> ) - failed. not exist is asset!", typeof(T), name);
                return null;
            }

            return managed.Load<T>();
        }

        public static AssetRequest<T> LoadAssetAsync<T>(string name) where T : Object
        {
            return new AssetRequest<T>(name);
        }

        public static void Unload(string name)
        {
            ManagedAsset managed = ManagedAsset.GetManagedAsset(name);
            if (managed == null)
            {
                Debug.LogErrorFormat("AssetManager.Unload( <color=#ff0000ff>'{0}'</color> ) - failed. not exist is asset!", name);
                return;
            }
            managed.Unload();
        }
        #endregion STATIC_INTERFACES

        #region FILE_IO
        public partial class AssetInfo
        {
            [LitJson.JsonIgnore]
            public static string FILE_NAME
            {
                get { return "AssetInfo.txt"; }
            }

            [LitJson.JsonIgnore]
            public static string BUNDLE_NAME
            {
                get { return "asset_info"; }
            }

            public string bundleName;
            public string assetPath;
            public string managedName;
        }

#if UNITY_EDITOR
        public partial class AssetInfo
        {
            [LitJson.JsonIgnore]
            public bool isJustAdded { get; private set; }
            [LitJson.JsonIgnore]
            public bool isChanged { get; private set; }
            [LitJson.JsonIgnore]
            static readonly int iLenRootPath = "Assets/".Length;
            public bool nameByPath = true;
            [LitJson.JsonIgnore]
            public bool isEdited { get; private set; }

            [LitJson.JsonIgnore]
            string defaultName
            {
                get { return assetPath.Substring(iLenRootPath); }
            }

            [LitJson.JsonIgnore]
            static Dictionary<string, AssetInfo> s_editingAssetInfos = null;
            [LitJson.JsonIgnore]
            static Dictionary<string, AssetInfo> s_savedAssetInfos = null;
            [LitJson.JsonIgnore]
            static Dictionary<string, AssetInfo> s_outputAssetInfos = null;
            [LitJson.JsonIgnore]
            static Dictionary<string, string> s_dictManagedNameToGUID = new Dictionary<string, string>();

            public static Dictionary<string, AssetInfo> LoadAssetInfos(string path)
            {
                Dictionary<string, AssetInfo> loadedAssetInfos = UtilFile.LoadJsonToObject<Dictionary<string, AssetInfo>>(System.IO.Path.Combine(path, FILE_NAME));
                if (loadedAssetInfos == null)
                    loadedAssetInfos = new Dictionary<string, AssetInfo>();
                return loadedAssetInfos;
            }

            static AssetInfo()
            {
                s_editingAssetInfos = LoadAssetInfos(BundleManager.EDITING_INFOS_PATH); // refresh editing asset info.

                string szAssetInfoPath = System.IO.Path.Combine(BundleManager.EDITING_INFOS_PATH, FILE_NAME);
                AssetImporter importer = AssetImporter.GetAtPath(szAssetInfoPath);
                if (importer != null && importer.assetBundleName != BUNDLE_NAME)
                    importer.assetBundleName = BUNDLE_NAME;

                Refresh(); // always needed editing info on editor.
            }

            public static bool IsEdited()
            {
                if (s_savedAssetInfos == null && s_editingAssetInfos == null)
                    return false;
                if (s_savedAssetInfos == null || s_editingAssetInfos == null)
                    return true;
                if (s_savedAssetInfos.Count != s_editingAssetInfos.Count)
                    return true;

                foreach (KeyValuePair<string, AssetInfo> savedInfo in s_savedAssetInfos)
                {
                    AssetInfo editingInfo = null;
                    s_editingAssetInfos.TryGetValue(savedInfo.Key, out editingInfo);
                    if (editingInfo == null || editingInfo.isEdited)
                        return true;
                }

                return false;
            }

            public static AssetInfo AddEditingInfo(string GUID, string bundleName, string assetPath, string managedName = null)
            {
                AssetInfo addedInfo = new AssetInfo();
                addedInfo.bundleName = bundleName;
                addedInfo.assetPath = assetPath;
                if (managedName == null)
                {
                    addedInfo.nameByPath = true;
                    addedInfo.managedName = addedInfo.defaultName;
                }
                else
                {
                    addedInfo.nameByPath = false;
                    addedInfo.managedName = managedName;
                }
                addedInfo.isJustAdded = true;
                addedInfo.isChanged = false;
                addedInfo.nameByPath = true;
                s_editingAssetInfos[GUID] = addedInfo; // s_editingAssetInfos must be not null. initialized at static creator

                return addedInfo;
            }

            public static AssetInfo GetEditingInfo(string GUID, string bundleName, string assetPath)
            {
                AssetInfo editingInfo = null;
                s_editingAssetInfos.TryGetValue(GUID, out editingInfo); // s_editingAssetInfos must be not null. initialized at static creator
                if (editingInfo == null)
                    editingInfo = AddEditingInfo(GUID, bundleName, assetPath);

                return editingInfo;
            }

            public static int SavedAssetCountInBundle(string bundleName)
            {
                if (s_savedAssetInfos == null)
                    return 0;

                int count = 0;
                foreach (AssetInfo savedInfo in s_savedAssetInfos.Values)
                {
                    if (savedInfo.bundleName == bundleName)
                        ++count;
                }
                return count;
            }

            public static int OutputAssetCountInBundle(string bundleName)
            {
                if (s_outputAssetInfos == null)
                    return 0;

                int count = 0;
                foreach (AssetInfo outputInfo in s_outputAssetInfos.Values)
                {
                    if (outputInfo.bundleName == bundleName)
                        ++count;
                }
                return count;
            }

            /// <summary>
            /// update AssetInfo from AssetDataBase.
            /// and check is edited
            /// </summary>
            public static void Refresh()
            {
                //Debug.Log("AssetInfo.Refresh()");

                s_savedAssetInfos = LoadAssetInfos(BundleManager.EDITING_INFOS_PATH);

                s_dictManagedNameToGUID.Clear();
                List<string> listRemove = new List<string>(s_editingAssetInfos.Keys);

                // update editing AssetInfo from AssetDataBase.
                foreach (string bundleName in AssetDatabase.GetAllAssetBundleNames())
                {
                    if (bundleName == BUNDLE_NAME)
                        continue;

                    string[] includedAssets = AssetDatabase.GetAssetPathsFromAssetBundle(bundleName);
                    if (includedAssets != null && includedAssets.Length > 0)
                    {
                        foreach (string includedAssetPath in includedAssets)
                        {
                            string GUID = AssetDatabase.AssetPathToGUID(includedAssetPath);
                            listRemove.Remove(GUID);

                            AssetInfo editingInfo = null;
                            s_editingAssetInfos.TryGetValue(GUID, out editingInfo);
                            if (editingInfo == null)
                            {   // add editing AssetInfo from AssetDatabase.
                                editingInfo = AddEditingInfo(GUID, bundleName, includedAssetPath);
                            }
                            else
                            {
                                editingInfo.bundleName = bundleName;
                                if (editingInfo.assetPath != includedAssetPath)
                                {   // if changed asset path,
                                    editingInfo.assetPath = includedAssetPath; // update asset path.
                                    if (editingInfo.nameByPath)
                                        editingInfo.managedName = editingInfo.defaultName;
                                }
                            }
                            if (s_dictManagedNameToGUID.ContainsKey(editingInfo.managedName))
                            {   // there is already exist managed name. fix this recursive.
                                if (editingInfo.nameByPath == false)
                                {
                                    editingInfo.nameByPath = true;
                                    editingInfo.managedName = editingInfo.defaultName;
                                }
                                else
                                {
                                    string sameNameGUID = s_dictManagedNameToGUID[editingInfo.managedName];
                                    AssetInfo sameNameInfo = null;
                                    s_editingAssetInfos.TryGetValue(sameNameGUID, out sameNameInfo);
                                    if (sameNameInfo != null) // must be not null.
                                    {
                                        sameNameInfo.nameByPath = true;
                                        sameNameInfo.managedName = sameNameInfo.defaultName;
                                        s_dictManagedNameToGUID[sameNameInfo.managedName] = sameNameGUID;
                                    }
                                }
                            }
                            s_dictManagedNameToGUID[editingInfo.managedName] = GUID;
                        }
                    }
                }

                // del editing AssetInfo, not exist in AssetDataBase.
                foreach (string GUID in listRemove)
                {
                    s_editingAssetInfos.Remove(GUID);
                }

                // check edited with saved data.
                foreach (KeyValuePair<string, AssetInfo> editingInfo in s_editingAssetInfos)
                {
                    editingInfo.Value.CheckEditing();
                }
            }

            /// <summary>
            /// load saved AssetInfo from target output directory
            /// and check is changed with output data.
            /// </summary>
            public static void LoadTargetAssetInfo(string szAssetInfo)
            {
                //Debug.Log("AssetInfo.LoadTargetAssetInfo()");

                try
                {
                    s_outputAssetInfos = LitJson.JsonMapper.ToObject<Dictionary<string, AssetInfo>>(szAssetInfo);
                }
                catch
                {
                    s_outputAssetInfos = null;
                }
                if (s_outputAssetInfos == null)
                {
                    s_outputAssetInfos = new Dictionary<string, AssetInfo>();
                    return;
                }

                // update status of editing AssetInfo from saved AssetInfo.
                foreach (KeyValuePair<string, AssetInfo> editingInfo in s_editingAssetInfos)
                {
                    AssetInfo targetInfo = null;
                    s_outputAssetInfos.TryGetValue(editingInfo.Key, out targetInfo);
                    if (targetInfo == null)
                    {   // mark isJustAdded
                        editingInfo.Value.isJustAdded = true;
                        editingInfo.Value.isChanged = false;
                    }
                    else
                    {   // check editing info is changed.
                        editingInfo.Value.isJustAdded = false;
                        editingInfo.Value.isChanged = targetInfo.IsChanged(editingInfo.Value);
                    }
                }

                //// restore editing AssetInfo from saved AssetInfo.
                //foreach (KeyValuePair<string, AssetInfo> targetInfo in loadedAssetInfo)
                //{
                //    AssetInfo editingInfo = null;
                //    s_editingAssetInfos.TryGetValue(targetInfo.Key, out editingInfo);
                //    if (editingInfo == null)
                //    {
                //        AddEditingInfo(targetInfo.Key, targetInfo.Value.bundleName, targetInfo.Value.assetName, targetInfo.Value.managedName);
                //    }
                //}
            }

            /// <summary>
            /// save editing info.
            /// </summary>
            public static void Save()
            {
                string szAssetInfoPath = System.IO.Path.Combine(BundleManager.EDITING_INFOS_PATH, FILE_NAME);
                UtilFile.SaveObjectToJson(s_editingAssetInfos, szAssetInfoPath);
                AssetDatabase.ImportAsset(szAssetInfoPath);

                s_savedAssetInfos = LoadAssetInfos(BundleManager.EDITING_INFOS_PATH);
                AssetImporter importer = AssetImporter.GetAtPath(szAssetInfoPath);
                if (importer != null && importer.assetBundleName != BUNDLE_NAME)
                    importer.assetBundleName = BUNDLE_NAME;
                Refresh();
            }

            public static void Revert()
            {
                foreach (KeyValuePair<string, AssetInfo> editingInfo in s_editingAssetInfos)
                {
                    if (editingInfo.Value.isEdited == false)
                        continue;

                    AssetInfo savedInfo = null;
                    s_savedAssetInfos.TryGetValue(editingInfo.Key, out savedInfo);
                    if (savedInfo == null)
                        continue;

                    //if (savedInfo.nameByPath)
                    //editingInfo.Value.managedName = savedInfo.managedName;
                }
            }

            public bool IsChanged(AssetInfo otherInfo)
            {
                if (otherInfo == null)
                    return true;

                if (bundleName != otherInfo.bundleName ||
                    assetPath != otherInfo.assetPath ||
                    managedName != otherInfo.managedName)
                    return true;

                return false;
            }

            public void CheckEditing()
            {
                AssetInfo savedInfo = null;
                s_savedAssetInfos.TryGetValue(s_dictManagedNameToGUID[managedName], out savedInfo);
                if (savedInfo == null)
                {
                    isEdited = true;
                }
                else
                {
                    if (bundleName != savedInfo.bundleName ||
                        assetPath != savedInfo.assetPath ||
                        managedName != savedInfo.managedName ||
                        nameByPath != savedInfo.nameByPath)
                        isEdited = true;
                    else
                        isEdited = false;
                }
            }

            /// <summary>
            /// validate managed name for AssetInfo.
            /// </summary>
            /// <param name="newManagedName">new name for asset</param>
            /// <returns>error message. if no error, return value will be null</returns>
            public static string ValidateManagedName(string newManagedName)
            {
                if (s_dictManagedNameToGUID.ContainsKey(newManagedName))
                    return "'" + newManagedName + "' is already exist.";

                return null;
            }
        }

        [MenuItem("Tools/AvoEx/CleanCache")]
        public static void CleanCache()
        {
            if (EditorUtility.DisplayDialog("CleanCache", "Are you sure you want to CleanCache?", "Clean", "Cancel"))
            {
                if (Caching.CleanCache())
                {
                    EditorUtility.DisplayDialog("CleanCache", "Success", "Ok");
                }
                else
                {
                    EditorUtility.DisplayDialog("CleanCache", "Failed - cache was in use", "Ok");
                }
            }
        }
#endif
        
        #endregion
    }
}