﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/* See the "http://avoex.com/avoex/default-license" for the full license governing this code. */

namespace AvoEx
{
    public class ObjectRequest : AsyncRequest
    {
        ManagedObject m_objInstance = null;
        ObjectPoolManager.ObjectPool m_objPool = null;

        public ManagedObject instance
        {
            get { return m_objInstance; }
        }

        public override float progress
        {
            get { return m_objPool.cachingProgress; }
        }

        public void Cancel()
        {
            if (m_isDone && m_objInstance != null)
            {
                m_objPool.PushInstance(m_objInstance);
                m_isDone = false;
                m_objInstance = null;
            }

            s_listObjectRequests.Remove(this);
        }

        private ObjectRequest(ObjectPoolManager.ObjectPool pool)
        {
            m_objPool = pool;

            Update();

            if (m_isDone)
                return;

            s_listObjectRequests.Add(this);
        }

        /// <summary>
        /// call once per frame.
        /// </summary>
        private void Update()
        {
            if (m_objPool.remainSize > 0) // 풀에 남은 인스턴스가 있다.
            {
                m_objInstance = m_objPool.PopInstance();
                m_isDone = true;
            }
            else if (m_objPool.isCaching) // 풀이 캐싱중이다.
            {
                // 캐싱 대기.
            }
            else if (m_objPool.isFailedLoad) // 풀이 리소스를 얻는데 실패했다.
            {
                m_error = string.Format("ObjectPoolManager.ObjectPool({0}) is load failed!!!", m_objPool.name);
                m_isDone = true;
                Debug.LogError(m_error);
                return;
            }
            else // 풀에 남은 인스턴스가 없는데, 캐싱 중도 아니다.
            {
                m_objPool.StartCacheAsync(1); // 풀 사이즈 늘림.
            }
        }

        #region STATIC_INTERFACES
        protected static List<ObjectRequest> s_listObjectRequests = new List<ObjectRequest>();

        /// <summary>
        /// assumes the AssetManager is initialized or error. Update() must called after the AssetManager is initialized.
        /// call once per frame.
        /// </summary>
        public static void UpdateRequests()
        {
            for (int i = 0; i < s_listObjectRequests.Count;)
            {
                s_listObjectRequests[i].Update();

                if (s_listObjectRequests[i].isDone)
                    s_listObjectRequests.RemoveAt(i);
                else
                    ++i;
            }
        }

        public static void ClearRequests()
        {
            s_listObjectRequests.Clear();
        }

        public static ObjectRequest PopInstanceAsync(string szName)
        {
            ObjectPoolManager.ObjectPool pool = ObjectPoolManager.ObjectPool.GetPool(szName);
            if (pool == null)
                return null;

            return new ObjectRequest(pool);
        }
        #endregion STATIC_INTERFACES
    }


    public partial class ObjectPoolManager : MonoSingleton<ObjectPoolManager>
    {
        private static Transform s_transform = null;

        /// <summary>
        /// 이름이 szName 인 GameObject의 인스턴스를 풀에서 얻어낸다.
        /// 다쓰고 나면, 반드시 PushInstance()로 돌려줘야 한다.
        /// </summary>
        /// <param name="szName">얻어낼 에셋의 이름</param>
        /// <returns>인스턴스에 붙어있는 ManagedObject 컴포넌트</returns>
        public static ManagedObject PopInstance(string szName)
        {
            if (Instance == null)
                return null;

            ObjectPool pool = ObjectPool.GetPool(szName);
            if (pool == null)
            {
                Debug.LogErrorFormat("ObjectPoolManager.PopInstance({0}) is failed. ObjectPool of '{0}' is not exist.", szName);
                return null;
            }

            return pool.PopInstance();
        }

        /// <summary>
        /// 다 사용한 인스턴스를 돌려준다.
        /// </summary>
        /// <param name="instManaged">다 사용한 인스턴스의 ManagedObject 컴포넌트</param>
        public static void PushInstance(ManagedObject instManaged)
        {
            if (instManaged == null || IsInstantiated == false)
                return;

            ObjectPool pool = ObjectPool.GetPool(instManaged.poolName);
            if (pool == null)
            {
                Debug.LogWarningFormat("ObjectPoolManager.PushInstance({0}) is failed. ObjectPool of '{0}' is not exist.", instManaged, instManaged.poolName);
                Destroy(instManaged.gameObject);
                return;
            }
            pool.PushInstance(instManaged);
        }

        /// <summary>
        /// 다 사용한 인스턴스를 돌려준다.
        /// </summary>
        /// <param name="instManaged">다 사용한 인스턴스의 ManagedObject 컴포넌트</param>
        public static void PushInstance(ManagedObject instManaged, float fDelay)
        {
            if (instManaged == null || IsInstantiated == false)
                return;

            Instance.StartCoroutine(_PushInstance(instManaged, fDelay));
        }

        private static IEnumerator _PushInstance(ManagedObject instManaged, float fDelay)
        {
            yield return new WaitForSeconds(fDelay);

            ObjectPool pool = ObjectPool.GetPool(instManaged.poolName);
            if (pool == null)
            {
                Debug.LogWarningFormat("ObjectPoolManager.PushInstance({0}) is failed. ObjectPool of '{0}' is not exist.", instManaged, instManaged.poolName);
                Destroy(instManaged.gameObject);
                yield break;
            }
            pool.PushInstance(instManaged);
        }

        /// <summary>
        /// 사용중인 모든 인스턴스들을 각자의 풀에 돌려준다.
        /// </summary>
        public static void PushAllInstaces()
        {
            if (IsInstantiated == false)
                return;

            var itorPools = ObjectPool.objectPools.GetEnumerator();
            while (itorPools.MoveNext())
            {
                itorPools.Current.Value.PushAll();
            }
        }

        /// <summary>
        /// 비동기로 iSize 만큼 풀의 크기를 조정한다.
        /// </summary>
        /// <param name="szName">생성할 풀의 이름</param>
        /// <param name="iSize">풀에 생성할 인스턴스 갯수</param>
        /// <returns></returns>
        public IEnumerator CoAdjustPoolSize(string szName, int iSize)
        {
            yield return CoAdjustPoolSize(ObjectPool.GetPool(szName), iSize);
        }

        private IEnumerator CoAdjustPoolSize(ObjectPool pool, int iSize)
        {
            if (pool == null)
                yield break;

            yield return pool.StartCacheAsync(iSize - pool.size);
        }

        public ObjectPool IncreasePoolAsync(string szName, int iAddSize)
        {
            ObjectPool pool = ObjectPool.GetPool(szName); // 오브젝트 매니저의 인스턴스가 생성된 이후이므로, 풀 생성이 실패할리는 없다.
            if (pool != null)
                pool.StartCacheAsync(iAddSize);

            return pool;
        }

        public void DestroyPool(string szName)
        {
            ObjectPool.DestroyPool(szName);
        }

        //
        protected override void OnAwake()
        {
            s_transform = transform;
        }

        protected override void OnQuit()
        {
            ObjectRequest.ClearRequests();
            ObjectPool.ClearPool();
            s_transform = null;
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            ObjectRequest.UpdateRequests();
        }
    }

    public partial class ObjectPoolManager
    {
        public partial class ObjectPool : MonoBehaviour
        {
            private string m_szName;
            private GameObject m_objPrefab = null;
            private Transform m_transform = null;
            private List<ManagedObject> m_lstActives = new List<ManagedObject>();
            private Stack<ManagedObject> m_stackDisables = new Stack<ManagedObject>();
            private Vector3 m_orgPrefabPosition = Vector3.zero;
            private Quaternion m_orgPrefabRotation = Quaternion.identity;
            private Vector3 m_orgPrefabScale = Vector3.one;
            private int m_orgLayer = 0;
            private bool m_bFailedLoad = false;
            private Coroutine m_coCaching = null;
            private int m_iCachingSize = 0;
            private float m_fCachingProgress = 0f;

            public GameObject prefab { get { return m_objPrefab; } }
            public int size { get { return m_lstActives.Count + m_stackDisables.Count; } }
            public int remainSize { get { return m_stackDisables.Count; } }
            public bool isFailedLoad { get { return m_bFailedLoad; } }
            public bool isCaching { get { return (m_coCaching != null); } }
            public Coroutine cachingCoroutine { get { return m_coCaching; } }
            public int cachingSize { get { return m_iCachingSize; } set { m_iCachingSize = value; } }
            public float cachingProgress { get { return m_fCachingProgress; } }

            void Awake()
            {
                MemoryScope.LoadedObject(this); // for managing scope
                m_transform = transform;
            }

            void OnDestroy()
            {
                Clear();
                MemoryScope.UnloadedObject(this); // for managing scope
            }

            public ManagedObject PopInstance()
            {
                if (scopeUsed == false)
                    MemoryScope.SetUsedToScope(this);

                ManagedObject instManaged = null;
                if (m_stackDisables.Count > 0)
                    instManaged = m_stackDisables.Pop();
                else
                    instManaged = CreateInstance();

                if (instManaged == null)
                    return null;

                m_lstActives.Add(instManaged);
                instManaged.isActivated = true;
                instManaged.cachedObject.SetActive(true);

                return instManaged;
            }

            public void PushInstance(ManagedObject instManaged)
            {
                if (instManaged == null)
                    return;

                if (m_lstActives.Contains(instManaged) == true)
                {
                    m_lstActives.Remove(instManaged); // 활성 목록에서 제거하고,
                    // 아래에 비활성 목록에 추가함.
                }
                else if (instManaged.isManaged == true)
                {
                    return; // 관리되고 있는 ManagedObject 인데, m_lstActives 에 없다면, 이미 Push 처리 완료 되있는 상태이므로, 리턴..
                    // 관리 중이 아닌 ManagedObject (ObjectPoolManager 에서 생성하지 않고, 외부에서 생성한 경우)라면, 비활성 목록(풀)에 추가함.
                }

                m_stackDisables.Push(instManaged);
                instManaged.isActivated = false;
                instManaged.RestoreInstance(m_transform, m_orgPrefabPosition, m_orgPrefabRotation, m_orgPrefabScale, m_orgLayer);
            }

            public void PushAll()
            {
                var itorActives = m_lstActives.GetEnumerator();
                while (itorActives.MoveNext())
                {
                    m_stackDisables.Push(itorActives.Current);
                    itorActives.Current.isActivated = false;
                    itorActives.Current.RestoreInstance(m_transform, m_orgPrefabPosition, m_orgPrefabRotation, m_orgPrefabScale, m_orgLayer);
                }
                m_lstActives.Clear();
            }

            /// <summary>
            /// ObjectPoolManager가 아닌 다른 경로로 ManagedObject가 지워지는 경우에만 호출된다.
            /// </summary>
            /// <param name="instManaged"></param>
            public void DelInstance(ManagedObject instManaged)
            {
                if (instManaged == null)
                    return;
                instManaged.isActivated = false;
                if (m_lstActives.Remove(instManaged))
                    return;
                Stack<ManagedObject> tempDisables = new Stack<ManagedObject>(m_stackDisables.Count);
                while (m_stackDisables.Count > 0)
                {
                    ManagedObject topInst = m_stackDisables.Pop();
                    if (topInst == instManaged)
                        break;
                }
                while (tempDisables.Count > 0)
                {
                    m_stackDisables.Push(tempDisables.Pop());
                }
            }

            IEnumerator CoCacheAsync(int iAddSize)
            {
                if (scopeUsed == false)
                {
                    MemoryScope.SetUsedToScope(this);
                    if (m_objPrefab != null)
                        AssetManager.LoadAsset<GameObject>(m_szName); // pool이 사용하는 asset도 MemoryScope 사용여부 체크.
                }

                m_iCachingSize += iAddSize;

                // 캐싱이 완료될때까지 코루틴이 끝나지 않아야 하므로, 주석처리함.
                //if (m_iCachingSize <= size)
                //{
                //    m_coCaching = null;
                //    yield break;
                //}

                if (m_objPrefab == null)
                {
                    m_fCachingProgress = 0f;
                    AssetRequest<GameObject> assetReq = AssetManager.LoadAssetAsync<GameObject>(m_szName);
                    while (assetReq.isDone == false)
                    {
                        m_fCachingProgress = assetReq.progress;
                        yield return null;
                    }
                    m_objPrefab = assetReq.asset;
                    if (m_objPrefab != null)
                    {
                        m_orgPrefabPosition = m_objPrefab.transform.localPosition;
                        m_orgPrefabRotation = m_objPrefab.transform.localRotation;
                        m_orgPrefabScale = m_objPrefab.transform.localScale;
                        m_orgLayer = m_objPrefab.layer;
                    }
                    else
                    {
                        m_bFailedLoad = true;
                    }
                }

                m_fCachingProgress = 1f;

                while (size < m_iCachingSize)
                {
                    ManagedObject instManaged = CreateInstance();
                    if (instManaged == null)
                    {
                        m_coCaching = null;
                        yield break;
                    }
                    m_stackDisables.Push(instManaged);
                    instManaged.cachedObject.SetActive(false);
                    yield return null;
                }

                m_coCaching = null;
            }

            public Coroutine StartCacheAsync(int iAddSize)
            {
                if (m_coCaching != null)
                {
                    m_iCachingSize += iAddSize;
                }
                else
                {
                    m_coCaching = StartCoroutine(CoCacheAsync(iAddSize));
                }

                return m_coCaching;
            }

            ManagedObject CreateInstance()
            {
                if (m_objPrefab == null)
                {
                    m_objPrefab = AssetManager.LoadAsset<GameObject>(m_szName); // 번들 다운로드는 이미 되어 있음을 전제로, 실시간 로딩.
                    if (m_objPrefab == null)
                    {
                        Debug.LogErrorFormat("ObjectPool.CreateInstance() is failed. name = '{0}'", m_szName);
                        return null;
                    }
                    else
                    {
                        m_orgPrefabPosition = m_objPrefab.transform.localPosition;
                        m_orgPrefabRotation = m_objPrefab.transform.localRotation;
                        m_orgPrefabScale = m_objPrefab.transform.localScale;
                        m_orgLayer = m_objPrefab.layer;
                    }
                }

                // Instantiate
                GameObject newInstance = Instantiate(m_objPrefab);

                // add ManagedObject
                ManagedObject instManaged = newInstance.GetComponent<ManagedObject>();
                if (instManaged == null)
                    instManaged = newInstance.AddComponent<ManagedObject>();
                instManaged.InitInstance(m_transform, m_szName);

                return instManaged;
            }

            void Clear()
            {
                m_iCachingSize = 0;
                if (m_coCaching != null)
                {
                    StopCoroutine(m_coCaching);
                    m_coCaching = null;
                }
                m_bFailedLoad = false;
                m_objPrefab = null;
                var itorActives = m_lstActives.GetEnumerator();
                while (itorActives.MoveNext())
                    itorActives.Current.DelInstance();
                m_lstActives.Clear();
                var itorDisables = m_stackDisables.GetEnumerator();
                while (itorDisables.MoveNext())
                    itorDisables.Current.DelInstance();
                m_stackDisables.Clear();
                AssetManager.Unload(m_szName);
            }
        }

        public partial class ObjectPool // statics
        {
            private const string PREFIX_POOL_NAME = "POOL_";
            private static Dictionary<string, ObjectPool> s_dicObjectPools = new Dictionary<string, ObjectPool>();
            public static Dictionary<string, ObjectPool> objectPools { get { return s_dicObjectPools; } }

            public static ObjectPool GetPool(string szName)
            {
                if (string.IsNullOrEmpty(szName))
                    return null;

                ObjectPool compPool = null;
                s_dicObjectPools.TryGetValue(szName, out compPool);
                if (compPool == null)
                {
                    if (s_transform != null)
                    {
                        GameObject objPool = new GameObject(PREFIX_POOL_NAME + szName);
                        objPool.transform.SetParent(s_transform, false);
                        compPool = objPool.AddComponent<ObjectPool>();
                        compPool.m_szName = szName;
                        s_dicObjectPools[szName] = compPool;
                    }
                    else
                    {
                        Debug.LogErrorFormat("ObjectPool.GetPool({0}) is failed. OjbectPoolManager is not instantiated.", szName);
                    }
                }

                return compPool;
            }

            public static void ClearPool()
            {
                var itorPools = s_dicObjectPools.GetEnumerator();
                while (itorPools.MoveNext())
                {
                    itorPools.Current.Value.Clear();
                    Destroy(itorPools.Current.Value);
                }
                s_dicObjectPools.Clear();
            }

            public static void DestroyPool(string szName)
            {
                ObjectPool pool = GetPool(szName);
                if (pool == null)
                    return;

                s_dicObjectPools.Remove(szName);
                Destroy(pool.gameObject);
            }
        }

        public partial class ObjectPool : IScopeObject
        {
            public int scopeIndex { get; set; } // set by MemoryScope only
            public bool scopeUsed { get; set; } // set by MemoryScope only
            public int objectInstanceID { get { return gameObject.GetInstanceID(); } }

            public void Unload()
            {
                DestroyPool(m_szName);
            }
        }
    }

}