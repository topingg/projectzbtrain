﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections.Generic;

/* See the "http://avoex.com/avoex/default-license" for the full license governing this code. */

namespace AvoEx
{

    public partial class AssetManagerEditor : EditorWindow
    {
        const float BUNDLE_VIEW_BUNDLE_LIST_WIDTH = 310f;
        const float ICON_WIDTH = 45f;
        Vector2 m_scrollBundles;
        Vector2 m_scrollBundleAssets;
        string m_selectedBundle;
        bool m_isExistUnusedBundle;
        string m_newBundleName;

        void OnGUIBundleView()
        {
            // top colume
            EditorGUILayout.BeginHorizontal();
            EditorGUI.BeginDisabledGroup(true);
            GUI.backgroundColor = Color.black;
            EditorGUILayout.LabelField("bundles : name (size)", EditorStyles.miniButton, GUILayout.Width(BUNDLE_VIEW_BUNDLE_LIST_WIDTH));
            EditorGUILayout.LabelField("included assets : name (path)", EditorStyles.miniButton);
            GUI.backgroundColor = Color.white;
            EditorGUI.EndDisabledGroup();
            EditorGUILayout.EndHorizontal();

            // scroll view
            EditorGUILayout.BeginHorizontal();

            OnGUIBundleList();

            OnGUIIncludedAssetsList();

            EditorGUILayout.EndHorizontal();

            // bottom menu
            OnGUIBundleMaker();
        }

        void OnGUIBundleMaker()
        {
            int iSelectedCount = Selection.objects.Length;

            EditorGUILayout.BeginHorizontal();
            m_newBundleName = EditorGUILayout.TextField(m_newBundleName, GUILayout.Width(BUNDLE_VIEW_BUNDLE_LIST_WIDTH));
            if (iSelectedCount > 0)
            {
                const int MAX_SELECTED_NAME_VIEW = 3;
                string selectedAssetNames = string.Format("selected assets({0}) : ", iSelectedCount);
                for (int i = 0; i < iSelectedCount && i < MAX_SELECTED_NAME_VIEW; ++i)
                {
                    if (i > 0)
                        selectedAssetNames += ", ";
                    selectedAssetNames += Selection.objects[i].name;
                }

                if (iSelectedCount > MAX_SELECTED_NAME_VIEW)
                    selectedAssetNames += ", ...";

                EditorGUILayout.LabelField(selectedAssetNames);
            }
            EditorGUILayout.EndHorizontal();

            bool bMakeBundle = false;
            string newBundleName = m_selectedBundle;

            EditorGUILayout.BeginHorizontal();

            if (iSelectedCount == 0 || string.IsNullOrEmpty(m_newBundleName) || BundleManager.BundleInfo.IsEditingBundle(m_newBundleName))
            {
                EditorGUI.BeginDisabledGroup(true);
                GUILayout.Button("New bundle with selected assets", GUILayout.Width(BUNDLE_VIEW_BUNDLE_LIST_WIDTH));
                EditorGUI.EndDisabledGroup();
            }
            else
            {
                GUI.backgroundColor = Color.green;
                if (GUILayout.Button("New bundle with selected assets", GUILayout.Width(BUNDLE_VIEW_BUNDLE_LIST_WIDTH)))
                {
                    bMakeBundle = true;
                    newBundleName = m_newBundleName;
                    m_newBundleName = "";
                    GUI.FocusControl("");
                }
                GUI.backgroundColor = Color.white;
            }

            if (string.IsNullOrEmpty(m_selectedBundle) == false)
            {
                if (iSelectedCount > 0)
                {
                    GUI.backgroundColor = Color.green;
                    if (GUILayout.Button(string.Format("Add selected assets to '{0}' bundle", m_selectedBundle)))
                    {
                        bMakeBundle = true;
                    }
                    GUI.backgroundColor = Color.white;
                }
                else
                {
                    EditorGUI.BeginDisabledGroup(true);
                    GUILayout.Button(string.Format("Add selected assets to '{0}' bundle", m_selectedBundle));
                    EditorGUI.EndDisabledGroup();
                }
            }


            if (bMakeBundle)
            {
                bool isIncludedFolder = false;
                foreach (Object selectedObject in Selection.objects)
                {
                    string objPath = AssetDatabase.GetAssetPath(selectedObject);
                    if (System.IO.Directory.Exists(objPath) == false)
                        continue;
                    isIncludedFolder = true;
                    break;
                }

                int dialogChosen = 0;
                if (isIncludedFolder)
                {
                    dialogChosen = EditorUtility.DisplayDialogComplex("", "Make bundles from 'folder' or 'each files in folder'?", "folder", "each files", "cancel");
                }

                if (dialogChosen == 0)
                {
                    foreach (Object selectedObject in Selection.objects)
                    {
                        SetBundleNameToObject(selectedObject, newBundleName, true);
                    }
                }
                else if (dialogChosen == 1)
                {
                    foreach (Object selectedObject in Selection.objects)
                    {
                        SetBundleNameToObject(selectedObject, newBundleName, false);
                    }
                }
            }

            EditorGUILayout.EndHorizontal();

            if (iSelectedCount == 0)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.HelpBox("select assets in project view, to make a new bundle.", MessageType.Info);
                EditorGUILayout.EndHorizontal();
            }
            else if (string.IsNullOrEmpty(m_newBundleName))
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.HelpBox("input name for new bundle.", MessageType.Info);
                EditorGUILayout.EndHorizontal();
            }
            else if (BundleManager.BundleInfo.IsEditingBundle(m_newBundleName))
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.HelpBox(string.Format("'{0}' is already exist bundle.", m_newBundleName), MessageType.Warning);
                EditorGUILayout.EndHorizontal();
            }
        }

        void OnGUIBundleList()
        {
            m_isExistUnusedBundle = false;
            m_scrollBundles = EditorGUILayout.BeginScrollView(m_scrollBundles, GUILayout.Width(BUNDLE_VIEW_BUNDLE_LIST_WIDTH));

#if USE_MANUAL_REFRESH_ASSETMANAGER
            int count = 0;
            int maxCount = AssetDatabase.GetAllAssetBundleNames().Length;
#endif
            foreach (string bundleName in AssetDatabase.GetAllAssetBundleNames())
            {
#if USE_MANUAL_REFRESH_ASSETMANAGER
                if (globalRefresh == true)
                    EditorUtility.DisplayProgressBar("Refresh bundle : " + bundleName, "(" + count++ + "/" + maxCount + ")", (float)count / (float)maxCount);
#endif
                if (bundleName == AssetManager.AssetInfo.BUNDLE_NAME)
                    continue;

                OnGUIBundleListItem(bundleName);
            }

#if USE_MANUAL_REFRESH_ASSETMANAGER
            EditorUtility.ClearProgressBar();
            globalRefresh = false;
#endif

            EditorGUI.BeginDisabledGroup(!m_isExistUnusedBundle);
            GUI.backgroundColor = Color.red;
            if (GUILayout.Button("Remove Unused Bundle"))
            {
                AssetDatabase.RemoveUnusedAssetBundleNames();
            }
            GUI.backgroundColor = Color.white;
            EditorGUI.EndDisabledGroup();

            EditorGUILayout.EndScrollView();
        }

#if USE_MANUAL_REFRESH_ASSETMANAGER
        Dictionary<string, string[]> cachedIncludeAssetNames = new Dictionary<string, string[]>();
        Dictionary<string, bool> cachedEdtingState = new Dictionary<string, bool>();
        Dictionary<string, bool> cachedChangingState = new Dictionary<string, bool>();
#endif

        void OnGUIBundleListItem(string bundleName)
        {
            if (string.IsNullOrEmpty(bundleName))
                return;

#if USE_MANUAL_REFRESH_ASSETMANAGER
            if (globalProjectChanged == true)
            {
                cachedIncludeAssetNames.Clear();
                cachedEdtingState.Clear();
                cachedChangingState.Clear();
            }
#endif

            bool isSelected = IsSelectedBundle(bundleName);
            BundleManager.BundleInfo editingInfo = BundleManager.BundleInfo.GetEditingInfo(bundleName);

#if USE_MANUAL_REFRESH_ASSETMANAGER
            string[] includedAssetNames = null;
            if (cachedIncludeAssetNames.ContainsKey(bundleName) == false)
            {
                includedAssetNames = AssetDatabase.GetAssetPathsFromAssetBundle(bundleName);
                cachedIncludeAssetNames.Add(bundleName, includedAssetNames);
            }
            else
            {
                includedAssetNames = cachedIncludeAssetNames[bundleName];
            }
#else
            string[] includedAssetNames = AssetDatabase.GetAssetPathsFromAssetBundle(bundleName);
#endif


            if (includedAssetNames.Length == 0)
                m_isExistUnusedBundle = true;

            bool isEdited = editingInfo.isEdited;
            bool isChanged = editingInfo.isChanged;

#if USE_MANUAL_REFRESH_ASSETMANAGER
            if (cachedEdtingState.ContainsKey(bundleName))
            {
                if (isEdited != cachedEdtingState[bundleName])
                    cachedEdtingState.Remove(bundleName);
            }

            if (cachedChangingState.ContainsKey(bundleName))
            {
                if (isChanged != cachedChangingState[bundleName])
                    cachedChangingState.Remove(bundleName);
            }
#endif

#if USE_MANUAL_REFRESH_ASSETMANAGER
            if (globalProjectChanged == true || cachedEdtingState.ContainsKey(bundleName) == false || cachedChangingState.ContainsKey(bundleName) == false)
#endif
            {
                if (isEdited == false)
                {
                    if (includedAssetNames.Length == AssetManager.AssetInfo.SavedAssetCountInBundle(bundleName))
                    {
                        foreach (string includedAssetName in includedAssetNames)
                        {
                            string GUID = AssetDatabase.AssetPathToGUID(includedAssetName);
                            AssetManager.AssetInfo includedAssetInfo = AssetManager.AssetInfo.GetEditingInfo(GUID, bundleName, includedAssetName);
                            if (includedAssetInfo.isEdited)
                            {
                                isEdited = true;
                                break;
                            }
                            if (includedAssetInfo.isChanged)
                            {
                                isChanged = true;
                            }
                        }
                    }
                    else
                    {
                        isEdited = true;
                        isChanged = true;
                    }
                }

                if (isChanged == false)
                {
                    if (includedAssetNames.Length != AssetManager.AssetInfo.OutputAssetCountInBundle(bundleName))
                        isChanged = true;
                }
            }

#if USE_MANUAL_REFRESH_ASSETMANAGER
            cachedEdtingState[bundleName] = isEdited;
            cachedChangingState[bundleName] = isChanged;
            globalProjectChanged = false;
#endif

            if (isSelected)
                GUI.backgroundColor = Color.yellow;
            else if (isEdited)
                GUI.backgroundColor = Color.green;
            else if (editingInfo.isJustAdded)
                GUI.backgroundColor = Color.cyan;
            else if (isChanged)
                GUI.backgroundColor = Color.magenta;
            else
                GUI.backgroundColor = Color.white;
            Rect rect = EditorGUILayout.BeginHorizontal(EditorStyles.textArea);
            GUI.backgroundColor = Color.white;

            float widthBundleName = 200f;
            if (isEdited)
            {
                GUI.color = Color.green;
                EditorGUILayout.LabelField("Edited", EditorStyles.miniButton, GUILayout.Width(ICON_WIDTH));
                GUI.color = Color.white;
                widthBundleName -= ICON_WIDTH;
            }
            else if (editingInfo.isJustAdded)
            {
                if (includedAssetNames.Length == 0)
                {
                    GUI.color = Color.red;
                    EditorGUILayout.LabelField("Unuse", EditorStyles.miniButton, GUILayout.Width(ICON_WIDTH));
                    GUI.color = Color.white;
                }
                else
                {
                    GUI.color = Color.cyan;
                    EditorGUILayout.LabelField("New", EditorStyles.miniButton, GUILayout.Width(ICON_WIDTH));
                    GUI.color = Color.white;
                }
                widthBundleName -= ICON_WIDTH;
            }
            else if (isChanged)
            {
                GUI.color = Color.magenta;
                EditorGUILayout.LabelField("Build", EditorStyles.miniButton, GUILayout.Width(ICON_WIDTH));
                GUI.color = Color.white;
                widthBundleName -= ICON_WIDTH;
            }

            BundleManager.BundleInfo outputInfo = null;
            if (outputBundleInfos != null)
                outputBundleInfos.TryGetValue(bundleName, out outputInfo);

            string showName = bundleName;
            if (outputInfo != null)
                showName = string.Format("{0} ({1})", bundleName, UtilFile.FileLengthToString(outputInfo.fileSize));

            if (isSelected)
            {
                float widthBtn = 48f;
                EditorGUILayout.LabelField(showName, GUILayout.Width(widthBundleName - widthBtn));

                GUI.backgroundColor = Color.red;
                if (GUILayout.Button("delete", GUILayout.Width(widthBtn)))
                {
                    bool forceRemove = true;
                    if (includedAssetNames.Length > 0)
                    {
                        string titleRemove = string.Format("Remove bundle name '{0}'?", bundleName);
                        string messageRemove = string.Format("AssetBundle name '{0}' is still in use.\nAre you sure you want to remove '{0}'?", bundleName);
                        forceRemove = EditorUtility.DisplayDialog(titleRemove, messageRemove, "Remove", "Cancel");
                    }

                    if (forceRemove)
                    {
                        AssetDatabase.RemoveAssetBundleName(bundleName, true);
                        BundleManager.BundleInfo.Refresh();
                    }
                }
                GUI.backgroundColor = Color.white;
            }
            else
            {
                EditorGUILayout.LabelField(showName, GUILayout.Width(widthBundleName));
            }

            bool isPreDownload = GUILayout.Toggle(editingInfo.isPreDownload, "PreDownload", EditorStyles.miniButton);
            if (isPreDownload != editingInfo.isPreDownload)
            {
                editingInfo.SetPreDownload(isPreDownload);
            }

            EditorGUILayout.EndHorizontal();

            ProcessInputBundleItem(rect, bundleName);
        }

        void OnGUIIncludedAssetsList()
        {
            m_scrollBundleAssets = EditorGUILayout.BeginScrollView(m_scrollBundleAssets);

            foreach (string includedAssetName in AssetDatabase.GetAssetPathsFromAssetBundle(m_selectedBundle))
            {
                OnGUIIncludedAssetsListItem(includedAssetName);
            }

            EditorGUILayout.EndScrollView();
        }

        void ProcessInputBundleItem(Rect rect, string bundleName)
        {
            if (UtilEvent.IsClicked(rect) == false)
                return;

            if (m_selectedBundle == bundleName)
                m_selectedBundle = "";
            else
                m_selectedBundle = bundleName;
            Event.current.Use();
        }

        bool IsSelectedBundle(string bundleName)
        {
            return m_selectedBundle == bundleName;
        }

        public static void SetBundleNameToObject(Object objToBundle, string bundleName, bool isUseFolder = true)
        {
            if (objToBundle == null)
                return;
            string objPath = AssetDatabase.GetAssetPath(objToBundle);
            bool isPath = System.IO.Directory.Exists(objPath);
            //Debug.LogFormat("name = {0}, type of {1}, path = {2}, exist = {3}", objPath.name, objPath.GetType(), objPath, isPath);
            if (isPath && isUseFolder == false)
            {
                string[] arrFilePaths = System.IO.Directory.GetFiles(objPath, "*", SearchOption.AllDirectories);
                for (int i = 0; i < arrFilePaths.Length; ++i)
                {
                    Object subObject = AssetDatabase.LoadAssetAtPath<Object>(arrFilePaths[i]);
                    if (subObject == null || subObject.GetType() == typeof(MonoScript))
                        continue;
                    //Debug.LogFormat("name = {0}, type of {1}, path = {2}", subObject.name, subObject.GetType(), AssetDatabase.GetAssetPath(subObject));

                    AssetImporter importer = AssetImporter.GetAtPath(arrFilePaths[i]);
                    if (importer != null)
                    {
                        importer.assetBundleName = bundleName;
                    }
                }
            }
            else
            {
                AssetImporter importer = AssetImporter.GetAtPath(objPath);
                if (importer != null)
                {
                    importer.assetBundleName = bundleName;
                }
            }
        }

        void OnSelectionChange()
        {
            if (curWindow != null)
                curWindow.Repaint();
        }
    }

}