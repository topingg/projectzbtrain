﻿using UnityEngine;
using UnityEditor;

/* See the "http://avoex.com/avoex/default-license" for the full license governing this code. */

namespace AvoEx
{
    public class AssetManagerPostProcessor : AssetPostprocessor
    {
        static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
        {
            //foreach (var str in importedAssets)
            //{
            //    Debug.Log("Reimported Asset: " + str);
            //}
            //foreach (var str in deletedAssets)
            //{
            //    Debug.Log("Deleted Asset: " + str);
            //}

            //for (var i = 0; i < movedAssets.Length; i++)
            //{
            //    Debug.Log("Moved Asset: " + movedAssets[i] + " from: " + movedFromAssetPaths[i]);
            //}

            if (AssetManagerEditor.isOpened)
            {
                if (AssetManagerEditor.isBuildingBundles == false)
                {
                    AssetManager.AssetInfo.Refresh(); // first update AssetInfo from AssetDataBase
                    BundleManager.BundleInfo.Refresh(); // and update BundleInfo from AssetDataBase
                    AssetManagerEditor.Refresh(true); // next update saved data then compare with AssetInfo
                }
            }
        }

        public void OnPostprocessAssetbundleNameChanged(string assetPath, string previousAssetBundleName, string newAssetBundleName)
        {
            //Debug.Log("Asset " + assetPath + " has been moved from assetBundle " + previousAssetBundleName + " to assetBundle " + newAssetBundleName + ".");
            if (AssetManagerEditor.isOpened)
            {
                AssetManager.AssetInfo.Refresh(); // first update AssetInfo from AssetDataBase
                BundleManager.BundleInfo.Refresh(); // and update BundleInfo from AssetDataBase
                AssetManagerEditor.Refresh(true); // next update saved data then compare with AssetInfo
            }
        }
    }
}
