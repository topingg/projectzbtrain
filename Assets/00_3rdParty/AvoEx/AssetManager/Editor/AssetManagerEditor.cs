﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections.Generic;

/* See the "http://avoex.com/avoex/default-license" for the full license governing this code. */

namespace AvoEx
{

    public partial class AssetManagerEditor : EditorWindow
    {
        static AssetManagerEditor curWindow = null;

        BundleEditorSettings editingBundleEditorSettings = null;
        BundleSettings editingBundleSettings = null;
        AssetBundleManifest savedBundleManifest = null;
        WWW m_wwwAssetInfo = null;
        AssetBundle m_bundleAssetInfo = null;

        Dictionary<string, BundleManager.BundleInfo> outputBundleInfos = null;

        static bool s_isBuildingBundles = false;
        static System.DateTime s_buildStartTime;
        public static bool isBuildingBundles
        {
            get
            {
                return s_isBuildingBundles;
            }
            private set
            {
                if (s_isBuildingBundles == value)
                    return;
                s_isBuildingBundles = value;
                if (s_isBuildingBundles)
                {
                    s_buildStartTime = System.DateTime.Now;
                    Debug.LogFormat("<color=#ffff00ff>Started Build AssetBundles</color> : {0}", s_buildStartTime);
                }
                else
                {
                    System.DateTime finishTime = System.DateTime.Now;
                    Debug.LogFormat("<color=#00ff00ff>Finished Build AssetBundles</color> : {0}, duration = {1}", finishTime, finishTime - s_buildStartTime);
                }
            }
        }

        //
        bool m_isBundleList = true;

        [MenuItem("Window/Asset Manager")]
        [MenuItem("Tools/AvoEx/Asset Manager")]
        public static void Open()
        {
            AssetManagerEditor window = GetWindow<AssetManagerEditor>(false, "AssetManager", true);
            if (window != null)
            {
                window.position = new Rect(220, 220, 800, 400);
                window.minSize = new Vector2(515f, 200f);
            }
        }

        public static void Refresh(bool repaint = false)
        {
            if (curWindow == null)
                return;
            curWindow.OnEnable();
            if (repaint)
                curWindow.Repaint();
        }

        void RefreshEditingStatus()
        {
            savedBundleManifest = LoadBundleManifest(editingBundleEditorSettings);
            LoadBundleAssetInfo(editingBundleEditorSettings);
            outputBundleInfos = BundleManager.BundleInfo.LoadTargetBundleInfo(editingBundleEditorSettings);
        }

        void LoadBundleAssetInfo(BundleEditorSettings settings)
        {
            if (Application.isPlaying)
                return;

            ClearBundleAssetInfo();

            if (settings == null)
                return;

            string szBundleAssetInfo = System.IO.Path.Combine(settings.TargetOutputPath, AssetManager.AssetInfo.BUNDLE_NAME);
            if (System.IO.File.Exists(szBundleAssetInfo))
                m_wwwAssetInfo = new WWW("file://" + szBundleAssetInfo);
        }

        void UpdateBundleAssetInfo()
        {
            if (Application.isPlaying)
                return;

            if (m_wwwAssetInfo == null)
                return;

            if (m_wwwAssetInfo.isDone == false)
                return;

            if (string.IsNullOrEmpty(m_wwwAssetInfo.error) == false)
            {
                Debug.LogWarningFormat("no output assetinfo. 'Build bundles'. error : {0}", m_wwwAssetInfo.error);
                ClearBundleAssetInfo();
                return;
            }

            m_bundleAssetInfo = m_wwwAssetInfo.assetBundle;
            if (m_bundleAssetInfo != null)
            {
                TextAsset txtAsset = m_bundleAssetInfo.LoadAsset<TextAsset>(AssetManager.AssetInfo.FILE_NAME);
                if (txtAsset != null)
                    AssetManager.AssetInfo.LoadTargetAssetInfo(txtAsset.text);
            }

            m_wwwAssetInfo.Dispose();
            m_wwwAssetInfo = null;

            if (curWindow != null)
                curWindow.Repaint();
        }

        void ClearBundleAssetInfo()
        {
            if (m_bundleAssetInfo != null)
            {
                m_bundleAssetInfo.Unload(true);
                m_bundleAssetInfo = null;
            }
            if (m_wwwAssetInfo != null)
            {
                m_wwwAssetInfo.Dispose();
                m_wwwAssetInfo = null;
            }
        }

        public static bool isOpened
        {
            get { return (curWindow == null ? false : true); }
        }


#if USE_MANUAL_REFRESH_ASSETMANAGER
        public static bool globalProjectChanged = true;
        public static bool globalRefresh = true;

        private void OnFocus()
        {
            globalProjectChanged = true;
        }

        private void OnLostFocus()
        {
            globalProjectChanged = true;
        }
#endif

        void OnEnable()
        {
            curWindow = this;
            editingBundleEditorSettings = BundleEditorSettings.Load(true);
            editingBundleSettings = BundleSettings.Load();
            RefreshEditingStatus();
        }

        void OnDisable()
        {
            ClearBundleAssetInfo();

            if (curWindow == this)
                curWindow = null;
        }

        void Update()
        {
            UpdateBundleAssetInfo();
        }

        void OnGUI()
        {
            OnGUITopMenu();

            if (m_isBundleList)
            {
                OnGUIBundleView();
            }
            else
            {
                OnGUIAssetView();
            }
        }

        void OnGUITopMenu()
        {
            const float LABEL_WIDTH = 100f;
            const float BUTTON_WIDTH = 80f;
            const float PLATFORM_POPUP_WIDTH = 100f;

            bool isChangedBundleEditorSettings = BundleEditorSettings.IsChanged(editingBundleEditorSettings);
            bool isChangedBundleSettings = BundleSettings.IsChanged(editingBundleSettings);
            bool isChangedAssetInfos = AssetManager.AssetInfo.IsEdited();
            bool isChangedBundleInfos = BundleManager.BundleInfo.IsEdited();

            bool isSaveable = isChangedBundleEditorSettings || isChangedBundleSettings || isChangedAssetInfos || isChangedBundleInfos;
            bool isRevertable = isChangedBundleEditorSettings || isChangedBundleSettings;

            bool isBuildable = !isSaveable;

#region TOP_MENU
            EditorGUILayout.BeginHorizontal();

            //if (GUILayout.Button("Refresh", GUILayout.Width(BUTTON_WIDTH)))
            //{
            //    GUI.FocusControl(null);
            //    AssetManager.AssetInfo.Refresh();
            //    BundleManager.BundleInfo.Refresh();
            //}

            EditorGUI.BeginDisabledGroup(!isSaveable);
            if (isSaveable)
                GUI.backgroundColor = Color.red;
            if (GUILayout.Button("Save", GUILayout.Width(BUTTON_WIDTH)))
            {
                GUI.FocusControl(null);

                if (isChangedBundleSettings)
                {
                    if (editingBundleEditorSettings.LastUsedDownloadURL(editingBundleSettings.downloadBaseUrl))
                        isChangedBundleEditorSettings = true; // if new download url, save editor settings.
                }
                if (isChangedBundleEditorSettings)
                    BundleEditorSettings.Save(editingBundleEditorSettings); // must be saved BundleEditorSettings before saving BundleSettings, save BundleSettings cause reload BundleEditorSettings.
                if (isChangedBundleSettings)
                    BundleSettings.Save(editingBundleSettings);
                if (isChangedAssetInfos)
                {
                    AssetManager.AssetInfo.Save();
                    LoadBundleAssetInfo(editingBundleEditorSettings);
                }
                if (isChangedBundleInfos)
                {
                    BundleManager.BundleInfo.Save();
                    outputBundleInfos = BundleManager.BundleInfo.LoadTargetBundleInfo(editingBundleEditorSettings);
                }

            }
            GUI.backgroundColor = Color.white;
            EditorGUI.EndDisabledGroup();
            EditorGUI.BeginDisabledGroup(!isRevertable);
            if (isRevertable)
                GUI.backgroundColor = Color.green;
            if (GUILayout.Button("Revert", GUILayout.Width(BUTTON_WIDTH)))
            {
                GUI.FocusControl(null);
                if (isChangedBundleEditorSettings)
                    editingBundleEditorSettings = BundleEditorSettings.Load(true);
                if (isChangedBundleSettings)
                    editingBundleSettings = BundleSettings.Load();
                //if (isChangedAssetInfos)
                //    AssetManager.AssetInfo.Revert();
                //if (isChangedBundleInfos)
                //    BundleManager.BundleInfo.Revert();

                RefreshEditingStatus(); // reload bundle manifest/AssetInfo/BundleInfo from new output path
            }
            GUI.backgroundColor = Color.white;
            EditorGUI.EndDisabledGroup();

#if USE_MANUAL_REFRESH_ASSETMANAGER
            EditorGUI.BeginDisabledGroup(false);
            GUI.backgroundColor = Color.blue;
            if (GUILayout.Button("Refresh", GUILayout.Width(BUTTON_WIDTH)))
            {
                GUI.FocusControl(null);
                globalProjectChanged = true;
                globalRefresh = true;
            }
            GUI.backgroundColor = Color.white;
            EditorGUI.EndDisabledGroup();
#endif

            EditorGUILayout.LabelField("Use 'Build Settings' platform", GUILayout.Width(165f));
            editingBundleEditorSettings.useActiveBuildTarget = EditorGUILayout.Toggle(editingBundleEditorSettings.useActiveBuildTarget, GUILayout.Width(20f));

            BundleManager.PLATFROM newTargetPlatform = editingBundleEditorSettings.targetPlatform;
            if (editingBundleEditorSettings.useActiveBuildTarget)
                newTargetPlatform = ActiveBuildTargetToPlatform();

            EditorGUI.BeginDisabledGroup(editingBundleEditorSettings.useActiveBuildTarget);
            newTargetPlatform = (BundleManager.PLATFROM)EditorGUILayout.EnumPopup(newTargetPlatform, GUILayout.Width(PLATFORM_POPUP_WIDTH));
            EditorGUI.EndDisabledGroup();
            if (newTargetPlatform != editingBundleEditorSettings.targetPlatform)
            {
                editingBundleEditorSettings.targetPlatform = newTargetPlatform; // changing platform means changing output path.
                RefreshEditingStatus(); // reload bundle manifest/AssetInfo/BundleInfo from new output path
            }

            EditorGUI.BeginDisabledGroup(!isBuildable);
            if (isBuildable)
                GUI.backgroundColor = Color.magenta;
            if (GUILayout.Button("Build bundles"))
            {
                if (editingBundleEditorSettings.targetPlatform != ActiveBuildTargetToPlatform())
                {
                    string buildMsg = string.Format("'Build Settings' platform is [{0}].\nAre you sure you want to build bundles for [{1}]?\nThis will reimport your assets. It may take a long time.", EditorUserBuildSettings.activeBuildTarget, newTargetPlatform);
                    if (EditorUtility.DisplayDialog("Build bundles", buildMsg, "Build bundles", "Cancel"))
                    {
                        BuildBundles();
                    }
                }
                else
                {
                    BuildBundles();
                }
                return;
            }
            GUI.backgroundColor = Color.white;
            EditorGUI.EndDisabledGroup();

            EditorGUILayout.EndHorizontal();
#endregion TOP_MENU

#region OUTPUT_SETTING
            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField("Output Folder", GUILayout.Width(LABEL_WIDTH));
            //editingBundleEditorSettings.outputPath = EditorGUILayout.TextField(editingBundleEditorSettings.outputPath);
            string newOutputPath = EditorGUILayout.TextField(editingBundleEditorSettings.outputPath);
            if (GUILayout.Button("Browse", GUILayout.Width(BUTTON_WIDTH)))
            {
                string browsePath = EditorUtility.SaveFolderPanel("Output Folder", editingBundleEditorSettings.outputPath, "");
                if (string.IsNullOrEmpty(browsePath) == false)
                    newOutputPath = browsePath;
            }
            if (newOutputPath != editingBundleEditorSettings.outputPath)
            {
                editingBundleEditorSettings.outputPath = newOutputPath;
                RefreshEditingStatus(); // reload bundle manifest/AssetInfo/BundleInfo from new output path
            }

            EditorGUILayout.EndHorizontal();
#endregion OUTPUT_SETTING

#region DOWNLOAD_SETTING
            EditorGUILayout.BeginHorizontal();

            if (editingBundleEditorSettings.listUsedDownloadURLs.Count > 0)
            {
                if (GUILayout.Button("Download URL", EditorStyles.toolbarDropDown, GUILayout.Width(LABEL_WIDTH)))
                {
                    GUI.FocusControl(null);
                    GenericMenu menuURLs = new GenericMenu();
                    for (int i = 0; i < editingBundleEditorSettings.listUsedDownloadURLs.Count; ++i)
                    {
                        string szOldURL = editingBundleEditorSettings.listUsedDownloadURLs[i];
                        menuURLs.AddItem(new GUIContent(szOldURL.Replace("/", "\u2215")), false,
                            delegate { editingBundleSettings.downloadBaseUrl = szOldURL; }
                            );
                    }

                    menuURLs.DropDown(new Rect(LABEL_WIDTH, 45f, Screen.width, 16));
                }
            }
            else
            {
                EditorGUILayout.LabelField("Download URL", GUILayout.Width(LABEL_WIDTH));
            }
            editingBundleSettings.downloadBaseUrl = EditorGUILayout.TextField(editingBundleSettings.downloadBaseUrl);

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField("Number of downloading concurrently when PreDownloading");
            editingBundleSettings.preDownloadCapacity = EditorGUILayout.IntSlider(editingBundleSettings.preDownloadCapacity, 1, 5);

            EditorGUILayout.EndHorizontal();
#endregion DOWNLOAD_SETTING

#region TESTMODE_SETTING
            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField("Test mode", GUILayout.Width(LABEL_WIDTH));
            BundleEditorSettings.TEST_MODE prevTestMode = editingBundleEditorSettings.testMode;
            OnGUITopMenuTestMode(BundleEditorSettings.TEST_MODE.DownloadUrl, "load from 'Download URL'", EditorStyles.miniButtonLeft);
            OnGUITopMenuTestMode(BundleEditorSettings.TEST_MODE.OutputBundle, "load from 'Output Folder'", EditorStyles.miniButtonMid);
            OnGUITopMenuTestMode(BundleEditorSettings.TEST_MODE.AssetDatabase, "load from 'AssetDataBase'", EditorStyles.miniButtonMid);
            OnGUITopMenuTestMode(BundleEditorSettings.TEST_MODE.StreamingAssets, "load from 'StreamingAssets'", EditorStyles.miniButtonRight);
            if (prevTestMode != editingBundleEditorSettings.testMode)
            {
                bool isChangedWithStreamingAssets = false;
                if (prevTestMode == BundleEditorSettings.TEST_MODE.StreamingAssets)
                {
                    isChangedWithStreamingAssets = true;
                    editingBundleSettings.loadFromStreamingAssets = false;

                    if (EditorUtility.DisplayDialog("Remove test bundles", "load from 'StreamingAssets' test mode is finished.\nRemove bundles in 'StreamingAsset/AvoEx/AssetBundles' ?", "No", "Remove bundles in StreamingAssets") == false)
                    {
                        Directory.Delete(Application.streamingAssetsPath + "/AvoEx/AssetBundles", true);
                        AssetDatabase.Refresh();
                    }
                }
                else if (editingBundleEditorSettings.testMode == BundleEditorSettings.TEST_MODE.StreamingAssets)
                {
                    isChangedWithStreamingAssets = true;
                    editingBundleSettings.loadFromStreamingAssets = true;
                }

                if (isChangedWithStreamingAssets)
                {   // StreamingAssets mode
                    RefreshEditingStatus(); // reload bundle manifest/AssetInfo/BundleInfo from new output path
                }
            }

            EditorGUILayout.EndHorizontal();

            if (editingBundleEditorSettings.testMode == BundleEditorSettings.TEST_MODE.DownloadUrl)
            {
                EditorGUILayout.HelpBox(string.Format("You must upload bundle files to '{0}'", editingBundleSettings.downloadBaseUrl), MessageType.Info);
            }
            else if (editingBundleEditorSettings.testMode == BundleEditorSettings.TEST_MODE.OutputBundle)
            {
                EditorGUILayout.HelpBox("load from 'Output Folder' is editor only. BundleManager will load from 'Download URL' on device.", MessageType.Warning);
            }
            else if (editingBundleEditorSettings.testMode == BundleEditorSettings.TEST_MODE.AssetDatabase)
            {
                EditorGUILayout.HelpBox("load from 'AssetDataBase' is editor only. BundleManager will load from 'Download URL' on devide.", MessageType.Warning);
            }
            else if (editingBundleEditorSettings.testMode == BundleEditorSettings.TEST_MODE.StreamingAssets)
            {
                EditorGUILayout.HelpBox("delete bundle files in 'StreamingAssets/AvoEx/AssetBundles' after test finished.", MessageType.Warning);
            }
#endregion TESTMODE_SETTING

#region 5TH_LINE
            EditorGUILayout.BeginHorizontal();

            m_isBundleList = GUILayout.Toggle(m_isBundleList, "Bundle View", EditorStyles.miniButtonLeft);
            m_isBundleList = !GUILayout.Toggle(!m_isBundleList, "Asset View", EditorStyles.miniButtonRight);

            EditorGUILayout.EndHorizontal();
#endregion 5TH_LINE
        }

        void OnGUITopMenuTestMode(BundleEditorSettings.TEST_MODE eMode, string label, GUIStyle style)
        {
            bool isEnabled = editingBundleEditorSettings.testMode == eMode;

            GUI.backgroundColor = isEnabled ? Color.red : Color.white;
            EditorGUI.BeginDisabledGroup(isEnabled);

            if (GUILayout.Toggle(isEnabled, label, style))
            {
                editingBundleEditorSettings.testMode = eMode;
            }

            EditorGUI.EndDisabledGroup();
            GUI.backgroundColor = Color.white;
        }

#region BUNDLE
        void BuildBundles()
        {
            isBuildingBundles = true;

            BundleEditorSettings curBundleEditorSettings = BundleEditorSettings.SavedSettings;
            if (curBundleEditorSettings == null)
            {
                Debug.LogError("BuildBundles() - failed : BundleEditorSettings is invaild.");
                return;
            }

            string outputDirectory = curBundleEditorSettings.TargetOutputPath;
            if (Directory.Exists(outputDirectory) == false)
                Directory.CreateDirectory(outputDirectory);

            savedBundleManifest = BuildPipeline.BuildAssetBundles(outputDirectory, BuildAssetBundleOptions.None, ToBuildTarget(curBundleEditorSettings.targetPlatform));
            if (savedBundleManifest == null)
            {
                savedBundleManifest = LoadBundleManifest(curBundleEditorSettings);
                Debug.LogError("BuildBundles() - failed : Can not build bundles.");
            }
            else
            {
                BundleManager.BundleInfo.Build(curBundleEditorSettings);
            }
            AssetDatabase.Refresh();

            LoadBundleAssetInfo(curBundleEditorSettings);
            outputBundleInfos = BundleManager.BundleInfo.LoadTargetBundleInfo(editingBundleEditorSettings);

            isBuildingBundles = false;
        }

        AssetBundleManifest LoadBundleManifest(BundleEditorSettings settings)
        {
            if (settings == null)
                return null;

            AssetBundleManifest bundleManifest = null;
            string path = settings.TargetOutputPath + "/" + settings.targetPlatform.ToString();
            if (File.Exists(path))
            {
                byte[] binaryManifest = File.ReadAllBytes(path);
                if (binaryManifest != null)
                {
#if UNITY_5_0 || UNITY_5_1 || UNITY_5_2
                    AssetBundle loadedBundle = AssetBundle.CreateFromMemoryImmediate(binaryManifest);
#else
                    AssetBundle loadedBundle = AssetBundle.LoadFromMemory(binaryManifest);
#endif
                    if (loadedBundle != null)
                    {
                        bundleManifest = loadedBundle.LoadAsset<AssetBundleManifest>("AssetBundleManifest");
                        loadedBundle.Unload(false);
                    }
                }
            }

            return bundleManifest;
        }

        BuildTarget ToBuildTarget(BundleManager.PLATFROM platform)
        {
            switch (BundleEditorSettings.SavedSettings.targetPlatform)
            {
                case BundleManager.PLATFROM.Standalones:
                    if (Application.platform == RuntimePlatform.OSXEditor)
                        return BuildTarget.StandaloneOSXUniversal;
                    else
                        return BuildTarget.StandaloneWindows;
                case BundleManager.PLATFROM.WebPlayer:
                    return BuildTarget.WebPlayer;
                case BundleManager.PLATFROM.iOS:
                    return BuildTarget.iOS;
                case BundleManager.PLATFROM.Android:
                    return BuildTarget.Android;
                default:
                    return BuildTarget.StandaloneWindows;
            }
        }

        BundleManager.PLATFROM ActiveBuildTargetToPlatform()
        {
            switch (EditorUserBuildSettings.activeBuildTarget)
            {
                case BuildTarget.Android:
                    return BundleManager.PLATFROM.Android;
                case BuildTarget.iOS:
                    return BundleManager.PLATFROM.iOS;
                case BuildTarget.WebPlayer:
                    return BundleManager.PLATFROM.WebPlayer;
                case BuildTarget.StandaloneWindows:
                case BuildTarget.StandaloneWindows64:
                case BuildTarget.StandaloneOSXIntel:
                case BuildTarget.StandaloneOSXIntel64:
                case BuildTarget.StandaloneOSXUniversal:
                    return BundleManager.PLATFROM.Standalones;
                default:
                    Debug.LogErrorFormat("{0} is not supported platform!", EditorUserBuildSettings.activeBuildTarget);
                    return BundleManager.PLATFROM.NONE;
            }
        }
#endregion BUNDLE
    }

    public class AssetInfoEditor : EditorWindow
    {
        AssetManager.AssetInfo m_assetInfo = null;
        string managedName = "";
        bool nameByPath = true;
        GUILayoutOption subjectGUIOption = GUILayout.Width(100f);

        public static void Init(AssetManager.AssetInfo assetInfo)
        {
            AssetInfoEditor window = GetWindow<AssetInfoEditor>(true);
            window.m_assetInfo = assetInfo;
            window.managedName = assetInfo.managedName;
            window.nameByPath = assetInfo.nameByPath;
            window.position = new Rect(Screen.width / 2, Screen.height / 2, 400, 100);
        }

        void OnLostFocus()
        {
            Close();
        }

        void OnGUI()
        {
            if (m_assetInfo != null)
            {
                EditorGUILayout.BeginHorizontal();
                GUI.backgroundColor = Color.cyan;
                nameByPath = GUILayout.Toggle(nameByPath, "use default name", EditorStyles.miniButtonLeft);
                nameByPath = !GUILayout.Toggle(!nameByPath, "edit name", EditorStyles.miniButtonRight);
                GUI.backgroundColor = Color.white;
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUI.BeginDisabledGroup(true);
                GUI.backgroundColor = Color.black;
                EditorGUILayout.LabelField("asset name", EditorStyles.miniButton, subjectGUIOption);
                GUI.backgroundColor = Color.white;
                EditorGUI.EndDisabledGroup();
                if (nameByPath)
                {
                    EditorGUILayout.LabelField(m_assetInfo.managedName);
                }
                else
                {
                    managedName = EditorGUILayout.TextField(managedName);
                }
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUI.BeginDisabledGroup(true);
                GUI.backgroundColor = Color.black;
                EditorGUILayout.LabelField("asset path", EditorStyles.miniButton, subjectGUIOption);
                GUI.backgroundColor = Color.white;
                EditorGUI.EndDisabledGroup();
                EditorGUILayout.LabelField(m_assetInfo.assetPath);
                EditorGUILayout.EndHorizontal();
            }

            EditorGUILayout.BeginHorizontal();
            if (nameByPath == m_assetInfo.nameByPath && managedName == m_assetInfo.managedName)
            {
                EditorGUI.BeginDisabledGroup(true);
                GUILayout.Button("Save");
                EditorGUI.EndDisabledGroup();
            }
            else
            {
                GUI.backgroundColor = Color.green;
                if (GUILayout.Button("Save"))
                {
                    string error = null;
                    if (m_assetInfo.managedName != managedName)
                        error = AssetManager.AssetInfo.ValidateManagedName(managedName);
                    if (string.IsNullOrEmpty(error))
                    {
                        m_assetInfo.nameByPath = nameByPath;
                        m_assetInfo.managedName = managedName;
                        AssetManager.AssetInfo.Refresh();
                        Close();
                        AssetManagerEditor.Refresh(true);
                    }
                    else
                    {
                        EditorUtility.DisplayDialog("Error", error, "Ok");
                    }
                }
                GUI.backgroundColor = Color.white;
            }

            GUI.backgroundColor = Color.red;
            if (GUILayout.Button("Cancel"))
            {
                Close();
            }
            EditorGUILayout.EndHorizontal();
        }
    }

}