﻿using UnityEngine;
using UnityEditor;
using System.Collections;

/* See the "http://avoex.com/avoex/default-license" for the full license governing this code. */

namespace AvoEx
{

    public partial class AssetManagerEditor : EditorWindow
    {
        Vector2 m_scrollAssets;
        string m_selectedAssetGUID;

        void OnGUIAssetView()
        {
            m_scrollAssets = EditorGUILayout.BeginScrollView(m_scrollAssets);

            string[] arrayBundleNames = AssetDatabase.GetAllAssetBundleNames();
            foreach (string bundleName in arrayBundleNames)
            {
                if (bundleName == AssetManager.AssetInfo.BUNDLE_NAME)
                    continue;

                BundleManager.BundleInfo editingInfo = BundleManager.BundleInfo.GetEditingInfo(bundleName);
                string[] arrayAssetPaths = AssetDatabase.GetAssetPathsFromAssetBundle(bundleName);
                foreach (string includedAssetName in arrayAssetPaths)
                {
                    OnGUIIncludedAssetsListItem(includedAssetName, !editingInfo.isPreDownload);
                }
            }

            EditorGUILayout.EndScrollView();
        }

        void OnGUIIncludedAssetsListItem(string includedAssetName, bool bMarkAsync = false)
        {
            if (string.IsNullOrEmpty(includedAssetName))
                return;
            AssetImporter includedAsset = AssetImporter.GetAtPath(includedAssetName);
            if (includedAsset == null)
                return;

            bool isIncludedByFolder = string.IsNullOrEmpty(includedAsset.assetBundleName);

            string GUID = AssetDatabase.AssetPathToGUID(includedAssetName);

            bool isSelected = IsSelectedAsset(GUID);
            AssetManager.AssetInfo editingInfo = AssetManager.AssetInfo.GetEditingInfo(GUID, m_selectedBundle, includedAssetName);

            if (isSelected)
                GUI.backgroundColor = Color.yellow;
            else if (editingInfo.isEdited)
                GUI.backgroundColor = Color.green;
            else if (editingInfo.isJustAdded)
                GUI.backgroundColor = Color.cyan;
            else if (editingInfo.isChanged)
                GUI.backgroundColor = Color.magenta;
            else
                GUI.backgroundColor = Color.white;
            Rect rect = EditorGUILayout.BeginHorizontal(EditorStyles.textArea);
            GUI.backgroundColor = Color.white;

            if (bMarkAsync)
            {
                GUI.color = new Color(0f, 0.5f, 1f);
                EditorGUILayout.LabelField("Async First", EditorStyles.miniButton, GUILayout.Width(80f));
                GUI.color = Color.white;
            }

            if (editingInfo.isEdited)
            {
                GUI.color = Color.green;
                EditorGUILayout.LabelField("Edited", EditorStyles.miniButton, GUILayout.Width(ICON_WIDTH));
                GUI.color = Color.white;
            }
            else if (editingInfo.isJustAdded)
            {
                GUI.color = Color.cyan;
                EditorGUILayout.LabelField("New", EditorStyles.miniButton, GUILayout.Width(ICON_WIDTH));
                GUI.color = Color.white;
            }
            else if (editingInfo.isChanged)
            {
                GUI.color = Color.magenta;
                EditorGUILayout.LabelField("Build", EditorStyles.miniButton, GUILayout.Width(ICON_WIDTH));
                GUI.color = Color.white;
            }

            EditorGUILayout.LabelField(string.Concat(editingInfo.managedName, " (", includedAssetName, ")"));

            if (IsSelectedAsset(GUID))
            {
                if (isIncludedByFolder)
                {
                    GUI.backgroundColor = Color.red;
                    if (GUILayout.Button("remove folder", GUILayout.Width(120f)))
                    {
                        string folderName = includedAssetName;
                        while (folderName != "Assets" && string.IsNullOrEmpty(folderName) == false)
                        {
                            folderName = folderName.Remove(folderName.LastIndexOf("/"));
                            AssetImporter folderAsset = AssetImporter.GetAtPath(folderName);
                            if (folderAsset == null)
                                break;
                            if (folderAsset.assetBundleName != m_selectedBundle)
                                continue;
                            if (EditorUtility.DisplayDialog("Remove folder", string.Format("Are you sure you want to remove '{0}' folder from '{1}' bundle?", folderName, m_selectedBundle), "remove", "cancel"))
                            {
                                folderAsset.assetBundleName = "";
                            }
                            break;
                        }
                    }
                }
                else
                {
                    GUI.backgroundColor = Color.green;
                    if (GUILayout.Button("rename", GUILayout.Width(60f)))
                    {
                        AssetInfoEditor.Init(editingInfo);
                    }

                    GUI.backgroundColor = Color.red;
                    if (GUILayout.Button("delete", GUILayout.Width(60f)))
                    {
                        includedAsset.assetBundleName = "";
                    }
                }

                GUI.backgroundColor = Color.white;
            }
            else if (isIncludedByFolder)
            {   // asset is included by folder.
                GUI.backgroundColor = Color.cyan;
                EditorGUILayout.LabelField("included by folder", EditorStyles.miniButton, GUILayout.Width(120f));
                GUI.backgroundColor = Color.white;
            }

            EditorGUILayout.EndHorizontal();

            ProcessInputAssetItem(rect, GUID);
        }

        void ProcessInputAssetItem(Rect rect, string assetGUID)
        {
            if (UtilEvent.IsClicked(rect) == false)
                return;

            if (m_selectedAssetGUID == assetGUID)
                m_selectedAssetGUID = "";
            else
                m_selectedAssetGUID = assetGUID;
            Event.current.Use();
        }

        bool IsSelectedAsset(string assetGUID)
        {
            return m_selectedAssetGUID == assetGUID;
        }
    }

}