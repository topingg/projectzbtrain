using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

/* See the "http://avoex.com/avoex/default-license" for the full license governing this code. */

namespace AvoEx
{

    public partial class BundleSettings
    {
        public string downloadBaseUrl;
        public int preDownloadCapacity = 0;
        public bool loadFromStreamingAssets = false;

        public static BundleSettings Load()
        {
            TextAsset textSettings = Resources.Load<TextAsset>(typeof(BundleSettings).ToString());
            if (textSettings == null)
                return new BundleSettings();
            return LitJson.JsonMapper.ToObject<BundleSettings>(textSettings.text);
        }
    }

    public partial class BundleManager : MonoSingleton<BundleManager>
    {
        public enum STATUS
        {
            NONE,
            INITIALIZE_STARTED,
            LOADING_MANIFEST,
            LOADED_BUNDLEINFO,
            PREDOWNLOADING,
            INITIALIZED,
            ERROR,
        }

        public enum PLATFROM
        {
            Standalones,
            WebPlayer,
            Android,
            iOS,
            NONE,
        }

        #region MEMBERS
        // on awake
        BundleSettings m_settings = null;
        PLATFROM m_currentPlatform;
        static string s_downloadPlatformUrl;
        static string s_customDownloadUrl;
        static bool s_autoPredownload = true;

        // on start
        AssetBundleManifest m_bundleManifest = null;
        static STATUS s_currentStatus = STATUS.NONE;
        static string s_error;
        bool m_bStartPredownload = false;

        // preloading
        List<string> m_listPreloadBundles = null;
        List<string> m_listDependCheckedBundles = new List<string>();
        public int curPreloadCount { get; private set; }
        public int maxPreloadCount { get; private set; }
        public long totalPreloadSize { get; private set; }
        public long totalDownloadSize { get; private set; }
        public List<AsyncBundleRequest> listPreloadRequests { get; private set; }
        #endregion MEMBERS

        #region PROPERTIES
        public static STATUS currentStatus
        {
            get
            {
#if UNITY_EDITOR
                if (BundleEditorSettings.SavedSettings.testMode == BundleEditorSettings.TEST_MODE.AssetDatabase)
                    return STATUS.INITIALIZED;
#endif
                return s_currentStatus;
            }
            private set
            {
#if DEBUG_BUNDLEMANAGER
                Debug.LogFormat("BundleManager STATUS changed <color=#00ff00ff>{0}</color> from {1}", value, s_currentStatus);
#endif
                s_currentStatus = value;
            }
        }
        
        public static string error
        {
            get { return s_error; }
            private set
            {
                s_error = value;
#if DEBUG_BUNDLEMANAGER
                if (string.IsNullOrEmpty(s_error) == false)
                    Debug.LogErrorFormat("BundleManager.{0}", s_error);
#endif
            }
        }

        public static bool isInitialized
        {
            get { return (currentStatus == STATUS.INITIALIZED); }
        }
        public static bool isReadyPredownload
        {
            get
            {
#if UNITY_EDITOR
                if (BundleEditorSettings.SavedSettings.testMode == BundleEditorSettings.TEST_MODE.AssetDatabase)
                    return true;
#endif
                return (currentStatus == STATUS.LOADED_BUNDLEINFO);
            }
        }

        public static bool isError
        {
            get { return (currentStatus == STATUS.ERROR); }
        }

        public static PLATFROM RuntimeBundlePlatform
        {
            get
            {
#if UNITY_EDITOR
                return BundleEditorSettings.SavedSettings.targetPlatform;
#else
                switch (Application.platform)
                {
                    case RuntimePlatform.Android:
                        return PLATFROM.Android;
                    case RuntimePlatform.IPhonePlayer:
                        return PLATFROM.iOS;
                    case RuntimePlatform.WindowsWebPlayer:
                    case RuntimePlatform.OSXWebPlayer:
                        return PLATFROM.WebPlayer;
                    case RuntimePlatform.WindowsPlayer:
                    case RuntimePlatform.OSXPlayer:
                        return PLATFROM.Standalones;
                    default:
                        Debug.LogError("BundleManager.GetRuntimePlatform - Not supported platform! = " + Application.platform.ToString());
                        return PLATFROM.NONE;
                }
#endif
            }
        }

        public static string customDownloadURL
        {
            get
            {
                return s_customDownloadUrl;
            }
            set
            {
                s_customDownloadUrl = value;
#if UNITY_EDITOR
                Debug.LogFormat("BundleManager.customDownloadURL = '{0}'", s_customDownloadUrl);
#endif
            }
        }
        #endregion PROPERTIES

        protected override void OnAwake()
        {
            m_settings = BundleSettings.Load();
            m_currentPlatform = RuntimeBundlePlatform;

#if UNITY_EDITOR
            if (BundleEditorSettings.SavedSettings.testMode == BundleEditorSettings.TEST_MODE.OutputBundle)
            {
                s_downloadPlatformUrl = "file://" + System.IO.Path.Combine(BundleEditorSettings.SavedSettings.outputPath, m_currentPlatform.ToString());
                Debug.LogWarningFormat("BundleManager.OnAwake() : load from <color=#ff0000ff>output folder</color>('{0}')", s_downloadPlatformUrl);
            }
            else if (BundleEditorSettings.SavedSettings.testMode == BundleEditorSettings.TEST_MODE.AssetDatabase)
            {
                s_downloadPlatformUrl = "file://" + System.IO.Path.Combine(Application.dataPath, EDITING_INFOS_PATH.Substring("Assets/".Length));
                Debug.LogWarningFormat("BundleManager.OnAwake() : load from <color=#ff0000ff>AssetDatabase</color>('{0}')", s_downloadPlatformUrl);
            }
            else if (BundleEditorSettings.SavedSettings.testMode == BundleEditorSettings.TEST_MODE.StreamingAssets)
            {
                s_downloadPlatformUrl = string.Concat("file://", Application.streamingAssetsPath, "/AvoEx/AssetBundles/", m_currentPlatform.ToString());
                Debug.LogWarningFormat("BundleManager.OnAwake() : load from <color=#ff0000ff>StreamingAssets</color>('{0}')", s_downloadPlatformUrl);
            }
            else
#endif
            {
                if (m_settings.loadFromStreamingAssets)
                {
                    s_downloadPlatformUrl = string.Concat(Application.streamingAssetsPath, "/AvoEx/AssetBundles/", m_currentPlatform.ToString());
#if DEBUG_BUNDLEMANAGER
                    Debug.LogWarningFormat("BundleManager.OnAwake() : load from <color=#ff0000ff>StreamingAssets</color>('{0}')", s_downloadPlatformUrl);
#endif
                }
                else if (string.IsNullOrEmpty(s_customDownloadUrl))
                {
                    s_downloadPlatformUrl = string.Concat(m_settings.downloadBaseUrl, "/", m_currentPlatform.ToString());
#if DEBUG_BUNDLEMANAGER
                    Debug.LogWarningFormat("BundleManager.OnAwake() : load from <color=#ff0000ff>download URL</color>('{0}')", s_downloadPlatformUrl);
#endif
                }
                else
                {
                    s_downloadPlatformUrl = s_customDownloadUrl;
#if UNITY_EDITOR || DEBUG_BUNDLEMANAGER
                    Debug.LogWarningFormat("BundleManager.OnAwake() : load from <color=#ff0000ff>customDownloadURL</color>('{0}')", s_downloadPlatformUrl);
#endif
                }
            }
        }

        // Use this for initialization
        void Start()
        {
            Initialize();
        }

        // Update is called once per frame
        void Update()
        {
            ManagedBundle.UpdateBundles();
            if (isInitialized || isError)
                AsyncBundleRequest.UpdateRequests(); // must called after initialized
        }

        //
        void Initialize()
        {
            if (currentStatus != STATUS.NONE)
                return; // Initialize() must called after Clear()
            currentStatus = STATUS.INITIALIZE_STARTED;
            m_bStartPredownload = s_autoPredownload;
            StartCoroutine(CoInitialize());
        }

        //
        void Clear()
        {
            StopAllCoroutines();

            currentStatus = STATUS.NONE;
            m_bundleManifest = null;
            if (m_listPreloadBundles != null)
            {
                m_listPreloadBundles.Clear();
                m_listPreloadBundles = null;
            }
            m_listDependCheckedBundles.Clear();
            error = string.Empty;
            m_bStartPredownload = false;

            AsyncBundleRequest.ClearRequests();
            ManagedBundle.ClearBundles();
        }

        void SetError(string errorMsg)
        {
            error = errorMsg;
            currentStatus = STATUS.ERROR;
        }

        IEnumerator CoInitialize()
        {
            // Wait for the Caching system to be ready
            while (!Caching.ready)
                yield return null;

            currentStatus = STATUS.LOADING_MANIFEST;
            yield return StartCoroutine(CoLoadBundleManifest());
            if (currentStatus == STATUS.ERROR)
                yield break;

            yield return StartCoroutine(CoLoadBundleInfo());
            if (currentStatus == STATUS.ERROR)
                yield break;

            if (m_listPreloadBundles != null && m_listPreloadBundles.Count > 0)
            {
                currentStatus = STATUS.LOADED_BUNDLEINFO;
                while (m_bStartPredownload == false)
                    yield return null;

                currentStatus = STATUS.PREDOWNLOADING;
                yield return StartCoroutine(CoPreLoadBundle());
                if (currentStatus == STATUS.ERROR)
                    yield break;
            }

            currentStatus = STATUS.INITIALIZED;
        }

        IEnumerator CoLoadBundleManifest()
        {
            string manifestUrl = GetDownloadUrl(m_currentPlatform.ToString());

            // Start the download
#if USE_UNITYWEBREQUEST
            using (UnityWebRequest www = UnityWebRequest.GetAssetBundle(manifestUrl))
            {
                yield return www.Send();
            
#else
            using (WWW www = new WWW(manifestUrl))
            {
                yield return www;
#endif
                if (string.IsNullOrEmpty(www.error) == false)
                {   // If downloading fails.
                    SetError(www.error);
                }
                else
                {
#if USE_UNITYWEBREQUEST
                    AssetBundle assetBundle = DownloadHandlerAssetBundle.GetContent(www);
                    if (assetBundle == null)
#else
                    // If downloading succeeds.                    
                    if (www.assetBundle == null)
#endif
                    {
                        SetError("failed download manifest - " + manifestUrl);
                    }
                    else
                    {
#if USE_UNITYWEBREQUEST
                        AssetBundleRequest bundleRequest = assetBundle.LoadAssetAsync<AssetBundleManifest>("AssetBundleManifest");
#else
                        AssetBundleRequest bundleRequest = www.assetBundle.LoadAssetAsync<AssetBundleManifest>("AssetBundleManifest");
#endif
                        yield return bundleRequest;

                        m_bundleManifest = bundleRequest.asset as AssetBundleManifest;

                        if (m_bundleManifest == null)
                        {
                            SetError("failed load manifest - " + manifestUrl);
                        }
                        else
                        {
                            if (ManagedBundle.Init(m_bundleManifest) == false)
                                SetError("failed init manifest - " + manifestUrl);
                        }

#if USE_UNITYWEBREQUEST
                        assetBundle.Unload(false);
#else
                        www.assetBundle.Unload(false);
#endif
                    }
                }
            }
        }

        IEnumerator CoLoadBundleInfo()
        {
            // load BundleInfo
            Dictionary<string, BundleInfo> dictBundleInfo = null;

            string bundleInfoUrl = GetDownloadUrl(BundleInfo.FILE_NAME);
            // Start the download
            using (WWW www = new WWW(bundleInfoUrl))
            {
                yield return www;

                if (string.IsNullOrEmpty(www.error) == false)
                {   // If downloading fails.
                    SetError(www.error);
                    yield break;
                }
                else
                {   // If downloading succeeds.
                    if (string.IsNullOrEmpty(www.text) == false)
                        dictBundleInfo = LitJson.JsonMapper.ToObject<Dictionary<string, BundleInfo>>(www.text);
                }
            }

            if (dictBundleInfo == null)
            {
                SetError("failed download bundleInfo - " + bundleInfoUrl);
                yield break;
            }

            // calc total download size.
            totalDownloadSize = 0;
            m_listPreloadBundles = new List<string>(dictBundleInfo.Count);
            m_listDependCheckedBundles.Clear();
            var itorBundleInfos = dictBundleInfo.Values.GetEnumerator();
            while (itorBundleInfos.MoveNext())
            {
                BundleInfo bundleInfo = itorBundleInfos.Current;
                if (bundleInfo == null)
                    continue;

                ManagedBundle bundle = ManagedBundle.GetManagedBundle(bundleInfo.bundleName);
                if (bundle == null)
                    continue;

                if (bundle.IsVersionCached == false)
                {
                    totalDownloadSize += bundleInfo.fileSize;
                }

                if (bundleInfo.isPreDownload)
                {
                    RecursiveAddPreloadBundle(bundleInfo.bundleName);
                }
            }

            // calc predownload size.
            totalPreloadSize = 0;
            for (int i = 0; i < m_listPreloadBundles.Count;)
            {
                string bundleName = m_listPreloadBundles[i];
                ManagedBundle bundle = ManagedBundle.GetManagedBundle(bundleName);
                if (bundle == null || bundle.IsVersionCached)
                {
                    m_listPreloadBundles.RemoveAt(i);
                    continue;
                }

                BundleInfo bundleInfo = null;
                dictBundleInfo.TryGetValue(bundleName, out bundleInfo);
                if (bundleInfo != null)
                {
                    totalPreloadSize += bundleInfo.fileSize;
                }
                else
                {
                    Debug.LogErrorFormat("Can't find '{0}' bundle. wrong bundle info.", bundleName);
                }
                ++i;
            }

#if DEBUG_BUNDLEMANAGER
            Debug.LogFormat("BundleManager.CoLoadBundleInfo() is finished. predownload size = <color=#ffff00ff>{0}</color> bytes ({1})", totalPreloadSize, UtilFile.FileLengthToString(totalPreloadSize));
#endif
        }

        void RecursiveAddPreloadBundle(string bundleName)
        {
            if (m_listDependCheckedBundles.Contains(bundleName))
                return;
            m_listDependCheckedBundles.Add(bundleName);

            string[] dependBundles = m_bundleManifest.GetDirectDependencies(bundleName);
            for (int i = 0; i < dependBundles.Length; ++i)
            {
                RecursiveAddPreloadBundle(dependBundles[i]); // depend bundle first.
            }

            if (m_listPreloadBundles.Contains(bundleName) == false)
                m_listPreloadBundles.Add(bundleName);
        }

        IEnumerator CoPreLoadBundle()
        {
            // ready predownload
            curPreloadCount = 0;
            maxPreloadCount = m_listPreloadBundles.Count;

            // Download
            listPreloadRequests = new List<AsyncBundleRequest>(m_settings.preDownloadCapacity);

            var itorBundles = m_listPreloadBundles.GetEnumerator();
            while (itorBundles.MoveNext())
            {
                listPreloadRequests.Add(LoadBundleAsync(itorBundles.Current));
                ++curPreloadCount;

                while (listPreloadRequests.Count >= m_settings.preDownloadCapacity)
                {
                    UpdatePreloadRequests(); // before initialized, manually called for predownloading.
                    // this makes update AsyncBundleRequest before initialiezed BundleManager. but It's okay. because manifest is already loaded when predownloading.

                    if (currentStatus == STATUS.ERROR)
                        yield break;
                    else
                        yield return null;
                }
            }

            while (listPreloadRequests.Count > 0)
            {
                UpdatePreloadRequests(); // before initialized, manually called for predownloading.
                // this makes update AsyncBundleRequest before initialiezed BundleManager. but It's okay. because manifest is already loaded when predownloading.

                if (currentStatus == STATUS.ERROR)
                    yield break;
                else
                    yield return null;
            }

            listPreloadRequests = null;

            // Clear
            itorBundles = m_listPreloadBundles.GetEnumerator();
            while (itorBundles.MoveNext())
            {
                UnloadBundle(itorBundles.Current); // decrease reference counter.
            }
        }

        void UpdatePreloadRequests()
        {
            if (listPreloadRequests == null)
                return;

            for (int i = 0; i < listPreloadRequests.Count;)
            {
                listPreloadRequests[i].Update();

                if (listPreloadRequests[i].isDone)
                {
                    if (string.IsNullOrEmpty(listPreloadRequests[i].error) == false)
                    {
                        SetError(listPreloadRequests[i].error);
                        return;
                    }
                    listPreloadRequests.RemoveAt(i);
                }
                else
                {
                    ++i;
                }
            }
        }

        public void StartPredownload()
        {
            m_bStartPredownload = true;
        }

        //
        public void  DownloadAllBundles()
        {
            StartCoroutine(ManagedBundle.CoDownloadAllAsync(m_settings.preDownloadCapacity));
        }

        #region STATIC_INTERFACES
        public static void InitManager(bool autoPredownload = true)
        {
            s_autoPredownload = autoPredownload;
            Instance.Initialize();
        }

        public static void ClearManager()
        {
            if (IsInstantiated)
                Instance.Clear();
        }

        public static AsyncBundleRequest LoadBundleAsync(string bundleName)
        {
            return new AsyncBundleRequest(bundleName);
        }

        public static AssetBundle LoadBundle(string bundleName)
        {
            ManagedBundle loadedBundle = ManagedBundle.LoadBundle(bundleName);
            if (loadedBundle != null)
                return loadedBundle.assetBundle;
            return null;
        }

        public static void UnloadBundle(string bundleName, int counter = 1)
        {
            ManagedBundle.UnloadBundle(bundleName, counter);
        }

        static string CombineUrl(string path1, string path2)
        {
#if UNITY_EDITOR
            if (BundleEditorSettings.SavedSettings.testMode == BundleEditorSettings.TEST_MODE.OutputBundle)
            {
                return System.IO.Path.Combine(path1, path2);
            }
            else
#endif
            {
#if UNITY_IPHONE
				if (BundleManager.Instance.m_settings.loadFromStreamingAssets)
					return string.Concat("file://", path1, "/", path2);

					return string.Concat(path1, "/", path2);
#else
                return string.Concat(path1, "/", path2);
#endif
            }
        }

        public static string GetDownloadUrl(string fileName)
        {
			string url = CombineUrl(s_downloadPlatformUrl, fileName);
#if DEBUG_BUNDLEMANAGER
			Debug.LogFormat("BundleManager.GetDownloadUrl( {0} ) = {1}", fileName, url);
#endif
			return url;
        }
#endregion STATIC_INTERFACES

    }

}