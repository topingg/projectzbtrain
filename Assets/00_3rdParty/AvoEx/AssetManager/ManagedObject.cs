﻿using UnityEngine;
using ObjectPool = AvoEx.ObjectPoolManager.ObjectPool;

/* See the "http://avoex.com/avoex/default-license" for the full license governing this code. */

namespace AvoEx
{

    public class ManagedObject : MonoBehaviour
    {
        private GameObject m_gameObject;
        private Transform m_transform;
        private string m_szPoolName;
        private bool m_bInPool = false;

        public GameObject cachedObject { get { return m_gameObject; } }
        public Transform cachedTransform { get { return m_transform; } }

        public string poolName
        {
            get { return m_szPoolName; }
        }
        public bool isManaged
        {
            get { return m_bInPool; }
        }
        public bool isActivated = false;

        #region OBJECT_POOL_MANAGER
        /// <summary>
        /// Do not call. only called by ObjectPoolManager
        /// </summary>
        /// <param name="trPool">Transform of included ObjectPool</param>
        /// <param name="szPoolName">name of included ObjectPool</param>
        public void InitInstance(Transform trPool, string szPoolName)
        {
            cachedTransform.SetParent(trPool, false);
            m_szPoolName = szPoolName;
            m_bInPool = true;
        }

        /// <summary>
        /// Do not call. only called by ObjectPoolManager
        /// </summary>
        /// <param name="trPool"></param>
        public virtual void RestoreInstance(Transform trPool, Vector3 orgPosition, Quaternion orgRotation, Vector3 orgScale, int orgLayer)
        {
            cachedTransform.localPosition = orgPosition;
            cachedTransform.localRotation = orgRotation;
            cachedTransform.localScale = orgScale;
            cachedTransform.SetParent(trPool, false);
            cachedObject.SetActive(false);
            SetLayer(orgLayer);
            m_bInPool = true;
        }

        /// <summary>
        /// Do not call. only called by ObjectPoolManager
        /// </summary>
        public void DelInstance()
        {
            m_szPoolName = string.Empty;
        }
        #endregion OBJECT_POOL_MANAGER

        #region MonoBehaviour
        // 어짜피 부모가 활성/비활성 중이면, 자식의 부모 못바꿈.
        // Cannot change GameObject hierarchy while activating or deactivating the parent.
        //void OnDisable()
        //{
        //    if (isActivated)
        //        ObjectPoolManager.PushInstance(this);
        //}

        protected virtual void Awake()
        {
            m_gameObject = gameObject;
            m_transform = transform;
        }

        protected virtual void OnDestroy()
        {
            if (m_bInPool == false)
                return;

            ObjectPool pool = ObjectPool.GetPool(m_szPoolName);
            if (pool == null)
                return;

            Debug.LogWarningFormat("Do not Destroy ManagedObject - '{0}'. use ObjectPoolManager.PushInstance().", name);
            pool.DelInstance(this);
        }
        #endregion MonoBehaviour

        public void SetLayer(int layer)
        {
            if (cachedObject.layer == layer)
                return;

            cachedObject.layer = layer;
            Transform[] children = cachedObject.GetComponentsInChildren<Transform>();
            for (int i = 0; i < children.Length; ++i)
            {
                children[i].gameObject.layer = layer;
            }
        }
    }

}