﻿using UnityEngine;
using System.Collections;

/* See the "http://avoex.com/avoex/default-license" for the full license governing this code. */

namespace AvoEx
{

    public class ManagedParticle : ManagedObject
    {
        public ParticleSystem m_particleSystem = null;
        public bool m_bUnscaledTime = false;
        private float m_fAppliedTimeScale = 1f;

        public override void RestoreInstance(Transform trPool, Vector3 orgPosition, Quaternion orgRotation, Vector3 orgScale, int orgLayer)
        {
            base.RestoreInstance(trPool, orgPosition, orgRotation, orgScale, orgLayer);

            if (m_particleSystem != null)
            {
                m_particleSystem.Stop();
                m_particleSystem.Clear();
            }
        }

        void CheckAlive()
        {
            if (m_particleSystem == null)
                return;

            if (isManaged)
            {
                if (m_particleSystem.IsAlive() == false)
                {
                    ObjectPoolManager.PushInstance(this);
                    return;
                }
            }

            if (m_bUnscaledTime)
            {
                float fCurTimeScale = Time.timeScale;
                if (fCurTimeScale > 0f)
                {
                    if (fCurTimeScale != m_fAppliedTimeScale)
                    {
                        m_fAppliedTimeScale = fCurTimeScale;
#if UNITY_5_5_OR_NEWER
                        var main = m_particleSystem.main;
                        main.simulationSpeed = 1f / fCurTimeScale;
#else
                        m_particleSystem.playbackSpeed = 1f / fCurTimeScale;
#endif

                        if (m_particleSystem.isPlaying == false)
                            m_particleSystem.Play();
                    }
                }
                else // 시간이 아주 멈춘 경우.
                {
                    if (m_fAppliedTimeScale != 0f)
                    {
#if UNITY_5_5_OR_NEWER
                        var main = m_particleSystem.main;
                        main.simulationSpeed = 1f;
#else
                        m_particleSystem.playbackSpeed = 1f;
#endif
                        m_fAppliedTimeScale = 0f;
                    }
                    m_particleSystem.Simulate(Time.unscaledDeltaTime, true, false);
                }
            }
        }

        public void SetUnscaledTime(bool bUnscaledTime)
        {
            if (m_bUnscaledTime == bUnscaledTime)
                return;

            m_bUnscaledTime = bUnscaledTime;

            if (m_bUnscaledTime)
            {

            }
            else
            {
                m_particleSystem.Play();
            }
        }

        protected override void Awake()
        {
            base.Awake();

            if (m_particleSystem == null)
                m_particleSystem = GetComponentInChildren<ParticleSystem>();
        }

        void OnEnable()
        {
            if (m_particleSystem != null)
                m_particleSystem.Play();
        }

        // 어짜피 부모가 활성/비활성 중이면, 자식의 부모 못바꿈.
        // Cannot change GameObject hierarchy while activating or deactivating the parent.
        //void OnDisable()
        //{
        //    if (isActivated)
        //        ObjectPoolManager.PushInstance(this);
        //}

        //// Use this for initialization
        //void Start()
        //{

        //}

        // Update is called once per frame
        void Update()
        {
            CheckAlive();
        }

        //void FixedUpdate()
        //{

        //}

    }

}