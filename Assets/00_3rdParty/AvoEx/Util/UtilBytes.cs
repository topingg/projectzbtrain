﻿/* See the "http://avoex.com/avoex/default-license/" for the full license governing this code. */

namespace AvoEx
{
    public static class UtilBytes
    {
        public static int MakeLong(short lowPart, short highPart)
        {
            return (int)((ushort)lowPart | (uint)(highPart << 16));
        }

        public static short LoWord(int dword)
        {
            return (short)dword;
        }

        public static short HiWord(int dword)
        {
            return (short)(dword >> 16);
        }

        public static short MakeWord(byte lowPart, byte highPart)
        {
            return (short)((byte)lowPart | (((short)highPart) << 8));
        }

        public static byte LoByte(short word)
        {
            return (byte)word;
        }

        public static byte HiByte(short word)
        {
            return (byte)(word >> 8);
        }
    }
}