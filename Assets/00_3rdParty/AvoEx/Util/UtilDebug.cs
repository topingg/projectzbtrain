﻿#if UNITY_EDITOR
#define ENABLE_LOG
#endif

#if ENABLE_LOG
using System;
using LitJson;
#else
using System;
using UnityEngine;
#endif

/* See the "http://avoex.com/avoex/default-license" for the full license governing this code. */

namespace AvoEx
{

    public static class UtilDebug
    {
#if ENABLE_LOG
        public static void LogObject(object logObject)
        {
            try
            {
                JsonWriter writer = new JsonWriter();
                writer.PrettyPrint = true;

                JsonMapper.ToJson(logObject, writer);

                UnityEngine.Debug.LogFormat("{0} = {1}", logObject, writer);
            }
            catch (Exception e)
            {
                UnityEngine.Debug.LogErrorFormat("UtilDebug.LogObject({0}) - Failed\n{1}", logObject, e);
            }
        }
#else
        public static void LogObject(object logObject)
        {
        }
#endif
    }

}

#if ENABLE_LOG

#else

public static class Debug
{
    public static bool developerConsoleVisible { get { return false; } set { } }
    public static bool isDebugBuild { get { return false; } }
    public static void Assert(bool condition) { }
    public static void Assert(bool condition, string message) { }
    public static void Assert(bool condition, string format, params object[] args) { }
    public static void Break() { }
    public static void ClearDeveloperConsole() { }
    public static void DebugBreak() { }
    public static void DrawLine(Vector3 start, Vector3 end) { }
    public static void DrawLine(Vector3 start, Vector3 end, Color color) { }
    public static void DrawLine(Vector3 start, Vector3 end, Color color, float duration) { }
    public static void DrawLine(Vector3 start, Vector3 end, Color color, float duration, bool depthTest) { }
    public static void DrawRay(Vector3 start, Vector3 dir) { }
    public static void DrawRay(Vector3 start, Vector3 dir, Color color) { }
    public static void DrawRay(Vector3 start, Vector3 dir, Color color, float duration) { }
    public static void DrawRay(Vector3 start, Vector3 dir, Color color, float duration, bool depthTest) { }

    public static void Log(object message) { }
    public static void Log(object message, UnityEngine.Object context) { }
    public static void LogError(object message) { }
    public static void LogError(object message, UnityEngine.Object context) { }
    public static void LogErrorFormat(string format, params object[] args) { }
    public static void LogErrorFormat(UnityEngine.Object context, string format, params object[] args) { }
    public static void LogException(Exception exception) { }
    public static void LogException(Exception exception, UnityEngine.Object context) { }
    public static void LogFormat(string format, params object[] args) { }
    public static void LogFormat(UnityEngine.Object context, string format, params object[] args) { }
    public static void LogWarning(object message) { }
    public static void LogWarning(object message, UnityEngine.Object context) { }
    public static void LogWarningFormat(string format, params object[] args) { }
    public static void LogWarningFormat(UnityEngine.Object context, string format, params object[] args) { }
}

#endif
