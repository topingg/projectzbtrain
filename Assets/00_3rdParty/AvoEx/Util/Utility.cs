﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

/* See the "http://avoex.com/avoex/default-license" for the full license governing this code. */

namespace AvoEx
{

    public static class Utility
    {
        public static bool ContainsEquals<T>(this IEnumerable<T> list1, IEnumerable<T> list2)
        {
            return (list1.Count() == list2.Count()) && !list1.Except(list2).Any() && !list2.Except(list1).Any(); 
        }
    }

}