﻿using UnityEngine;

/* See the "http://avoex.com/avoex/default-license" for the full license governing this code. */

namespace AvoEx
{
    public abstract class MonoSingleton<T> : MonoBehaviour where T : MonoSingleton<T>
    {
        static T singleInstance = null;
        static bool onQuit = false;

        bool m_isAwake = false;

        public static T Instance
        {
            get
            {
                if (singleInstance == null)
                {
                    if (onQuit)
                    {
#if DEBUG_MONOSINGLETON
                        Debug.LogErrorFormat("MonoSingleton<{0}> - can't not created on application quit", typeof(T));
#endif
                        return null;
                    }

                    singleInstance = FindObjectOfType<T>();

                    if (singleInstance == null)
                    {
#if DEBUG_MONOSINGLETON
                        Debug.LogErrorFormat("MonoSingleton<{0}>.Instance - new GameObject()", typeof(T));
#endif
                        singleInstance = new GameObject(typeof(T).ToString(), typeof(T)).GetComponent<T>();
                    }
#if DEBUG_MONOSINGLETON
                    else
                    {
                        Debug.LogErrorFormat("MonoSingleton<{0}>.Instance - FindObjectOfType<{0}>()", typeof(T));
                    }
#endif

                    // FindObjectOfType<T>(); 에서 찾을 경우, Awake() 가 아직 호출 되지 않은 상태일 경우가 발생 (다른 Awake() 에서 Instance 로 접근 하는 경우)
                    // Instance를 넘겨 주기 전에 Awake() 처리를 완료 하고 넘겨 주기 위해서, Awake() 수동 호출함, 대신 두번 호출 되지 않게 처리했음.
                    singleInstance.Awake();
                }
                return singleInstance;
            }
        }

        public static bool IsInstantiated
        {
            get { return singleInstance == null ? false : true; }
        }

        private void Awake()
        {
            if (m_isAwake)
                return; // Awake() 되기 전에 Instance 를 사용하는 경우를 대비해서, 수동 호출 하게 되는 경우, 두번 호출 되지 않게 하기 위해서.
            m_isAwake = true;

            if (singleInstance == this)
            {
#if DEBUG_MONOSINGLETON
                Debug.LogFormat("MonoSingleton<{0}>.Awake() - first awake!!!", typeof(T));
#endif
                DontDestroyOnLoad(gameObject);
                singleInstance.OnAwake();
            }
            else if (singleInstance == null)
            {
#if DEBUG_MONOSINGLETON
                Debug.LogFormat("MonoSingleton<{0}>.Awake() - first instance!!!", typeof(T));
#endif
                DontDestroyOnLoad(gameObject);
                singleInstance = this as T;
                singleInstance.OnAwake();
            }
            else
            {
#if DEBUG_MONOSINGLETON
                Debug.LogErrorFormat("MonoSingleton<{0}>.Awake() - instantiated one more instance!!!", typeof(T));
#endif
                Destroy(gameObject);
            }
        }

        private void OnDestroy()
        {
            if (singleInstance == this)
            {
#if DEBUG_MONOSINGLETON
                Debug.LogFormat("MonoSingleton<{0}>.OnDestroy() - singleton is destroying..", typeof(T));
#endif
                onQuit = true; // 파괴되면 다시 생성 하지 않는다. 다시 생성이 필요할 경우, onQuit를 false로 초기화 하고 생성하는 로직을 추가해야함.
                singleInstance.OnQuit();
                singleInstance = null;
            }
        }

        /// <summary>
        /// use this like Awake()
        /// </summary>
        protected virtual void OnAwake() { }

        /// <summary>
        /// use this like OnDestroy()
        /// </summary>
        protected virtual void OnQuit() { }

    }
}