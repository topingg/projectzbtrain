﻿using UnityEngine;
using System.IO;
using LitJson;

/* See the "http://avoex.com/avoex/default-license" for the full license governing this code. */

namespace AvoEx
{
    public static class UtilFile
    {
        static readonly float KILO_BYTE_SIZE = Mathf.Pow(2f, 10f);
        static readonly float MEGA_BYTE_SIZE = Mathf.Pow(KILO_BYTE_SIZE, 2f);
        static readonly double GIGA_BYTE_SIZE = Mathf.Pow(KILO_BYTE_SIZE, 3f);
        static readonly double TERA_BYTE_SIZE = Mathf.Pow(KILO_BYTE_SIZE, 4f);

        public static string FileLengthToString(long fileLength)
        {
            string szFileLength;
            if (fileLength < KILO_BYTE_SIZE)
                szFileLength = fileLength.ToString() + " Bytes";
            else if (fileLength < MEGA_BYTE_SIZE)
                szFileLength = string.Format("{0:0.##}", fileLength / KILO_BYTE_SIZE) + " KB";
            else if (fileLength < GIGA_BYTE_SIZE)
                szFileLength = string.Format("{0:0.##}", fileLength / MEGA_BYTE_SIZE) + " MB";
            else if (fileLength < TERA_BYTE_SIZE)
                szFileLength = string.Format("{0:0.##}", fileLength / GIGA_BYTE_SIZE) + " GB";
            else
                szFileLength = string.Format("{0:0.##}", fileLength / TERA_BYTE_SIZE) + " TB";

            return szFileLength;
        }

        #region LITJSON_IO
        public static T LoadJsonToObject<T>(string loadPath)
        {
            try
            {
                using (TextReader txtReader = new StreamReader(loadPath))
                {
                    T data = JsonMapper.ToObject<T>(txtReader);
                    if (data == null)
                    {
                        Debug.LogError("UtilFile.LoadObject( " + loadPath + " ) - failed read json data");
                        data = default(T);
                    }

                    txtReader.Close();
                    return data;
                }
            }
            catch
            {
                return default(T);
            }
        }

        public static void SaveObjectToJson<T>(T saveObject, string savePath, bool prettyPrint = true)
        {
            using (TextWriter txtWriter = new StreamWriter(savePath))
            {
                JsonWriter writer = new JsonWriter(txtWriter);
                writer.PrettyPrint = prettyPrint;
                JsonMapper.ToJson(saveObject, writer);

                txtWriter.Flush();
                txtWriter.Close();
            }
        }
        #endregion LITJSON_IO
    }
}