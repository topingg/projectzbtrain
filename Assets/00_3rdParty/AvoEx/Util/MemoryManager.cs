﻿using UnityEngine;
using System.Collections.Generic;

/* See the "http://avoex.com/avoex/default-license" for the full license governing this code. */

namespace AvoEx
{
    public interface IScopeObject
    {
        int scopeIndex { get; set; } // set by MemoryScope only
        bool scopeUsed { get; set; } // set by MemoryScope only
        int objectInstanceID { get; }
        void Unload();
    }

    /// <summary>
    /// 해제될때 Unload 호출 되어야 함.
    /// </summary>
    public abstract partial class MemObject : IScopeObject
    {
        private Object m_memoryObject = null;

        public int scopeIndex { get; set; } // set by MemoryScope only
        public bool scopeUsed { get; set; } // set by MemoryScope only
        public int objectInstanceID { get { return (m_memoryObject == null ? 0 : m_memoryObject.GetInstanceID()); } }

        /// <summary>
        /// check using this MemObject
        /// </summary>
        protected Object memoryObject
        {
            get
            {
                if (m_memoryObject != null && scopeUsed == false)
                    MemoryScope.SetUsedToScope(this);
                return m_memoryObject;
            }
        }

        /// <summary>
        /// called by derived class when object loaded.
        /// </summary>
        /// <param name="newObject">newly loaded object</param>
        protected void SetMemoryObject(Object newObject)
        {
            if (newObject == null)
                return;
            if (m_memoryObject != null) // already loaded before
                return;
            m_memoryObject = newObject; // first load

            MemoryScope.LoadedObject(this); // for managing scope
        }


        public void Unload()
        {
            if (m_memoryObject == null)
            {
#if DEBUG_MEMOBJECT
                Debug.LogWarning("MemObject.Unload() is failed. nothing to unload.");
#endif
                return;
            }
#if DEBUG_MEMOBJECT
            Debug.LogFormat("MemObject({0}).Unload()", m_memoryObject);
#endif
            MemoryScope.UnloadedObject(this); // must called before m_memoryObject = null, UnloadedObject() is needed m_memoryObject.GetInstanceID().

            Release(); // unload for derived class
            m_memoryObject = null; // unload
        }

        public abstract void Release();

        public override string ToString()
        {
            if (m_memoryObject != null)
                return string.Format("{0}('{1}')", base.ToString(), m_memoryObject);
            return base.ToString();
        }
    }

    public class MemoryScope
    {
        string m_name = string.Empty;
        int m_index = 0;
        List<int> m_listLoadedObjectIDs = new List<int>(AssetManager.ManagedAsset.Count); // list of loaded MemObject on this scope. enough capacity is for avoidance of garbage.

        public string name { get { return m_name; } }
        public int index { get { return m_index; } }

        MemoryScope(string name, int index)
        {
            m_name = name;
            m_index = index;
        }

        void AddLoadedObject(int instanceID) // instanceID must be not added before. called by SetLoadedToScope() only.
        {
            m_listLoadedObjectIDs.Add(instanceID);
        }

        void AddLoadedObjects(List<int> listInstanceIDs)
        {
            if (listInstanceIDs == null && listInstanceIDs.Count == 0)
                return;

            var itorInstanceIDs = listInstanceIDs.GetEnumerator();
            while (itorInstanceIDs.MoveNext())
            {
                if (m_listLoadedObjectIDs.Contains(itorInstanceIDs.Current))
                    continue;
                m_listLoadedObjectIDs.Add(itorInstanceIDs.Current);
            }
        }

        void Clear()
        {
            var itorLoadedID = m_listLoadedObjectIDs.GetEnumerator();
            while (itorLoadedID.MoveNext())
            {
                UnloadObject(itorLoadedID.Current);
            }
            m_listLoadedObjectIDs.Clear();
        }

        List<int> Clear(List<int> listException)
        {
            List<int> listLoadedInException = new List<int>(listException.Count);
            var itorLoadedID = m_listLoadedObjectIDs.GetEnumerator();
            while (itorLoadedID.MoveNext())
            {
                if (listException.Contains(itorLoadedID.Current))
                {
                    listLoadedInException.Add(itorLoadedID.Current); // if contains in listException, do not unload, pass instance id to next scope.
                    continue;
                }
                UnloadObject(itorLoadedID.Current);
            }
            m_listLoadedObjectIDs.Clear();

            return listLoadedInException;
        }

        #region STATICS
        static MemoryScope s_currentScope = null;
        static MemoryScope s_previousScope = null;
        static Stack<MemoryScope> s_stackMemoryScope = new Stack<MemoryScope>();
        static List<IScopeObject> s_listUsedObjects = new List<IScopeObject>(); // list of used MemObject on currentScope

        private static Dictionary<int, IScopeObject> s_dictScopeObjects = new Dictionary<int, IScopeObject>();

        public static void LoadedObject(IScopeObject loadedObject)
        {
            if (s_currentScope == null || s_currentScope.index == loadedObject.scopeIndex)
                return;
            s_dictScopeObjects[loadedObject.objectInstanceID] = loadedObject;

            s_currentScope.AddLoadedObject(loadedObject.objectInstanceID);
            loadedObject.scopeIndex = s_currentScope.index;
#if DEBUG_MEMOBJECT
            Debug.LogFormat("MemoryScope.<color=#ff00ffff>SetLoadedToScope</color>('{0}') : scope({2}) = '{1}', count = '<color=#ff0000ff>{3}</color>'", loadedObject, s_currentScope.name, s_currentScope.index, s_dictScopeObjects.Count);
#endif
        }

        public static void UnloadedObject(IScopeObject unloadedObject)
        {
            s_dictScopeObjects.Remove(unloadedObject.objectInstanceID);
            unloadedObject.scopeIndex = 0;
            unloadedObject.scopeUsed = false;

#if DEBUG_MEMOBJECT
            if (s_currentScope != null)
                Debug.LogFormat("MemoryScope.<color=#00ffffff>UnloadedObject</color>('{0}') : scope({2}) = '{1}', count = '<color=#ff0000ff>{3}</color>'", unloadedObject, s_currentScope.name, s_currentScope.index, s_dictScopeObjects.Count);
#endif
        }

        static void UnloadObject(int instanceID)
        {
            IScopeObject unloadObject = null;
            s_dictScopeObjects.TryGetValue(instanceID, out unloadObject);
            if (unloadObject != null)
                unloadObject.Unload();
#if DEBUG_MEMOBJECT
            else
                Debug.LogFormat("MemoryScope.UnloadObject({0}) is failed. already unloaded instance.", instanceID);
#endif
        }

        public static void SetUsedToScope(IScopeObject usedObject)
        {
            if (s_previousScope == null) // SetUsedToScope() is needed only BeginNextScope().
                return;
            s_listUsedObjects.Add(usedObject);
            usedObject.scopeUsed = true;
#if DEBUG_MEMOBJECT
            Debug.LogFormat("MemoryScope.<color=#ff00ffff>SetUsedToScope</color>('{0}') : scope({2}) = '{1}', count = '<color=#ff0000ff>{3}</color>'", usedObject, s_currentScope.name, s_currentScope.index, s_dictScopeObjects.Count);
#endif
        }

        public static void BeginScope(string scopeName)
        {
            if (s_previousScope != null)
            {
                Debug.LogErrorFormat("BeginScope('{0}') is failed. call EndPrevScope() first.", scopeName);
                return;
            }

            if (s_currentScope != null)
                s_stackMemoryScope.Push(s_currentScope);
            s_currentScope = new MemoryScope(scopeName, s_stackMemoryScope.Count + 1);
#if DEBUG_MEMOBJECT
            Debug.LogFormat("MemoryScope.<color=#ffff00ff>BeginScope()</color> : scope({1}) = '{0}'", s_currentScope.name, s_currentScope.index);
#endif
        }

        public static void EndScope()
        {
            if (s_previousScope != null)
            {
                Debug.LogErrorFormat("EndScope() is failed. call EndPrevScope() first.");
                return;
            }

            if (s_currentScope == null)
                return;
#if DEBUG_MEMOBJECT
            Debug.LogFormat("MemoryScope.<color=#00ff00ff>EndScope()</color> : scope({1}) = '{0}' is started. count = '<color=#ff0000ff>{2}</color>'", s_currentScope.name, s_currentScope.index, s_dictScopeObjects.Count);
#endif
            s_currentScope.Clear();

#if DEBUG_MEMOBJECT
            Debug.LogFormat("MemoryScope.<color=#00ff00ff>EndScope()</color> : scope({1}) = '{0}' is finished.", s_currentScope.name, s_currentScope.index);
#endif
        }

        public static void BeginNextScope(string scopeName)
        {
            if (s_previousScope != null)
            {
                Debug.LogErrorFormat("BeginNextScope('{0}') is failed. call EndPrevScope() first.", scopeName);
                return;
            }

            if (s_currentScope == null)
            {
                BeginScope(scopeName);
                return;
            }
            s_listUsedObjects.Clear();
            s_previousScope = s_currentScope;
            s_currentScope = new MemoryScope(scopeName, s_currentScope.index);
#if DEBUG_MEMOBJECT
            Debug.LogFormat("MemoryScope.<color=#ffff00ff>BeginNextScope()</color> : scope({1}) = '{0}'", s_currentScope.name, s_currentScope.index);
#endif
        }

        public static void EndPrevScope()
        {
            if (s_previousScope == null)
            {
                Debug.LogFormat("MemoryScope.EndPrevScope() : not exist previous scope.");
                return;
            }

#if DEBUG_MEMOBJECT
            Debug.LogFormat("MemoryScope.<color=#00ff00ff>EndPrevScope()</color> : previous scope({1}) = '{0}', count = '<color=#ff0000ff>{2}</color>'", s_previousScope.name, s_previousScope.index, s_dictScopeObjects.Count);
#endif
            if (s_listUsedObjects.Count > 0)
            {
                List<int> listUsedInstanceIDs = new List<int>(s_listUsedObjects.Count);
                var itorUsedObjects = s_listUsedObjects.GetEnumerator();
                while (itorUsedObjects.MoveNext())
                {
                    listUsedInstanceIDs.Add(itorUsedObjects.Current.objectInstanceID);
                    itorUsedObjects.Current.scopeUsed = false;
                }
                s_listUsedObjects.Clear();

                List<int> listPrevLoadedIDs = s_previousScope.Clear(listUsedInstanceIDs); // clear previous MemoryScope, except used MemObject by current scope
                s_currentScope.AddLoadedObjects(listPrevLoadedIDs); // pass excepted IDs that loaded in previous MemoryScope to current MemoryScope.
            }
            else
            {
                s_previousScope.Clear(); // clear all previous MemoryScope.
            }
            s_previousScope = null;

            Resources.UnloadUnusedAssets();
#if DEBUG_MEMOBJECT
            Debug.LogFormat("MemoryScope.<color=#00ff00ff>EndPrevScope()</color> : current scope({1}) = '{0}', count = '<color=#ff0000ff>{2}</color>'", s_currentScope.name, s_currentScope.index, s_dictScopeObjects.Count);
#endif
        }
        #endregion STATICS


    }

    public class MemoryManager : MonoBehaviour
    {

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }

}