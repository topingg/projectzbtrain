﻿using UnityEngine;
using System;

/* See the "http://avoex.com/unity/encrypttype/license/" for the full license governing this code. */

namespace AvoEx.EncryptType
{
    [Serializable]
    public struct EncByte : IEncryptor<byte>, IComparable, IComparable<EncByte>, IEquatable<EncByte>, IFormattable, ISerializationCallbackReceiver
    {
        [SerializeField]
        private byte camValue;
        private bool detectMode;
        private byte encKey;
        private byte encValue;

        public EncByte(byte initValue)
        {
            detectMode = false;
            camValue = 0;
            encKey = 0;
            encValue = 0;
            Value = initValue;
        }

        private byte Decrypt()
        {
            return (byte)(encValue ^ encKey);
        }

        private void Encrypt(byte value)
        {
            encKey = (byte)EncryptManager.Key;
            encValue = (byte)(value ^ encKey);
        }

        private byte Value
        {
            set
            {
                detectMode = EncryptManager.IsDetecting;
                if (detectMode)
                    camValue = value;
                Encrypt(value);
            }
            get
            {
                byte decrypted = Decrypt();
                if (detectMode && camValue != decrypted)
                {
                    EncryptManager.OnDetectedCheating();
                    camValue = decrypted;
                }
                return decrypted;
            }
        }

        #region IEncryptor
        public byte ToSystemType()
        {
            return Value;
        }

        public void SetValue(byte value)
        {
            Value = value;
        }
        #endregion IEncryptor

        #region operator
        public static implicit operator byte(EncByte x)
        {
            return x.Value;
        }
        public static implicit operator EncByte(byte x)
        {
            return new EncByte(x);
        }

        public static EncByte operator ++(EncByte x)
        {
            x.Value = (byte)(x.Value + 1);
            return x;
        }
        public static EncByte operator --(EncByte x)
        {
            x.Value = (byte)(x.Value - 1);
            return x;
        }
        #endregion operator

        #region IComparable
        public int CompareTo(EncByte other)
        {
            return Value.CompareTo(other.Value);
        }

        public int CompareTo(object obj)
        {
            return obj is EncByte ? CompareTo((EncByte)obj) : 1;
        }
        #endregion IComparable

        #region IEquatable
        public bool Equals(EncByte other)
        {
            return this.Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            return obj is EncByte && Equals((EncByte)obj);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
        #endregion IEquatable

        #region IFormattable
        public override string ToString()
        {
            return Value.ToString();
        }

        public string ToString(string format)
        {
            return Value.ToString(format);
        }

        public string ToString(IFormatProvider provider)
        {
            return Value.ToString(provider);
        }

        public string ToString(string format, IFormatProvider provider)
        {
            return Value.ToString(format, provider);
        }
        #endregion IFormattable

        #region ISerializationCallbackReceiver
        public void OnAfterDeserialize()
        {
            Encrypt(camValue);
        }

        public void OnBeforeSerialize()
        {
            camValue = Decrypt();
        }
        #endregion ISerializationCallbackReceiver
    }

    [Serializable]
    public struct EncSByte : IEncryptor<sbyte>, IComparable, IComparable<EncSByte>, IEquatable<EncSByte>, IFormattable, ISerializationCallbackReceiver
    {
        [SerializeField]
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9 
        private int camValue;
#else
        private sbyte camValue;
#endif
        private bool detectMode;
        private sbyte encKey;
        private sbyte encValue;

        public EncSByte(sbyte initValue)
        {
            detectMode = false;
            camValue = 0;
            encKey = 0;
            encValue = 0;
            Value = initValue;
        }

        private sbyte Decrypt()
        {
            return (sbyte)(encValue ^ encKey);
        }

        private void Encrypt(sbyte value)
        {
            encKey = (sbyte)EncryptManager.Key;
            encValue = (sbyte)(value ^ encKey);
        }

        private sbyte Value
        {
            set
            {
                detectMode = EncryptManager.IsDetecting;
                if (detectMode)
                    camValue = value;
                Encrypt(value);
            }
            get
            {
                sbyte decrypted = Decrypt();
                if (detectMode && camValue != decrypted)
                {
                    EncryptManager.OnDetectedCheating();
                    camValue = decrypted;
                }
                return decrypted;
            }
        }

        #region IEncryptor
        public sbyte ToSystemType()
        {
            return Value;
        }

        public void SetValue(sbyte value)
        {
            Value = value;
        }
        #endregion IEncryptor

        #region operator
        public static implicit operator sbyte(EncSByte x)
        {
            return x.Value;
        }
        public static implicit operator EncSByte(sbyte x)
        {
            return new EncSByte(x);
        }

        public static EncSByte operator ++(EncSByte x)
        {
            x.Value = (sbyte)(x.Value + 1);
            return x;
        }
        public static EncSByte operator --(EncSByte x)
        {
            x.Value = (sbyte)(x.Value - 1);
            return x;
        }
        #endregion operator

        #region IComparable
        public int CompareTo(EncSByte other)
        {
            return Value.CompareTo(other.Value);
        }

        public int CompareTo(object obj)
        {
            return obj is EncSByte ? CompareTo((EncSByte)obj) : 1;
        }
        #endregion IComparable

        #region IEquatable
        public bool Equals(EncSByte other)
        {
            return this.Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            return obj is EncSByte && Equals((EncSByte)obj);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
        #endregion IEquatable

        #region IFormattable
        public override string ToString()
        {
            return Value.ToString();
        }

        public string ToString(string format)
        {
            return Value.ToString(format);
        }

        public string ToString(IFormatProvider provider)
        {
            return Value.ToString(provider);
        }

        public string ToString(string format, IFormatProvider provider)
        {
            return Value.ToString(format, provider);
        }
        #endregion IFormattable

        #region ISerializationCallbackReceiver
        public void OnAfterDeserialize()
        {
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9 
            Encrypt((sbyte)camValue);
#else
            Encrypt(camValue);
#endif
        }

        public void OnBeforeSerialize()
        {
            camValue = Decrypt();
        }
        #endregion ISerializationCallbackReceiver
    }
}
