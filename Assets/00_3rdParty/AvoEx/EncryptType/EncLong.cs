﻿using UnityEngine;
using System;

/* See the "http://avoex.com/unity/encrypttype/license/" for the full license governing this code. */

namespace AvoEx.EncryptType
{
    [Serializable]
    public struct EncLong : IEncryptor<long>, IComparable, IComparable<EncLong>, IEquatable<EncLong>, IFormattable, ISerializationCallbackReceiver
    {
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9 
        [SerializeField]
        private string serializeValue;
#else
        [SerializeField]
#endif
        private long camValue;
		private bool detectMode;
        private long encKey;
        private long encValue;


        public EncLong(long initValue)
        {
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9 
            serializeValue = "0";
#endif
			detectMode = false;
			camValue = 0L;
			encKey = 0L;
			encValue = 0L;
			Value = initValue;
        }

        private long Decrypt()
        {
            return encValue ^ encKey;
        }

        private void Encrypt(long value)
        {
            encKey = EncryptManager.Key;
            encValue = value ^ encKey;
        }

        private long Value
        {
            set
            {
				detectMode = EncryptManager.IsDetecting;
				if (detectMode)
					camValue = value;
                Encrypt(value);
            }
            get
            {
                long decrypted = Decrypt();
				if (detectMode && camValue != decrypted)
                {
                    EncryptManager.OnDetectedCheating();
                    camValue = decrypted;
                }
                return decrypted;
            }
        }

        #region IEncryptor
        public long ToSystemType()
        {
            return Value;
        }

        public void SetValue(long value)
        {
            Value = value;
        }
        #endregion IEncryptor

        #region operator
        public static implicit operator long(EncLong x)
        {
            return x.Value;
        }
        public static implicit operator EncLong(long x)
        {
            return new EncLong(x);
        }

        public static EncLong operator ++(EncLong x)
        {
            x.Value = x.Value + 1;
            return x;
        }
        public static EncLong operator --(EncLong x)
        {
            x.Value = x.Value - 1;
            return x;
        }
        #endregion operator

        #region IComparable
        public int CompareTo(EncLong other)
        {
            return Value.CompareTo(other.Value);
        }

        public int CompareTo(object obj)
        {
            return obj is EncLong ? CompareTo((EncLong)obj) : 1;
        }
        #endregion IComparable

        #region IEquatable
        public bool Equals(EncLong other)
        {
            return this.Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            return obj is EncLong && Equals((EncLong)obj);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
        #endregion IEquatable

        #region IFormattable
        public override string ToString()
        {
            return Value.ToString();
        }

        public string ToString(string format)
        {
            return Value.ToString(format);
        }

        public string ToString(IFormatProvider provider)
        {
            return Value.ToString(provider);
        }

        public string ToString(string format, IFormatProvider provider)
        {
            return Value.ToString(format, provider);
        }
        #endregion IFormattable

        #region ISerializationCallbackReceiver
        public void OnAfterDeserialize()
        {
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9 
            camValue = long.Parse(serializeValue);
#endif
            Encrypt(camValue);
        }

        public void OnBeforeSerialize()
        {
            camValue = Decrypt();
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9 
            serializeValue = camValue.ToString();
#endif
        }
        #endregion ISerializationCallbackReceiver
    }

    [Serializable]
    public struct EncULong : IEncryptor<ulong>, IComparable, IComparable<EncULong>, IEquatable<EncULong>, IFormattable, ISerializationCallbackReceiver
    {
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9 
        [SerializeField]
        private string serializeValue;
#else
        [SerializeField]
#endif
        private long camValue;
		private bool detectMode;
        private long encKey;
        private long encValue;

        public EncULong(ulong initValue)
        {
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9 
            serializeValue = "0";
#endif
			detectMode = false;
			camValue = 0L;
			encKey = 0L;
			encValue = 0L;
			Value = initValue;
        }

        private long Decrypt()
        {
            return encValue ^ encKey;
        }

        private void Encrypt(long value)
        {
            encKey = EncryptManager.Key;
            encValue = (long)value ^ encKey;
        }

        private ulong Value
        {
            set
            {
				detectMode = EncryptManager.IsDetecting;
				if (detectMode)
					camValue = (long)value;
                Encrypt((long)value);
            }
            get
            {
                long decrypted = Decrypt();
				if (detectMode && camValue != decrypted)
                {
                    EncryptManager.OnDetectedCheating();
                    camValue = decrypted;
                }
                return (ulong)decrypted;
            }
        }

        #region IEncryptor
        public ulong ToSystemType()
        {
            return Value;
        }

        public void SetValue(ulong value)
        {
            Value = value;
        }
        #endregion IEncryptor

        #region operator
        public static implicit operator ulong(EncULong x)
        {
            return x.Value;
        }
        public static implicit operator EncULong(ulong x)
        {
            return new EncULong(x);
        }

        public static EncULong operator ++(EncULong x)
        {
            x.Value = x.Value + 1;
            return x;
        }
        public static EncULong operator --(EncULong x)
        {
            x.Value = x.Value - 1;
            return x;
        }
        #endregion operator

        #region IComparable
        public int CompareTo(EncULong other)
        {
            return Value.CompareTo(other.Value);
        }

        public int CompareTo(object obj)
        {
            return obj is EncULong ? CompareTo((EncULong)obj) : 1;
        }
        #endregion IComparable

        #region IEquatable
        public bool Equals(EncULong other)
        {
            return this.Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            return obj is EncULong && Equals((EncULong)obj);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
        #endregion IEquatable

        #region IFormattable
        public override string ToString()
        {
            return Value.ToString();
        }

        public string ToString(string format)
        {
            return Value.ToString(format);
        }

        public string ToString(IFormatProvider provider)
        {
            return Value.ToString(provider);
        }

        public string ToString(string format, IFormatProvider provider)
        {
            return Value.ToString(format, provider);
        }
        #endregion IFormattable

        #region ISerializationCallbackReceiver
        public void OnAfterDeserialize()
        {
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9 
            camValue = (long)ulong.Parse(serializeValue);
#endif
            Encrypt(camValue);
        }

        public void OnBeforeSerialize()
        {
            camValue = Decrypt();
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9 
            serializeValue = camValue.ToString();
#endif
        }
        #endregion ISerializationCallbackReceiver
    }
}
