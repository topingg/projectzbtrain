﻿using UnityEngine;
using System;

/* See the "http://avoex.com/unity/encrypttype/license/" for the full license governing this code. */

namespace AvoEx.EncryptType
{
    [Serializable]
    public struct EncShort : IEncryptor<short>, IComparable, IComparable<EncShort>, IEquatable<EncShort>, IFormattable, ISerializationCallbackReceiver
    {
        [SerializeField]
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9 
        private int camValue;
#else
        private short camValue;
#endif

        private bool detectMode;
        private short encKey;
        private short encValue;

        public EncShort(short initValue)
        {
			detectMode = false;
			camValue = 0;
			encKey = 0;
			encValue = 0;
			Value = initValue;
        }

        private short Decrypt()
        {
            return (short)(encValue ^ encKey);
        }

        private void Encrypt(short value)
        {
            encKey = (short)EncryptManager.Key;
            encValue = (short)(value ^ encKey);
        }

        private short Value
        {
            set
            {
				detectMode = EncryptManager.IsDetecting;
				if (detectMode)
					camValue = value;
                Encrypt(value);
            }
            get
            {
                short decrypted = Decrypt();
				if (detectMode && camValue != decrypted)
                {
                    EncryptManager.OnDetectedCheating();
                    camValue = decrypted;
                }
                return decrypted;
            }
        }

        #region IEncryptor
        public short ToSystemType()
        {
            return Value;
        }

        public void SetValue(short value)
        {
            Value = value;
        }
        #endregion IEncryptor

        #region operator
        public static implicit operator short(EncShort x)
        {
            return x.Value;
        }
        public static implicit operator EncShort(short x)
        {
            return new EncShort(x);
        }

        public static EncShort operator ++(EncShort x)
        {
            x.Value = (short)(x.Value + 1);
            return x;
        }
        public static EncShort operator --(EncShort x)
        {
            x.Value = (short)(x.Value - 1);
            return x;
        }
        #endregion operator

        #region IComparable
        public int CompareTo(EncShort other)
        {
            return Value.CompareTo(other.Value);
        }

        public int CompareTo(object obj)
        {
            return obj is EncShort ? CompareTo((EncShort)obj) : 1;
        }
        #endregion IComparable

        #region IEquatable
        public bool Equals(EncShort other)
        {
            return this.Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            return obj is EncShort && Equals((EncShort)obj);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
        #endregion IEquatable

        #region IFormattable
        public override string ToString()
        {
            return Value.ToString();
        }

        public string ToString(string format)
        {
            return Value.ToString(format);
        }

        public string ToString(IFormatProvider provider)
        {
            return Value.ToString(provider);
        }

        public string ToString(string format, IFormatProvider provider)
        {
            return Value.ToString(format, provider);
        }
        #endregion IFormattable

        #region ISerializationCallbackReceiver
        public void OnAfterDeserialize()
        {
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9 
            Encrypt((short)camValue);
#else
            Encrypt(camValue);
#endif
        }

        public void OnBeforeSerialize()
        {
            camValue = Decrypt();
        }
        #endregion ISerializationCallbackReceiver
    }

    [Serializable]
    public struct EncUShort : IEncryptor<ushort>, IComparable, IComparable<EncUShort>, IEquatable<EncUShort>, IFormattable, ISerializationCallbackReceiver
    {
        [SerializeField]
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9 
        private int camValue;
#else
        private ushort camValue;
#endif
        private bool detectMode;
        private ushort encKey;
        private ushort encValue;

        public EncUShort(ushort initValue)
        {
			detectMode = false;
			camValue = 0;
			encKey = 0;
			encValue = 0;
			Value = initValue;
        }

        private ushort Decrypt()
        {
            return (ushort)(encValue ^ encKey);
        }

        private void Encrypt(ushort value)
        {
            encKey = (ushort)EncryptManager.Key;
            encValue = (ushort)(value ^ encKey);
        }

        private ushort Value
        {
            set
            {
				detectMode = EncryptManager.IsDetecting;
				if (detectMode)
					camValue = value;
                Encrypt(value);
            }
            get
            {
                ushort decrypted = Decrypt();
				if (detectMode && camValue != decrypted)
                {
                    EncryptManager.OnDetectedCheating();
                    camValue = decrypted;
                }
                return decrypted;
            }
        }

        #region IEncryptor
        public ushort ToSystemType()
        {
            return Value;
        }

        public void SetValue(ushort value)
        {
            Value = value;
        }
        #endregion IEncryptor

        #region operator
        public static implicit operator ushort(EncUShort x)
        {
            return x.Value;
        }
        public static implicit operator EncUShort(ushort x)
        {
            return new EncUShort(x);
        }

        public static EncUShort operator ++(EncUShort x)
        {
            x.Value = (ushort)(x.Value + 1);
            return x;
        }
        public static EncUShort operator --(EncUShort x)
        {
            x.Value = (ushort)(x.Value - 1);
            return x;
        }
        #endregion operator

        #region IComparable
        public int CompareTo(EncUShort other)
        {
            return Value.CompareTo(other.Value);
        }

        public int CompareTo(object obj)
        {
            return obj is EncUShort ? CompareTo((EncUShort)obj) : 1;
        }
        #endregion IComparable

        #region IEquatable
        public bool Equals(EncUShort other)
        {
            return this.Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            return obj is EncUShort && Equals((EncUShort)obj);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
        #endregion IEquatable

        #region IFormattable
        public override string ToString()
        {
            return Value.ToString();
        }

        public string ToString(string format)
        {
            return Value.ToString(format);
        }

        public string ToString(IFormatProvider provider)
        {
            return Value.ToString(provider);
        }

        public string ToString(string format, IFormatProvider provider)
        {
            return Value.ToString(format, provider);
        }
        #endregion IFormattable

        #region ISerializationCallbackReceiver
        public void OnAfterDeserialize()
        {
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9 
            Encrypt((ushort)camValue);
#else
            Encrypt(camValue);
#endif
        }

        public void OnBeforeSerialize()
        {
            camValue = Decrypt();
        }
        #endregion ISerializationCallbackReceiver
    }
}
