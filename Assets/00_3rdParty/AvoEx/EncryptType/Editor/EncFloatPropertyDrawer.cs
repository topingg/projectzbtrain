﻿using UnityEngine;
using UnityEditor;
using System.Runtime.InteropServices;

/* See the "http://avoex.com/unity/encrypttype/license/" for the full license governing this code. */

namespace AvoEx.EncryptType.Editor
{
    [CustomPropertyDrawer(typeof(EncFloat))]
    public class EncFloatPropertyDrawer : EncryptTypePropertyDrawer
    {
        protected override void EditValue(Rect position, SerializedProperty camValue, GUIContent label)
        {
            float newValue = camValue.floatValue;
            EditorGUI.BeginChangeCheck();
            newValue = EditorGUI.FloatField(position, label, newValue);
            if (EditorGUI.EndChangeCheck())
            {
                camValue.floatValue = newValue;
            }
        }
    }

    [CustomPropertyDrawer(typeof(EncDouble))]
    public class EncDoublePropertyDrawer : EncryptTypePropertyDrawer
    {
        protected override void EditValue(Rect position, SerializedProperty camValue, GUIContent label)
        {
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9 
            string newValue = camValue.stringValue;
            EditorGUI.BeginChangeCheck();
            newValue = EditorGUI.TextField(position, label, newValue);
            if (EditorGUI.EndChangeCheck())
            {
                double doubleValue = 0;
                if (double.TryParse(newValue, out doubleValue))
                    camValue.stringValue = newValue;
            }
#else
            double newValue = camValue.doubleValue;
            EditorGUI.BeginChangeCheck();
            newValue = EditorGUI.DoubleField(position, label, newValue);
            if (EditorGUI.EndChangeCheck())
            {
                camValue.doubleValue = newValue;
            }
#endif
        }
    }
}
