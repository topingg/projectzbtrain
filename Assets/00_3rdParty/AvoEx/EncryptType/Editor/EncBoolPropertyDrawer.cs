﻿using UnityEngine;
using UnityEditor;

/* See the "http://avoex.com/unity/encrypttype/license/" for the full license governing this code. */

namespace AvoEx.EncryptType.Editor
{
    [CustomPropertyDrawer(typeof(EncBool))]
    public class EncBoolPropertyDrawer : EncryptTypePropertyDrawer
    {
        protected override void EditValue(Rect position, SerializedProperty camValue, GUIContent label)
        {
            bool newValue = camValue.boolValue;
            EditorGUI.BeginChangeCheck();
            newValue = EditorGUI.Toggle(position, label, newValue);
            if (EditorGUI.EndChangeCheck())
            {
                camValue.boolValue = newValue;
            }
        }
    }
}
