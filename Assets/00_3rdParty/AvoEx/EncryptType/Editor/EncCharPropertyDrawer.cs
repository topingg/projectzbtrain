﻿using UnityEngine;
using UnityEditor;

/* See the "http://avoex.com/unity/encrypttype/license/" for the full license governing this code. */

namespace AvoEx.EncryptType.Editor
{
    [CustomPropertyDrawer(typeof(EncChar))]
    public class EncCharPropertyDrawer : EncryptTypePropertyDrawer
    {
        protected override void EditValue(Rect position, SerializedProperty camValue, GUIContent label)
        {
            int newValue = camValue.intValue;
            string textValue = ((char)newValue).ToString();
            EditorGUI.BeginChangeCheck();
            textValue = EditorGUI.TextField(position, label, textValue);
            if (textValue.Length > 0)
                newValue = textValue[0];
            else
                newValue = 0;
            if (EditorGUI.EndChangeCheck())
            {
                camValue.intValue = newValue;
            }
        }
    }
}