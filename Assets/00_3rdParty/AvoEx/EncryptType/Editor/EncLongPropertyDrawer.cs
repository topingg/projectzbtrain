﻿using UnityEngine;
using UnityEditor;
using System;

/* See the "http://avoex.com/unity/encrypttype/license/" for the full license governing this code. */

namespace AvoEx.EncryptType.Editor
{
	[CustomPropertyDrawer(typeof(EncLong))]
    public class EncLongPropertyDrawer : EncryptTypePropertyDrawer
	{
        protected override void EditValue(Rect position, SerializedProperty camValue, GUIContent label)
        {
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9 
            string newValue = camValue.stringValue;
            EditorGUI.BeginChangeCheck();
            newValue = EditorGUI.TextField(position, label, newValue);
            if (EditorGUI.EndChangeCheck())
            {
                long longValue = 0;
                if (long.TryParse(newValue, out longValue))
                    camValue.stringValue = newValue;
            }
#else
            long newValue = camValue.longValue;
            EditorGUI.BeginChangeCheck();
            newValue = EditorGUI.LongField(position, label, newValue);
            if (EditorGUI.EndChangeCheck())
            {
                camValue.longValue = newValue;
            }
#endif
        }
	}
	
	[CustomPropertyDrawer(typeof(EncULong))]
    public class EncULongPropertyDrawer : EncryptTypePropertyDrawer
	{
        protected override void EditValue(Rect position, SerializedProperty camValue, GUIContent label)
        {
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9 
            string newValue = camValue.stringValue;
            EditorGUI.BeginChangeCheck();
            try
            {
                ulong.Parse(EditorGUI.TextField(position, label, newValue));
            }
            catch (OverflowException e)
            {
                if (e.Message.Equals("Negative number"))
                {
                    newValue = "0";
                }
                else
                {
                    newValue = ulong.MaxValue.ToString();
                }
            }
            catch (Exception)
            {
                newValue = "0";
            }
            if (EditorGUI.EndChangeCheck())
            {
                camValue.stringValue = newValue;
            }
#else
            long newValue = camValue.longValue;
            string textValue = ((ulong)newValue).ToString();
            EditorGUI.BeginChangeCheck();
            try
            {
                newValue = (long)ulong.Parse(EditorGUI.TextField(position, label, textValue));
            }
            catch (OverflowException e)
            {
                if (e.Message.Equals("Negative number"))
                {
                    newValue = 0;
                }
                else
                {
                    ulong tempValue = ulong.MaxValue;
                    newValue = (long)tempValue;
                }
            }
            catch (Exception)
            {
                newValue = 0;
            }
            if (EditorGUI.EndChangeCheck())
            {
                camValue.longValue = newValue;
            }
#endif
        }

	}
}