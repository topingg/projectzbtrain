﻿using UnityEngine;
using UnityEditor;

/* See the "http://avoex.com/unity/encrypttype/license/" for the full license governing this code. */

namespace AvoEx.EncryptType.Editor
{
    [CustomPropertyDrawer(typeof(EncByte))]
    public class EncBytePropertyDrawer : EncryptTypePropertyDrawer
    {
        protected override void EditValue(Rect position, SerializedProperty camValue, GUIContent label)
        {
            byte newValue = (byte)camValue.intValue;
            EditorGUI.BeginChangeCheck();
            newValue = (byte)Mathf.Clamp(EditorGUI.IntField(position, label, newValue), byte.MinValue, byte.MaxValue);
            if (EditorGUI.EndChangeCheck())
            {
                camValue.intValue = newValue;
            }
        }
    }

    [CustomPropertyDrawer(typeof(EncSByte))]
    public class EncSBytePropertyDrawer : EncryptTypePropertyDrawer
    {
        protected override void EditValue(Rect position, SerializedProperty camValue, GUIContent label)
        {
            sbyte newValue = (sbyte)camValue.intValue;
            EditorGUI.BeginChangeCheck();
            newValue = (sbyte)Mathf.Clamp(EditorGUI.IntField(position, label, newValue), sbyte.MinValue, sbyte.MaxValue);
            if (EditorGUI.EndChangeCheck())
            {
                camValue.intValue = newValue;
            }
        }
    }
}