﻿using UnityEngine;
using UnityEditor;

/* See the "http://avoex.com/unity/encrypttype/license/" for the full license governing this code. */

namespace AvoEx.EncryptType.Editor
{
    [CustomEditor(typeof(EncryptManager))]
    public class EncryptManagerEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            EncryptManager scriptTarget = target as EncryptManager;

            scriptTarget.keyUpdateInterval = EditorGUILayout.Slider("Key Update Interval", scriptTarget.keyUpdateInterval, EncryptManager.MIN_INTERVAL, EncryptManager.MAX_INTERVAL);
            if (EditorApplication.isPlaying)
            {
                if (EncryptManager.IsDetecting)
                {
                    EditorGUILayout.LabelField("EncryptManager running on detect mode.");

                    if (EncryptManager.onDetectedCheating != null)
                    {
                        EditorGUILayout.LabelField("Action on cheating detected", EncryptManager.onDetectedCheating.Target.GetType().ToString() + "." + EncryptManager.onDetectedCheating.Method.Name + "();");
                    }
                    else if (scriptTarget.quitOnCheatDetected)
                    {
                        EditorGUILayout.LabelField("Quit on cheating detected");
                    }
                    else
                    {
                        EditorGUILayout.HelpBox("Do nothing on cheating detected. set action with 'EncryptManager.StartDetecting(Action)' or check 'Quit on cheating detected'", MessageType.Warning);
                    }
                }
                else
                {
                    EditorGUILayout.LabelField("EncryptManager running on normal mode.");
                    EditorGUILayout.HelpBox("if you want to detect cheating, use 'EncryptManager.StartDetecting()' or check 'Auto Start Detecting'.", MessageType.Info);
                }
            }
            else
            {
                scriptTarget.autoStartDetecting = EditorGUILayout.Toggle("Auto Start Detecting", scriptTarget.autoStartDetecting);

                if (scriptTarget.autoStartDetecting)
                {
                    scriptTarget.quitOnCheatDetected = EditorGUILayout.Toggle("Quit on cheating detected", scriptTarget.quitOnCheatDetected);
                }
                else
                {

                }
            }


            if (GUI.changed)
                EditorUtility.SetDirty(target);
        }

        [MenuItem("GameObject/Create EncryptManager")]
        [MenuItem("Tools/AvoEx/EncryptType/Create EncryptManager")]
        public static void CreateEncryptManager()
        {
            Object instance = GameObject.FindObjectOfType<EncryptManager>();
            if (instance == null)
                instance = new GameObject(typeof(EncryptManager).ToString(), typeof(EncryptManager));
            else
                Debug.LogWarning(typeof(EncryptManager).ToString() + " is already exist!");
        }
    }
}
