﻿using UnityEngine;
using UnityEditor;
using System;

/* See the "http://avoex.com/unity/encrypttype/license/" for the full license governing this code. */

namespace AvoEx.EncryptType.Editor
{
    [CustomPropertyDrawer(typeof(EncInt))]
    public class EncIntPropertyDrawer : EncryptTypePropertyDrawer
    {
        protected override void EditValue(Rect position, SerializedProperty camValue, GUIContent label)
        {
            int newValue = camValue.intValue;
            EditorGUI.BeginChangeCheck();
            newValue = EditorGUI.IntField(position, label, newValue);
            if (EditorGUI.EndChangeCheck())
            {
                camValue.intValue = newValue;
            }
        }
    }

    [CustomPropertyDrawer(typeof(EncUInt))]
    public class EncUIntPropertyDrawer : EncryptTypePropertyDrawer
    {
        protected override void EditValue(Rect position, SerializedProperty camValue, GUIContent label)
        {
            int newValue = camValue.intValue;
            string textValue = ((uint)newValue).ToString();
            EditorGUI.BeginChangeCheck();
            try
            {
                newValue = (int)uint.Parse(EditorGUI.TextField(position, label, textValue));
            }
            catch (OverflowException e)
            {
                if (e.Message.Equals("Negative number"))
                {
                    newValue = 0;
                }
                else
                {
                    uint tempValue = uint.MaxValue;
                    newValue = (int)tempValue;
                }
            }
            catch (Exception)
            {
                newValue = 0;
            }
            if (EditorGUI.EndChangeCheck())
            {
                camValue.intValue = newValue;
            }
        }
    }
}
