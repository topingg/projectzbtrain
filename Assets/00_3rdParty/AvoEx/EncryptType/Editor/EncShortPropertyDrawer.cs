﻿using UnityEngine;
using UnityEditor;

/* See the "http://avoex.com/unity/encrypttype/license/" for the full license governing this code. */

namespace AvoEx.EncryptType.Editor
{
    [CustomPropertyDrawer(typeof(EncShort))]
    public class EncShortPropertyDrawer : EncryptTypePropertyDrawer
    {
        protected override void EditValue(Rect position, SerializedProperty camValue, GUIContent label)
        {
            short newValue = (short)camValue.intValue;
            EditorGUI.BeginChangeCheck();
            newValue = (short)Mathf.Clamp(EditorGUI.IntField(position, label, (int)newValue), short.MinValue, short.MaxValue);
            if (EditorGUI.EndChangeCheck())
            {
                camValue.intValue = newValue;
            }
        }
    }

    [CustomPropertyDrawer(typeof(EncUShort))]
    public class EncUShortPropertyDrawer : EncryptTypePropertyDrawer
    {
        protected override void EditValue(Rect position, SerializedProperty camValue, GUIContent label)
        {
            ushort newValue = (ushort)camValue.intValue;
            EditorGUI.BeginChangeCheck();
            newValue = (ushort)Mathf.Clamp(EditorGUI.IntField(position, label, (int)newValue), ushort.MinValue, ushort.MaxValue);
            if (EditorGUI.EndChangeCheck())
            {
                camValue.intValue = newValue;
            }
        }
    }
}