﻿using UnityEngine;
using UnityEditor;

/* See the "http://avoex.com/unity/encrypttype/license/" for the full license governing this code. */

namespace AvoEx.EncryptType
{
    public abstract class EncryptTypePropertyDrawer : PropertyDrawer
    {
        protected abstract void EditValue(Rect position, SerializedProperty camValue, GUIContent label);

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // Using BeginProperty / EndProperty on the parent property means that
            // prefab override logic works on the entire property.
            EditorGUI.BeginProperty(position, label, property);

            SerializedProperty camValue = property.FindPropertyRelative("camValue");
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9 
            if (camValue == null)
                camValue = property.FindPropertyRelative("serializeValue");
#endif
            if (camValue == null)
            {
                Debug.LogError(property.name);
                return;
            }

            EditValue(position, camValue, label);

            EditorGUI.EndProperty();
        }
    }
}