----------------------------------------------------------------
EncryptType

You can apply the EncryptType to your project within five minutes
and protect from memory cheating.

optimized for Unity 5.

more informations:
http://avoex.com
https://www.facebook.com/avoexgames

----------------------------------------------------------------
How to use EncryptType

Just three steps to end.

 1. using AvoEx.EncryptType;
 2. replace system data types to encrypt types. (int -> EncInt)
 3. call EncryptManager.StartDetecting();

watch video tutorial.
https://youtu.be/yHrO5CHDrMo


----------------------------------------------------------------
EncryptTypes

 - int
 - uint
 - long
 - ulong
 - float
 - double
 - bool
 - byte
 - sbyte
 - short
 - ushort
 - char
 - decimal


----------------------------------------------------------------
version history

1.1.88
 - fixed define for Unity 4.X

1.1.75
 - moved menu item 'AvoEx > EncryptType > Create EncryptManager' to 'Tools > AvoEx > EncryptType > Create EncryptManager'
 - renamed Util/Utility.cs to Util/UtilEvent.cs

1.1.68
 - now EncryptType supports Unity 4.5

1.0.37
 - EncryptTypes use ISerializationCallbackReceiver interface
 - EncryptTypes inspector serialzed value by ISerializationCallbackReceiver
 - fixed usigned EncryptTypes inspector's input field.
 - fixed detect mode toggle button in EncryptExample.

1.0.26
 - first release