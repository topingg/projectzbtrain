using UnityEngine;
using System;

/* See the "http://avoex.com/unity/encrypttype/license/" for the full license governing this code. */

namespace AvoEx.EncryptType
{
    public interface IEncryptor<T>
    {
        T ToSystemType();
        void SetValue(T value);
    }

    public class EncryptManager : MonoSingleton<EncryptManager>
    {
        public const float MIN_INTERVAL = 0.02f;
		public const float MAX_INTERVAL = 1f;
        public const float DEFAULT_INTERVAL = MIN_INTERVAL;

        public bool autoStartDetecting = false;
        public bool quitOnCheatDetected = true;
        public static Action onDetectedCheating { get; private set; }
        static bool isDetecting = false;
        static long curKey = DateTime.Now.Ticks;

		[Range(MIN_INTERVAL, MAX_INTERVAL)]
		public float keyUpdateInterval = DEFAULT_INTERVAL;
        float nextUpdateTime = 0f;

        public static void OnDetectedCheating()
        {
            if (onDetectedCheating != null)
            {
                onDetectedCheating();
            }
            else if (EncryptManager.IsInstantiated && EncryptManager.Instance.quitOnCheatDetected)
            {
                Debug.Log("CheatDectected !!!");
                
#if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
#else
                Application.Quit();
#endif
            }
        }

        public static bool IsDetecting
        {
            get { return isDetecting; }
        }

        public static long Key
        {
            get { return curKey; }
        }

        public static void StartDetecting(Action actionDetected = null)
        {
            if (actionDetected != null)
                onDetectedCheating = actionDetected;
            else
                Instance.quitOnCheatDetected = true;
            isDetecting = true;
            Instance.nextUpdateTime = 0f;
        }

        public static void StopDetecting()
        {
            isDetecting = false;
        }

        public static float KeyUpdateInterval
        {
            get
            {
				return Instance.keyUpdateInterval;
            }
            set
            {
				Instance.keyUpdateInterval = Mathf.Clamp(value, MIN_INTERVAL, MAX_INTERVAL);
            }
        }

        void UpdateKey()
        {
            if (nextUpdateTime < Time.realtimeSinceStartup)
            {
                nextUpdateTime = Time.realtimeSinceStartup + keyUpdateInterval;
                curKey = DateTime.Now.Ticks;
            }
        }

        // Use this for initialization
        void Start()
        {
            if (autoStartDetecting)
                StartDetecting();
        }

        // Update is called once per frame
        void Update()
        {
            UpdateKey();
        }
    }
}
