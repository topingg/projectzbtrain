﻿using UnityEngine;
using System;
using System.Runtime.InteropServices;

/* See the "http://avoex.com/unity/encrypttype/license/" for the full license governing this code. */

namespace AvoEx.EncryptType
{
    public struct EncDecimal : IEncryptor<decimal>, IComparable, IComparable<EncDecimal>, IEquatable<EncDecimal>, IFormattable
    {
        [StructLayout(LayoutKind.Explicit)]
        private struct UnionValue
        {
            [FieldOffset(0)]
            public decimal orgType;

            [FieldOffset(0)]
            public long encType1;

            [FieldOffset(8)]
            public long encType2;
        }

        private decimal camValue;
        private bool detectMode;
        private long encKey;
        private decimal encValue;
        private UnionValue encryptor;

        public EncDecimal(decimal initValue)
        {
            detectMode = false;
            camValue = 0m;
            encKey = 0L;
            encValue = 0m;
            encryptor.orgType = 0m;
            encryptor.encType1 = 0L;
            encryptor.encType2 = 0L;
            Value = initValue;
        }

        private decimal Decrypt()
        {
            encryptor.orgType = encValue;
            encryptor.encType1 ^= encKey;
            encryptor.encType2 ^= encKey;
            return encryptor.orgType;
        }

        private void Encrypt(decimal value)
        {
            encKey = EncryptManager.Key;
            encryptor.orgType = value;
            encryptor.encType1 ^= encKey;
            encryptor.encType2 ^= encKey;
            encValue = encryptor.orgType;
        }

        private decimal Value
        {
            set
            {
                detectMode = EncryptManager.IsDetecting;
                if (detectMode)
                    camValue = value;
                Encrypt(value);
            }
            get
            {
                decimal decrypted = Decrypt();
                if (detectMode && camValue != decrypted)
                {
                    EncryptManager.OnDetectedCheating();
                    camValue = decrypted;
                }
                return decrypted;
            }
        }

        #region IEncryptor
        public decimal ToSystemType()
        {
            return Value;
        }

        public void SetValue(decimal value)
        {
            Value = value;
        }
        #endregion IEncryptor

        #region operator
        public static implicit operator decimal(EncDecimal x)
        {
            return x.Value;
        }
        public static implicit operator EncDecimal(decimal x)
        {
            return new EncDecimal(x);
        }

        public static EncDecimal operator ++(EncDecimal x)
        {
            x.Value = x.Value + 1;
            return x;
        }
        public static EncDecimal operator --(EncDecimal x)
        {
            x.Value = x.Value - 1;
            return x;
        }
        #endregion operator

        #region IComparable
        public int CompareTo(EncDecimal other)
        {
            return Value.CompareTo(other.Value);
        }

        public int CompareTo(object obj)
        {
            return obj is EncDecimal ? CompareTo((EncDecimal)obj) : 1;
        }
        #endregion IComparable

        #region IEquatable
        public bool Equals(EncDecimal other)
        {
            return this.Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            return obj is EncDecimal && Equals((EncDecimal)obj);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
        #endregion IEquatable

        #region IFormattable
        public override string ToString()
        {
            return Value.ToString();
        }

        public string ToString(string format)
        {
            return Value.ToString(format);
        }

        public string ToString(IFormatProvider provider)
        {
            return Value.ToString(provider);
        }

        public string ToString(string format, IFormatProvider provider)
        {
            return Value.ToString(format, provider);
        }
        #endregion IFormattable
    }
}
