﻿using UnityEngine;
using System.Collections;
using AvoEx.EncryptType;

/* See the "http://avoex.com/unity/encrypttype/license/" for the full license governing this code. */

public class EncryptExample : MonoBehaviour
{
    public EncInt hitPoint = 100;
	public EncUInt uintTest;
    public EncBool boolTest;
    public EncByte byteTest;
    public EncSByte sbyteTest;
    public EncChar charTest;
    public EncFloat floatTest;
    public EncDouble doubleTest;
    public EncShort shortTest;
    public EncUShort ushortTest;
    public EncLong longTest;
    public EncULong ulongTest;
    public static bool isDetected { get; private set; }

    void OnGUI()
    {
        if (isDetected)
        {
            GUILayout.Label("Cheating Detected !!!", GUILayout.MinWidth(400f));
            if (GUILayout.Button("Quit application", GUILayout.MinHeight(200f)))
            {
                Application.Quit();
            }
        }
        else
        {
            GUILayout.BeginArea(new Rect(10, 10, 200, 80));
            //
            GUILayout.BeginHorizontal();
            GUILayout.Label("HitPoint = ");
            GUILayout.Label(hitPoint.ToString());
            bool isDetecting = GUILayout.Toggle(EncryptManager.IsDetecting, "Detect Mode");
            if (isDetecting != EncryptManager.IsDetecting)
            {
                OnToggleDetectMode(isDetecting);
            }
            GUILayout.EndHorizontal();

            if (GUILayout.Button("Damage (1~5)"))
            {
                OnClickSubHP();
            }
            if (GUILayout.Button("Heal (+50)"))
            {
                OnClickAddHP();
            }
            //
            GUILayout.EndArea();
        }
    }

    void Start()
    {
        EncryptManager.StartDetecting(OnCheatingDetected);
    }

    //
    void OnCheatingDetected()
    {
        isDetected = true;
    }

    //
    public void OnClickSubHP()
    {
        hitPoint = Mathf.Max(0, hitPoint - Random.Range(1, 5));
    }

    public void OnClickAddHP()
    {
        hitPoint += 50;
    }

    public void OnToggleDetectMode(bool value)
    {
        if (value)
            EncryptManager.StartDetecting(OnCheatingDetected);
        else
            EncryptManager.StopDetecting();
    }

}
