﻿using UnityEngine;
using System.Collections;
using System.Diagnostics;
//using Debug = UnityEngine.Debug;
using AvoEx.EncryptType;
using UnityEngine.UI;

/* See the "http://avoex.com/unity/encrypttype/license/" for the full license governing this code. */

public class EncTypePerformanceTestUnity5 : MonoBehaviour
{
    public Text textTestCount = null;
    public Text textTestResult = null;

    public int testingCount = 5000000;

    public bool TestEncInt = true;
    public bool TestEncUInt = true;
    public bool TestEncFloat = true;
    public bool TestEncDouble = true;
    public bool TestEncLong = true;
    public bool TestEncULong = true;
    public bool TestEncDecimal = true;
    public bool TestEncBool = true;
    public bool TestEncChar = true;
    public bool TestEncByte = true;
    public bool TestEncSByte = true;
    public bool TestEncShort = true;
    public bool TestEncUShort = true;

    void Update()
    {
        if (textTestCount != null)
            textTestCount.text = "Test Count = " + testingCount.ToString();
    }

    public void TestPerformance()
    {
        ClearLog();

        if (TestEncInt)
            TestAssignment<int, EncInt>(testingCount);

        if (TestEncUInt)
            TestAssignment<uint, EncUInt>(testingCount);

        if (TestEncFloat)
            TestAssignment<float, EncFloat>(testingCount);

        if (TestEncDouble)
            TestAssignment<double, EncDouble>(testingCount);

        if (TestEncLong)
            TestAssignment<long, EncLong>(testingCount);

        if (TestEncULong)
            TestAssignment<ulong, EncULong>(testingCount);

        if (TestEncDecimal)
            TestAssignment<decimal, EncDecimal>(testingCount);

        if (TestEncBool)
            TestAssignment<bool, EncBool>(testingCount);

        if (TestEncChar)
            TestAssignment<char, EncChar>(testingCount);

        if (TestEncByte)
            TestAssignment<byte, EncByte>(testingCount);

        if (TestEncSByte)
            TestAssignment<sbyte, EncSByte>(testingCount);

        if (TestEncShort)
            TestAssignment<short, EncShort>(testingCount);

        if (TestEncUShort)
            TestAssignment<ushort, EncUShort>(testingCount);
    }

    void ClearLog()
    {
        if (textTestResult)
        {
            textTestResult.text = "";
        }
    }

    void LogTestTitle<T1, T2>()
    {
        string strLog = "---------------------------------------------\n" + typeof(T1).ToString() + " vs " + typeof(T2).ToString() + " Performance Test\n";
#if UNITY_EDITOR
        //Debug.Log(strLog);
#endif
        if (textTestResult)
        {
            textTestResult.text += strLog;
        }
    }

    void LogTestResult<T1, T2>(long ms, int count)
    {
        string strLog = "  " + typeof(T1).ToString() + " = " + typeof(T2).ToString() + "; " + count.ToString() + " count : " + ms + " ms, (" + (ms * 0.001f).ToString() + " sec)\n";
#if UNITY_EDITOR
        //Debug.Log(strLog);
#endif
        if (textTestResult)
        {
            textTestResult.text += strLog;
        }
    }

    void TestAssignment<T, EncT>(int count)
        where T : struct
        where EncT : IEncryptor<T>
    {
        LogTestTitle<T, EncT>();

        EncT encryptedType = default(EncT);
        T notEncryptedType = default(T);
        T temp = default(T);

        Stopwatch sw = Stopwatch.StartNew();
        for (int i = 0; i < count; ++i)
        {
            temp = encryptedType.ToSystemType();
        }
        sw.Stop();
        LogTestResult<T, EncT>(sw.ElapsedMilliseconds, count);

        sw.Reset();
        sw.Start();
        for (int i = 0; i < count; ++i)
        {
            encryptedType.SetValue(temp);
        }
        sw.Stop();
        LogTestResult<EncT, T>(sw.ElapsedMilliseconds, count);
        

        sw.Reset();
        sw.Start();
        for (int i = 0; i < count; ++i)
        {
            temp = notEncryptedType;
        }
        sw.Stop();
        LogTestResult<T, T>(sw.ElapsedMilliseconds, count);
    }

    public void OnSliderChanged(float value)
    {
        testingCount = (int)value;
    }

    public void OnToggleInt(bool value)
    {
        TestEncInt = value;
    }
    public void OnToggleUInt(bool value)
    {
        TestEncUInt = value;
    }

    public void OnToggleFloat(bool value)
    {
        TestEncFloat = value;
    }
    public void OnToggleDouble(bool value)
    {
        TestEncDouble = value;
    }

    public void OnToggleLong(bool value)
    {
        TestEncLong = value;
    }
    public void OnToggleULong(bool value)
    {
        TestEncULong = value;
    }

    public void OnToggleBool(bool value)
    {
        TestEncBool = value;
    }

    public void OnToggleChar(bool value)
    {
        TestEncChar = value;
    }

    public void OnToggleShort(bool value)
    {
        TestEncShort = value;
    }
    public void OnToggleUShort(bool value)
    {
        TestEncUShort = value;
    }

    public void OnToggleByte(bool value)
    {
        TestEncByte = value;
    }
    public void OnToggleSByte(bool value)
    {
        TestEncSByte = value;
    }

    public void OnToggleDecimal(bool value)
    {
        TestEncDecimal = value;
    }
}