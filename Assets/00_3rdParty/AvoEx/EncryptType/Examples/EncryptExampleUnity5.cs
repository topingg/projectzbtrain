﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using AvoEx.EncryptType;

/* See the "http://avoex.com/unity/encrypttype/license/" for the full license governing this code. */

public class EncryptExampleUnity5 : MonoBehaviour
{
    public EncInt hitPoint = 100;
	public EncUInt uintTest;
    public EncBool boolTest;
    public EncByte byteTest;
    public EncSByte sbyteTest;
    public EncChar charTest;
    public EncFloat floatTest;
    public EncDouble doubleTest;
    public EncShort shortTest;
    public EncUShort ushortTest;
    public EncLong longTest;
    public EncULong ulongTest;
    public GameObject objDetecedPopup = null;
    public Text textHP = null;

    void Start()
    {
        EncryptManager.StartDetecting(OnCheatingDetected);
    }

    // Update is called once per frame
    void Update()
    {
        if (textHP != null)
            textHP.text = hitPoint.ToString();
    }

    //
    void OnCheatingDetected()
    {
        if (objDetecedPopup != null)
            objDetecedPopup.SetActive(true);
    }

    //
    public void OnClickSubHP()
    {
        hitPoint = Mathf.Max(0, hitPoint - Random.Range(1, 5));
    }

    public void OnClickAddHP()
    {
        hitPoint += 50;
    }

    public void OnToggleDetectMode(bool value)
    {
        if (value)
            EncryptManager.StartDetecting(OnCheatingDetected);
        else
            EncryptManager.StopDetecting();
    }

    public void OnClickQuit()
    {
        if (objDetecedPopup != null)
            objDetecedPopup.SetActive(false);
        Application.Quit();
    }
}
