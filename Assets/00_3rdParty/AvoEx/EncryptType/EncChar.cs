﻿using UnityEngine;
using System;

/* See the "http://avoex.com/unity/encrypttype/license/" for the full license governing this code. */

namespace AvoEx.EncryptType
{
    [Serializable]
    public struct EncChar : IEncryptor<char>, IComparable, IComparable<EncChar>, IEquatable<EncChar>, ISerializationCallbackReceiver
    {
        [SerializeField]
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9 
        private int camValue;
#else
        private char camValue;
#endif
        private bool detectMode;
        private char encKey;
        private char encValue;

        public EncChar(char initValue)
        {
            detectMode = false;
            camValue = (char)0;
            encKey = (char)0;
            encValue = (char)0;
            Value = initValue;
        }

        private char Decrypt()
        {
            return (char)(encValue ^ encKey);
        }

        private void Encrypt(char value)
        {
            encKey = (char)EncryptManager.Key;
            encValue = (char)(value ^ encKey);
        }

        private char Value
        {
            set
            {
                detectMode = EncryptManager.IsDetecting;
                if (detectMode)
                    camValue = value;
                Encrypt(value);
            }
            get
            {
                char decrypted = Decrypt();
                if (detectMode && camValue != decrypted)
                {
                    EncryptManager.OnDetectedCheating();
                    camValue = decrypted;
                }
                return decrypted;
            }
        }

        #region IEncryptor
        public char ToSystemType()
        {
            return Value;
        }

        public void SetValue(char value)
        {
            Value = value;
        }
        #endregion IEncryptor

        #region operator
        public static implicit operator char(EncChar x)
        {
            return x.Value;
        }
        public static implicit operator EncChar(char x)
        {
            return new EncChar(x);
        }

        public static EncChar operator ++(EncChar x)
        {
            x.Value = (char)(x.Value + 1);
            return x;
        }
        public static EncChar operator --(EncChar x)
        {
            x.Value = (char)(x.Value - 1);
            return x;
        }
        #endregion operator

        #region IComparable
        public int CompareTo(EncChar other)
        {
            return Value.CompareTo(other.Value);
        }

        public int CompareTo(object obj)
        {
            return obj is EncChar ? CompareTo((EncChar)obj) : 1;
        }
        #endregion IComparable

        #region IEquatable
        public bool Equals(EncChar other)
        {
            return this.Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            return obj is EncChar && Equals((EncChar)obj);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
        #endregion IEquatable

        #region IFormattable
        public override string ToString()
        {
            return Value.ToString();
        }

        public string ToString(IFormatProvider provider)
        {
            return Value.ToString(provider);
        }
        #endregion IFormattable

        #region ISerializationCallbackReceiver
        public void OnAfterDeserialize()
        {
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9 
            Encrypt((char)camValue);
#else
            Encrypt(camValue);
#endif
        }

        public void OnBeforeSerialize()
        {
            camValue = Decrypt();
        }
        #endregion ISerializationCallbackReceiver
    }
}
