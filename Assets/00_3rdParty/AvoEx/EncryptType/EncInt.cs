﻿using UnityEngine;
using System;

/* See the "http://avoex.com/unity/encrypttype/license/" for the full license governing this code. */

namespace AvoEx.EncryptType
{
    [Serializable]
    public struct EncInt : IEncryptor<int>, IComparable, IComparable<EncInt>, IEquatable<EncInt>, IFormattable, ISerializationCallbackReceiver
    {
        [SerializeField]
        private int camValue;
        private bool detectMode;
        private int encKey;
        private int encValue;

        public EncInt(int initValue)
        {
            detectMode = false;
            camValue = 0;
            encKey = 0;
            encValue = 0;
            Value = initValue;
        }

        private int Decrypt()
        {
            return encValue ^ encKey;
        }

        private void Encrypt(int value)
        {
            encKey = (int)EncryptManager.Key;
            encValue = value ^ encKey;
        }

        private int Value
        {
            set
            {
                detectMode = EncryptManager.IsDetecting;
                if (detectMode)
                    camValue = value;
                Encrypt(value);
            }
            get
            {
                int decrypted = Decrypt();
                if (detectMode && camValue != decrypted)
                {
                    EncryptManager.OnDetectedCheating();
                    camValue = decrypted;
                }
                return decrypted;
            }
        }

        #region IEncryptor
        public int ToSystemType()
        {
            return Value;
        }

        public void SetValue(int value)
        {
            Value = value;
        }
        #endregion IEncryptor

        #region operator
        public static implicit operator int(EncInt x)
        {
            return x.Value;
        }
        public static implicit operator EncInt(int x)
        {
            return new EncInt(x);
        }

        public static EncInt operator ++(EncInt x)
        {
            x.Value = x.Value + 1;
            return x;
        }
        public static EncInt operator --(EncInt x)
        {
            x.Value = x.Value - 1;
            return x;
        }
        #endregion operator

        #region IComparable
        public int CompareTo(EncInt other)
        {
            return Value.CompareTo(other.Value);
        }

        public int CompareTo(object obj)
        {
            return obj is EncInt ? CompareTo((EncInt)obj) : 1;
        }
        #endregion IComparable

        #region IEquatable
        public bool Equals(EncInt other)
        {
            return this.Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            return obj is EncInt && Equals((EncInt)obj);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
        #endregion IEquatable

        #region IFormattable
        public override string ToString()
        {
            return Value.ToString();
        }

        public string ToString(string format)
        {
            return Value.ToString(format);
        }

        public string ToString(IFormatProvider provider)
        {
            return Value.ToString(provider);
        }

        public string ToString(string format, IFormatProvider provider)
        {
            return Value.ToString(format, provider);
        }
        #endregion IFormattable

        #region ISerializationCallbackReceiver
        public void OnAfterDeserialize()
        {
            Encrypt(camValue);
        }

        public void OnBeforeSerialize()
        {
            camValue = Decrypt();
        }
        #endregion ISerializationCallbackReceiver
    }

    [Serializable]
    public struct EncUInt : IEncryptor<uint>, IComparable, IComparable<EncUInt>, IEquatable<EncUInt>, IFormattable, ISerializationCallbackReceiver
    {
        [SerializeField]
        private int camValue;
        private bool detectMode;
        private int encKey;
        private int encValue;

        public EncUInt(uint initValue)
        {
            detectMode = false;
            camValue = 0;
            encKey = 0;
            encValue = 0;
            Value = initValue;
        }

        private int Decrypt()
        {
            return encValue ^ encKey;
        }

        private void Encrypt(int value)
        {
            encKey = (int)EncryptManager.Key;
            encValue = value ^ encKey;
        }

        private uint Value
        {
            set
            {
                detectMode = EncryptManager.IsDetecting;
                if (detectMode)
                    camValue = (int)value;
                Encrypt((int)value);
            }
            get
            {
                int decrypted = Decrypt();
                if (detectMode && camValue != decrypted)
                {
                    EncryptManager.OnDetectedCheating();
                    camValue = decrypted;
                }
                return (uint)decrypted;
            }
        }

        #region IEncryptor
        public uint ToSystemType()
        {
            return Value;
        }

        public void SetValue(uint value)
        {
            Value = value;
        }
        #endregion IEncryptor

        #region operator
        public static implicit operator uint(EncUInt x)
        {
            return x.Value;
        }
        public static implicit operator EncUInt(uint x)
        {
            return new EncUInt(x);
        }

        public static EncUInt operator ++(EncUInt x)
        {
            x.Value = x.Value + 1;
            return x;
        }
        public static EncUInt operator --(EncUInt x)
        {
            x.Value = x.Value - 1;
            return x;
        }
        #endregion operator

        #region IComparable
        public int CompareTo(EncUInt other)
        {
            return Value.CompareTo(other.Value);
        }

        public int CompareTo(object obj)
        {
            return obj is EncUInt ? CompareTo((EncUInt)obj) : 1;
        }
        #endregion IComparable

        #region IEquatable
        public bool Equals(EncUInt other)
        {
            return this.Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            return obj is EncUInt && Equals((EncUInt)obj);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
        #endregion IEquatable

        #region IFormattable
        public override string ToString()
        {
            return Value.ToString();
        }

        public string ToString(string format)
        {
            return Value.ToString(format);
        }

        public string ToString(IFormatProvider provider)
        {
            return Value.ToString(provider);
        }

        public string ToString(string format, IFormatProvider provider)
        {
            return Value.ToString(format, provider);
        }
        #endregion IFormattable

        #region ISerializationCallbackReceiver
        public void OnAfterDeserialize()
        {
            Encrypt(camValue);
        }

        public void OnBeforeSerialize()
        {
            camValue = Decrypt();
        }
        #endregion ISerializationCallbackReceiver
    }
}
