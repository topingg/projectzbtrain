﻿using UnityEngine;
using System;
using System.Runtime.InteropServices;

/* See the "http://avoex.com/unity/encrypttype/license/" for the full license governing this code. */

namespace AvoEx.EncryptType
{
    [Serializable]
    public struct EncFloat : IEncryptor<float>, IComparable, IComparable<EncFloat>, IEquatable<EncFloat>, IFormattable, ISerializationCallbackReceiver
    {
        [StructLayout(LayoutKind.Explicit)]
        private struct UnionValue
        {
            [FieldOffset(0)]
            public float orgType;

            [FieldOffset(0)]
            public int encType;
        }

        [SerializeField]
        private float camValue;
        private bool detectMode;
        private int encKey;
#if OLD
        private float encValue;
#else
        private int encValue;
#endif
        private UnionValue encryptor;

        public EncFloat(float initValue)
        {
            detectMode = false;
            camValue = 0f;
            encKey = 0;
            encValue = 0;
            encryptor.orgType = 0f;
            encryptor.encType = 0;
            Value = initValue;
        }

        private float Decrypt()
        {
#if OLD
            encryptor.orgType = encValue;
            encryptor.encType ^= encKey;
#else
            encryptor.encType = encValue ^ encKey;
#endif
            return encryptor.orgType;
        }

        private void Encrypt(float value)
        {
            encKey = (int)EncryptManager.Key;
            encryptor.orgType = value;
            encryptor.encType ^= encKey;
#if OLD
            encValue = encryptor.orgType;
#else
            encValue = encryptor.encType;
#endif
        }

        private float Value
        {
            set
            {
                detectMode = EncryptManager.IsDetecting;
                if (detectMode)
                    camValue = value;
                Encrypt(value);
            }
            get
            {
                float decrypted = Decrypt();
                if (detectMode && camValue != decrypted)
                {
                    EncryptManager.OnDetectedCheating();
                    camValue = decrypted;
                }
                return decrypted;
            }
        }

        #region IEncryptor
        public float ToSystemType()
        {
            return Value;
        }

        public void SetValue(float value)
        {
            Value = value;
        }
        #endregion IEncryptor

        #region operator
        public static implicit operator float(EncFloat x)
        {
            return x.Value;
        }
        public static implicit operator EncFloat(float x)
        {
            return new EncFloat(x);
        }

        public static EncFloat operator ++(EncFloat x)
        {
            x.Value = x.Value + 1f;
            return x;
        }
        public static EncFloat operator --(EncFloat x)
        {
            x.Value = x.Value - 1f;
            return x;
        }
        #endregion operator

        #region IComparable
        public int CompareTo(EncFloat other)
        {
            return Value.CompareTo(other.Value);
        }

        public int CompareTo(object obj)
        {
            return obj is EncFloat ? CompareTo((EncFloat)obj) : 1;
        }
        #endregion IComparable

        #region IEquatable
        public bool Equals(EncFloat other)
        {
            return this.Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            return obj is EncFloat && Equals((EncFloat)obj);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
        #endregion IEquatable

        #region IFormattable
        public override string ToString()
        {
            return Value.ToString();
        }

        public string ToString(string format)
        {
            return Value.ToString(format);
        }

        public string ToString(IFormatProvider provider)
        {
            return Value.ToString(provider);
        }

        public string ToString(string format, IFormatProvider provider)
        {
            return Value.ToString(format, provider);
        }
        #endregion IFormattable

        #region ISerializationCallbackReceiver
        public void OnAfterDeserialize()
        {
            Encrypt(camValue);
        }

        public void OnBeforeSerialize()
        {
            camValue = Decrypt();
        }
        #endregion ISerializationCallbackReceiver
    }


    [Serializable]
    public struct EncDouble : IEncryptor<double>, IComparable, IComparable<EncDouble>, IEquatable<EncDouble>, IFormattable, ISerializationCallbackReceiver
    {
        [StructLayout(LayoutKind.Explicit)]
        private struct UnionValue
        {
            [FieldOffset(0)]
            public double orgType;

            [FieldOffset(0)]
            public long encType;
        }

#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
        [SerializeField]
        private string serializeValue;
#else
        [SerializeField]
#endif
        private double camValue;
        private bool detectMode;
        private long encKey;
#if OLD
        private double encValue;
#else
        private long encValue;
#endif
        private UnionValue encryptor;

        public EncDouble(double initValue)
        {
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
            serializeValue = "0";
#endif
            detectMode = false;
            camValue = 0D;
            encKey = 0L;
            encValue = 0L;
            encryptor.orgType = 0D;
            encryptor.encType = 0L;
            Value = initValue;
        }

        private double Decrypt()
        {
#if OLD
            encryptor.orgType = encValue;
            encryptor.encType ^= encKey;
#else
            encryptor.encType = encValue ^ encKey;
#endif
            return encryptor.orgType;
        }

        private void Encrypt(double value)
        {
            encKey = EncryptManager.Key;
            encryptor.orgType = value;
            encryptor.encType ^= encKey;
#if OLD
            encValue = encryptor.orgType;
#else
            encValue = encryptor.encType;
#endif
        }

        private double Value
        {
            set
            {
                detectMode = EncryptManager.IsDetecting;
                if (detectMode)
                    camValue = value;
                Encrypt(value);
            }
            get
            {
                double decrypted = Decrypt();
                if (detectMode && camValue != decrypted)
                {
                    EncryptManager.OnDetectedCheating();
                    camValue = decrypted;
                }
                return decrypted;
            }
        }

        #region IEncryptor
        public double ToSystemType()
        {
            return Value;
        }

        public void SetValue(double value)
        {
            Value = value;
        }
        #endregion IEncryptor

        #region operator
        public static implicit operator double(EncDouble x)
        {
            return x.Value;
        }
        public static implicit operator EncDouble(double x)
        {
            return new EncDouble(x);
        }

        public static EncDouble operator ++(EncDouble x)
        {
            x.Value = x.Value + 1D;
            return x;
        }
        public static EncDouble operator --(EncDouble x)
        {
            x.Value = x.Value - 1D;
            return x;
        }
        #endregion operator

        #region IComparable
        public int CompareTo(EncDouble other)
        {
            return Value.CompareTo(other.Value);
        }

        public int CompareTo(object obj)
        {
            return obj is EncDouble ? CompareTo((EncDouble)obj) : 1;
        }
        #endregion IComparable

        #region IEquatable
        public bool Equals(EncDouble other)
        {
            return this.Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            return obj is EncDouble && Equals((EncDouble)obj);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
        #endregion IEquatable

        #region IFormattable
        public override string ToString()
        {
            return Value.ToString();
        }

        public string ToString(string format)
        {
            return Value.ToString(format);
        }

        public string ToString(IFormatProvider provider)
        {
            return Value.ToString(provider);
        }

        public string ToString(string format, IFormatProvider provider)
        {
            return Value.ToString(format, provider);
        }
        #endregion IFormattable

        #region ISerializationCallbackReceiver
        public void OnAfterDeserialize()
        {
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
            camValue = double.Parse(serializeValue);
#endif
            Encrypt(camValue);
        }

        public void OnBeforeSerialize()
        {
            camValue = Decrypt();
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
            serializeValue = camValue.ToString();
#endif
        }
        #endregion ISerializationCallbackReceiver
    }
}
