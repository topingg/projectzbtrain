﻿using UnityEngine;
using System;

/* See the "http://avoex.com/unity/encrypttype/license/" for the full license governing this code. */

namespace AvoEx.EncryptType
{
    [Serializable]
    public struct EncBool : IEncryptor<bool>, IComparable, IComparable<EncBool>, IEquatable<EncBool>, ISerializationCallbackReceiver
    {
        [SerializeField]
        private bool camValue;
        private bool detectMode;
        private bool encKey;
        private bool encValue;

        public EncBool(bool initValue)
        {
            detectMode = false;
            camValue = false;
            encKey = false;
            encValue = false;
            Value = initValue;
        }

        private bool Decrypt()
        {
            return encKey ? !encValue : encValue;
        }

        private void Encrypt(bool value)
        {
            encKey = (EncryptManager.Key & 0x01) == 0x01;
            encValue = encKey ? !value : value;
        }

        private bool Value
        {
            set
            {
                detectMode = EncryptManager.IsDetecting;
                if (detectMode)
                    camValue = value;
                Encrypt(value);
            }
            get
            {
                bool decrypted = Decrypt();
                if (detectMode && camValue != decrypted)
                {
                    EncryptManager.OnDetectedCheating();
                    camValue = decrypted;
                }
                return decrypted;
            }
        }

        #region IEncryptor
        public bool ToSystemType()
        {
            return Value;
        }

        public void SetValue(bool value)
        {
            Value = value;
        }
        #endregion IEncryptor

        #region operator
        public static implicit operator bool(EncBool x)
        {
            return x.Value;
        }
        public static implicit operator EncBool(bool x)
        {
            return new EncBool(x);
        }
        #endregion operator

        #region IComparable
        public int CompareTo(EncBool other)
        {
            return Value.CompareTo(other.Value);
        }

        public int CompareTo(object obj)
        {
            return obj is EncBool ? CompareTo((EncBool)obj) : 1;
        }
        #endregion IComparable

        #region IEquatable
        public bool Equals(EncBool other)
        {
            return this.Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            return obj is EncBool && Equals((EncBool)obj);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
        #endregion IEquatable

        #region IFormattable
        public override string ToString()
        {
            return Value.ToString();
        }

        public string ToString(IFormatProvider provider)
        {
            return Value.ToString(provider);
        }
        #endregion

        #region ISerializationCallbackReceiver
        public void OnAfterDeserialize()
        {
            Encrypt(camValue);
        }

        public void OnBeforeSerialize()
        {
            camValue = Decrypt();
        }
        #endregion ISerializationCallbackReceiver
    }
}
