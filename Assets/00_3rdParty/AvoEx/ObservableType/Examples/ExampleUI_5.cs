﻿#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
#else
using UnityEngine;
using UnityEngine.UI;
using AvoEx.ObservableType;

/* See the "http://avoex.com/?p=30" for the full license governing this code. */

public class ExampleUI_5 : MonoBehaviour
{
    public ExamplePlayer_5 curPlayer = null;
    public Text textHitPoint = null;
    public bool isDelete = false;

    void OnDestroy()
    {
    }

    // Use this for initialization
    void Start()
    {
        if (curPlayer != null)
            curPlayer.playerHP.AddObserver(OnChangedHp, true);

        if (isDelete)
            Destroy(gameObject);
    }

    void OnChangedHp(ObFloat hitPoint)
    {
        Debug.Log("ObservableTypeExample.OnChangedHp( " + hitPoint.Value.ToString() + " )");
        if (textHitPoint != null)
            textHitPoint.text = hitPoint.Value.ToString();
    }
}
#endif