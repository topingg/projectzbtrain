﻿#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
#else
using UnityEngine;
using UnityEngine.UI;
using AvoEx.ObservableType;

/* See the "http://avoex.com/?p=30" for the full license governing this code. */

public class ExamplePlayer_5 : MonoBehaviour
{
    const int MAX_HITPOINT = 100;
    public ObFloat playerHP = new ObFloat(MAX_HITPOINT);
    public Slider sliderHitPoint = null;

    //
    void Start()
    {
        playerHP.AddObserver(OnChangedHp, true);
    }

    // 
    public void Hit()
    {
        playerHP.Value = playerHP - 10;
        if (playerHP < 0)
            playerHP.Value = 0;
    }

    public void Heal()
    {
        playerHP.Value = Mathf.Min(playerHP + 20, MAX_HITPOINT);
    }

    void OnChangedHp(ObFloat hitPoint)
    {
        Debug.Log("ExamplePlayer.OnChangedHp( " + hitPoint.Value.ToString() + " )");
        if (sliderHitPoint != null)
            sliderHitPoint.value = hitPoint.Value;
    }
}
#endif