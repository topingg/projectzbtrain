﻿using UnityEngine;
using System;
#if ENCRYPT_TYPE
using AvoEx.EncryptType;
#endif

/* See the "http://avoex.com/avoex/default-license/" for the full license governing this code. */

namespace AvoEx.ObservableType
{
    [Serializable]
    public class ObByte : ObservableType<byte, ObByte>
    {
        public ObByte(byte initValue = 0)
            : base(initValue)
        {
        }

        #region explicit_castings
        public static explicit operator float(ObByte x)
        {
            return (float)x.Value;
        }
        public static explicit operator double(ObByte x)
        {
            return (double)x.Value;
        }
        public static explicit operator long(ObByte x)
        {
            return (long)x.Value;
        }
        public static explicit operator ulong(ObByte x)
        {
            return (ulong)x.Value;
        }
        public static explicit operator int(ObByte x)
        {
            return (int)x.Value;
        }
        public static explicit operator uint(ObByte x)
        {
            return (uint)x.Value;
        }
        public static explicit operator short(ObByte x)
        {
            return (short)x.Value;
        }
        public static explicit operator ushort(ObByte x)
        {
            return (ushort)x.Value;
        }
        public static explicit operator byte(ObByte x)
        {
            return (byte)x.Value;
        }
        public static explicit operator sbyte(ObByte x)
        {
            return (sbyte)x.Value;
        }
        #endregion explicit_castings

        #region unary_operators
        public static ObByte operator ++(ObByte x)
        {
            x.Value = (byte)(x.Value + 1);
            return x;
        }
        public static ObByte operator --(ObByte x)
        {
            x.Value = (byte)(x.Value - 1);
            return x;
        }
        #endregion unary_operators

        public override bool Equals(byte newValue)
        {
            return (curValue == newValue);
        }
    }

    [Serializable]
    public class ObSByte : ObservableType<sbyte, ObSByte>
    {
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
        [SerializeField]
        int serializeValue;

        public override void OnAfterDeserialize()
        {
            preValue = (sbyte)UtilBytes.HiWord(serializeValue);
            curValue = (sbyte)UtilBytes.LoWord(serializeValue);
            OnChanged();
        }

        public override void OnBeforeSerialize()
        {
            serializeValue = UtilBytes.MakeLong(curValue, preValue);
        }
#endif
        public ObSByte(sbyte initValue = 0)
            : base(initValue)
        {
        }

        #region explicit_castings
        public static explicit operator float(ObSByte x)
        {
            return (float)x.Value;
        }
        public static explicit operator double(ObSByte x)
        {
            return (double)x.Value;
        }
        public static explicit operator long(ObSByte x)
        {
            return (long)x.Value;
        }
        public static explicit operator ulong(ObSByte x)
        {
            return (ulong)x.Value;
        }
        public static explicit operator int(ObSByte x)
        {
            return (int)x.Value;
        }
        public static explicit operator uint(ObSByte x)
        {
            return (uint)x.Value;
        }
        public static explicit operator short(ObSByte x)
        {
            return (short)x.Value;
        }
        public static explicit operator ushort(ObSByte x)
        {
            return (ushort)x.Value;
        }
        public static explicit operator byte(ObSByte x)
        {
            return (byte)x.Value;
        }
        public static explicit operator sbyte(ObSByte x)
        {
            return (sbyte)x.Value;
        }
        #endregion explicit_castings

        #region unary_operators
        public static ObSByte operator ++(ObSByte x)
        {
            x.Value = (sbyte)(x.Value + 1);
            return x;
        }
        public static ObSByte operator --(ObSByte x)
        {
            x.Value = (sbyte)(x.Value - 1);
            return x;
        }
        #endregion unary_operators

        public override bool Equals(sbyte newValue)
        {
            return (curValue == newValue);
        }
    }

#if ENCRYPT_TYPE
    [Serializable]
    public class ObEncByte : ObservableType<byte, EncByte, ObEncByte>
    {
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
        [SerializeField]
        int serializeValue;

        public override void OnAfterDeserialize()
        {
            preValue = (byte)UtilBytes.HiWord(serializeValue);
            curValue = (byte)UtilBytes.LoWord(serializeValue);
            OnChanged();
        }

        public override void OnBeforeSerialize()
        {
            serializeValue = UtilBytes.MakeLong(curValue, preValue);
        }
#endif
        public ObEncByte(byte initValue = 0)
            : base(initValue)
        {
        }

        #region explicit_castings
        public static explicit operator float(ObEncByte x)
        {
            return (float)x.Value;
        }
        public static explicit operator double(ObEncByte x)
        {
            return (double)x.Value;
        }
        public static explicit operator long(ObEncByte x)
        {
            return (long)x.Value;
        }
        public static explicit operator ulong(ObEncByte x)
        {
            return (ulong)x.Value;
        }
        public static explicit operator int(ObEncByte x)
        {
            return (int)x.Value;
        }
        public static explicit operator uint(ObEncByte x)
        {
            return (uint)x.Value;
        }
        public static explicit operator short(ObEncByte x)
        {
            return (short)x.Value;
        }
        public static explicit operator ushort(ObEncByte x)
        {
            return (ushort)x.Value;
        }
        public static explicit operator byte(ObEncByte x)
        {
            return (byte)x.Value;
        }
        public static explicit operator sbyte(ObEncByte x)
        {
            return (sbyte)x.Value;
        }
        #endregion explicit_castings

        #region unary_operators
        public static ObEncByte operator ++(ObEncByte x)
        {
            x.Value = (byte)(x.Value + 1);
            return x;
        }
        public static ObEncByte operator --(ObEncByte x)
        {
            x.Value = (byte)(x.Value - 1);
            return x;
        }
        #endregion unary_operators

        public override bool Equals(byte newValue)
        {
            return (curValue == newValue);
        }
    }

    [Serializable]
    public class ObEncSByte : ObservableType<sbyte, EncSByte, ObEncSByte>
    {
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
        [SerializeField]
        int serializeValue;

        public override void OnAfterDeserialize()
        {
            preValue = (sbyte)UtilBytes.HiWord(serializeValue);
            curValue = (sbyte)UtilBytes.LoWord(serializeValue);
            OnChanged();
        }

        public override void OnBeforeSerialize()
        {
            serializeValue = UtilBytes.MakeLong(curValue, preValue);
        }
#endif

        public ObEncSByte(sbyte initValue = 0)
            : base(initValue)
        {
        }

        #region explicit_castings
        public static explicit operator float(ObEncSByte x)
        {
            return (float)x.Value;
        }
        public static explicit operator double(ObEncSByte x)
        {
            return (double)x.Value;
        }
        public static explicit operator long(ObEncSByte x)
        {
            return (long)x.Value;
        }
        public static explicit operator ulong(ObEncSByte x)
        {
            return (ulong)x.Value;
        }
        public static explicit operator int(ObEncSByte x)
        {
            return (int)x.Value;
        }
        public static explicit operator uint(ObEncSByte x)
        {
            return (uint)x.Value;
        }
        public static explicit operator short(ObEncSByte x)
        {
            return (short)x.Value;
        }
        public static explicit operator ushort(ObEncSByte x)
        {
            return (ushort)x.Value;
        }
        public static explicit operator byte(ObEncSByte x)
        {
            return (byte)x.Value;
        }
        public static explicit operator sbyte(ObEncSByte x)
        {
            return (sbyte)x.Value;
        }
        #endregion explicit_castings

        #region unary_operators
        public static ObEncSByte operator ++(ObEncSByte x)
        {
            x.Value = (sbyte)(x.Value + 1);
            return x;
        }
        public static ObEncSByte operator --(ObEncSByte x)
        {
            x.Value = (sbyte)(x.Value - 1);
            return x;
        }
        #endregion unary_operators

        public override bool Equals(sbyte newValue)
        {
            return (curValue == newValue);
        }
    }
#endif
}