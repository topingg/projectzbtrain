﻿using UnityEngine;
using UnityEditor;
using System;

/* See the "http://avoex.com/avoex/default-license/" for the full license governing this code. */

namespace AvoEx.ObservableType
{
    [CustomPropertyDrawer(typeof(ObInt))]
    public class ObIntPropertyDrawer : ObservableTypePropertyDrawer<int, ObInt>
    {
        protected override int PreValue
        {
            get { return preProperty.intValue; }
        }

        protected override int CurValue
        {
            get { return curProperty.intValue; }
        }

        protected override void SetCurValue(int editValue)
        {
            preProperty.intValue = curProperty.intValue;
            curProperty.intValue = editValue;
        }

        protected override int EditValue(GUIContent label, int editValue)
        {
            return EditorGUI.IntField(rtCurValue, label, editValue);
        }
    }

    [CustomPropertyDrawer(typeof(ObUInt))]
    public class ObUIntPropertyDrawer : ObservableTypePropertyDrawer<int, ObUInt>
    {
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9        
        protected override void InitProperties(SerializedProperty property)
        {
            preProperty = property.FindPropertyRelative("serializePreValue");
            curProperty = property.FindPropertyRelative("serializeValue");
        }
#endif
        protected override int PreValue
        {
            get { return preProperty.intValue; }
        }

        protected override int CurValue
        {
            get { return curProperty.intValue; }
        }

        protected override void SetCurValue(int editValue)
        {
            preProperty.intValue = curProperty.intValue;
            curProperty.intValue = editValue;
        }

        protected override int EditValue(GUIContent label, int editValue)
        {
            string textValue = ((uint)editValue).ToString();
            try
            {
                return (int)uint.Parse(EditorGUI.TextField(rtCurValue, label, textValue));
            }
            catch (OverflowException e)
            {
                if (e.Message.Equals("Negative number"))
                {
                    return 0;
                }
                else
                {
                    uint tempValue = uint.MaxValue;
                    return (int)tempValue;
                }
            }
            catch (Exception)
            {
                return CurValue;
            }
        }
    }

#if ENCRYPT_TYPE
    [CustomPropertyDrawer(typeof(ObEncInt))]
    public class ObEncIntPropertyDrawer : ObservableTypePropertyDrawer<int, ObEncInt>
    {
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
        protected override void InitProperties(SerializedProperty property)
        {
            preProperty = property.FindPropertyRelative("preValue");
            camProperty = property.FindPropertyRelative("serializeValue");
        }
#endif
        protected override int PreValue
        {
            get { return preProperty.intValue; }
        }

        protected override int CurValue
        {
            get { return camProperty.intValue; }
        }

        protected override void SetCurValue(int editValue)
        {
            preProperty.intValue = camProperty.intValue;
            camProperty.intValue = editValue;
        }

        protected override int EditValue(GUIContent label, int editValue)
        {
            return EditorGUI.IntField(rtCurValue, label, editValue);
        }
    }

    [CustomPropertyDrawer(typeof(ObEncUInt))]
    public class ObEncUIntPropertyDrawer : ObservableTypePropertyDrawer<int, ObEncUInt>
    {
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9        
        protected override void InitProperties(SerializedProperty property)
        {
            preProperty = property.FindPropertyRelative("serializePreValue");
            camProperty = property.FindPropertyRelative("serializeValue");
        }
#endif
        protected override int PreValue
        {
            get { return preProperty.intValue; }
        }

        protected override int CurValue
        {
            get { return camProperty.intValue; }
        }

        protected override void SetCurValue(int editValue)
        {
            preProperty.intValue = camProperty.intValue;
            camProperty.intValue = editValue;
        }

        protected override int EditValue(GUIContent label, int editValue)
        {
            string textValue = ((uint)editValue).ToString();
            try
            {
                return (int)uint.Parse(EditorGUI.TextField(rtCurValue, label, textValue));
            }
            catch (OverflowException e)
            {
                if (e.Message.Equals("Negative number"))
                {
                    return 0;
                }
                else
                {
                    uint tempValue = uint.MaxValue;
                    return (int)tempValue;
                }
            }
            catch (Exception)
            {
                return CurValue;
            }
        }
    }
#endif
}