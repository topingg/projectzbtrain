﻿using UnityEngine;
using UnityEditor;

/* See the "http://avoex.com/avoex/default-license/" for the full license governing this code. */

namespace AvoEx.ObservableType
{
	public abstract class ObservableTypePropertyDrawer<T, thisT> : PropertyDrawer
		where T : struct
		where thisT : Observable<thisT>
	{
		bool started = false;
		T editValue;

		const float paddingWidth = 20f;
		protected Rect rtCurValue;
		Rect rtPreValue;
        protected SerializedProperty preProperty;
        protected SerializedProperty curProperty;
        protected SerializedProperty camProperty;

		void AdjustRect(Rect position)
		{
			float preWidth = position.width * 0.3f;
            float curWidth = position.width - preWidth;
			rtCurValue = new Rect(position.x, position.y, curWidth - paddingWidth, position.height);
			rtPreValue = new Rect(curWidth, position.y, preWidth, position.height);
		}

		protected abstract T PreValue { get; }
        protected abstract T CurValue { get; }
		protected abstract void SetCurValue(T editValue);
        protected abstract T EditValue(GUIContent label, T editValue);

		

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			// Using BeginProperty / EndProperty on the parent property means that
			// prefab override logic works on the entire property.
			EditorGUI.BeginProperty(position, label, property);

            InitProperties(property);
		
			AdjustRect(position);
			
			if (started == false)
			{
				started = true;
				editValue = CurValue;
			}

			bool isEnterPressed = UtilEvent.IsEnterPressed();

            // curValue
			if (CurValue.Equals(editValue) == false)
			{
				if (typeof(T) == typeof(bool) || isEnterPressed || Event.current.rawType == EventType.MouseUp)
				{
					SetCurValue(editValue);
				}
			}

			editValue = EditValue(label, editValue);
			
			// preValue
			EditorGUI.LabelField(rtPreValue, "preValue = " + PreValue.ToString());
			
			EditorGUI.EndProperty();
		}

        protected virtual void InitProperties(SerializedProperty property)
        {
            preProperty = property.FindPropertyRelative("preValue");
            curProperty = property.FindPropertyRelative("curValue");
            camProperty = curProperty.FindPropertyRelative("camValue");
        }
	}

    public class ObservableTypeEditor
    {
        public const string ENCRYPT_SYMBOL = @"ENCRYPT_TYPE";

        [MenuItem("Tools/AvoEx/ObservableType/Use with EncryptType")]
        public static void UseEncryptType()
        {
            bool isExist = false;
            foreach (System.Reflection.Assembly assembly in System.AppDomain.CurrentDomain.GetAssemblies())
            {
                var type = assembly.GetType("AvoEx.EncryptType.EncryptManager");
                if (type != null)
                    isExist = true;
            }

            if (isExist == false)
            {
                Debug.LogError("Not exist EncryptType");
                return;
            }

            string currentSymbols = PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup);
            if (System.Text.RegularExpressions.Regex.IsMatch(currentSymbols, @"\b"+ENCRYPT_SYMBOL+@"\b"))
                return;
            if (currentSymbols.EndsWith(";"))
                currentSymbols += ENCRYPT_SYMBOL;
            else if (currentSymbols.Length > 0)
                currentSymbols += ";" + ENCRYPT_SYMBOL;
            else
                currentSymbols = ENCRYPT_SYMBOL;

            PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup, currentSymbols);
        }
    }
}