﻿using UnityEngine;
using UnityEditor;

/* See the "http://avoex.com/avoex/default-license/" for the full license governing this code. */

namespace AvoEx.ObservableType
{
	[CustomPropertyDrawer(typeof(ObFloat))]
	public class ObFloatPropertyDrawer : ObservableTypePropertyDrawer<float, ObFloat>
	{
		protected override float PreValue
		{
			get { return preProperty.floatValue; }
		}
		
		protected override float CurValue
		{
			get { return curProperty.floatValue; }
		}
		
		protected override void SetCurValue(float editValue)
		{
			preProperty.floatValue = curProperty.floatValue;
			curProperty.floatValue = editValue;
		}
		
		protected override float EditValue(GUIContent label, float editValue)
		{
			return EditorGUI.FloatField(rtCurValue, label, editValue);
		}
	}
	
	[CustomPropertyDrawer(typeof(ObDouble))]
	public class ObDoublePropertyDrawer : ObservableTypePropertyDrawer<double, ObDouble>
	{
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9        
        protected override void InitProperties(SerializedProperty property)
        {
            preProperty = property.FindPropertyRelative("serializePreValue");
            curProperty = property.FindPropertyRelative("serializeValue");
        }

        protected override double PreValue
        {
            get { return double.Parse(preProperty.stringValue); }
        }

        protected override double CurValue
        {
            get { return double.Parse(curProperty.stringValue); }
        }

        protected override void SetCurValue(double editValue)
        {
            preProperty.stringValue = curProperty.stringValue;
            curProperty.stringValue = editValue.ToString();
        }

        protected override double EditValue(GUIContent label, double editValue)
        {
            double newValue;
            if (double.TryParse(EditorGUI.TextField(rtCurValue, label, editValue.ToString()), out newValue))
            {
                return newValue;
            }

            return CurValue;
        }
#else
		protected override double PreValue
		{
			get { return preProperty.doubleValue; }
		}
		
		protected override double CurValue
		{
			get { return curProperty.doubleValue; }
		}
		
		protected override void SetCurValue(double editValue)
		{
			preProperty.doubleValue = curProperty.doubleValue;
			curProperty.doubleValue = editValue;
		}
		
		protected override double EditValue(GUIContent label, double editValue)
		{
			return EditorGUI.DoubleField(rtCurValue, label, editValue);
		}
#endif
	}

#if ENCRYPT_TYPE
	[CustomPropertyDrawer(typeof(ObEncFloat))]
	public class ObEncFloatPropertyDrawer : ObservableTypePropertyDrawer<float, ObEncFloat>
	{
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9        
        protected override void InitProperties(SerializedProperty property)
        {
            preProperty = property.FindPropertyRelative("preValue");
            curProperty = property.FindPropertyRelative("serializeValue");
        }

        protected override float PreValue
        {
            get { return preProperty.floatValue; }
        }

        protected override float CurValue
        {
            get { return curProperty.floatValue; }
        }

        protected override void SetCurValue(float editValue)
        {
            preProperty.floatValue = curProperty.floatValue;
            curProperty.floatValue = editValue;
        }
#else
		protected override float PreValue
		{
			get { return preProperty.floatValue; }
		}
		
		protected override float CurValue
		{
			get { return camProperty.floatValue; }
		}
		
		protected override void SetCurValue(float editValue)
		{
			preProperty.floatValue = camProperty.floatValue;
			camProperty.floatValue = editValue;
		}
#endif
		
		protected override float EditValue(GUIContent label, float editValue)
		{
			return EditorGUI.FloatField(rtCurValue, label, editValue);
		}
	}
	
	[CustomPropertyDrawer(typeof(ObEncDouble))]
	public class ObEncDoublePropertyDrawer : ObservableTypePropertyDrawer<double, ObEncDouble>
	{
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9        
        protected override void InitProperties(SerializedProperty property)
        {
            preProperty = property.FindPropertyRelative("serializePreValue");
            curProperty = property.FindPropertyRelative("serializeValue");
        }

        protected override double PreValue
        {
            get { return double.Parse(preProperty.stringValue); }
        }

        protected override double CurValue
        {
            get { return double.Parse(curProperty.stringValue); }
        }

        protected override void SetCurValue(double editValue)
        {
            preProperty.stringValue = curProperty.stringValue;
            curProperty.stringValue = editValue.ToString();
        }

        protected override double EditValue(GUIContent label, double editValue)
        {
            double newValue;
            if (double.TryParse(EditorGUI.TextField(rtCurValue, label, editValue.ToString()), out newValue))
            {
                return newValue;
            }

            return CurValue;
        }
#else
		protected override double PreValue
		{
			get { return preProperty.doubleValue; }
		}
		
		protected override double CurValue
		{
			get { return camProperty.doubleValue; }
		}
		
		protected override void SetCurValue(double editValue)
		{
			preProperty.doubleValue = camProperty.doubleValue;
			camProperty.doubleValue = editValue;
		}
		
		protected override double EditValue(GUIContent label, double editValue)
		{
			return EditorGUI.DoubleField(rtCurValue, label, editValue);
		}
#endif
	}
#endif
}