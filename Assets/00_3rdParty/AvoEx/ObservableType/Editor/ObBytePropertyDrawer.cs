﻿using UnityEngine;
using UnityEditor;
using System;

/* See the "http://avoex.com/avoex/default-license/" for the full license governing this code. */

namespace AvoEx.ObservableType
{
	[CustomPropertyDrawer(typeof(ObByte))]
	public class ObBytePropertyDrawer : ObservableTypePropertyDrawer<byte, ObByte>
	{
		protected override byte PreValue
		{
			get { return (byte)preProperty.intValue; }
		}
		
		protected override byte CurValue
		{
			get { return (byte)curProperty.intValue; }
		}
		
		protected override void SetCurValue(byte editValue)
		{
			preProperty.intValue = curProperty.intValue;
			curProperty.intValue = editValue;
		}
		
		protected override byte EditValue(GUIContent label, byte editValue)
		{
			return (byte)Mathf.Clamp(EditorGUI.IntField(rtCurValue, label, editValue), byte.MinValue, byte.MaxValue);
		}
	}
	
	[CustomPropertyDrawer(typeof(ObSByte))]
	public class ObSBytePropertyDrawer : ObservableTypePropertyDrawer<sbyte, ObSByte>
	{
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
        protected override void InitProperties(SerializedProperty property)
        {
            curProperty = property.FindPropertyRelative("serializeValue");
        }

        protected override sbyte PreValue
        {
            get { return (sbyte)UtilBytes.HiWord(curProperty.intValue); }
        }

        protected override sbyte CurValue
        {
            get { return (sbyte)UtilBytes.LoWord(curProperty.intValue); }
        }

        protected override void SetCurValue(sbyte editValue)
        {
            curProperty.intValue = UtilBytes.MakeLong(editValue, CurValue);
        }
#else
		protected override sbyte PreValue
		{
			get { return (sbyte)preProperty.intValue; }
		}
		
		protected override sbyte CurValue
		{
			get { return (sbyte)curProperty.intValue; }
		}
		
		protected override void SetCurValue(sbyte editValue)
		{
			preProperty.intValue = curProperty.intValue;
			curProperty.intValue = editValue;
		}
#endif
		
		protected override sbyte EditValue(GUIContent label, sbyte editValue)
		{
			return (sbyte)Mathf.Clamp(EditorGUI.IntField(rtCurValue, label, editValue), sbyte.MinValue, sbyte.MaxValue);
		}
	}
	
#if ENCRYPT_TYPE
	[CustomPropertyDrawer(typeof(ObEncByte))]
	public class ObEncBytePropertyDrawer : ObservableTypePropertyDrawer<byte, ObEncByte>
	{
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
        protected override void InitProperties(SerializedProperty property)
        {
            curProperty = property.FindPropertyRelative("serializeValue");
        }

        protected override byte PreValue
        {
            get { return (byte)UtilBytes.HiWord(curProperty.intValue); }
        }

        protected override byte CurValue
        {
            get { return (byte)UtilBytes.LoWord(curProperty.intValue); }
        }

        protected override void SetCurValue(byte editValue)
        {
            curProperty.intValue = UtilBytes.MakeLong(editValue, CurValue);
        }
#else
		protected override byte PreValue
		{
			get { return (byte)preProperty.intValue; }
		}
		
		protected override byte CurValue
		{
			get { return (byte)camProperty.intValue; }
		}
		
		protected override void SetCurValue(byte editValue)
		{
			preProperty.intValue = camProperty.intValue;
			camProperty.intValue = editValue;
		}
#endif
		
		protected override byte EditValue(GUIContent label, byte editValue)
		{
			return (byte)Mathf.Clamp(EditorGUI.IntField(rtCurValue, label, editValue), byte.MinValue, byte.MaxValue);
		}
	}
	
	[CustomPropertyDrawer(typeof(ObEncSByte))]
	public class ObEncSBytePropertyDrawer : ObservableTypePropertyDrawer<sbyte, ObEncSByte>
	{
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
        protected override void InitProperties(SerializedProperty property)
        {
            curProperty = property.FindPropertyRelative("serializeValue");
        }

        protected override sbyte PreValue
        {
            get { return (sbyte)UtilBytes.HiWord(curProperty.intValue); }
        }

        protected override sbyte CurValue
        {
            get { return (sbyte)UtilBytes.LoWord(curProperty.intValue); }
        }

        protected override void SetCurValue(sbyte editValue)
        {
            curProperty.intValue = UtilBytes.MakeLong(editValue, CurValue);
        }
#else
		protected override sbyte PreValue
		{
			get { return (sbyte)preProperty.intValue; }
		}
		
		protected override sbyte CurValue
		{
			get { return (sbyte)camProperty.intValue; }
		}
		
		protected override void SetCurValue(sbyte editValue)
		{
			preProperty.intValue = camProperty.intValue;
			camProperty.intValue = editValue;
		}
#endif
		
		protected override sbyte EditValue(GUIContent label, sbyte editValue)
		{
			return (sbyte)Mathf.Clamp(EditorGUI.IntField(rtCurValue, label, editValue), sbyte.MinValue, sbyte.MaxValue);
		}
	}
#endif
}