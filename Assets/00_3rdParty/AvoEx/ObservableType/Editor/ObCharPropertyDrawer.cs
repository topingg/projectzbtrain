﻿using UnityEngine;
using UnityEditor;
using System;

/* See the "http://avoex.com/avoex/default-license/" for the full license governing this code. */

namespace AvoEx.ObservableType
{
	[CustomPropertyDrawer(typeof(ObChar))]
	public class ObCharPropertyDrawer : ObservableTypePropertyDrawer<char, ObChar>
	{
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
        protected override void InitProperties(SerializedProperty property)
        {
            curProperty = property.FindPropertyRelative("serializeValue");
        }

        protected override char PreValue
        {
            get { return (char)UtilBytes.HiWord(curProperty.intValue); }
        }

        protected override char CurValue
        {
            get { return (char)UtilBytes.LoWord(curProperty.intValue); }
        }

        protected override void SetCurValue(char editValue)
        {
            curProperty.intValue = UtilBytes.MakeLong((short)editValue, (short)CurValue);
        }
#else
		protected override char PreValue
		{
			get { return (char)preProperty.intValue; }
		}
		
		protected override char CurValue
		{
			get { return (char)curProperty.intValue; }
		}
		
		protected override void SetCurValue(char editValue)
		{
			preProperty.intValue = curProperty.intValue;
			curProperty.intValue = editValue;
		}
#endif
		
		protected override char EditValue(GUIContent label, char editValue)
		{
			string textValue = editValue.ToString();
			textValue = EditorGUI.TextField(rtCurValue, label, textValue);
			if (textValue.Length > 0)
				return textValue[0];
			else
				return (char)0;
		}
	}

#if ENCRYPT_TYPE
	[CustomPropertyDrawer(typeof(ObEncChar))]
	public class ObEncCharPropertyDrawer : ObservableTypePropertyDrawer<char, ObEncChar>
	{
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
        protected override void InitProperties(SerializedProperty property)
        {
            curProperty = property.FindPropertyRelative("serializeValue");
        }

        protected override char PreValue
        {
            get { return (char)UtilBytes.HiWord(curProperty.intValue); }
        }

        protected override char CurValue
        {
            get { return (char)UtilBytes.LoWord(curProperty.intValue); }
        }

        protected override void SetCurValue(char editValue)
        {
            curProperty.intValue = UtilBytes.MakeLong((short)editValue, (short)CurValue);
        }
#else
		protected override char PreValue
		{
			get { return (char)preProperty.intValue; }
		}
		
		protected override char CurValue
		{
			get { return (char)camProperty.intValue; }
		}
		
		protected override void SetCurValue(char editValue)
		{
			preProperty.intValue = camProperty.intValue;
			camProperty.intValue = editValue;
		}
#endif
		
		protected override char EditValue(GUIContent label, char editValue)
		{
			string textValue = editValue.ToString();
			textValue = EditorGUI.TextField(rtCurValue, label, textValue);
			if (textValue.Length > 0)
				return textValue[0];
			else
				return (char)0;
		}
	}
#endif
}