﻿using UnityEngine;
using UnityEditor;

/* See the "http://avoex.com/avoex/default-license/" for the full license governing this code. */

namespace AvoEx.ObservableType
{
	[CustomPropertyDrawer(typeof(ObBool))]
	public class ObBoolPropertyDrawer : ObservableTypePropertyDrawer<bool, ObBool>
	{
		protected override bool PreValue
		{
			get { return preProperty.boolValue; }
		}
		
		protected override bool CurValue
		{
			get { return curProperty.boolValue; }
		}
		
		protected override void SetCurValue(bool editValue)
		{
			preProperty.boolValue = curProperty.boolValue;
			curProperty.boolValue = editValue;
		}
		
		protected override bool EditValue(GUIContent label, bool editValue)
		{
			return EditorGUI.Toggle(rtCurValue, label, editValue);
		}
	}

#if ENCRYPT_TYPE
	[CustomPropertyDrawer(typeof(ObEncBool))]
	public class ObEncBoolPropertyDrawer : ObservableTypePropertyDrawer<bool, ObEncBool>
	{
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
        protected override void InitProperties(SerializedProperty property)
        {
            preProperty = property.FindPropertyRelative("preValue");
            camProperty = property.FindPropertyRelative("serializeValue");
        }
#endif
		protected override bool PreValue
		{
			get { return preProperty.boolValue; }
		}
		
		protected override bool CurValue
		{
			get { return camProperty.boolValue; }
		}
		
		protected override void SetCurValue(bool editValue)
		{
			preProperty.boolValue = camProperty.boolValue;
			camProperty.boolValue = editValue;
		}
		
		protected override bool EditValue(GUIContent label, bool editValue)
		{
			return EditorGUI.Toggle(rtCurValue, label, editValue);
		}
	}
#endif
}