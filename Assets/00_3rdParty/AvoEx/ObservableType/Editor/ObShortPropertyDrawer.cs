﻿using UnityEngine;
using UnityEditor;
using System;

/* See the "http://avoex.com/avoex/default-license/" for the full license governing this code. */

namespace AvoEx.ObservableType
{
	[CustomPropertyDrawer(typeof(ObShort))]
	public class ObShortPropertyDrawer : ObservableTypePropertyDrawer<short, ObShort>
	{
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
        protected override void InitProperties(SerializedProperty property)
        {
            curProperty = property.FindPropertyRelative("serializeValue");
        }

        protected override short PreValue
        {
            get { return UtilBytes.HiWord(curProperty.intValue); }
        }

        protected override short CurValue
        {
            get { return UtilBytes.LoWord(curProperty.intValue); }
        }

        protected override void SetCurValue(short editValue)
        {
            curProperty.intValue = UtilBytes.MakeLong(editValue, CurValue);
        }
#else
		protected override short PreValue
		{
			get { return (short)preProperty.intValue; }
		}
		
		protected override short CurValue
		{
			get { return (short)curProperty.intValue; }
		}
		
		protected override void SetCurValue(short editValue)
		{
			preProperty.intValue = curProperty.intValue;
			curProperty.intValue = editValue;
		}
#endif
        protected override short EditValue(GUIContent label, short editValue)
        {
            return (short)Mathf.Clamp(EditorGUI.IntField(rtCurValue, label, (int)editValue), short.MinValue, short.MaxValue);
        }
	}
	
	[CustomPropertyDrawer(typeof(ObUShort))]
	public class ObUShortPropertyDrawer : ObservableTypePropertyDrawer<ushort, ObUShort>
	{
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
        protected override void InitProperties(SerializedProperty property)
        {
            curProperty = property.FindPropertyRelative("serializeValue");
        }

        protected override ushort PreValue
        {
            get { return (ushort)UtilBytes.HiWord(curProperty.intValue); }
        }

        protected override ushort CurValue
        {
            get { return (ushort)UtilBytes.LoWord(curProperty.intValue); }
        }

        protected override void SetCurValue(ushort editValue)
        {
            curProperty.intValue = UtilBytes.MakeLong((short)editValue, (short)CurValue);
        }
#else
		protected override ushort PreValue
		{
			get { return (ushort)preProperty.intValue; }
		}
		
		protected override ushort CurValue
		{
			get { return (ushort)curProperty.intValue; }
		}
		
		protected override void SetCurValue(ushort editValue)
		{
			preProperty.intValue = curProperty.intValue;
			curProperty.intValue = editValue;
		}
#endif
		
		protected override ushort EditValue(GUIContent label, ushort editValue)
		{
			string textValue = ((ushort)editValue).ToString();
			try
			{
				return ushort.Parse(EditorGUI.TextField(rtCurValue, label, textValue));
			}
			catch (OverflowException e)
			{
				if (e.Message.Equals("Negative number"))
				{
					return 0;
				}
				else
				{
					return ushort.MaxValue;
				}
			}
			catch (Exception)
			{
				return CurValue;
			}
		}
	}
	
#if ENCRYPT_TYPE
	[CustomPropertyDrawer(typeof(ObEncShort))]
	public class ObEncShortPropertyDrawer : ObservableTypePropertyDrawer<short, ObEncShort>
	{
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
        protected override void InitProperties(SerializedProperty property)
        {
            curProperty = property.FindPropertyRelative("serializeValue");
        }

        protected override short PreValue
        {
            get { return UtilBytes.HiWord(curProperty.intValue); }
        }

        protected override short CurValue
        {
            get { return UtilBytes.LoWord(curProperty.intValue); }
        }

        protected override void SetCurValue(short editValue)
        {
            curProperty.intValue = UtilBytes.MakeLong(editValue, CurValue);
        }
#else
		protected override short PreValue
		{
			get { return (short)preProperty.intValue; }
		}
		
		protected override short CurValue
		{
			get { return (short)camProperty.intValue; }
		}
		
		protected override void SetCurValue(short editValue)
		{
			preProperty.intValue = camProperty.intValue;
			camProperty.intValue = editValue;
		}
#endif
		
		protected override short EditValue(GUIContent label, short editValue)
		{
			return (short)Mathf.Clamp(EditorGUI.IntField(rtCurValue, label, (int)editValue), short.MinValue, short.MaxValue);
		}
	}
	
	[CustomPropertyDrawer(typeof(ObEncUShort))]
	public class ObEncUShortPropertyDrawer : ObservableTypePropertyDrawer<ushort, ObEncUShort>
	{
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
        protected override void InitProperties(SerializedProperty property)
        {
            curProperty = property.FindPropertyRelative("serializeValue");
        }

        protected override ushort PreValue
        {
            get { return (ushort)UtilBytes.HiWord(curProperty.intValue); }
        }

        protected override ushort CurValue
        {
            get { return (ushort)UtilBytes.LoWord(curProperty.intValue); }
        }

        protected override void SetCurValue(ushort editValue)
        {
            curProperty.intValue = UtilBytes.MakeLong((short)editValue, (short)CurValue);
        }
#else
		protected override ushort PreValue
		{
			get { return (ushort)preProperty.intValue; }
		}
		
		protected override ushort CurValue
		{
			get { return (ushort)camProperty.intValue; }
		}
		
		protected override void SetCurValue(ushort editValue)
		{
			preProperty.intValue = camProperty.intValue;
			camProperty.intValue = editValue;
		}
#endif
		
		protected override ushort EditValue(GUIContent label, ushort editValue)
		{
			string textValue = ((ushort)editValue).ToString();
			try
			{
				return ushort.Parse(EditorGUI.TextField(rtCurValue, label, textValue));
			}
			catch (OverflowException e)
			{
				if (e.Message.Equals("Negative number"))
				{
					return 0;
				}
				else
				{
					return ushort.MaxValue;
				}
			}
			catch (Exception)
			{
				return CurValue;
			}
		}
	}
#endif
}