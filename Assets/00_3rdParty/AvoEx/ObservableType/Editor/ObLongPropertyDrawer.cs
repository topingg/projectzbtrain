﻿using UnityEngine;
using UnityEditor;
using System;

/* See the "http://avoex.com/avoex/default-license/" for the full license governing this code. */

namespace AvoEx.ObservableType
{
	[CustomPropertyDrawer(typeof(ObLong))]
	public class ObLongPropertyDrawer : ObservableTypePropertyDrawer<long, ObLong>
	{
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9        
        protected override void InitProperties(SerializedProperty property)
        {
            preProperty = property.FindPropertyRelative("serializePreValue");
            curProperty = property.FindPropertyRelative("serializeValue");
        }

        protected override long PreValue
        {
            get { return long.Parse(preProperty.stringValue); }
        }

        protected override long CurValue
        {
            get { return long.Parse(curProperty.stringValue); }
        }

        protected override void SetCurValue(long editValue)
        {
            preProperty.stringValue = curProperty.stringValue;
            curProperty.stringValue = editValue.ToString();
        }

        protected override long EditValue(GUIContent label, long editValue)
        {
            long newValue;
            if (long.TryParse(EditorGUI.TextField(rtCurValue, label, editValue.ToString()), out newValue))
            {
                return newValue;
            }

            return CurValue;
        }
#else
		protected override long PreValue
		{
			get { return preProperty.longValue; }
		}
		
		protected override long CurValue
		{
			get { return curProperty.longValue; }
		}
		
		protected override void SetCurValue(long editValue)
		{
			preProperty.longValue = curProperty.longValue;
			curProperty.longValue = editValue;
		}
		
		protected override long EditValue(GUIContent label, long editValue)
		{
			return EditorGUI.LongField(rtCurValue, label, editValue);
		}
#endif
	}
	
	[CustomPropertyDrawer(typeof(ObULong))]
	public class ObULongPropertyDrawer : ObservableTypePropertyDrawer<ulong, ObULong>
	{
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9        
        protected override void InitProperties(SerializedProperty property)
        {
            preProperty = property.FindPropertyRelative("serializePreValue");
            curProperty = property.FindPropertyRelative("serializeValue");
        }

        protected override ulong PreValue
        {
            get { return ulong.Parse(preProperty.stringValue); }
        }

        protected override ulong CurValue
        {
            get { return ulong.Parse(curProperty.stringValue); }
        }

        protected override void SetCurValue(ulong editValue)
        {
            preProperty.stringValue = curProperty.stringValue;
            curProperty.stringValue = editValue.ToString();
        }

        protected override ulong EditValue(GUIContent label, ulong editValue)
        {
            ulong newValue;
            if (ulong.TryParse(EditorGUI.TextField(rtCurValue, label, editValue.ToString()), out newValue))
            {
                return newValue;
            }

            return CurValue;
        }
#else
		protected override ulong PreValue
		{
			get { return (ulong)preProperty.longValue; }
		}
		
		protected override ulong CurValue
		{
			get { return (ulong)curProperty.longValue; }
		}
		
		protected override void SetCurValue(ulong editValue)
		{
			preProperty.longValue = curProperty.longValue;
			curProperty.longValue = (long)editValue;
		}
		
		protected override ulong EditValue(GUIContent label, ulong editValue)
		{
			string textValue = editValue.ToString();
			try
			{
				return ulong.Parse(EditorGUI.TextField(rtCurValue, label, textValue));
			}
			catch (OverflowException e)
			{
				if (e.Message.Equals("Negative number"))
				{
					return 0;
				}
				else
				{
					return ulong.MaxValue;
				}
			}
			catch (Exception)
			{
				return CurValue;
			}
		}
#endif
    }

#if ENCRYPT_TYPE
	[CustomPropertyDrawer(typeof(ObEncLong))]
	public class ObEncLongPropertyDrawer : ObservableTypePropertyDrawer<long, ObEncLong>
	{
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9        
        protected override void InitProperties(SerializedProperty property)
        {
            preProperty = property.FindPropertyRelative("serializePreValue");
            curProperty = property.FindPropertyRelative("serializeValue");
        }

        protected override long PreValue
        {
            get { return long.Parse(preProperty.stringValue); }
        }

        protected override long CurValue
        {
            get { return long.Parse(curProperty.stringValue); }
        }

        protected override void SetCurValue(long editValue)
        {
            preProperty.stringValue = curProperty.stringValue;
            curProperty.stringValue = editValue.ToString();
        }

        protected override long EditValue(GUIContent label, long editValue)
        {
            long newValue;
            if (long.TryParse(EditorGUI.TextField(rtCurValue, label, editValue.ToString()), out newValue))
            {
                return newValue;
            }

            return CurValue;
        }
#else
		protected override long PreValue
		{
			get { return preProperty.longValue; }
		}
		
		protected override long CurValue
		{
			get { return camProperty.longValue; }
		}
		
		protected override void SetCurValue(long editValue)
		{
			preProperty.longValue = camProperty.longValue;
			camProperty.longValue = editValue;
		}
		
		protected override long EditValue(GUIContent label, long editValue)
		{
			return EditorGUI.LongField(rtCurValue, label, editValue);
		}
#endif
	}
	
	[CustomPropertyDrawer(typeof(ObEncULong))]
	public class ObEncULongPropertyDrawer : ObservableTypePropertyDrawer<ulong, ObEncULong>
	{
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9        
        protected override void InitProperties(SerializedProperty property)
        {
            preProperty = property.FindPropertyRelative("serializePreValue");
            curProperty = property.FindPropertyRelative("serializeValue");
        }

        protected override ulong PreValue
        {
            get { return ulong.Parse(preProperty.stringValue); }
        }

        protected override ulong CurValue
        {
            get { return ulong.Parse(curProperty.stringValue); }
        }

        protected override void SetCurValue(ulong editValue)
        {
            preProperty.stringValue = curProperty.stringValue;
            curProperty.stringValue = editValue.ToString();
        }

        protected override ulong EditValue(GUIContent label, ulong editValue)
        {
            ulong newValue;
            if (ulong.TryParse(EditorGUI.TextField(rtCurValue, label, editValue.ToString()), out newValue))
            {
                return newValue;
            }

            return CurValue;
        }
#else
		protected override ulong PreValue
		{
			get { return (ulong)preProperty.longValue; }
		}
		
		protected override ulong CurValue
		{
			get { return (ulong)camProperty.longValue; }
		}
		
		protected override void SetCurValue(ulong editValue)
		{
			preProperty.longValue = camProperty.longValue;
			camProperty.longValue = (long)editValue;
		}
		
		protected override ulong EditValue(GUIContent label, ulong editValue)
		{
			string textValue = editValue.ToString();
			try
			{
				return ulong.Parse(EditorGUI.TextField(rtCurValue, label, textValue));
			}
			catch (OverflowException e)
			{
				if (e.Message.Equals("Negative number"))
				{
					return 0;
				}
				else
				{
					return ulong.MaxValue;
				}
			}
			catch (Exception)
			{
				return CurValue;
			}
		}
#endif
	}
#endif
}