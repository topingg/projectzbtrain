﻿using UnityEngine;
using System;
using System.Collections.Generic;
#if ENCRYPT_TYPE
using AvoEx.EncryptType;
#endif

/* See the "http://avoex.com/avoex/default-license/" for the full license governing this code. */

namespace AvoEx.ObservableType
{
    public abstract class Observable<T> where T : Observable<T>
    {
        public delegate void EventChanged(T target);
        private EventChanged onChanged = null;
        private Delegate[] arrayCallbacks = null;

        /// <summary>
        /// propagate observable value to observers.
        /// if callback occurs exception, will be deleted from observable.
        /// </summary>
        public void OnChanged()
        {
            if (arrayCallbacks == null)
                return;

            bool isChanged = false;
            foreach (EventChanged callback in arrayCallbacks)
            {
                if (TryCallback(callback) == false)
                {
                    onChanged -= callback;
                    isChanged = true;
                    Debug.LogWarningFormat("Removed from observer list. Target = '{0}', Method = '{1}'",
                        (callback.Target == null ? "null" : callback.Target.ToString()),
                        callback.Method.ToString());
                }
            }

            if (isChanged)
            {
                if (onChanged == null)
                    arrayCallbacks = null;
                else
                    arrayCallbacks = onChanged.GetInvocationList();
            }
        }

        /// <summary>
        /// add callback function to observable.
        /// </summary>
        /// <param name="callbackFunc">callback function to be called on changed.</param>
        /// <param name="callbackNow">if callback function added success, callback immediatly.</param>
        public void AddObserver(EventChanged callbackFunc, bool callbackNow = false)
        {
            if (callbackNow && TryCallback(callbackFunc) == false)
            {
                Debug.LogWarningFormat("Failed add to observer list. Target = '{0}', Method = '{1}'",
                    (callbackFunc.Target == null ? "null" : callbackFunc.Target.ToString()),
                    callbackFunc.Method.ToString());
                return;
            }

            onChanged += callbackFunc;
            arrayCallbacks = onChanged.GetInvocationList();
        }

        /// <summary>
        /// delete callback function from observable.
        /// </summary>
        /// <param name="callbackFunc">callback function to be deleted from observable.</param>
        public void DelObserver(EventChanged callbackFunc)
        {
            onChanged -= callbackFunc;
            if (onChanged == null)
                arrayCallbacks = null;
            else
                arrayCallbacks = onChanged.GetInvocationList();
        }

        private bool TryCallback(EventChanged callback)
        {
            if (callback == null)
                return false;

            if (callback.Target is UnityEngine.Object && (UnityEngine.Object)callback.Target == false)
                return false;

            try
            {
                callback((T)this);
                return true;
            }
            catch (Exception e)
            {
                Debug.LogError(e.ToString());
                return false;
            }
        }
    }

    public interface IObservableType<T>
    {
        T Value { get; set; }
        T PreValue { get; }
        void ResetPrevalue();
    }

    [Serializable]
    public class ObservableType<T, thisT> : Observable<thisT>, IObservableType<T>, ISerializationCallbackReceiver
        where T : struct
        where thisT : ObservableType<T, thisT>
    {
        [SerializeField]
        protected T preValue;
        [SerializeField]
        protected T curValue;

        private object thisLock = new object();

        public ObservableType(T initValue = default(T))
        {
            curValue = initValue;
        }

        /// <summary>
        /// current value.
        /// set value will call OnChanged().
        /// </summary>
        public T Value
        {
            get
            {
                return curValue;
            }
            set
            {
                lock (thisLock)
                {
                    if (Equals(value))
                        return;

                    preValue = curValue;
                    curValue = value;

                    OnChanged();
                }
            }
        }

        /// <summary>
        /// previous value.
        /// </summary>
        public T PreValue
        {
            get { return preValue; }
        }

        /// <summary>
        /// make the previous value to the same value as current value.
        /// </summary>
        public void ResetPrevalue()
        {
            preValue = curValue;
        }

        public static implicit operator T(ObservableType<T, thisT> x)
        {
            return x.Value;
        }

        /// <summary>
        /// ValueType.Equals(object) create garbage. override Equals(T) to avoid garbage.
        /// </summary>
        /// <param name="newValue">value to compare</param>
        /// <returns>returns true if newValue and curValue are same value</returns>
        public virtual bool Equals(T newValue)
        {
            return curValue.Equals(newValue);
        }

        #region ISerializationCallbackReceiver
        public virtual void OnAfterDeserialize()
        {
            OnChanged();
        }

        public virtual void OnBeforeSerialize()
        {
        }
        #endregion
    }

#if ENCRYPT_TYPE
    [Serializable]
    public class ObservableType<T, EncT, thisT> : Observable<thisT>, IObservableType<T>, ISerializationCallbackReceiver
        where T : struct
        where EncT : IEncryptor<T>
        where thisT : ObservableType<T, EncT, thisT>
    {
        [SerializeField]
        protected T preValue;
        [SerializeField]
        protected EncT curValue;

        private object thisLock = new object();

        public ObservableType(T initValue = default(T))
        {
            curValue.SetValue(initValue);
        }

        /// <summary>
        /// current value.
        /// set value will call OnChanged().
        /// </summary>
        public T Value
        {
            get
            {
                return curValue.ToSystemType();
            }
            set
            {
                lock (thisLock)
                {
                    T oldValue = curValue.ToSystemType();
                    if (Equals(value))
                        return;

                    preValue = oldValue;
                    curValue.SetValue(value);

                    OnChanged();
                }
            }
        }

        /// <summary>
        /// previous value.
        /// </summary>
        public T PreValue
        {
            get { return preValue; }
        }

        /// <summary>
        /// make the previous value to the same value as current value.
        /// </summary>
        public void ResetPrevalue()
        {
            preValue = curValue.ToSystemType();
        }

        public static implicit operator T(ObservableType<T, EncT, thisT> x)
        {
            return x.Value;
        }

        #region ISerializationCallbackReceiver
        public virtual void OnAfterDeserialize()
        {
            OnChanged();
        }

        public virtual void OnBeforeSerialize()
        {
        }
        #endregion

        /// <summary>
        /// Equals(object) occurs garbage collect. override EqualsValue() to avoid garbage collect.
        /// </summary>
        /// <param name="newValue">value to compare</param>
        /// <returns>returns true if newValue and curValue are same value</returns>
        public virtual bool Equals(T newValue)
        {
            return curValue.Equals(newValue);
        }
    }
#endif
}