﻿using UnityEngine;
using System;
#if ENCRYPT_TYPE
using AvoEx.EncryptType;
#endif

/* See the "http://avoex.com/avoex/default-license/" for the full license governing this code. */

namespace AvoEx.ObservableType
{
    [Serializable]
    public class ObInt : ObservableType<int, ObInt>
    {
        public ObInt(int initValue = 0)
            : base(initValue)
        {
        }

        #region explicit_castings
        public static explicit operator float(ObInt x)
        {
            return (float)x.Value;
        }
        public static explicit operator double(ObInt x)
        {
            return (double)x.Value;
        }
        public static explicit operator long(ObInt x)
        {
            return (long)x.Value;
        }
        public static explicit operator ulong(ObInt x)
        {
            return (ulong)x.Value;
        }
        public static explicit operator int(ObInt x)
        {
            return (int)x.Value;
        }
        public static explicit operator uint(ObInt x)
        {
            return (uint)x.Value;
        }
        public static explicit operator short(ObInt x)
        {
            return (short)x.Value;
        }
        public static explicit operator ushort(ObInt x)
        {
            return (ushort)x.Value;
        }
        public static explicit operator byte(ObInt x)
        {
            return (byte)x.Value;
        }
        public static explicit operator sbyte(ObInt x)
        {
            return (sbyte)x.Value;
        }
        #endregion explicit_castings

        #region unary_operators
        public static ObInt operator ++(ObInt x)
        {
            x.Value = x.Value + 1;
            return x;
        }
        public static ObInt operator --(ObInt x)
        {
            x.Value = x.Value - 1;
            return x;
        }
        #endregion unary_operators

        public override bool Equals(int newValue)
        {
            return (curValue == newValue);
        }
    }

    [Serializable]
    public class ObUInt : ObservableType<uint, ObUInt>
    {
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
        [SerializeField]
        int serializePreValue;
        [SerializeField]
        int serializeValue;

        public override void OnAfterDeserialize()
        {
            preValue = (uint)serializePreValue;
            curValue = (uint)serializeValue;
            OnChanged();
        }

        public override void OnBeforeSerialize()
        {
            serializePreValue = (int)preValue;
            serializeValue = (int)curValue;
        }
#endif
        public ObUInt(uint initValue = 0)
            : base(initValue)
        {
        }

        #region explicit_castings
        public static explicit operator float(ObUInt x)
        {
            return (float)x.Value;
        }
        public static explicit operator double(ObUInt x)
        {
            return (double)x.Value;
        }
        public static explicit operator long(ObUInt x)
        {
            return (long)x.Value;
        }
        public static explicit operator ulong(ObUInt x)
        {
            return (ulong)x.Value;
        }
        public static explicit operator int(ObUInt x)
        {
            return (int)x.Value;
        }
        public static explicit operator uint(ObUInt x)
        {
            return (uint)x.Value;
        }
        public static explicit operator short(ObUInt x)
        {
            return (short)x.Value;
        }
        public static explicit operator ushort(ObUInt x)
        {
            return (ushort)x.Value;
        }
        public static explicit operator byte(ObUInt x)
        {
            return (byte)x.Value;
        }
        public static explicit operator sbyte(ObUInt x)
        {
            return (sbyte)x.Value;
        }
        #endregion explicit_castings

        #region unary_operators
        public static ObUInt operator ++(ObUInt x)
        {
            x.Value = x.Value + 1;
            return x;
        }
        public static ObUInt operator --(ObUInt x)
        {
            x.Value = x.Value - 1;
            return x;
        }
        #endregion unary_operators

        public override bool Equals(uint newValue)
        {
            return (curValue == newValue);
        }
    }

#if ENCRYPT_TYPE
    [Serializable]
    public class ObEncInt : ObservableType<int, EncInt, ObEncInt>
    {
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
        [SerializeField]
        int serializeValue;

        public override void OnAfterDeserialize()
        {
            curValue = serializeValue;
            OnChanged();
        }

        public override void OnBeforeSerialize()
        {
            serializeValue = (int)curValue;
        }
#endif

        public ObEncInt(int initValue = 0)
            : base(initValue)
        {
        }

        #region explicit_castings
        public static explicit operator float(ObEncInt x)
        {
            return (float)x.Value;
        }
        public static explicit operator double(ObEncInt x)
        {
            return (double)x.Value;
        }
        public static explicit operator long(ObEncInt x)
        {
            return (long)x.Value;
        }
        public static explicit operator ulong(ObEncInt x)
        {
            return (ulong)x.Value;
        }
        public static explicit operator int(ObEncInt x)
        {
            return (int)x.Value;
        }
        public static explicit operator uint(ObEncInt x)
        {
            return (uint)x.Value;
        }
        public static explicit operator short(ObEncInt x)
        {
            return (short)x.Value;
        }
        public static explicit operator ushort(ObEncInt x)
        {
            return (ushort)x.Value;
        }
        public static explicit operator byte(ObEncInt x)
        {
            return (byte)x.Value;
        }
        public static explicit operator sbyte(ObEncInt x)
        {
            return (sbyte)x.Value;
        }
        #endregion explicit_castings

        #region unary_operators
        public static ObEncInt operator ++(ObEncInt x)
        {
            x.Value = x.Value + 1;
            return x;
        }
        public static ObEncInt operator --(ObEncInt x)
        {
            x.Value = x.Value - 1;
            return x;
        }
        #endregion unary_operators

        public override bool Equals(int newValue)
        {
            return (curValue == newValue);
        }
    }

    [Serializable]
    public class ObEncUInt : ObservableType<uint, EncUInt, ObEncUInt>
    {
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
        [SerializeField]
        int serializePreValue;
        [SerializeField]
        int serializeValue;

        public override void OnAfterDeserialize()
        {
            preValue = (uint)serializePreValue;
            curValue = (uint)serializeValue;
            OnChanged();
        }

        public override void OnBeforeSerialize()
        {
            serializePreValue = (int)preValue;
            serializeValue = (int)curValue.ToSystemType();
        }
#endif

        public ObEncUInt(uint initValue = 0)
            : base(initValue)
        {
        }

        #region explicit_castings
        public static explicit operator float(ObEncUInt x)
        {
            return (float)x.Value;
        }
        public static explicit operator double(ObEncUInt x)
        {
            return (double)x.Value;
        }
        public static explicit operator long(ObEncUInt x)
        {
            return (long)x.Value;
        }
        public static explicit operator ulong(ObEncUInt x)
        {
            return (ulong)x.Value;
        }
        public static explicit operator int(ObEncUInt x)
        {
            return (int)x.Value;
        }
        public static explicit operator uint(ObEncUInt x)
        {
            return (uint)x.Value;
        }
        public static explicit operator short(ObEncUInt x)
        {
            return (short)x.Value;
        }
        public static explicit operator ushort(ObEncUInt x)
        {
            return (ushort)x.Value;
        }
        public static explicit operator byte(ObEncUInt x)
        {
            return (byte)x.Value;
        }
        public static explicit operator sbyte(ObEncUInt x)
        {
            return (sbyte)x.Value;
        }
        #endregion explicit_castings

        #region unary_operators
        public static ObEncUInt operator ++(ObEncUInt x)
        {
            x.Value = x.Value + 1;
            return x;
        }
        public static ObEncUInt operator --(ObEncUInt x)
        {
            x.Value = x.Value - 1;
            return x;
        }
        #endregion unary_operators

        public override bool Equals(uint newValue)
        {
            return (curValue == newValue);
        }
    }
#endif
}