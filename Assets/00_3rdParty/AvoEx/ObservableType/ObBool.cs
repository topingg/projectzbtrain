﻿using UnityEngine;
using System;
#if ENCRYPT_TYPE
using AvoEx.EncryptType;
#endif

/* See the "http://avoex.com/avoex/default-license/" for the full license governing this code. */

namespace AvoEx.ObservableType
{
	[Serializable]
	public class ObBool : ObservableType<bool, ObBool>
	{
        public ObBool(bool initValue = false)
            : base(initValue)
        {
        }

        public override bool Equals(bool newValue)
        {
            return (curValue == newValue);
        }
    }
	
#if ENCRYPT_TYPE
	[Serializable]
	public class ObEncBool : ObservableType<bool, EncBool, ObEncBool>
	{
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
        [SerializeField]
        bool serializeValue;

        public override void OnAfterDeserialize()
        {
            curValue = serializeValue;
            OnChanged();
        }

        public override void OnBeforeSerialize()
        {
            serializeValue = curValue;
        }
#endif
        public ObEncBool(bool initValue = false)
            : base(initValue)
        {
        }

        public override bool Equals(bool newValue)
        {
            return (curValue == newValue);
        }
    }
#endif
}