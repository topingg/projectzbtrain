----------------------------------------------------------------
ObservableType

ObservableType can easily detect a value changing.
It makes the game development easier.

optimized for Unity 5.

more informations:
http://avoex.com
https://www.facebook.com/avoexgames


----------------------------------------------------------------
How to use ObservableType

 - apply ObservableType.
 1. using AvoEx.ObservableType;
 2. replace type to ObservableType.
 3. replace ��=�� to ��.Value =��

 - detect value changing.
 1. make callback function.
 2. add callback function to ObservableType.

watch video tutorial.
https://youtu.be/5kJcCmaG2MI


----------------------------------------------------------------
ObservableTypes

 - int => ObInt
 - uint => ObUInt
 - long => ObLong
 - ulong => ObULong
 - float => ObFloat
 - double => ObDouble
 - bool => ObBool
 - byte => ObByte
 - sbyte => ObSByte
 - short => ObShort
 - ushort => ObUShort
 - char => ObChar
 - decimal => ObDecimal


----------------------------------------------------------------
version history

1.0.28
 - fixed null reference exception.
 - improve log.

1.0.26 (Thanks to jisuart@jisuart.com helped to solve this problems)
 - fixed create garbage on set Value.
 - fixed scrubbing property name in inspector.
 - fixed ObDecimal

1.0.18
 - updated readme.txt

1.0.12
 - porting to Unity 4.5

1.0.4
 - added constructors default value.
 - added exception process when Observable.OnChanged
 - improved Observable.OnChanged performance.

 
----------------------------------------------------------------
ObservableType with EncryptType

You can protect ObservalbeType from memory cheating.

 1. import EncryptType. (https://www.assetstore.unity3d.com/#!/content/35073)
 2. add Define Symbol 'ENCRYPT_TYPE'.
	- 'AvoEx > ObservableType > Use with EncryptType' or 'use DefineManager to define ENCRYPT_TYPE(https://www.assetstore.unity3d.com/#!/content/38125)'

  
 ----------------------------------------------------------------
ObservableTypes with EncryptType

 - int => ObEncInt
 - uint => ObEncUInt
 - long => ObEncLong
 - ulong => ObEncULong
 - float => ObEncFloat
 - double => ObEncDouble
 - bool => ObEncBool
 - byte => ObEncByte
 - sbyte => ObEncSByte
 - short => ObEncShort
 - ushort => ObEncUShort
 - char => ObEncChar
 - decimal => ObEncDecimal
