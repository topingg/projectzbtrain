﻿using UnityEngine;
using System;
#if ENCRYPT_TYPE
using AvoEx.EncryptType;
#endif

/* See the "http://avoex.com/avoex/default-license/" for the full license governing this code. */

namespace AvoEx.ObservableType
{
    [Serializable]
    public class ObChar : ObservableType<char, ObChar>
    {
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
        [SerializeField]
        int serializeValue;

        public override void OnAfterDeserialize()
        {
            preValue = (char)UtilBytes.HiWord(serializeValue);
            curValue = (char)UtilBytes.LoWord(serializeValue);
            OnChanged();
        }

        public override void OnBeforeSerialize()
        {
            serializeValue = UtilBytes.MakeLong((short)curValue, (short)preValue);
        }
#endif

        public ObChar(char initValue = default(char))
            : base(initValue)
        {
        }

        #region explicit_castings
        public static explicit operator float(ObChar x)
        {
            return (float)x.Value;
        }
        public static explicit operator double(ObChar x)
        {
            return (double)x.Value;
        }
        public static explicit operator long(ObChar x)
        {
            return (long)x.Value;
        }
        public static explicit operator ulong(ObChar x)
        {
            return (ulong)x.Value;
        }
        public static explicit operator int(ObChar x)
        {
            return (int)x.Value;
        }
        public static explicit operator uint(ObChar x)
        {
            return (uint)x.Value;
        }
        public static explicit operator short(ObChar x)
        {
            return (short)x.Value;
        }
        public static explicit operator ushort(ObChar x)
        {
            return (ushort)x.Value;
        }
        public static explicit operator byte(ObChar x)
        {
            return (byte)x.Value;
        }
        public static explicit operator sbyte(ObChar x)
        {
            return (sbyte)x.Value;
        }
        #endregion explicit_castings

        #region unary_operators
        public static ObChar operator ++(ObChar x)
        {
            x.Value = (char)(x.Value + 1);
            return x;
        }
        public static ObChar operator --(ObChar x)
        {
            x.Value = (char)(x.Value - 1);
            return x;
        }
        #endregion unary_operators

        public override bool Equals(char newValue)
        {
            return (curValue == newValue);
        }
    }

#if ENCRYPT_TYPE
    [Serializable]
    public class ObEncChar : ObservableType<char, EncChar, ObEncChar>
    {
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
        [SerializeField]
        int serializeValue;

        public override void OnAfterDeserialize()
        {
            preValue = (char)UtilBytes.HiWord(serializeValue);
            curValue = (char)UtilBytes.LoWord(serializeValue);
            OnChanged();
        }

        public override void OnBeforeSerialize()
        {
            serializeValue = UtilBytes.MakeLong((short)curValue.ToSystemType(), (short)preValue);
        }
#endif

        public ObEncChar(char initValue = default(char))
            : base(initValue)
        {
        }

        #region explicit_castings
        public static explicit operator float(ObEncChar x)
        {
            return (float)x.Value;
        }
        public static explicit operator double(ObEncChar x)
        {
            return (double)x.Value;
        }
        public static explicit operator long(ObEncChar x)
        {
            return (long)x.Value;
        }
        public static explicit operator ulong(ObEncChar x)
        {
            return (ulong)x.Value;
        }
        public static explicit operator int(ObEncChar x)
        {
            return (int)x.Value;
        }
        public static explicit operator uint(ObEncChar x)
        {
            return (uint)x.Value;
        }
        public static explicit operator short(ObEncChar x)
        {
            return (short)x.Value;
        }
        public static explicit operator ushort(ObEncChar x)
        {
            return (ushort)x.Value;
        }
        public static explicit operator byte(ObEncChar x)
        {
            return (byte)x.Value;
        }
        public static explicit operator sbyte(ObEncChar x)
        {
            return (sbyte)x.Value;
        }
        #endregion explicit_castings

        #region unary_operators
        public static ObEncChar operator ++(ObEncChar x)
        {
            x.Value = (char)(x.Value + 1);
            return x;
        }
        public static ObEncChar operator --(ObEncChar x)
        {
            x.Value = (char)(x.Value - 1);
            return x;
        }
        #endregion unary_operators

        public override bool Equals(char newValue)
        {
            return (curValue == newValue);
        }
    }
#endif
}