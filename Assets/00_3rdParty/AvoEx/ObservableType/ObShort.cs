﻿using UnityEngine;
using System;
#if ENCRYPT_TYPE
using AvoEx.EncryptType;
#endif

/* See the "http://avoex.com/avoex/default-license/" for the full license governing this code. */

namespace AvoEx.ObservableType
{
    [Serializable]
    public class ObShort : ObservableType<short, ObShort>
    {
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
        [SerializeField]
        int serializeValue;

        public override void OnAfterDeserialize()
        {
            preValue = UtilBytes.HiWord(serializeValue);
            curValue = UtilBytes.LoWord(serializeValue);
            OnChanged();
        }

        public override void OnBeforeSerialize()
        {
            serializeValue = UtilBytes.MakeLong(curValue, preValue);
        }
#endif

		public ObShort(short initValue = 0)
			: base(initValue)
		{
		}

        #region explicit_castings
        public static explicit operator float(ObShort x)
        {
            return (float)x.Value;
        }
        public static explicit operator double(ObShort x)
        {
            return (double)x.Value;
        }
        public static explicit operator long(ObShort x)
        {
            return (long)x.Value;
        }
        public static explicit operator ulong(ObShort x)
        {
            return (ulong)x.Value;
        }
        public static explicit operator int(ObShort x)
        {
            return (int)x.Value;
        }
        public static explicit operator uint(ObShort x)
        {
            return (uint)x.Value;
        }
        public static explicit operator short(ObShort x)
        {
            return (short)x.Value;
        }
        public static explicit operator ushort(ObShort x)
        {
            return (ushort)x.Value;
        }
        public static explicit operator byte(ObShort x)
        {
            return (byte)x.Value;
        }
        public static explicit operator sbyte(ObShort x)
        {
            return (sbyte)x.Value;
        }
        #endregion explicit_castings

        #region unary_operators
        public static ObShort operator ++(ObShort x)
        {
            x.Value = (short)(x.Value + 1);
            return x;
        }
        public static ObShort operator --(ObShort x)
        {
            x.Value = (short)(x.Value - 1);
            return x;
        }
        #endregion unary_operators

        public override bool Equals(short newValue)
        {
            return (curValue == newValue);
        }
    }

    [Serializable]
    public class ObUShort : ObservableType<ushort, ObUShort>
    {
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
        [SerializeField]
        int serializeValue;

        public override void OnAfterDeserialize()
        {
            preValue = (ushort)UtilBytes.HiWord(serializeValue);
            curValue = (ushort)UtilBytes.LoWord(serializeValue);
            OnChanged();
        }

        public override void OnBeforeSerialize()
        {
            serializeValue = UtilBytes.MakeLong((short)curValue, (short)preValue);
        }
#endif
		public ObUShort(ushort initValue = 0)
			: base(initValue)
		{
		}

        #region explicit_castings
        public static explicit operator float(ObUShort x)
        {
            return (float)x.Value;
        }
        public static explicit operator double(ObUShort x)
        {
            return (double)x.Value;
        }
        public static explicit operator long(ObUShort x)
        {
            return (long)x.Value;
        }
        public static explicit operator ulong(ObUShort x)
        {
            return (ulong)x.Value;
        }
        public static explicit operator int(ObUShort x)
        {
            return (int)x.Value;
        }
        public static explicit operator uint(ObUShort x)
        {
            return (uint)x.Value;
        }
        public static explicit operator short(ObUShort x)
        {
            return (short)x.Value;
        }
        public static explicit operator ushort(ObUShort x)
        {
            return (ushort)x.Value;
        }
        public static explicit operator byte(ObUShort x)
        {
            return (byte)x.Value;
        }
        public static explicit operator sbyte(ObUShort x)
        {
            return (sbyte)x.Value;
        }
        #endregion explicit_castings

        #region unary_operators
        public static ObUShort operator ++(ObUShort x)
        {
            x.Value = (ushort)(x.Value + 1);
            return x;
        }
        public static ObUShort operator --(ObUShort x)
        {
            x.Value = (ushort)(x.Value - 1);
            return x;
        }
        #endregion unary_operators

        public override bool Equals(ushort newValue)
        {
            return (curValue == newValue);
        }
    }

#if ENCRYPT_TYPE
    [Serializable]
    public class ObEncShort : ObservableType<short, EncShort, ObEncShort>
    {
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
        [SerializeField]
        int serializeValue;

        public override void OnAfterDeserialize()
        {
            preValue = UtilBytes.HiWord(serializeValue);
            curValue = UtilBytes.LoWord(serializeValue);
            OnChanged();
        }

        public override void OnBeforeSerialize()
        {
            serializeValue = UtilBytes.MakeLong(curValue, preValue);
        }
#endif

		public ObEncShort(short initValue = 0)
			: base(initValue)
		{
		}

        #region explicit_castings
        public static explicit operator float(ObEncShort x)
        {
            return (float)x.Value;
        }
        public static explicit operator double(ObEncShort x)
        {
            return (double)x.Value;
        }
        public static explicit operator long(ObEncShort x)
        {
            return (long)x.Value;
        }
        public static explicit operator ulong(ObEncShort x)
        {
            return (ulong)x.Value;
        }
        public static explicit operator int(ObEncShort x)
        {
            return (int)x.Value;
        }
        public static explicit operator uint(ObEncShort x)
        {
            return (uint)x.Value;
        }
        public static explicit operator short(ObEncShort x)
        {
            return (short)x.Value;
        }
        public static explicit operator ushort(ObEncShort x)
        {
            return (ushort)x.Value;
        }
        public static explicit operator byte(ObEncShort x)
        {
            return (byte)x.Value;
        }
        public static explicit operator sbyte(ObEncShort x)
        {
            return (sbyte)x.Value;
        }
        #endregion explicit_castings

        #region unary_operators
        public static ObEncShort operator ++(ObEncShort x)
        {
            x.Value = (short)(x.Value + 1);
            return x;
        }
        public static ObEncShort operator --(ObEncShort x)
        {
            x.Value = (short)(x.Value - 1);
            return x;
        }
        #endregion unary_operators

        public override bool Equals(short newValue)
        {
            return (curValue == newValue);
        }
    }

    [Serializable]
    public class ObEncUShort : ObservableType<ushort, EncUShort, ObEncUShort>
    {
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
        [SerializeField]
        int serializeValue;

        public override void OnAfterDeserialize()
        {
            preValue = (ushort)UtilBytes.HiWord(serializeValue);
            curValue = (ushort)UtilBytes.LoWord(serializeValue);
            OnChanged();
        }

        public override void OnBeforeSerialize()
        {
            serializeValue = UtilBytes.MakeLong((short)curValue.ToSystemType(), (short)preValue);
        }
#endif

		public ObEncUShort(ushort initValue = 0)
			: base(initValue)
		{
		}

        #region explicit_castings
        public static explicit operator float(ObEncUShort x)
        {
            return (float)x.Value;
        }
        public static explicit operator double(ObEncUShort x)
        {
            return (double)x.Value;
        }
        public static explicit operator long(ObEncUShort x)
        {
            return (long)x.Value;
        }
        public static explicit operator ulong(ObEncUShort x)
        {
            return (ulong)x.Value;
        }
        public static explicit operator int(ObEncUShort x)
        {
            return (int)x.Value;
        }
        public static explicit operator uint(ObEncUShort x)
        {
            return (uint)x.Value;
        }
        public static explicit operator short(ObEncUShort x)
        {
            return (short)x.Value;
        }
        public static explicit operator ushort(ObEncUShort x)
        {
            return (ushort)x.Value;
        }
        public static explicit operator byte(ObEncUShort x)
        {
            return (byte)x.Value;
        }
        public static explicit operator sbyte(ObEncUShort x)
        {
            return (sbyte)x.Value;
        }
        #endregion explicit_castings

        #region unary_operators
        public static ObEncUShort operator ++(ObEncUShort x)
        {
            x.Value = (ushort)(x.Value + 1);
            return x;
        }
        public static ObEncUShort operator --(ObEncUShort x)
        {
            x.Value = (ushort)(x.Value - 1);
            return x;
        }
        #endregion unary_operators

        public override bool Equals(ushort newValue)
        {
            return (curValue == newValue);
        }
    }
#endif
}