﻿using UnityEngine;
using System;
#if ENCRYPT_TYPE
using AvoEx.EncryptType;
#endif

/* See the "http://avoex.com/avoex/default-license/" for the full license governing this code. */

namespace AvoEx.ObservableType
{
    [Serializable]
    public class ObLong : ObservableType<long, ObLong>
    {
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
        [SerializeField]
        string serializePreValue;
        [SerializeField]
        string serializeValue;

        public override void OnAfterDeserialize()
        {
            preValue = long.Parse(serializePreValue);
            curValue = long.Parse(serializeValue);
            OnChanged();
        }

        public override void OnBeforeSerialize()
        {
            serializePreValue = preValue.ToString();
            serializeValue = curValue.ToString();
        }
#endif
        public ObLong(long initValue = 0L)
            : base(initValue)
        {
        }

        #region explicit_castings
        public static explicit operator float(ObLong x)
        {
            return (float)x.Value;
        }
        public static explicit operator double(ObLong x)
        {
            return (double)x.Value;
        }
        public static explicit operator long(ObLong x)
        {
            return (long)x.Value;
        }
        public static explicit operator ulong(ObLong x)
        {
            return (ulong)x.Value;
        }
        public static explicit operator int(ObLong x)
        {
            return (int)x.Value;
        }
        public static explicit operator uint(ObLong x)
        {
            return (uint)x.Value;
        }
        public static explicit operator short(ObLong x)
        {
            return (short)x.Value;
        }
        public static explicit operator ushort(ObLong x)
        {
            return (ushort)x.Value;
        }
        public static explicit operator byte(ObLong x)
        {
            return (byte)x.Value;
        }
        public static explicit operator sbyte(ObLong x)
        {
            return (sbyte)x.Value;
        }
        #endregion explicit_castings

        #region unary_operators
        public static ObLong operator ++(ObLong x)
        {
            x.Value = x.Value + 1L;
            return x;
        }
        public static ObLong operator --(ObLong x)
        {
            x.Value = x.Value - 1L;
            return x;
        }
        #endregion unary_operators

        public override bool Equals(long newValue)
        {
            return (curValue == newValue);
        }
    }

    [Serializable]
    public class ObULong : ObservableType<ulong, ObULong>
    {
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
        [SerializeField]
        string serializePreValue;
        [SerializeField]
        string serializeValue;

        public override void OnAfterDeserialize()
        {
            preValue = ulong.Parse(serializePreValue);
            curValue = ulong.Parse(serializeValue);
            OnChanged();
        }

        public override void OnBeforeSerialize()
        {
            serializePreValue = preValue.ToString();
            serializeValue = curValue.ToString();
        }
#endif
        public ObULong(ulong initValue = 0L)
            : base(initValue)
        {
        }

        #region explicit_castings
        public static explicit operator float(ObULong x)
        {
            return (float)x.Value;
        }
        public static explicit operator double(ObULong x)
        {
            return (double)x.Value;
        }
        public static explicit operator long(ObULong x)
        {
            return (long)x.Value;
        }
        public static explicit operator ulong(ObULong x)
        {
            return (ulong)x.Value;
        }
        public static explicit operator int(ObULong x)
        {
            return (int)x.Value;
        }
        public static explicit operator uint(ObULong x)
        {
            return (uint)x.Value;
        }
        public static explicit operator short(ObULong x)
        {
            return (short)x.Value;
        }
        public static explicit operator ushort(ObULong x)
        {
            return (ushort)x.Value;
        }
        public static explicit operator byte(ObULong x)
        {
            return (byte)x.Value;
        }
        public static explicit operator sbyte(ObULong x)
        {
            return (sbyte)x.Value;
        }
        #endregion explicit_castings

        #region unary_operators
        public static ObULong operator ++(ObULong x)
        {
            x.Value = x.Value + 1L;
            return x;
        }
        public static ObULong operator --(ObULong x)
        {
            x.Value = x.Value - 1L;
            return x;
        }
        #endregion unary_operators

        public override bool Equals(ulong newValue)
        {
            return (curValue == newValue);
        }
    }

#if ENCRYPT_TYPE
    [Serializable]
    public class ObEncLong : ObservableType<long, EncLong, ObEncLong>
    {
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
        [SerializeField]
        string serializePreValue;
        [SerializeField]
        string serializeValue;

        public override void OnAfterDeserialize()
        {
            preValue = long.Parse(serializePreValue);
            curValue = long.Parse(serializeValue);
            OnChanged();
        }

        public override void OnBeforeSerialize()
        {
            serializePreValue = preValue.ToString();
            serializeValue = curValue.ToString();
        }
#endif
		public ObEncLong(long initValue = 0L)
			: base(initValue)
		{
		}

    #region explicit_castings
        public static explicit operator float(ObEncLong x)
        {
            return (float)x.Value;
        }
        public static explicit operator double(ObEncLong x)
        {
            return (double)x.Value;
        }
        public static explicit operator long(ObEncLong x)
        {
            return (long)x.Value;
        }
        public static explicit operator ulong(ObEncLong x)
        {
            return (ulong)x.Value;
        }
        public static explicit operator int(ObEncLong x)
        {
            return (int)x.Value;
        }
        public static explicit operator uint(ObEncLong x)
        {
            return (uint)x.Value;
        }
        public static explicit operator short(ObEncLong x)
        {
            return (short)x.Value;
        }
        public static explicit operator ushort(ObEncLong x)
        {
            return (ushort)x.Value;
        }
        public static explicit operator byte(ObEncLong x)
        {
            return (byte)x.Value;
        }
        public static explicit operator sbyte(ObEncLong x)
        {
            return (sbyte)x.Value;
        }
        #endregion explicit_castings

    #region unary_operators
        public static ObEncLong operator ++(ObEncLong x)
        {
            x.Value = x.Value + 1L;
            return x;
        }
        public static ObEncLong operator --(ObEncLong x)
        {
            x.Value = x.Value - 1L;
            return x;
        }
        #endregion unary_operators

        public override bool Equals(long newValue)
        {
            return (curValue == newValue);
        }
    }

    [Serializable]
    public class ObEncULong : ObservableType<ulong, EncULong, ObEncULong>
    {
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
        [SerializeField]
        string serializePreValue;
        [SerializeField]
        string serializeValue;

        public override void OnAfterDeserialize()
        {
            preValue = ulong.Parse(serializePreValue);
            curValue = ulong.Parse(serializeValue);
            OnChanged();
        }

        public override void OnBeforeSerialize()
        {
            serializePreValue = preValue.ToString();
            serializeValue = curValue.ToString();
        }
#endif
		public ObEncULong(ulong initValue = 0L)
			: base(initValue)
		{
		}

    #region explicit_castings
        public static explicit operator float(ObEncULong x)
        {
            return (float)x.Value;
        }
        public static explicit operator double(ObEncULong x)
        {
            return (double)x.Value;
        }
        public static explicit operator long(ObEncULong x)
        {
            return (long)x.Value;
        }
        public static explicit operator ulong(ObEncULong x)
        {
            return (ulong)x.Value;
        }
        public static explicit operator int(ObEncULong x)
        {
            return (int)x.Value;
        }
        public static explicit operator uint(ObEncULong x)
        {
            return (uint)x.Value;
        }
        public static explicit operator short(ObEncULong x)
        {
            return (short)x.Value;
        }
        public static explicit operator ushort(ObEncULong x)
        {
            return (ushort)x.Value;
        }
        public static explicit operator byte(ObEncULong x)
        {
            return (byte)x.Value;
        }
        public static explicit operator sbyte(ObEncULong x)
        {
            return (sbyte)x.Value;
        }
        #endregion explicit_castings

    #region unary_operators
        public static ObEncULong operator ++(ObEncULong x)
        {
            x.Value = x.Value + 1L;
            return x;
        }
        public static ObEncULong operator --(ObEncULong x)
        {
            x.Value = x.Value - 1L;
            return x;
        }
        #endregion unary_operators

        public override bool Equals(ulong newValue)
        {
            return (curValue == newValue);
        }
    }
#endif
}