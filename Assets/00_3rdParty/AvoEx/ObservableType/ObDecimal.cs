﻿using UnityEngine;
#if ENCRYPT_TYPE
using AvoEx.EncryptType;
#endif

/* See the "http://avoex.com/avoex/default-license/" for the full license governing this code. */

namespace AvoEx.ObservableType
{
    public class ObDecimal : ObservableType<decimal, ObDecimal>
    {
        public ObDecimal(decimal initValue)
            : base(initValue)
        {
        }

        #region explicit_castings
        public static explicit operator float(ObDecimal x)
        {
            return (float)x.Value;
        }
        public static explicit operator double(ObDecimal x)
        {
            return (double)x.Value;
        }
        public static explicit operator long(ObDecimal x)
        {
            return (long)x.Value;
        }
        public static explicit operator ulong(ObDecimal x)
        {
            return (ulong)x.Value;
        }
        public static explicit operator int(ObDecimal x)
        {
            return (int)x.Value;
        }
        public static explicit operator uint(ObDecimal x)
        {
            return (uint)x.Value;
        }
        public static explicit operator short(ObDecimal x)
        {
            return (short)x.Value;
        }
        public static explicit operator ushort(ObDecimal x)
        {
            return (ushort)x.Value;
        }
        public static explicit operator byte(ObDecimal x)
        {
            return (byte)x.Value;
        }
        public static explicit operator sbyte(ObDecimal x)
        {
            return (sbyte)x.Value;
        }
        #endregion explicit_castings

        #region unary_operators
        public static ObDecimal operator ++(ObDecimal x)
        {
            x.Value = x.Value + 1L;
            return x;
        }
        public static ObDecimal operator --(ObDecimal x)
        {
            x.Value = x.Value - 1L;
            return x;
        }
        #endregion unary_operators

        public override bool Equals(decimal newValue)
        {
            return (curValue == newValue);
        }
    }

#if ENCRYPT_TYPE
    public class ObEncDecimal : ObservableType<decimal, EncDecimal, ObEncDecimal>
    {
        public ObEncDecimal(decimal initValue)
            : base(initValue)
        {
        }

        #region explicit_castings
        public static explicit operator float(ObEncDecimal x)
        {
            return (float)x.Value;
        }
        public static explicit operator double(ObEncDecimal x)
        {
            return (double)x.Value;
        }
        public static explicit operator long(ObEncDecimal x)
        {
            return (long)x.Value;
        }
        public static explicit operator ulong(ObEncDecimal x)
        {
            return (ulong)x.Value;
        }
        public static explicit operator int(ObEncDecimal x)
        {
            return (int)x.Value;
        }
        public static explicit operator uint(ObEncDecimal x)
        {
            return (uint)x.Value;
        }
        public static explicit operator short(ObEncDecimal x)
        {
            return (short)x.Value;
        }
        public static explicit operator ushort(ObEncDecimal x)
        {
            return (ushort)x.Value;
        }
        public static explicit operator byte(ObEncDecimal x)
        {
            return (byte)x.Value;
        }
        public static explicit operator sbyte(ObEncDecimal x)
        {
            return (sbyte)x.Value;
        }
        #endregion explicit_castings

        #region unary_operators
        public static ObEncDecimal operator ++(ObEncDecimal x)
        {
            x.Value = x.Value + 1L;
            return x;
        }
        public static ObEncDecimal operator --(ObEncDecimal x)
        {
            x.Value = x.Value - 1L;
            return x;
        }
        #endregion unary_operators

        public override bool Equals(decimal newValue)
        {
            return (curValue == newValue);
        }
    }
#endif
}