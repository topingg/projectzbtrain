﻿using UnityEngine;
using System;
#if ENCRYPT_TYPE
using AvoEx.EncryptType;
#endif

/* See the "http://avoex.com/avoex/default-license/" for the full license governing this code. */

namespace AvoEx.ObservableType
{
    [Serializable]
    public class ObFloat : ObservableType<float, ObFloat>
    {
		public ObFloat(float initValue = 0f)
			: base(initValue)
		{
		}

        #region explicit_castings
        public static explicit operator float(ObFloat x)
        {
            return (float)x.Value;
        }
        public static explicit operator double(ObFloat x)
        {
            return (double)x.Value;
        }
        public static explicit operator long(ObFloat x)
        {
            return (long)x.Value;
        }
        public static explicit operator ulong(ObFloat x)
        {
            return (ulong)x.Value;
        }
        public static explicit operator int(ObFloat x)
        {
            return (int)x.Value;
        }
        public static explicit operator uint(ObFloat x)
        {
            return (uint)x.Value;
        }
        public static explicit operator short(ObFloat x)
        {
            return (short)x.Value;
        }
        public static explicit operator ushort(ObFloat x)
        {
            return (ushort)x.Value;
        }
        public static explicit operator byte(ObFloat x)
        {
            return (byte)x.Value;
        }
        public static explicit operator sbyte(ObFloat x)
        {
            return (sbyte)x.Value;
        }
        #endregion explicit_castings

        #region unary_operators
        public static ObFloat operator ++(ObFloat x)
        {
            x.Value = x.Value + 1f;
            return x;
        }
        public static ObFloat operator --(ObFloat x)
        {
            x.Value = x.Value - 1f;
            return x;
        }
        #endregion unary_operators

        public override bool Equals(float newValue)
        {
            return (curValue == newValue);
        }
    }

    [Serializable]
    public class ObDouble : ObservableType<double, ObDouble>
    {
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
        [SerializeField]
        string serializePreValue;
        [SerializeField]
        string serializeValue;

        public override void OnAfterDeserialize()
        {
            preValue = double.Parse(serializePreValue);
            curValue = double.Parse(serializeValue);
            OnChanged();
        }

        public override void OnBeforeSerialize()
        {
            serializePreValue = preValue.ToString();
            serializeValue = curValue.ToString();
        }
#endif

		public ObDouble(double initValue = 0D)
			: base(initValue)
		{
		}

        #region explicit_castings
        public static explicit operator float(ObDouble x)
        {
            return (float)x.Value;
        }
        public static explicit operator double(ObDouble x)
        {
            return (double)x.Value;
        }
        public static explicit operator long(ObDouble x)
        {
            return (long)x.Value;
        }
        public static explicit operator ulong(ObDouble x)
        {
            return (ulong)x.Value;
        }
        public static explicit operator int(ObDouble x)
        {
            return (int)x.Value;
        }
        public static explicit operator uint(ObDouble x)
        {
            return (uint)x.Value;
        }
        public static explicit operator short(ObDouble x)
        {
            return (short)x.Value;
        }
        public static explicit operator ushort(ObDouble x)
        {
            return (ushort)x.Value;
        }
        public static explicit operator byte(ObDouble x)
        {
            return (byte)x.Value;
        }
        public static explicit operator sbyte(ObDouble x)
        {
            return (sbyte)x.Value;
        }
        #endregion explicit_castings

        #region unary_operators
        public static ObDouble operator ++(ObDouble x)
        {
            x.Value = x.Value + 1D;
            return x;
        }
        public static ObDouble operator --(ObDouble x)
        {
            x.Value = x.Value - 1D;
            return x;
        }
        #endregion unary_operators

        public override bool Equals(double newValue)
        {
            return (curValue == newValue);
        }
    }

#if ENCRYPT_TYPE
    [Serializable]
    public class ObEncFloat : ObservableType<float, EncFloat, ObEncFloat>
    {
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
        [SerializeField]
        float serializeValue;

        public override void OnAfterDeserialize()
        {
            curValue = serializeValue;
            OnChanged();
        }

        public override void OnBeforeSerialize()
        {
            serializeValue = curValue;
        }
#endif

        public ObEncFloat(float initValue = 0f)
			: base(initValue)
		{
		}

        #region explicit_castings
        public static explicit operator float(ObEncFloat x)
        {
            return (float)x.Value;
        }
        public static explicit operator double(ObEncFloat x)
        {
            return (double)x.Value;
        }
        public static explicit operator long(ObEncFloat x)
        {
            return (long)x.Value;
        }
        public static explicit operator ulong(ObEncFloat x)
        {
            return (ulong)x.Value;
        }
        public static explicit operator int(ObEncFloat x)
        {
            return (int)x.Value;
        }
        public static explicit operator uint(ObEncFloat x)
        {
            return (uint)x.Value;
        }
        public static explicit operator short(ObEncFloat x)
        {
            return (short)x.Value;
        }
        public static explicit operator ushort(ObEncFloat x)
        {
            return (ushort)x.Value;
        }
        public static explicit operator byte(ObEncFloat x)
        {
            return (byte)x.Value;
        }
        public static explicit operator sbyte(ObEncFloat x)
        {
            return (sbyte)x.Value;
        }
        #endregion explicit_castings

        #region unary_operators
        public static ObEncFloat operator ++(ObEncFloat x)
        {
            x.Value = x.Value + 1f;
            return x;
        }
        public static ObEncFloat operator --(ObEncFloat x)
        {
            x.Value = x.Value - 1f;
            return x;
        }
        #endregion unary_operators

        public override bool Equals(float newValue)
        {
            return (curValue == newValue);
        }
    }

    [Serializable]
    public class ObEncDouble : ObservableType<double, EncDouble, ObEncDouble>
    {
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
        [SerializeField]
        string serializePreValue;
        [SerializeField]
        string serializeValue;

        public override void OnAfterDeserialize()
        {
            preValue = double.Parse(serializePreValue);
            curValue = double.Parse(serializeValue);
            OnChanged();
        }

        public override void OnBeforeSerialize()
        {
            serializePreValue = preValue.ToString();
            serializeValue = curValue.ToString();
        }
#endif

        public ObEncDouble(double initValue = 0D)
			: base(initValue)
		{
		}

        #region explicit_castings
        public static explicit operator float(ObEncDouble x)
        {
            return (float)x.Value;
        }
        public static explicit operator double(ObEncDouble x)
        {
            return (double)x.Value;
        }
        public static explicit operator long(ObEncDouble x)
        {
            return (long)x.Value;
        }
        public static explicit operator ulong(ObEncDouble x)
        {
            return (ulong)x.Value;
        }
        public static explicit operator int(ObEncDouble x)
        {
            return (int)x.Value;
        }
        public static explicit operator uint(ObEncDouble x)
        {
            return (uint)x.Value;
        }
        public static explicit operator short(ObEncDouble x)
        {
            return (short)x.Value;
        }
        public static explicit operator ushort(ObEncDouble x)
        {
            return (ushort)x.Value;
        }
        public static explicit operator byte(ObEncDouble x)
        {
            return (byte)x.Value;
        }
        public static explicit operator sbyte(ObEncDouble x)
        {
            return (sbyte)x.Value;
        }
        #endregion explicit_castings

        #region unary_operators
        public static ObEncDouble operator ++(ObEncDouble x)
        {
            x.Value = x.Value + 1D;
            return x;
        }
        public static ObEncDouble operator --(ObEncDouble x)
        {
            x.Value = x.Value - 1D;
            return x;
        }
        #endregion unary_operators

        public override bool Equals(double newValue)
        {
            return (curValue == newValue);
        }
    }
#endif
}