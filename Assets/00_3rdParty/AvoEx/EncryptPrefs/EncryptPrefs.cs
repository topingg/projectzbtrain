﻿using UnityEngine;
using System;

/* See the "http://avoex.com/avoex/default-license/" for the full license governing this code. */

namespace AvoEx
{
    public static class EncryptPrefs
    {
        public const string ENCRYPTPREFS_SETTING_FILE = "encryptprefs";
        static byte[] vector;

        const string FLOAT_POSTFIX = "!F";
        const string INT_POSTFIX = "!I";
        const string STRING_POSTFIX = "!S";
        const string DOUBLE_POSTFIX = "!D";
        const string LONG_POSTFIX = "!L";
        const string BOOL_POSTFIX = "!B";
        const string SHORT_POSTFIX = "!s";

        static EncryptPrefs()
        {
            LoadVector();
        }

        public static void LoadVector()
        {
            TextAsset settingFile = Resources.Load<TextAsset>(ENCRYPTPREFS_SETTING_FILE);
            if (settingFile == null)
            {
                Debug.LogError("failed load setting file : " + ENCRYPTPREFS_SETTING_FILE);
                return;
            }
            vector = settingFile.bytes;
        }

        public static string EncryptKey(string key)
        {
            return AesEncryptor.EncryptIV(key, vector);
        }

        static byte[] GetEncryptedData(string encrypted, string postfix)
        {
            if (string.IsNullOrEmpty(encrypted) || encrypted.EndsWith(postfix) == false)
                return null;

            try
            {
                return Convert.FromBase64String(encrypted.Remove(encrypted.Length - postfix.Length));
            }
            catch
            {
                return null;
            }
        }

#region PLAYER_PREFS
        public static void DeleteAll()
        {
            PlayerPrefs.DeleteAll();
        }
        public static void DeleteKey(string key)
        {
            PlayerPrefs.DeleteKey(EncryptKey(key));
        }

        public static float GetFloat(string key)
        {
            string encrypted = PlayerPrefs.GetString(EncryptKey(key));
            byte[] encryptedBytes = GetEncryptedData(encrypted, FLOAT_POSTFIX);
            if (encryptedBytes != null)
                return AesEncryptor.DecryptFloat(encryptedBytes);

            return 0f;
        }
        public static float GetFloat(string key, float defaultValue)
        {
            string encrypted = PlayerPrefs.GetString(EncryptKey(key));
            byte[] encryptedBytes = GetEncryptedData(encrypted, FLOAT_POSTFIX);
            if (encryptedBytes != null)
                return AesEncryptor.DecryptFloat(encryptedBytes);

            return defaultValue;
        }

        public static int GetInt(string key)
        {
            string encrypted = PlayerPrefs.GetString(EncryptKey(key));
            byte[] encryptedBytes = GetEncryptedData(encrypted, INT_POSTFIX);
            if (encryptedBytes != null)
                return AesEncryptor.DecryptInt(encryptedBytes);

            return 0;
        }
        public static int GetInt(string key, int defaultValue)
        {
            string encrypted = PlayerPrefs.GetString(EncryptKey(key));
            byte[] encryptedBytes = GetEncryptedData(encrypted, INT_POSTFIX);
            if (encryptedBytes != null)
                return AesEncryptor.DecryptInt(encryptedBytes);

            return defaultValue;
        }

        public static string GetString(string key)
        {
            string encrypted = PlayerPrefs.GetString(EncryptKey(key));
            byte[] encryptedBytes = GetEncryptedData(encrypted, STRING_POSTFIX);
            if (encryptedBytes != null)
                return AesEncryptor.DecryptString(encryptedBytes);

            return "";
        }
        public static string GetString(string key, string defaultValue)
        {
            string encrypted = PlayerPrefs.GetString(EncryptKey(key));
            byte[] encryptedBytes = GetEncryptedData(encrypted, STRING_POSTFIX);
            if (encryptedBytes != null)
                return AesEncryptor.DecryptString(encryptedBytes);

            return defaultValue;
        }

        public static bool HasKey(string key)
        {
            return PlayerPrefs.HasKey(EncryptKey(key));
        }

        public static void Save()
        {
            PlayerPrefs.Save();
        }

        public static void SetFloat(string key, float value)
        {
            PlayerPrefs.SetString(EncryptKey(key), AesEncryptor.Encrypt(value) + FLOAT_POSTFIX);
        }
        public static void SetInt(string key, int value)
        {
            PlayerPrefs.SetString(EncryptKey(key), AesEncryptor.Encrypt(value) + INT_POSTFIX);
        }
        public static void SetString(string key, string value)
        {
            PlayerPrefs.SetString(EncryptKey(key), AesEncryptor.Encrypt(value) + STRING_POSTFIX);
        }
#endregion PLAYER_PREFS

#region EXTENDED_PREFS
        public static double GetDouble(string key)
        {
            string encrypted = PlayerPrefs.GetString(EncryptKey(key));
            byte[] encryptedBytes = GetEncryptedData(encrypted, DOUBLE_POSTFIX);
            if (encryptedBytes != null)
                return AesEncryptor.DecryptDouble(encryptedBytes);

            return 0;
        }
        public static double GetDouble(string key, double defaultValue)
        {
            string encrypted = PlayerPrefs.GetString(EncryptKey(key));
            byte[] encryptedBytes = GetEncryptedData(encrypted, DOUBLE_POSTFIX);
            if (encryptedBytes != null)
                return AesEncryptor.DecryptDouble(encryptedBytes);

            return defaultValue;
        }

        public static long GetLong(string key)
        {
            string encrypted = PlayerPrefs.GetString(EncryptKey(key));
            byte[] encryptedBytes = GetEncryptedData(encrypted, LONG_POSTFIX);
            if (encryptedBytes != null)
                return AesEncryptor.DecryptLong(encryptedBytes);

            return 0L;
        }
        public static long GetLong(string key, long defaultValue)
        {
            string encrypted = PlayerPrefs.GetString(EncryptKey(key));
            byte[] encryptedBytes = GetEncryptedData(encrypted, LONG_POSTFIX);
            if (encryptedBytes != null)
                return AesEncryptor.DecryptLong(encryptedBytes);

            return defaultValue;
        }

        public static bool GetBool(string key)
        {
            string encrypted = PlayerPrefs.GetString(EncryptKey(key));
            byte[] encryptedBytes = GetEncryptedData(encrypted, BOOL_POSTFIX);
            if (encryptedBytes != null)
                return AesEncryptor.DecryptBool(encryptedBytes);

            return false;
        }
        public static bool GetBool(string key, bool defaultValue)
        {
            string encrypted = PlayerPrefs.GetString(EncryptKey(key));
            byte[] encryptedBytes = GetEncryptedData(encrypted, BOOL_POSTFIX);
            if (encryptedBytes != null)
                return AesEncryptor.DecryptBool(encryptedBytes);

            return defaultValue;
        }

        public static short GetShort(string key)
        {
            string encrypted = PlayerPrefs.GetString(EncryptKey(key));
            byte[] encryptedBytes = GetEncryptedData(encrypted, SHORT_POSTFIX);
            if (encryptedBytes != null)
                return AesEncryptor.DecryptShort(encryptedBytes);

            return 0;
        }
        public static short GetShort(string key, short defaultValue)
        {
            string encrypted = PlayerPrefs.GetString(EncryptKey(key));
            byte[] encryptedBytes = GetEncryptedData(encrypted, SHORT_POSTFIX);
            if (encryptedBytes != null)
                return AesEncryptor.DecryptShort(encryptedBytes);

            return defaultValue;
        }

        public static void SetDouble(string key, double value)
        {
            PlayerPrefs.SetString(EncryptKey(key), AesEncryptor.Encrypt(value) + DOUBLE_POSTFIX);
        }
        public static void SetLong(string key, long value)
        {
            PlayerPrefs.SetString(EncryptKey(key), AesEncryptor.Encrypt(value) + LONG_POSTFIX);
        }
        public static void SetBool(string key, bool value)
        {
            PlayerPrefs.SetString(EncryptKey(key), AesEncryptor.Encrypt(value) + BOOL_POSTFIX);
        }
        public static void SetShort(string key, short value)
        {
            PlayerPrefs.SetString(EncryptKey(key), AesEncryptor.Encrypt(value) + SHORT_POSTFIX);
        }
#endregion EXTENDED_PREFS

#if UNITY_EDITOR
        public static byte[] IV
        {
            get { return vector; }
        }

        public static bool IsFloat(string encrypted)
        {
            if (string.IsNullOrEmpty(encrypted))
                return false;
            return encrypted.EndsWith(FLOAT_POSTFIX);
        }

        public static bool IsInt(string encrypted)
        {
            if (string.IsNullOrEmpty(encrypted))
                return false;
            return encrypted.EndsWith(INT_POSTFIX);
        }

        public static bool IsString(string encrypted)
        {
            if (string.IsNullOrEmpty(encrypted))
                return false;
            return encrypted.EndsWith(STRING_POSTFIX);
        }

        public static bool IsDouble(string encrypted)
        {
            if (string.IsNullOrEmpty(encrypted))
                return false;
            return encrypted.EndsWith(DOUBLE_POSTFIX);
        }

        public static bool IsLong(string encrypted)
        {
            if (string.IsNullOrEmpty(encrypted))
                return false;
            return encrypted.EndsWith(LONG_POSTFIX);
        }

        public static bool IsBool(string encrypted)
        {
            if (string.IsNullOrEmpty(encrypted))
                return false;
            return encrypted.EndsWith(BOOL_POSTFIX);
        }

        public static bool IsShort(string encrypted)
        {
            if (string.IsNullOrEmpty(encrypted))
                return false;
            return encrypted.EndsWith(SHORT_POSTFIX);
        }
#endif
    }
}