﻿using UnityEngine;
using System.Collections;
using AvoEx;

public class EncryptPrefsSample : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {
        string saveKey = "SAVE_LEVEL";
        //EncryptPrefs.SetInt(saveKey, 10);
        int savedLevel = EncryptPrefs.GetInt(saveKey);
        Debug.Log(saveKey + " = " + savedLevel.ToString());
        float pi = EncryptPrefs.GetFloat("PI");
        Debug.Log("PI = " + pi.ToString());
    }

    // Update is called once per frame
    void Update()
    {

    }
}
