﻿using UnityEngine;
using System.Collections;
using AvoEx;

/* See the "http://avoex.com/avoex/default-license/" for the full license governing this code. */

namespace AvoEx
{

    public class ExampleEncryptPref : MonoBehaviour
    {
        public const float MARGIN = 10f;
        public const float WIDTH = 500f;
        public const float HEIGHT_LINE = 30f;
        public const float HEIGHT_AREA = 120f;
        public const float WIDTH_BUTTON = 50f;

        public const float DEFINE_WIDTH = 100f;
        public const float VALUE_WIDTH = WIDTH - DEFINE_WIDTH;

        string saveKey = "LASTWORD";
        string saveValue = "I AM YOUR FATHER...";
        string result = "";
        string loadKey = "";
        string loadedValue = "";

        void OnGUI()
        {
            GUILayout.BeginArea(new Rect(MARGIN, MARGIN, WIDTH, HEIGHT_AREA));
            GUILayout.BeginHorizontal();
            GUILayout.Label("saveKey = ", GUILayout.Width(DEFINE_WIDTH));
            saveKey = GUILayout.TextField(saveKey, GUILayout.Width(VALUE_WIDTH));
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Label("saveValue = ", GUILayout.Width(DEFINE_WIDTH));
            saveValue = GUILayout.TextField(saveValue, GUILayout.Width(VALUE_WIDTH));
            GUILayout.EndHorizontal();
            GUILayout.EndArea();

            GUILayout.BeginArea(new Rect(MARGIN, MARGIN + HEIGHT_LINE + HEIGHT_LINE, WIDTH, HEIGHT_LINE));
            if (GUILayout.Button("EncryptPrefs.SetString(\"" + saveKey + "\", \"" + saveValue + "\");"))
            {
                EncryptPrefs.SetString(saveKey, saveValue);
                EncryptPrefs.Save();
                result = "Saved key = \"" + saveKey + "\", value = \"" + saveValue + "\"";
                Debug.Log(result);
            }
            GUILayout.EndArea();

            GUILayout.BeginArea(new Rect(MARGIN, MARGIN + HEIGHT_LINE + HEIGHT_LINE + HEIGHT_LINE, WIDTH, HEIGHT_LINE));
            GUILayout.Label(result);
            GUILayout.EndArea();

            GUILayout.BeginArea(new Rect(MARGIN, MARGIN + HEIGHT_AREA + HEIGHT_LINE, WIDTH, HEIGHT_AREA));
            GUILayout.BeginHorizontal();
            GUILayout.Label("loadKey = ", GUILayout.Width(DEFINE_WIDTH));
            loadKey = GUILayout.TextArea(loadKey, GUILayout.Width(VALUE_WIDTH));
            GUILayout.EndHorizontal();
            GUILayout.EndArea();

            GUILayout.BeginArea(new Rect(MARGIN, MARGIN + HEIGHT_AREA + HEIGHT_LINE + HEIGHT_LINE, WIDTH, HEIGHT_LINE));
            if (GUILayout.Button("loadedValue = EncryptPrefs.GetString(\"" + loadKey + "\");"))
            {
                loadedValue = EncryptPrefs.GetString(loadKey);
                Debug.Log("Loaded key = \"" + loadKey + "\", value = \"" + loadedValue + "\"");
            }
            GUILayout.EndArea();

            GUILayout.BeginArea(new Rect(MARGIN, MARGIN + HEIGHT_AREA + HEIGHT_AREA, WIDTH, HEIGHT_AREA));
            GUILayout.BeginHorizontal();
            GUILayout.Label("loadedValue = ", GUILayout.Width(DEFINE_WIDTH));
            GUILayout.Label(loadedValue);
            GUILayout.EndHorizontal();
            GUILayout.EndArea();
        }
    }

}