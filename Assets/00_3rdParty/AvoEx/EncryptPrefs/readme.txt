----------------------------------------------------------------
EncryptPrefs

PlayerPrefs GUI editor/encryptor.
insert/edit/delete/encrypt easily with GUI editor.


more informations:
http://avoex.com
https://www.facebook.com/avoexgames

----------------------------------------------------------------
How to use EncryptPrefs

just use AvoEx.EncryptPrefs instead PlayerPrefs.
 - PlayerPrefs.SetString(); -> AvoEx.EncryptPrefs.SetString();

and you can edit with GUI editor.
 - Window/EncryptPrefs - PlayerPrefs editor



watch video tutorial.
http://youtu.be/LnVDAEpEPcc


-------------------------------------------------------
Caution

If you run Unity on Windows, you must use only english characters or number to product name and company name.
PlayerPrefs saved abnormally with not english product and company name on Windows.

more informations:
http://avoex.com/development/windows-pc%EC%97%90%EC%84%9C-playerprefs-%EC%A0%80%EC%9E%A5-%EC%9C%84%EC%B9%98/


-------------------------------------------------------
version history

1.0.44
 - supporting Window phone 8.

1.0.42
 - supporting double, long, bool, short type.

1.0.37
 - added warning comment on Windows editor with not english product name and company name.
 - fixed build error.

1.0.27
 - fixed editor refreshing in osx
 - added readme.txt
 
1.0.18
 - supporting OSX

1.0.15
 - added refresh button to PlayerPrefs editor
 - added Example/ExampleAesEncryptor.cs

1.0.14
 - fixed delete PrefItem.