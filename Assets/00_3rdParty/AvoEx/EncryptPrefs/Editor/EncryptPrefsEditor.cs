﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using AvoEx;
using System.IO;
using System.Text;
#if UNITY_EDITOR_WIN
using Microsoft.Win32;
#elif UNITY_EDITOR_OSX
using System;
using System.Xml;
#endif

/* See the "http://avoex.com/avoex/default-license/" for the full license governing this code. */

namespace AvoEx
{

    public class EncryptPrefsEditor : EditorWindow
    {
        #region SUB_CLASS
        class PrefItem
        {
            public enum VALUE_TYPE
            {
                STRING,
                INT,
                FLOAT,
                DOUBLE,
                LONG,
                BOOL,
                SHORT,
            }

            public enum VALUE_TYPE_ORG
            {
                STRING,
                INT,
                FLOAT,
            }

            public string savedkey { get; private set; }
            public object value { get; private set; }
            public VALUE_TYPE valueType { get; private set; }

            public string originalkey { get; private set; }
            public object originalValue { get; private set; }
            public bool isEncrypted { get; private set; }

            public string editingKey;
            public object editingValue;

            public PrefItem(string newKey)
            {
                savedkey = newKey;

                try
                {
                    originalkey = AesEncryptor.DecryptIV(savedkey, EncryptPrefs.IV);
                    isEncrypted = true;
                }
                catch
                {
                    originalkey = savedkey;
                    isEncrypted = false;
                }

                if (isEncrypted)
                {
                    value = PlayerPrefs.GetString(newKey);
                    if (EncryptPrefs.IsString((string)value))
                    {
                        valueType = VALUE_TYPE.STRING;
                        originalValue = EncryptPrefs.GetString(originalkey);
                    }
                    else if (EncryptPrefs.IsInt((string)value))
                    {
                        valueType = VALUE_TYPE.INT;
                        originalValue = EncryptPrefs.GetInt(originalkey);
                    }
                    else if (EncryptPrefs.IsFloat((string)value))
                    {
                        valueType = VALUE_TYPE.FLOAT;
                        originalValue = EncryptPrefs.GetFloat(originalkey);
                    }
                    else if (EncryptPrefs.IsDouble((string)value))
                    {
                        valueType = VALUE_TYPE.DOUBLE;
                        originalValue = EncryptPrefs.GetDouble(originalkey);
                    }
                    else if (EncryptPrefs.IsLong((string)value))
                    {
                        valueType = VALUE_TYPE.LONG;
                        originalValue = EncryptPrefs.GetLong(originalkey);
                    }
                    else if (EncryptPrefs.IsBool((string)value))
                    {
                        valueType = VALUE_TYPE.BOOL;
                        originalValue = EncryptPrefs.GetBool(originalkey);
                    }
                    else if (EncryptPrefs.IsShort((string)value))
                    {
                        valueType = VALUE_TYPE.SHORT;
                        originalValue = EncryptPrefs.GetShort(originalkey);
                    }
                }
                else
                {
                    if (PlayerPrefs.GetString(newKey, "") != "" || PlayerPrefs.GetString(newKey, ".") != ".")
                    {
                        value = PlayerPrefs.GetString(newKey);
                        valueType = VALUE_TYPE.STRING;
                        originalValue = value;
                    }
                    else if (PlayerPrefs.GetInt(newKey, int.MinValue) != int.MinValue || PlayerPrefs.GetInt(newKey, int.MaxValue) != int.MaxValue)
                    {
                        value = PlayerPrefs.GetInt(newKey);
                        valueType = VALUE_TYPE.INT;
                        originalValue = value;
                    }
                    else if (PlayerPrefs.GetFloat(newKey, float.MinValue) != float.MinValue || PlayerPrefs.GetFloat(newKey, float.MaxValue) != float.MaxValue)
                    {
                        value = PlayerPrefs.GetFloat(newKey);
                        valueType = VALUE_TYPE.FLOAT;
                        originalValue = value;
                    }
                }

                editingKey = originalkey;
                editingValue = originalValue;
            }

            public PrefItem(bool newIsEncrypted, string newKey, object newValue, VALUE_TYPE newValueType)
            {
                isEncrypted = newIsEncrypted;
                originalkey = editingKey = newKey;
                originalValue = editingValue = newValue;
                valueType = newValueType;
                Save();
            }

            public void Save()
            {
                if (originalkey != editingKey)
                    Delete();

                if (isEncrypted)
                {
                    if (valueType == VALUE_TYPE.STRING)
                        EncryptPrefs.SetString(editingKey, (string)editingValue);
                    else if (valueType == VALUE_TYPE.INT)
                        EncryptPrefs.SetInt(editingKey, (int)editingValue);
                    else if (valueType == VALUE_TYPE.FLOAT)
                        EncryptPrefs.SetFloat(editingKey, (float)editingValue);
                    else if (valueType == VALUE_TYPE.DOUBLE)
                        EncryptPrefs.SetDouble(editingKey, (double)editingValue);
                    else if (valueType == VALUE_TYPE.LONG)
                        EncryptPrefs.SetLong(editingKey, (long)editingValue);
                    else if (valueType == VALUE_TYPE.BOOL)
                        EncryptPrefs.SetBool(editingKey, (bool)editingValue);
                    else if (valueType == VALUE_TYPE.SHORT)
                        EncryptPrefs.SetShort(editingKey, (short)editingValue);

                    savedkey = EncryptPrefs.EncryptKey(editingKey);
                    value = PlayerPrefs.GetString(savedkey);
                }
                else
                {
                    if (valueType == VALUE_TYPE.STRING)
                        PlayerPrefs.SetString(editingKey, (string)editingValue);
                    else if (valueType == VALUE_TYPE.INT)
                        PlayerPrefs.SetInt(editingKey, (int)editingValue);
                    else if (valueType == VALUE_TYPE.FLOAT)
                        PlayerPrefs.SetFloat(editingKey, (float)editingValue);

                    savedkey = editingKey;
                    value = editingValue;
                }

                PlayerPrefs.Save();

                originalkey = editingKey;
                originalValue = editingValue;
            }

            public void Delete()
            {
                PlayerPrefs.DeleteKey(savedkey);
                PlayerPrefs.Save();
            }

            public void Revert()
            {
                editingKey = originalkey;
                editingValue = originalValue;
            }

            public bool IsChanged
            {
                get
                {
                    if (originalkey == editingKey && originalValue.Equals(editingValue))
                        return false;
                    return true;
                }
            }

            #region STATIC
            static Dictionary<string, PrefItem> s_dicPrefItems = new Dictionary<string, PrefItem>();
            public static Dictionary<string, PrefItem> DictionaryPrefItems
            {
                get { return s_dicPrefItems; }
            }

            static List<string> GetPrefKeys()
            {
                List<string> listKeys = new List<string>();
#if UNITY_EDITOR_WIN
                string prefsFilePath = string.Format("Software\\{0}\\{1}",
                    Encoding.Default.GetString(Encoding.UTF8.GetBytes(PlayerSettings.companyName)),
                    Encoding.Default.GetString(Encoding.UTF8.GetBytes(PlayerSettings.productName)));

                RegistryKey key = Registry.CurrentUser.OpenSubKey(prefsFilePath);
                if (key != null)
                {
                    foreach (string valueName in key.GetValueNames())
                    {
                        string prefKey = System.Text.RegularExpressions.Regex.Match(valueName, @"(?<KEY>[\S\s]+)_h[0-9]+").Groups["KEY"].Value;
                        if (string.IsNullOrEmpty(prefKey))
                            prefKey = valueName;
                        listKeys.Add(prefKey);
                    }
                }
#elif UNITY_EDITOR_OSX
                string prefsFilePath = string.Format("{0}/Library/Preferences/unity.{1}.{2}.plist", Environment.GetFolderPath(Environment.SpecialFolder.Personal), PlayerSettings.companyName, PlayerSettings.productName);
				if (File.Exists(prefsFilePath))
				{
					Dictionary<string, object> dicPrefs = (Dictionary<string, object>)PlistCS.Plist.readPlist(prefsFilePath);

					foreach (string key in dicPrefs.Keys)
					{
						listKeys.Add(key);
					}
				}
#endif
                return listKeys;
            }

            public static void LoadPrefItem()
            {
                s_dicPrefItems.Clear();
                List<string> listPrefKeys = GetPrefKeys();

                foreach (string prefKey in listPrefKeys)
                {
                    PrefItem newPrefItem = new PrefItem(prefKey);
                    s_dicPrefItems[newPrefItem.savedkey] = newPrefItem;
                }
            }

            public static void AddPrefItem(bool isEncrypted, string newKey, object newValue, VALUE_TYPE newValueType)
            {
                PrefItem newPrefItem = new PrefItem(isEncrypted, newKey, newValue, newValueType);
                s_dicPrefItems[newPrefItem.savedkey] = newPrefItem;
            }

            public static void DelPrefItem(string savedKey)
            {
                PrefItem delItem;
                if (s_dicPrefItems.TryGetValue(savedKey, out delItem))
                {
                    if (delItem != null)
                        delItem.Delete();
                    s_dicPrefItems.Remove(savedKey);
                }
            }

            public static void ApplyNewVector()
            {
                foreach (PrefItem prefItem in s_dicPrefItems.Values)
                {
                    if (prefItem.isEncrypted == false)
                        continue;
                    prefItem.Delete();
                    prefItem.Save();
                }
            }
            #endregion STATIC
        }
        #endregion SUB_CLASS

        const float KEY_MODE_WIDTH = 80f;
        const float KEY_LABEL_WIDTH = 250f;
        const float DELETE_BUTTON_WIDTH = 60f;

        byte[] defaultVector = new byte[16] { 0x47, 0x41, 0x1A, 0x89, 0xB5, 0x02, 0x6F, 0x62, 0xF1, 0x0F, 0x63, 0x91, 0x96, 0x33, 0x59, 0x50 };

        bool isEncryptMode = true;
        string addingKey = "";
        PrefItem.VALUE_TYPE addingType = PrefItem.VALUE_TYPE.STRING;
        PrefItem.VALUE_TYPE_ORG addingTypeOrg = PrefItem.VALUE_TYPE_ORG.STRING;
        object addingValue = "";

        Vector2 scrollPrefsList;
        string deleteReserved;

        [MenuItem("Tools/AvoEx/EncryptPrefs - PlayerPrefs editor")]
        [MenuItem("Window/EncryptPrefs - PlayerPrefs editor")]
        public static void Open()
        {
            EncryptPrefsEditor window = EditorWindow.GetWindow<EncryptPrefsEditor>(false, "Prefs Editor", true);
            if (window != null)
            {
                window.position = new Rect(210, 210, 800, 300);
                window.minSize = new Vector2(600f, 200f);
            }
        }

        void OnEnable()
        {
            InitAddingEditor();
            PrefItem.LoadPrefItem();
        }

        void OnGUI()
        {
            OnGUITopMenu();
            OnGUIPrefsList();
            OnGUIBottomMenu();
        }

        void OnGUITopMenu()
        {
            if (NeverChangedVector())
            {
                EditorGUILayout.BeginHorizontal();

                EditorGUI.BeginDisabledGroup(true);
                EditorGUILayout.LabelField("Encrypt Vector = ", GUILayout.Width(100f));
                EditorGUILayout.LabelField(System.BitConverter.ToString(EncryptPrefs.IV), EditorStyles.textField);
                EditorGUI.EndDisabledGroup();

                if (GUILayout.Button("generate new vector"))
                {
                    if (EditorUtility.DisplayDialog("Warning!!!", "DO NOT GENERATE NEW VECTOR AFTER RELEASE.", "generate", "cancel"))
                    {
                        File.WriteAllBytes("Assets/AvoEx/EncryptPrefs/Resources/" + EncryptPrefs.ENCRYPTPREFS_SETTING_FILE + ".bytes", AesEncryptor.GenerateIV());
                        AssetDatabase.Refresh();
                        EncryptPrefs.LoadVector();
                        PrefItem.ApplyNewVector();
                    }
                }

                EditorGUILayout.EndHorizontal();

                EditorGUILayout.HelpBox("You need to generate new vector at least once before release. DO NOT CHANGE VECTOR AFTER RELEASE", MessageType.Warning);
            }
        }

        void OnGUIPrefsList()
        {
            // title
            EditorGUILayout.BeginHorizontal();
            EditorGUI.BeginDisabledGroup(true);
            GUI.backgroundColor = Color.black;
            EditorGUILayout.LabelField("MODE", EditorStyles.miniButton, GUILayout.Width(KEY_MODE_WIDTH));
            EditorGUILayout.LabelField("KEY", EditorStyles.miniButton, GUILayout.Width(KEY_LABEL_WIDTH));
            EditorGUILayout.LabelField("TYPE", EditorStyles.miniButton, GUILayout.Width(DELETE_BUTTON_WIDTH));
            EditorGUILayout.LabelField("VALUE", EditorStyles.miniButton);
            EditorGUI.EndDisabledGroup();
            GUI.backgroundColor = Color.blue;
            if (GUILayout.Button("Refresh", GUILayout.Width(DELETE_BUTTON_WIDTH)))
            {
                PrefItem.LoadPrefItem();
            }
            GUI.backgroundColor = Color.white;
            EditorGUILayout.EndHorizontal();

#if UNITY_EDITOR_WIN
            // PlayerPrefs bug..
            bool isValidProductName = System.Text.RegularExpressions.Regex.IsMatch(PlayerSettings.companyName, @"[a-zA-Z0-9]$") && System.Text.RegularExpressions.Regex.IsMatch(PlayerSettings.productName, @"[a-zA-Z0-9]$");
            if (isValidProductName == false)
            {
                string registryPath = string.Format("HKEY_CURRENT_USER\\Software\\{0}\\{1}",
                    Encoding.Default.GetString(Encoding.UTF8.GetBytes(PlayerSettings.companyName)),
                    Encoding.Default.GetString(Encoding.UTF8.GetBytes(PlayerSettings.productName)));
                EditorGUILayout.HelpBox("Can't read PlayerPrefs with GUI editor.\nPlayerPrefs saved to windows registry\n" + registryPath +
                    "\ncan't open registry with this key.\nIf you want read PlayerPrefs data with GUI editor. Run Unity on OSX or Use only alphabet and number to your Company Name(" + PlayerSettings.companyName + ") and Product Name(" + PlayerSettings.productName + ").", MessageType.Warning);
                EditorGUILayout.HelpBox("Company Name(" + PlayerSettings.companyName + ")과 Product Name(" + PlayerSettings.productName + ")이 영문이나 숫자가 아니면, 윈도우즈용 유니티 에디터에서 열람 기능을 사용할 수 없습니다.\nPlayerPrefs가 윈도우 레지스트리에 아래와 같이 저장되게 되는데,\n"
                    + registryPath + "\nregedit로도 열기가 불가능하기 때문입니다(Mac에서는 문제 없음).\n에디터로 열람 기능만 되지 않을뿐, EncryptPrefs 클래스 사용에는 아무런 영향이 없으며, 에디터로 데이터를 저장하는 기능은 사용이 가능합니다.", MessageType.Warning);
            }
#endif

            // list
            scrollPrefsList = EditorGUILayout.BeginScrollView(scrollPrefsList);

            foreach (PrefItem curItem in PrefItem.DictionaryPrefItems.Values)
            {
                if (curItem.savedkey == "UnityGraphicsQuality")
                    continue;
                OnGUIPrefsItem(curItem);
            }

            if (string.IsNullOrEmpty(deleteReserved) == false)
            {
                PrefItem.DelPrefItem(deleteReserved);
                deleteReserved = string.Empty;
            }

#if UNITY_EDITOR_OSX
			EditorGUILayout.HelpBox("It takes few seconds until the edit is applied to the editor window in OSX", MessageType.Warning);
#endif

            EditorGUILayout.EndScrollView();
        }

        void OnGUIPrefsItem(PrefItem prefItem)
        {
            if (prefItem == null)
                return;

            EditorGUILayout.BeginHorizontal();

            try
            {
                if (prefItem.isEncrypted)
                {
                    // key
                    string EDITOR_ITEM_KEY = DecryptItemKey(prefItem.savedkey);
                    bool isDecryptedKey = EditorPrefs.GetBool(EDITOR_ITEM_KEY, true);
                    GUI.backgroundColor = Color.cyan;
                    bool newDecryptedKey = GUILayout.Toggle(isDecryptedKey, (isDecryptedKey ? "EncryptPrefs" : "Saved Data"), EditorStyles.miniButton, GUILayout.Width(KEY_MODE_WIDTH));
                    GUI.backgroundColor = Color.white;
                    if (isDecryptedKey != newDecryptedKey)
                    {
                        EditorPrefs.SetBool(EDITOR_ITEM_KEY, newDecryptedKey);
                        GUI.FocusControl(null);
                    }

                    if (newDecryptedKey)
                    {
                        prefItem.editingKey = EditorGUILayout.TextField(prefItem.editingKey, GUILayout.Width(KEY_LABEL_WIDTH));

                        // value
                        EditorGUI.BeginDisabledGroup(true);
                        EditorGUILayout.LabelField(prefItem.valueType.ToString(), EditorStyles.miniButton, GUILayout.Width(DELETE_BUTTON_WIDTH));
                        EditorGUI.EndDisabledGroup();

                        if (prefItem.valueType == PrefItem.VALUE_TYPE.INT)
                            prefItem.editingValue = EditorGUILayout.IntField((int)prefItem.editingValue);
                        else if (prefItem.valueType == PrefItem.VALUE_TYPE.FLOAT)
                            prefItem.editingValue = EditorGUILayout.FloatField((float)prefItem.editingValue);
                        else if (prefItem.valueType == PrefItem.VALUE_TYPE.STRING)
                            prefItem.editingValue = EditorGUILayout.TextField(prefItem.editingValue.ToString());
                        else if (prefItem.valueType == PrefItem.VALUE_TYPE.DOUBLE)
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
                            InputDouble(ref prefItem.editingValue);
#else
                            prefItem.editingValue = EditorGUILayout.DoubleField((double)prefItem.editingValue);
#endif
                        else if (prefItem.valueType == PrefItem.VALUE_TYPE.LONG)
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
                            InputLong(ref prefItem.editingValue);
#else
                            prefItem.editingValue = EditorGUILayout.LongField((long)prefItem.editingValue);
#endif
                        else if (prefItem.valueType == PrefItem.VALUE_TYPE.BOOL)
                            prefItem.editingValue = EditorGUILayout.Toggle((bool)prefItem.editingValue);
                        else if (prefItem.valueType == PrefItem.VALUE_TYPE.SHORT)
                            prefItem.editingValue = (short)EditorGUILayout.IntField((short)prefItem.editingValue);
                    }
                    else
                    {
                        EditorGUILayout.LabelField(prefItem.savedkey, GUILayout.Width(KEY_LABEL_WIDTH));
                        EditorGUI.BeginDisabledGroup(true);
                        EditorGUILayout.LabelField(prefItem.valueType.ToString(), EditorStyles.miniButton, GUILayout.Width(DELETE_BUTTON_WIDTH));
                        EditorGUI.EndDisabledGroup();
                        EditorGUILayout.LabelField(prefItem.value.ToString());
                    }
                }
                else
                {
                    // key
                    EditorGUI.BeginDisabledGroup(true);
                    GUI.backgroundColor = Color.yellow;
                    EditorGUILayout.LabelField("PlayerPrefs", EditorStyles.miniButton, GUILayout.Width(KEY_MODE_WIDTH));
                    GUI.backgroundColor = Color.white;
                    EditorGUI.EndDisabledGroup();
                    prefItem.editingKey = EditorGUILayout.TextField(prefItem.editingKey, GUILayout.Width(KEY_LABEL_WIDTH));

                    // value
                    EditorGUI.BeginDisabledGroup(true);
                    EditorGUILayout.LabelField(prefItem.valueType.ToString(), EditorStyles.miniButton, GUILayout.Width(DELETE_BUTTON_WIDTH));
                    EditorGUI.EndDisabledGroup();

                    if (prefItem.valueType == PrefItem.VALUE_TYPE.INT)
                        prefItem.editingValue = EditorGUILayout.IntField((int)prefItem.editingValue);
                    else if (prefItem.valueType == PrefItem.VALUE_TYPE.FLOAT)
                        prefItem.editingValue = EditorGUILayout.FloatField((float)prefItem.editingValue);
                    else
                        prefItem.editingValue = EditorGUILayout.TextField(prefItem.editingValue.ToString());
                }

                if (prefItem.IsChanged)
                {
                    GUI.backgroundColor = Color.green;
                    if (GUILayout.Button("save", GUILayout.Width(DELETE_BUTTON_WIDTH)))
                    {
                        prefItem.Save();
                        GUI.FocusControl(null);
                    }
                    GUI.backgroundColor = Color.red;
                    if (GUILayout.Button("revert", GUILayout.Width(DELETE_BUTTON_WIDTH)))
                    {
                        prefItem.Revert();
                        GUI.FocusControl(null);
                    }
                    GUI.backgroundColor = Color.white;
                }
                else
                {
                    GUI.backgroundColor = Color.black;
                    if (GUILayout.Button("delete", GUILayout.Width(DELETE_BUTTON_WIDTH)))
                    {
                        if (EditorUtility.DisplayDialog("Warning!", "delete " + prefItem.originalkey + ".\nYou can not undo this action.", "delete", "cancel"))
                        {
                            deleteReserved = prefItem.savedkey;
                            GUI.FocusControl(null);
                        }
                    }
                    GUI.backgroundColor = Color.white;
                }
            }
            catch
            {
                EditorGUILayout.HelpBox("Refresh after a few seconds.", MessageType.Error);
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
                if (prefItem.valueType != PrefItem.VALUE_TYPE.INT && prefItem.valueType != PrefItem.VALUE_TYPE.FLOAT)
                    EditorGUILayout.HelpBox("If you saved string on Unity 5, can't load on Unity 4.x.", MessageType.Warning);
#endif

                GUI.backgroundColor = Color.black;
                if (GUILayout.Button("delete", GUILayout.Width(DELETE_BUTTON_WIDTH)))
                {
                    if (EditorUtility.DisplayDialog("Warning!", "delete " + prefItem.originalkey + ".\nYou can not undo this action.", "delete", "cancel"))
                    {
                        deleteReserved = prefItem.savedkey;
                        GUI.FocusControl(null);
                    }
                }
                GUI.backgroundColor = Color.white;
            }

            EditorGUILayout.EndHorizontal();

        }

        void OnGUIBottomMenu()
        {

            EditorGUILayout.Separator();

            EditorGUILayout.BeginHorizontal();
            bool isInvalid = true;
            try
            {
                // mode
                if (isEncryptMode)
                    GUI.backgroundColor = Color.cyan;
                else
                    GUI.backgroundColor = Color.yellow;
                isEncryptMode = GUILayout.Toggle(isEncryptMode, isEncryptMode ? "EncryptPrefs" : "PlayerPrefs", EditorStyles.miniButton, GUILayout.Width(KEY_MODE_WIDTH));
                GUI.backgroundColor = Color.white;


                addingKey = EditorGUILayout.TextField(addingKey, GUILayout.Width(KEY_LABEL_WIDTH));

                // value
                if (isEncryptMode)
                {
                    PrefItem.VALUE_TYPE newValueType = (PrefItem.VALUE_TYPE)EditorGUILayout.EnumPopup(addingType, EditorStyles.miniButton, GUILayout.Width(DELETE_BUTTON_WIDTH));
                    if (addingType != newValueType)
                    {
                        addingType = newValueType;
                        if (addingType == PrefItem.VALUE_TYPE.INT)
                            addingValue = 0;
                        else if (addingType == PrefItem.VALUE_TYPE.FLOAT)
                            addingValue = 0f;
                        else if (addingType == PrefItem.VALUE_TYPE.STRING)
                            addingValue = "";
                        else if (addingType == PrefItem.VALUE_TYPE.DOUBLE)
                            addingValue = 0D;
                        else if (addingType == PrefItem.VALUE_TYPE.LONG)
                            addingValue = 0L;
                        else if (addingType == PrefItem.VALUE_TYPE.BOOL)
                            addingValue = false;
                        else if (addingType == PrefItem.VALUE_TYPE.SHORT)
                            addingValue = (short)0;
                    }
                }
                else
                {
                    PrefItem.VALUE_TYPE_ORG newValueType = (PrefItem.VALUE_TYPE_ORG)EditorGUILayout.EnumPopup(addingTypeOrg, EditorStyles.miniButton, GUILayout.Width(DELETE_BUTTON_WIDTH));
                    if (addingTypeOrg != newValueType)
                    {
                        addingTypeOrg = newValueType;
                        if (addingTypeOrg == PrefItem.VALUE_TYPE_ORG.INT)
                        {
                            addingType = PrefItem.VALUE_TYPE.INT;
                            addingValue = 0;
                        }
                        else if (addingTypeOrg == PrefItem.VALUE_TYPE_ORG.FLOAT)
                        {
                            addingType = PrefItem.VALUE_TYPE.FLOAT;
                            addingValue = 0f;
                        }
                        else if (addingTypeOrg == PrefItem.VALUE_TYPE_ORG.STRING)
                        {
                            addingType = PrefItem.VALUE_TYPE.STRING;
                            addingValue = "";
                        }
                    }
                }

                if (addingType == PrefItem.VALUE_TYPE.INT)
                    addingValue = EditorGUILayout.IntField((int)addingValue);
                else if (addingType == PrefItem.VALUE_TYPE.FLOAT)
                    addingValue = EditorGUILayout.FloatField((float)addingValue);
                else if (addingType == PrefItem.VALUE_TYPE.STRING)
                    addingValue = EditorGUILayout.TextField(addingValue.ToString());
                else if (addingType == PrefItem.VALUE_TYPE.DOUBLE)
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
                    InputDouble(ref addingValue);
#else
                    addingValue = EditorGUILayout.DoubleField((double)addingValue);
#endif
                else if (addingType == PrefItem.VALUE_TYPE.LONG)
#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
                    InputLong(ref addingValue);
#else
                    addingValue = EditorGUILayout.LongField((long)addingValue);
#endif
                else if (addingType == PrefItem.VALUE_TYPE.BOOL)
                    addingValue = EditorGUILayout.Toggle((bool)addingValue);
                else if (addingType == PrefItem.VALUE_TYPE.SHORT)
                    addingValue = (short)EditorGUILayout.IntField((short)addingValue);


                if (string.IsNullOrEmpty(addingKey) == false && string.IsNullOrEmpty(addingValue.ToString()) == false)
                    isInvalid = false;

                EditorGUI.BeginDisabledGroup(isInvalid);
                GUI.backgroundColor = Color.green;
                if (GUILayout.Button("add"))
                {
                    PrefItem.AddPrefItem(isEncryptMode, addingKey, addingValue, addingType);
                    InitAddingEditor();
                    GUI.FocusControl(null);
                }
                GUI.backgroundColor = Color.white;
                EditorGUI.EndDisabledGroup();


                GUI.backgroundColor = Color.red;
                if (GUILayout.Button("delete all"))
                {
                    if (EditorUtility.DisplayDialog("Warning!!", "You can not undo this action.", "Delete all", "cancel"))
                    {
                        PlayerPrefs.DeleteAll();
                        PlayerPrefs.Save();
                        PrefItem.LoadPrefItem();
                    }
                }
                GUI.backgroundColor = Color.white;

            }
            catch
            {
                EditorGUILayout.HelpBox("Refresh after a few seconds.", MessageType.Error);

                InitAddingEditor();
            }
            EditorGUILayout.EndHorizontal();

            if (isInvalid)
            {
                EditorGUILayout.HelpBox("you must input KEY & VALUE to add", MessageType.Info);
            }

        }

        //
        string DecryptItemKey(string key)
        {
            return key + "_DECRYPT_KEY";
        }

        bool NeverChangedVector()
        {
            if (defaultVector == null || EncryptPrefs.IV == null)
                return false;
            if (defaultVector.Length != EncryptPrefs.IV.Length)
                return false;
            for (int i = 0; i < defaultVector.Length; i++)
            {
                if (defaultVector[i] != EncryptPrefs.IV[i])
                    return false;
            }
            return true;
        }

        void InitAddingEditor()
        {
            addingKey = "";
            addingType = PrefItem.VALUE_TYPE.STRING;
            addingTypeOrg = PrefItem.VALUE_TYPE_ORG.STRING;
            addingValue = "";
        }

#if UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
        void InputLong(ref object editingValue)
        {
            string newValue = editingValue.ToString();
            EditorGUI.BeginChangeCheck();
            newValue = EditorGUILayout.TextField(newValue);
            if (EditorGUI.EndChangeCheck())
            {
                long longValue = 0;
                if (long.TryParse(newValue, out longValue))
                    editingValue = longValue;
            }
        }

        void InputDouble(ref object editingValue)
        {
            string newValue = editingValue.ToString();
            EditorGUI.BeginChangeCheck();
            newValue = EditorGUILayout.TextField(newValue);
            if (EditorGUI.EndChangeCheck())
            {
                double doubleValue = 0;
                if (double.TryParse(newValue, out doubleValue))
                    editingValue = doubleValue;
            }
        }
#endif
    }

}