﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using cakeslice;

namespace cakeslice
{
    public class OutlineAnimation : MonoBehaviour
    {
        public float m_fDuration = 1f;

        bool pingPong = false;
        float fPastTime = 0f;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            Color c = GetComponent<OutlineEffect>().lineColor0;

            if(pingPong)
            {
                fPastTime += Time.deltaTime;
                //c.a += Mathf.Lerp( Time.deltaTime;

                if(fPastTime >= m_fDuration)
                //if(c.a >= m_fDuration)
                    pingPong = false;
            }
            else
            {
                fPastTime -= Time.deltaTime;
                //c.a -= Time.deltaTime;

                if(fPastTime <= 0)
                //if(c.a <= 0)
                    pingPong = true;
            }

            c.a = Mathf.MoveTowards(c.a, 1f, fPastTime);

            //c.a = Mathf.Clamp01(c.a);
            GetComponent<OutlineEffect>().lineColor0 = c;
            GetComponent<OutlineEffect>().UpdateMaterialsPublicProperties();
        }
    }
}