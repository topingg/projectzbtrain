#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("BtLuKrNALfs5740FmOcGZeX9T8HoO58oG1t+Fipmv0q3xJThT6duFSmod2uyS2AqH3BBzXoJbHrA99VhIO8Ed5X6TB1uC6kuFCFp6jrTp88WzdkrIDmbf013coqpbqu8+GswVd9cUl1t31xXX99cXF2a1Uhu/yAiFpNIBZsLn9RTLqMxQHaQ33nZRHVyP2ZeL6vpE3v1z9IyHc6xLRWaSx7iRyf94WnY+KC+RvK8qZMaKWwEO/5+irhpE+OYmNMnm/AMbBUHXbAxo6q+ssi0USuddD9OCBL6zimwJ/4Aj/g8h+A8Wn1HUCxFDwNKd7gcbd9cf21QW1R32xXbqlBcXFxYXV6lbcLwRutFoUkeiqnJ7O8ueF485vVeu4Yr++N1Bl9eXF1c");
        private static int[] order = new int[] { 12,7,3,11,5,7,6,9,9,10,13,13,12,13,14 };
        private static int key = 93;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
