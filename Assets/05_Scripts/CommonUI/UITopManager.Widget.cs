﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Topping
{
    public partial class UITopManager
    {
        #region Public Variables
        public UIPopup m_uiPopup;
        public UIAnnounce m_uiAnnounce;
        public UIToast m_uiToast;
        public UINotification m_uiNoti;
        #endregion Public Variables

        #region Properties
        public bool IsPopupOpend { get { return m_uiPopup == null ? false : m_uiPopup.IsOpened; } }
        #endregion Properties

        #region Static Reflextion

        #region UI Popup
        /// <summary>
        /// 1. 간단 팝업 표시. (1버튼 팝업)
        /// </summary>
        public static void OpenPopup(string szMsg, string szBtn1, bool isNewPopup = false)
        {
            Instance.m_uiPopup.OpenPopup(szMsg, szBtn1, isNewPopup);
        }

        /// <summary>
        /// 2. 팝업 표시. (1버튼 팝업)
        /// </summary>
        public static void OpenPopup(bool isNewPopup = false, string szMsg = null, string szBtn1 = null, Action callBack1 = null, UIPopup.ENUM_BUTTON_COLOR eColor1 = UIPopup.ENUM_BUTTON_COLOR.NORMAL)
        {
            Instance.m_uiPopup.OpenPopup(isNewPopup, szMsg, szBtn1, callBack1, eColor1);
        }

        /// <summary>
        /// 3. 팝업 표시. (1버튼 팝업)
        /// </summary>
        public static void OpenPopup(bool isNewPopup = false, string szMsg = null, string szBtn1 = null, Action callBack1 = null, Color btn1Color = default(Color))
        {
            Instance.m_uiPopup.OpenPopup(isNewPopup, szMsg, szBtn1, callBack1, btn1Color);
        }

        /// <summary>
        /// 4. 팝업 표시. (2버튼, 2버튼은 닫기)
        /// </summary>
        public static void OpenPopup(bool isNewPopup = false, string szMsg = null, string szBtn1 = null, Action callBack1 = null, string szBtn2 = null, UIPopup.ENUM_BUTTON_COLOR eColor2 = UIPopup.ENUM_BUTTON_COLOR.RED)
        {
            Instance.m_uiPopup.OpenPopup(isNewPopup, szMsg, szBtn1, callBack1, szBtn2, eColor2);
        }

        /// <summary>
        /// 5. 팝업 표시. (2버튼, 2버튼은 닫기)
        /// </summary>
        public static void OpenPopup(bool isNewPopup = false, string szMsg = null, string szBtn1 = null, Action callBack1 = null, string szBtn2 = null, Color btn2Color = default(Color))
        {
            Instance.m_uiPopup.OpenPopup(isNewPopup, szMsg, szBtn1, callBack1, szBtn2, btn2Color);
        }

        /// <summary>
        /// 6. 팝업 표시. (2버튼 팝업)
        /// </summary>
        public static void OpenPopup(bool isNewPopup = false, string szMsg = null, string szBtn1 = null, Action callBack1 = null, string szBtn2 = null, Action callBack2 = null, UIPopup.ENUM_BUTTON_COLOR eColor1 = UIPopup.ENUM_BUTTON_COLOR.NORMAL, UIPopup.ENUM_BUTTON_COLOR eColor2 = UIPopup.ENUM_BUTTON_COLOR.NORMAL)
        {
            Instance.m_uiPopup.OpenPopup(isNewPopup, szMsg, szBtn1, callBack1, szBtn2, callBack2, eColor1, eColor2);
        }

        /// <summary>
        /// 7. 팝업 표시. (2버튼 팝업)
        /// </summary>
        public static void OpenPopup(bool isNewPopup = false, string szMsg = null, string szBtn1 = null, Action callBack1 = null, string szBtn2 = null, Action callBack2 = null, Color btn1Color = default(Color), Color btn2Color = default(Color))
        {
            Instance.m_uiPopup.OpenPopup(isNewPopup, szMsg, szBtn1, callBack1, szBtn2, callBack2, btn1Color, btn2Color);
        }

        /// <summary>
        /// 팝업 닫기. 
        /// </summary>
        public static void ClosePopup()
        {
            Instance.m_uiPopup.ClosePopup();
        }

        /// <summary>
        /// 팝업 닫기. 
        /// </summary>
        public static void HidePopup()
        {
            Instance.m_uiPopup.HidePopup();
        }

        /// <summary>
        /// 취소키 동작 타입 설정. 
        /// </summary>
        public static void SetBackActionTypePopup(UIPopup.ENUM_BACK_KEY_ACTION eType)
        {
            Instance.m_uiPopup.SetBackActionType(eType);
        }

        /// <summary>
        /// 취소키 동작. 
        /// </summary>
        public static void OnBackPressedPopup()
        {
            if (Instance.IsPopupOpend)
                Instance.m_uiPopup.OnBackPressed();
        }

        /// <summary>
        /// 팝업을 연달아 열면 이벤트가 꼬여버리는 현상때문에 만든 메소드. 
        /// UIPopup 클래스 외부에서 이 메소드를 사용할 필요는 없음. 
        /// </summary>
        public static void NextPopup(string szMsg = null, string szBtn1 = null, Action callBack1 = null, string szBtn2 = null, Action callBack2 = null, Color btn1Color = default(Color), Color btn2Color = default(Color))
        {
            HidePopup();
            Instance.StartCoroutine(Instance._CrtOpenNextPopup(szMsg, szBtn1, callBack1, szBtn2, callBack2, btn1Color, btn2Color));
        }

        private IEnumerator _CrtOpenNextPopup(string szMsg = null, string szBtn1 = null, Action callBack1 = null, string szBtn2 = null, Action callBack2 = null, Color btn1Color = default(Color), Color btn2Color = default(Color))
        {
            yield return new WaitForEndOfFrame();
            OpenPopup(false, szMsg, szBtn1, callBack1, szBtn2, callBack2, btn1Color, btn2Color);
        }
        #endregion UI Popup

        #region UI Annouce
        /// <summary>
        /// 아나운서 메시지 추가. 
        /// </summary>
        public static void SetAnnounce(string szContent)
        {
            Instance.m_uiAnnounce.SetAnnounce(szContent);
        }

        /// <summary>
        /// 아나운서 메시지 삭제. 
        /// </summary>
        public static void ClearAnnounce()
        {
            Instance.m_uiAnnounce.ClearAnnouce();
        }

        /// <summary>
        /// 아나운서 메시지 표시.
        /// </summary>
        public static void ShowAnnounce(float fStartDelay = 0f)
        {
            Instance.m_uiAnnounce.ShowAnnounce(fStartDelay);
        }

        /// <summary>
        /// 아나운서 메시지 숨김. 
        /// </summary>
        public static void HideAnnounce()
        {
            Instance.m_uiAnnounce.HideAnnounce();
        }
        #endregion UI Announce

        #region UI Toast
        /// <summary>
        /// 토스트 메시지 표시. 
        /// </summary>
        public static void ShowToast(string szMsg, ENUM_UI_POSITION eType = ENUM_UI_POSITION.BOT)
        {
            Instance.m_uiToast.ShowToast(szMsg, eType);
        }

        /// <summary>
        /// 토스트 메시지 표시. 
        /// </summary>
        public static void ShowToast(string szMsg, Vector2 vPos)
        {
            Instance.m_uiToast.ShowToast(szMsg, vPos);
        }

        /// <summary>
        /// 토스트 메시지 숨김. 
        /// </summary>
        public static void HideToast()
        {
            Instance.m_uiToast.HideToast();
        }
        #endregion UI Toast

        #region UI Notification
        /// <summary>
        /// 알림 메시지 추가. 
        /// </summary>
        public static void SetNoti(string szContent, string szIconName = null)
        {
            Instance.m_uiNoti.SetNoti(szContent, szIconName);
        }

        /// <summary>
        /// 알림 메시지 삭제. 
        /// </summary>
        public static void ClearNoti()
        {
            Instance.m_uiNoti.ClearNoti();
        }

        /// <summary>
        /// 알림 메시지 표시.
        /// </summary>
        public static void ShowNoti(float fStartDelay = 0f)
        {
            Instance.m_uiNoti.ShowNoti(fStartDelay);
        }

        /// <summary>
        /// 알림 메시지 숨김. 
        /// </summary>
        public static void HideNoti()
        {
            Instance.m_uiNoti.HideNoti();
        }
        #endregion UI Notification

        #endregion Static Reflextion
    }

    #region UI Popup
    /// <summary>
    /// 공용 팝업창. 
    /// </summary>
    [Serializable]
    public class UIPopup
    {
        #region Enum
        public enum ENUM_BUTTON_COLOR
        {
            NORMAL,
            RED,
            GREEN,
            BLUE,
            YELLOW,
        }

        public enum ENUM_BACK_KEY_ACTION
        {
            CLOSE,
            BUTTON_1,
            BUTTON_2,
        }
        #endregion Enum

        #region Public Variables
        public GameObject m_objPopup;
        public UILabel m_lblPopupMsg;
        public UIGrid m_gridPopupBtn;
        public UIButton m_btnPopupBtn1;
        public UIButton m_btnPopupBtn2;
        public UILabel m_lblPopupBtn1;
        public UILabel m_lblPopupBtn2;
        #endregion Public Variables

        #region Private Variables
        private readonly Color _colorRed = new Color(1f, 0.29f, 0.4f);
        private readonly Color _colorGreen = new Color(0f, 0.9f, 0.23f);
        private readonly Color _colorBlue = new Color(0f, 0.5f, 1f);
        private readonly Color _colorYellow = new Color(1f, 1f, 0.23f);

        private ENUM_BACK_KEY_ACTION _eBackActionType = ENUM_BACK_KEY_ACTION.CLOSE;
        #endregion Private Variables

        #region Properties
        public bool IsOpened { get { return m_objPopup.activeInHierarchy; } }
        #endregion Properties

        #region Game Methods
        private Color _GetButtonColor(ENUM_BUTTON_COLOR eColor)
        {
            Color returnColor = Color.white;
            switch (eColor)
            {
                case ENUM_BUTTON_COLOR.RED:
                    returnColor = _colorRed;
                    break;
                case ENUM_BUTTON_COLOR.GREEN:
                    returnColor = _colorGreen;
                    break;
                case ENUM_BUTTON_COLOR.BLUE:
                    returnColor = _colorBlue;
                    break;
                case ENUM_BUTTON_COLOR.YELLOW:
                    returnColor = _colorYellow;
                    break;
            }
            return returnColor;
        }

        /// <summary>
        /// 1. 간단 팝업 표시. (1버튼 팝업)
        /// </summary>
        public void OpenPopup(string szMsg, string szBtn1, bool isNewPopup = false)
        {
            OpenPopup(isNewPopup, szMsg, szBtn1, null, default(Color));
        }

        /// <summary>
        /// 2. 팝업 표시. (1버튼 팝업)
        /// </summary>
        public void OpenPopup(bool isNewPopup = false, string szMsg = null, string szBtn1 = null, Action callBack1 = null, ENUM_BUTTON_COLOR eColor1 = ENUM_BUTTON_COLOR.NORMAL)
        {
            OpenPopup(isNewPopup, szMsg, szBtn1, callBack1, _GetButtonColor(eColor1));
        }

        /// <summary>
        /// 3. 팝업 표시. (1버튼 팝업)
        /// </summary>
        public void OpenPopup(bool isNewPopup = false, string szMsg = null, string szBtn1 = null, Action callBack1 = null, Color btn1Color = default(Color))
        {
            OpenPopup(isNewPopup, szMsg, szBtn1, callBack1, null, null, btn1Color, default(Color));
        }

        /// <summary>
        /// 4. 팝업 표시. (2버튼, 2버튼은 닫기)
        /// </summary>
        public void OpenPopup(bool isNewPopup = false, string szMsg = null, string szBtn1 = null, Action callBack1 = null, string szBtn2 = null, ENUM_BUTTON_COLOR eColor2 = ENUM_BUTTON_COLOR.RED)
        {
            OpenPopup(isNewPopup, szMsg, szBtn1, callBack1, szBtn2, _GetButtonColor(eColor2));
        }

        /// <summary>
        /// 5. 팝업 표시. (2버튼, 2버튼은 닫기)
        /// </summary>
        public void OpenPopup(bool isNewPopup = false, string szMsg = null, string szBtn1 = null, Action callBack1 = null, string szBtn2 = null, Color btn2Color = default(Color))
        {
            OpenPopup(isNewPopup, szMsg, szBtn1, callBack1, szBtn2, null, default(Color), btn2Color);
        }

        /// <summary>
        /// 6. 팝업 표시. (2버튼 팝업)
        /// </summary>
        public void OpenPopup(bool isNewPopup = false, string szMsg = null, string szBtn1 = null, Action callBack1 = null, string szBtn2 = null, Action callBack2 = null, ENUM_BUTTON_COLOR eColor1 = ENUM_BUTTON_COLOR.NORMAL, ENUM_BUTTON_COLOR eColor2 = ENUM_BUTTON_COLOR.NORMAL)
        {
            OpenPopup(isNewPopup, szMsg, szBtn1, callBack1, szBtn2, callBack2, _GetButtonColor(eColor1), _GetButtonColor(eColor2));
        }

        /// <summary>
        /// 7. 팝업 표시. (2버튼 팝업)
        /// </summary>
        public void OpenPopup(bool isNewPopup = false, string szMsg = null, string szBtn1 = null, Action callBack1 = null, string szBtn2 = null, Action callBack2 = null, Color btn1Color = default(Color), Color btn2Color = default(Color))
        {
            if (isNewPopup)
            {
                UITopManager.NextPopup(szMsg, szBtn1, callBack1, szBtn2, callBack2, btn1Color, btn2Color);
                return;
            }

            // 열려있으면 리턴. 
            else if (IsOpened)
                return;

            if (GameManager.Instance.IsAppFocused)
                SoundManager.PlaySFX("popup_open");

            // UI Window 숨기기. 
            UITopManager.SetActiveUIWindow(false);

            // UI Window 를 숨길때 커버를 없애므로 아래에 위치. 
            {
                // 게임 화면에서 팝업이 뜰 때는 블러 사용하지 않음. 
                if (GameManager.GameSceneType != GameManager.ENUM_GAME_SCENE_TYPE.GAME)
                    UITopManager.ShowUIBlur();

                UITopManager.ShowBackCover();
            }


            // 1 버튼일 경우. 
            if (callBack1 == null && callBack2 == null)
            {
                callBack1 = ClosePopup;
            }
            // 2 버튼인데 2번 버튼에 이벤트가 없는 경우. 
            else if (!szBtn2.IsNull() && callBack2 == null)
            {
                callBack2 = ClosePopup;
            }

            // 2 버튼에서 1 번 버튼 메소드를 생략한 경우. 
            if (callBack1 == null)
                callBack1 = ClosePopup;

            // 팝업 메시지. 
            m_lblPopupMsg.text = szMsg;

            // 버튼 1. 
            m_lblPopupBtn1.text = szBtn1;
            if (callBack1 != null)
            {
                EventDelegate.Callback callback = new EventDelegate.Callback(callBack1);
                EventDelegate del = new EventDelegate(callback) { oneShot = true };
                EventDelegate.Set(m_btnPopupBtn1.onClick, del);
            }
            if (btn1Color != default(Color))
            {
                m_btnPopupBtn1.defaultColor = btn1Color;
                m_btnPopupBtn1.hover = btn1Color;
                m_btnPopupBtn1.pressed = btn1Color;
            }
            else
            {
                m_btnPopupBtn1.defaultColor = Color.white;
                m_btnPopupBtn1.hover = Color.white;
                m_btnPopupBtn1.pressed = Color.white;
            }
            m_btnPopupBtn1.gameObject.SetActivate(callBack1 != null);

            // 버튼 2. 
            m_lblPopupBtn2.text = szBtn2;
            if (callBack2 != null)
            {
                EventDelegate.Callback callback = new EventDelegate.Callback(callBack2);
                EventDelegate del = new EventDelegate(callback) { oneShot = true };
                EventDelegate.Set(m_btnPopupBtn2.onClick, del);
            }
            if (btn2Color != default(Color))
            {
                m_btnPopupBtn2.defaultColor = btn2Color;
                m_btnPopupBtn2.hover = btn2Color;
                m_btnPopupBtn2.pressed = btn2Color;
            }
            else
            {
                m_btnPopupBtn2.defaultColor = Color.white;
                m_btnPopupBtn2.hover = Color.white;
                m_btnPopupBtn2.pressed = Color.white;
            }
            m_btnPopupBtn2.gameObject.SetActivate(callBack2 != null);

            // 버튼 배치 수정. 
            m_gridPopupBtn.Reposition();
            m_objPopup.SetActivate(true);
        }

        /// <summary>
        /// 팝업 닫기. 
        /// </summary>
        public void ClosePopup()
        {
            HidePopup();

            // Next Popup 설정 문제로 Close 할 때만 초기화 시킴. 
            _eBackActionType = ENUM_BACK_KEY_ACTION.CLOSE;

            UITopManager.HideUIBlur();
            UITopManager.HideBackCover();

            UITopManager.SetActiveUIWindow(true);
        }

        /// <summary>
        /// 팝업 숨김. 
        /// </summary>
        public void HidePopup()
        {
            m_btnPopupBtn1.onClick.Clear();
            m_btnPopupBtn2.onClick.Clear();
            m_objPopup.SetActivate(false);
        }

        /// <summary>
        /// 취소키 동작 타입 설정. 
        /// </summary>
        public void SetBackActionType(ENUM_BACK_KEY_ACTION eType)
        {
            _eBackActionType = eType;
        }

        /// <summary>
        /// 취소키 동작. 
        /// </summary>
        public void OnBackPressed()
        {
            SoundManager.PlaySFX("tick");

            switch (_eBackActionType)
            {
                case ENUM_BACK_KEY_ACTION.BUTTON_1:
                    EventDelegate.Execute(m_btnPopupBtn1.onClick);
                    break;

                case ENUM_BACK_KEY_ACTION.BUTTON_2:
                    EventDelegate.Execute(m_btnPopupBtn2.onClick);
                    break;

                default:
                    ClosePopup();
                    break;
            }
        }
        #endregion Game Methods
    }
    #endregion UI Popup

    #region UI Announce
    [Serializable]
    public class UIAnnounce
    {
        #region Public Variables
        public GameObject m_objAnnounce;
        public UILabel m_lblAnnounce;
        public UITweenController m_tweenAnnounce;
        #endregion Public Variables

        #region Private Variables
        private Queue<string> _queAnnounce = new Queue<string>();
        private bool _bShowAnnounce = false;
        private float _fStartDelay = 0f;
        private float _fStartDelayTime = 0f;
        private float _fShowingTime = 0f;
        #endregion Private Variables

        #region Game Methods
        public void Update(float fDeltaTime)
        {
            if (!_bShowAnnounce)
                return;

            if (!GameManager.Instance.IsPlaying)
                return;

            // 시작 딜레이 체크. 
            if (_fStartDelayTime < _fStartDelay)
            {
                _fStartDelayTime += fDeltaTime;
                return;
            }

            // 표시 딜레이 체크. 
            if (_fShowingTime > 0f)
            {
                _fShowingTime -= fDeltaTime;
                return;
            }

            if (_queAnnounce.Count < 1)
            {
                HideAnnounce();
                return;
            }

            m_lblAnnounce.alpha = 0f;
            m_lblAnnounce.SetText(_queAnnounce.Dequeue());
            m_objAnnounce.SetActivate(true);

            // 트윈 실행.
            m_tweenAnnounce.ForcePlayForward();

            // 표시 시간 설정. 
            _fShowingTime = 4f;
        }

        /// <summary>
        /// 아나운서 메시지 추가. 
        /// </summary>
        public void SetAnnounce(string szContent)
        {
            if (szContent.IsNull())
                return;

            _queAnnounce.Enqueue(szContent);
        }

        /// <summary>
        /// 아나운서 메시지 삭제. 
        /// </summary>
        public void ClearAnnouce()
        {
            _queAnnounce.Clear();
            HideAnnounce();
        }

        /// <summary>
        /// 아나운서 메시지 표시.
        /// </summary>
        public void ShowAnnounce(float fStartDelay = 0f)
        {
            _fStartDelay = fStartDelay;
            _bShowAnnounce = true;
        }

        /// <summary>
        /// 아나운서 메시지 숨김. 
        /// </summary>
        public void HideAnnounce()
        {
            _queAnnounce.Clear();

            _bShowAnnounce = false;
            _fStartDelay = 0f;
            _fStartDelayTime = 0f;
            _fShowingTime = 0f;

            m_objAnnounce.SetActivate(false);
        }
        #endregion Game Methods
    }
    #endregion UI Announce

    #region UI Toast
    [Serializable]
    public class UIToast
    {
        #region Public Variables
        public GameObject m_objToast;
        public Transform m_trToast;
        public UILabel m_lblToastMsg;
        public List<UITweenController> m_lstTweenToast;
        #endregion Public Variables

        /// <summary>
        /// 토스트 메시지 표시. 
        /// </summary>
        public void ShowToast(string szMsg, UITopManager.ENUM_UI_POSITION eType = UITopManager.ENUM_UI_POSITION.BOT)
        {
            switch (eType)
            {
                case UITopManager.ENUM_UI_POSITION.TOP:
                    ShowToast(szMsg, ProjectDefines.TOAST_MSG_POSITION_TOP);
                    break;
                case UITopManager.ENUM_UI_POSITION.MID:
                    ShowToast(szMsg, ProjectDefines.TOAST_MSG_POSITION_MID);
                    break;
                case UITopManager.ENUM_UI_POSITION.BOT:
                    ShowToast(szMsg, ProjectDefines.TOAST_MSG_POSITION_BOT);
                    break;
            }
        }

        /// <summary>
        /// 토스트 메시지 표시. 
        /// </summary>
        public void ShowToast(string szMsg, Vector2 vPos)
        {
            m_objToast.SetActivate(true);

            if (vPos != default(Vector2))
                m_trToast.SetPosition(vPos, true);

            m_lblToastMsg.SetText(szMsg);

            for (int i = 0, cnt = m_lstTweenToast.Count; i < cnt; i++)
            {
                m_lstTweenToast[i].ForcePlayForward(true, HideToast);
            }
        }

        /// <summary>
        /// 토스트 메시지 숨김. 
        /// </summary>
        public void HideToast()
        {
            m_objToast.SetActivate(false);
        }
    }
    #endregion UI Toast

    #region UI Notification
    [Serializable]
    public class UINotification
    {
        #region Public Variables
        public GameObject m_objNoti;
        public UISprite m_sprIcon;
        public UILabel m_lblWithIcon;
        public UILabel m_lblWithOutIcon;
        public UITweenController m_tweenNoti;
        #endregion Public Variables

        #region Private Variables
        private Queue<string> _queNoti = new Queue<string>();
        private bool _bShowNoti = false;
        private float _fStartDelay = 0f;
        private float _fStartDelayTime = 0f;
        private float _fShowingTime = 0f;
        #endregion Private Variables

        #region Game Methods
        public void Update(float fDeltaTime)
        {
            if (!_bShowNoti)
                return;

            // 시작 딜레이 체크. 
            if (_fStartDelayTime < _fStartDelay)
            {
                _fStartDelayTime += fDeltaTime;
                return;
            }

            // 표시 딜레이 체크. 
            if (_fShowingTime > 0f)
            {
                _fShowingTime -= fDeltaTime;
                return;
            }

            if (_queNoti.Count < 1)
            {
                HideNoti();
                return;
            }

            string[] arrMsg = _queNoti.Dequeue().Split('|');
            if (arrMsg.Length > 1)
            {
                m_sprIcon.cachedGameObject.SetActivate(true);
                m_sprIcon.spriteName = arrMsg[0];
                m_lblWithIcon.SetText(arrMsg[1]);
                m_lblWithOutIcon.SetText();
                m_lblWithIcon.cachedGameObject.SetActivate(true);
                m_lblWithOutIcon.cachedGameObject.SetActivate(false);
            }
            else
            {
                m_sprIcon.cachedGameObject.SetActivate(false);
                m_lblWithIcon.SetText();
                m_lblWithOutIcon.SetText(arrMsg[0]);
                m_lblWithIcon.cachedGameObject.SetActivate(false);
                m_lblWithOutIcon.cachedGameObject.SetActivate(true);
            }
            m_objNoti.SetActivate(true);

            // 트윈 실행.
            m_tweenNoti.ForcePlayForward();

            SoundManager.PlaySFX("notification");

            // 표시 시간 설정. 
            _fShowingTime = 4.5f;
        }

        /// <summary>
        /// 알림 메시지 추가. 
        /// </summary>
        public void SetNoti(string szContent, string szIconName = null)
        {
            if (szContent.IsNull())
                return;

            string szNoti = szContent;
            if (!szIconName.IsNull())
                szNoti = string.Concat(szIconName, "|", szNoti);

            _queNoti.Enqueue(szNoti);
        }

        /// <summary>
        /// 알림 메시지 삭제. 
        /// </summary>
        public void ClearNoti()
        {
            _queNoti.Clear();
            HideNoti();
        }

        /// <summary>
        /// 알림 메시지 표시.
        /// </summary>
        public void ShowNoti(float fStartDelay = 0f)
        {
            _fStartDelay = fStartDelay;
            _bShowNoti = true;
        }

        /// <summary>
        /// 알림 메시지 숨김. 
        /// </summary>
        public void HideNoti()
        {
            _queNoti.Clear();

            _bShowNoti = false;
            _fStartDelay = 0f;
            _fStartDelayTime = 0f;
            _fShowingTime = 0f;

            m_objNoti.SetActivate(false);
        }
        #endregion Game Methods
    }
    #endregion UI Notification
}