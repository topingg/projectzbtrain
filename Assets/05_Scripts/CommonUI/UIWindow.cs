﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Topping
{
    public abstract class UIWindow : MonoBehaviour
    {
        #region Delegates
        public event Action OnWindowOpened;
        public event Action OnWindowClosed;
        #endregion Delegates

        #region Public Variables
        public List<UITweenController> m_lstWindowTweener;
        #endregion Public Variables

        #region Private Variables
        private GameObject _cachedGameObject;
        private Transform _cachedTransform;

        private static Stack<UIWindow> _lstUIWindows = new Stack<UIWindow>();
        private Action _openCallBack = null;
        private Action _closeCallBack = null;

        protected bool _isBackPressed = false;
        #endregion Private Variables

        #region Properties
        public GameObject CachedGameObject { get { return _cachedGameObject ?? (_cachedGameObject = gameObject); } }
        public Transform CachedTransform { get { return _cachedTransform ?? (_cachedTransform = transform); } }

        public static Stack<UIWindow> ListWindows { get { return _lstUIWindows; } }
        #endregion Properties

        #region Mono
        protected virtual void Start() { }
        protected virtual void OnEnable()
        {
            InitWindow();
        }
        protected virtual void OnDisable() { }
        #endregion Mono

        #region Game Methods
        /// <summary>
        /// 취소키 입력. 
        /// </summary>
        public virtual void OnBackPressed() { }

        /// <summary>
        /// 윈도우 초기화. 
        /// </summary>
        public virtual void InitWindow()
        {
            _openCallBack = null;
            _closeCallBack = null;
            _isBackPressed = false;
        }

        /// <summary>
        /// 열림 트윈효과. 
        /// </summary>
        public virtual void ShowWindow(bool bResetToBegining = true, Action callBack = null)
        {
            if (m_lstWindowTweener != null)
            {
                bool bRegEvent = false;
                for (int i = 0, cnt = m_lstWindowTweener.Count; i < cnt; i++)
                {
                    if (m_lstWindowTweener[i] == null)
                        continue;

                    if (!bRegEvent)
                    {
                        m_lstWindowTweener[i].ForcePlayForward(bResetToBegining, callBack);
                        bRegEvent = true;
                    }
                    else
                    {
                        m_lstWindowTweener[i].ForcePlayForward(bResetToBegining);
                    }
                }
            }
        }

        /// <summary>
        /// 닫힘 트윈효과. 
        /// </summary>
        public virtual void HideWindow(bool bResetToBegining = true, Action callBack = null)
        {
            if (m_lstWindowTweener != null)
            {
                bool bRegEvent = false;
                for (int i = 0, cnt = m_lstWindowTweener.Count; i < cnt; i++)
                {
                    if (m_lstWindowTweener[i] == null)
                        continue;

                    if (!bRegEvent)
                    {
                        m_lstWindowTweener[i].ForcePlayReverse(bResetToBegining, callBack);
                        bRegEvent = true;
                    }
                    else
                    {
                        m_lstWindowTweener[i].ForcePlayReverse(bResetToBegining);
                    }
                }
            }
        }

        /// <summary>
        /// 창 열기. 
        /// </summary>
        public virtual void OpenWindow(bool bEnableTween = true, bool bResetToBegining = true, Action callBack = null)
        {
            if (_lstUIWindows.Contains(this))
            {
                // 상위 뎁스에 열린 윈도우들 닫기. 
                while (_lstUIWindows.Peek() != this)
                {
                    UIWindow window = _lstUIWindows.Pop();
                    window.ForceClose();
                }
            }
            else
            {
                _lstUIWindows.Push(this);
            }

            CachedGameObject.SetActivate(true);

            if (!bEnableTween)
            {
                _OpenWindow();
                return;
            }

            _openCallBack = callBack;
            ShowWindow(bResetToBegining, _OpenWindow);
        }

        private void _OpenWindow()
        {
            if (_openCallBack != null)
                _openCallBack();

            if (OnWindowOpened != null)
                OnWindowOpened();

            /*try
            {
                Debug.LogError("=== In List ===");
                var itorWindows = _lstUIWindows.GetEnumerator();
                while (itorWindows.MoveNext())
                {
                    Debug.LogError(itorWindows.Current.CachedGameObject.name);
                }
                Debug.LogError("===============");
            }
            catch { }*/
        }

        /// <summary>
        /// 창 닫기. 
        /// </summary>
        public virtual void CloseWindow(bool bEnableTween = true, bool bResetToBegining = true, Action callBack = null)
        {
            if (!bEnableTween)
            {
                _CloseWindow();
                return;
            }

            _closeCallBack = callBack;
            HideWindow(bResetToBegining, _CloseWindow);
        }

        private void _CloseWindow()
        {
            // 리스트 해제. 
            if (_lstUIWindows != null && _lstUIWindows.Count > 0)
            {
                if (_lstUIWindows.Peek() == this)
                    _lstUIWindows.Pop();
            }

            if (_closeCallBack != null)
                _closeCallBack();

            if (OnWindowClosed != null)
                OnWindowClosed();

            CachedGameObject.SetActivate(false);

            /*try
            {
                Debug.LogError("=== In List ===");
                var itorWindows = _lstUIWindows.GetEnumerator();
                while (itorWindows.MoveNext())
                {
                    Debug.LogError(itorWindows.Current.CachedGameObject.name);
                }
                Debug.LogError("===============");
            }
            catch { }*/
        }

        /// <summary>
        /// 현재 열려있는 창 조회하기. 
        /// </summary>
        public static UIWindow GetCurrentWindow(bool bPopWindow = false)
        {
            UIWindow window = null;

            if (_lstUIWindows != null && _lstUIWindows.Count > 0)
            {
                if (bPopWindow)
                    window = _lstUIWindows.Pop();
                else
                    window = _lstUIWindows.Peek();
            }

            return window;
            
        }

        /// <summary>
        /// 이전 창 열기. 
        /// </summary>
        public static void OpenPrevWindow(bool bCloseTween = true, bool bOpenTween = true, bool bCloseTweenReset = true, bool bOpenTweenReset = true, Action closeCallBack = null, Action openCallBack = null)
        {
            try
            {
                UIWindow curWindow = GetCurrentWindow(true);
                UIWindow prevWindow = GetCurrentWindow();

                // 이전 창 열기. 
                if (prevWindow != null)
                    prevWindow.ForceOpen(bOpenTween, bOpenTweenReset, openCallBack);

                // 현재 창 닫기. 
                if (curWindow != null)
                    curWindow.ForceClose(bCloseTween, bCloseTweenReset, closeCallBack);
            }
            catch(Exception e)
            {
                Debug.LogError("Error!! Can't open prev window or prev window is not exist..\n"+ e.Message);
            }
        }

        /// <summary>
        /// 모든 창 닫기. 
        /// </summary>
        public static void ClearWindows()
        {
            if (_lstUIWindows == null)
                _lstUIWindows = new Stack<UIWindow>();

            while(_lstUIWindows.Count > 0)
            {
                _lstUIWindows.Pop().ForceClose(false);
            }

            _lstUIWindows.Clear();

            for (int i = 0, cnt = UITopManager.Instance.m_lstUIWindows.Count; i < cnt; i++)
            {
                UITopManager.Instance.m_lstUIWindows[i].ForceClose();
            }

            UITopManager.SetActiveUIWindow(true);
        }
        #endregion Game Methods
    }

    public static partial class ExtensionMethods
    {
        #region Extension Methods
        /// <summary>
        /// UIWindow 확장 메소드. 
        /// (Null 체크, Window Open)
        /// </summary>
        public static bool ForceOpen(this UIWindow target, bool bEnableTween = true, bool bResetToBegining = true, Action callBack = null)
        {
            bool bResult = false;

            if (target != null)
            {
                target.OpenWindow(bEnableTween, bResetToBegining, callBack);
                bResult = true;
            }

            return bResult;
        }

        /// <summary>
        /// UIWindow 확장 메소드. 
        /// (Null 체크, Window Close)
        /// </summary>
        public static bool ForceClose(this UIWindow target, bool bEnableTween = true, bool bResetToBegining = true, Action callBack = null)
        {
            bool bResult = false;

            if (target != null)
            {
                target.CloseWindow(bEnableTween, bResetToBegining, callBack);
                bResult = true;
            }

            return bResult;
        }
        #endregion Extension Methods
    }
}