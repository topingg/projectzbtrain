﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AvoEx;
using Prime31;

namespace Topping
{
    /// <summary>
    /// 가장 위의 Depth에 표시되는 UI 매니저. 
    /// </summary>
    public partial class UITopManager : MonoSingleton<UITopManager>
    {
        #region Enum
        public enum ENUM_UI_POSITION
        {
            TOP,
            MID,
            BOT,
        }
        #endregion Enum

        #region Public Variables
        /// <summary>
        /// 메인캠 Blur. 
        /// </summary>
        public SnzBlurEffect m_mainCamBlur;

        /// <summary>
        /// UI 배경 Blur. 
        /// </summary>
        public SnzBlurEffect m_UICamBlur;

        /// <summary>
        /// 화면 가리기용 커버. 
        /// </summary>
        public UISprite m_sprCover;

        /// <summary>
        /// 터치 방지용 백 커버.
        /// </summary>
        public UISprite m_sprBackCover;

        /// <summary>
        /// 대기용 커버. 
        /// </summary>
        public GameObject m_objWaitCover;

        /// <summary>
        /// 일시정지 카운터. 
        /// </summary>
        public GameObject m_objPauseCount;

        /// <summary>
        /// UI Window 그룹 오브젝트. 
        /// </summary>
        public GameObject m_objUIWindows;

        /// <summary>
        /// UI Window 리스트. 
        /// </summary>
        public List<UIWindow> m_lstUIWindows;

        /// <summary>
        /// 크레딧 오브젝트. 
        /// </summary>
        public List<GameObject> m_lstCreditObjects;
        #endregion Public Variables

        #region Private Variables
        private Coroutine _crtWaiting = null;
        private Coroutine _crtShareScreenShot = null;
        #endregion Private Variables

        #region Properties
        public bool IsWaiting { get { return m_objWaitCover.activeInHierarchy; } }
        #endregion Properties

        #region Mono
        private void Start()
        {
            // 혹시 모를 윈도우 찌꺼기 삭제. 
            UIWindow.ClearWindows();

            HideUIBlur();
            HideCover();
            HideBackCover();

            HideAnnounce();
            HideToast();
            ClosePopup();

            ShowCredits(false);

            // Test Mode.
            EncryptPrefs.SetBool("TEST_CHAR_SEL_BG_BLUR", true);        // 기본값 : 블러 있음. 
        }

        private void Update()
        {
            m_uiAnnounce.Update(Time.deltaTime);
            m_uiNoti.Update(Time.unscaledDeltaTime);
        }
        #endregion Mono

        #region Game Methods
        /// <summary>
        /// UI Window 그룹 활성화. 
        /// </summary>
        public static void SetActiveUIWindow(bool bActive)
        {
            Instance.m_objUIWindows.SetActivate(bActive);
        }

        /// <summary>
        /// UI Window 조회. 
        /// </summary>
        public static UIWindow GetUIWindow(Type winType)
        {
            UIWindow window = null;

            for (int i = 0, cnt = Instance.m_lstUIWindows.Count; i < cnt; i++)
            {
                if (Instance.m_lstUIWindows[i] == null)
                    continue;

                if (Instance.m_lstUIWindows[i].GetType() == winType)
                    window = Instance.m_lstUIWindows[i];
            }

            return window;
        }

        /// <summary>
        /// 1 Depth UI Blur 표시.  
        /// </summary>
        public static void ShowUIBlur()
        {
            //m_UICamBlur.Opacity = 1f;
            Instance.m_UICamBlur.enabled = true;
        }

        /// <summary>
        /// 1 Depth UI Blur 숨김. 
        /// </summary>
        public static void HideUIBlur()
        {
            // 팝업이 열려있는지 확인. 
            if (Instance.m_uiPopup.IsOpened)
                return;

            // 열린 창이 있는지 확인. 
            var itorWindows = UIWindow.ListWindows.GetEnumerator();
            while (itorWindows.MoveNext())
            {
                if (itorWindows.Current == null)
                    continue;

                if (itorWindows.Current.CachedGameObject.activeInHierarchy)
                    return;
            }

            //m_UICamBlur.Opacity = 0f;
            Instance.m_UICamBlur.enabled = false;
        }

        /// <summary>
        /// 커버 표시. 
        /// </summary>
        public static void ShowCover(Color color = default(Color), float fRestoreTime = 0f)
        {
            if (Instance.m_sprCover == null)
                return;

            if (color == default(Color))
                color = Color.white;

            Instance.m_sprCover.color = color;
            Instance.m_sprCover.cachedGameObject.SetActivate(true);

            if (fRestoreTime > 0f)
                Instance.StartCoroutine(_RestoreCover(fRestoreTime));
        }

        private static IEnumerator _RestoreCover(float fTime)
        {
            yield return new WaitForSeconds(fTime);
            HideCover();
        }

        /// <summary>
        /// 커버 숨기기. 
        /// </summary>
        public static void HideCover()
        {
            if (Instance.m_sprCover != null)
                Instance.m_sprCover.cachedGameObject.SetActivate(false);
        }

        /// <summary>
        /// 백 커버 표시. 
        /// </summary>
        public static void ShowBackCover(Color color = default(Color), float fRestoreTime = 0f)
        {
            if (Instance.m_sprBackCover == null)
                return;

            if (color == default(Color))
                color = new Color(0f, 0f, 0f, 0.5f);

            Instance.m_sprBackCover.color = color;
            Instance.m_sprBackCover.cachedGameObject.SetActivate(true);

            if (fRestoreTime > 0f)
                Instance.StartCoroutine(_RestoreBackCover(fRestoreTime));
        }

        private static IEnumerator _RestoreBackCover(float fTime)
        {
            yield return new WaitForSeconds(fTime);
            HideBackCover();
        }

        /// <summary>
        /// 커버 숨기기. 
        /// </summary>
        public static void HideBackCover()
        {
            // 팝업이 열려있는지 확인. 
            if (Instance.m_uiPopup.IsOpened)
                return;

            // 열린 창이 있는지 확인. 
            var itorWindows = UIWindow.ListWindows.GetEnumerator();
            while (itorWindows.MoveNext())
            {
                if (itorWindows.Current == null)
                    continue;

                if (itorWindows.Current.CachedGameObject.activeInHierarchy)
                    return;
            }

            if (Instance.m_sprBackCover != null)
                Instance.m_sprBackCover.cachedGameObject.SetActivate(false);
        }

        /// <summary>
        /// 대기용 커버 활성화. 
        /// </summary>
        public static void SetActiveWaitCover(bool bActive, float fWaitTime = 0f)
        {
            Instance.m_objWaitCover.SetActivate(bActive);

            if (Instance._crtWaiting != null)
                Instance.StopCoroutine(Instance._crtWaiting);

            if (bActive && fWaitTime > 0f)
                Instance._crtWaiting = Instance.StartCoroutine(Instance._CrtWating(fWaitTime));
        }

        /// <summary>
        /// 대기 시간 체크. 
        /// </summary>
        /// <returns></returns>
        private IEnumerator _CrtWating(float fWaitTime)
        {
            yield return new WaitForSeconds(fWaitTime);

            SetActiveWaitCover(false);
        }

        /// <summary>
        /// 카운트 다운 활성화. 
        /// </summary>
        public static void SetActivePauseCount(bool bActive)
        {
            Instance.m_objPauseCount.SetActivate(bActive);
        }

        /// <summary>
        /// 일시정지 팝업 표시. 
        /// </summary>
        public static void OpenPausePopup()
        {
            // 일시정지 팝업. 
            UITopManager.SetBackActionTypePopup(UIPopup.ENUM_BACK_KEY_ACTION.BUTTON_1);
            UITopManager.OpenPopup(true, "PAUSE",
            "RESUME", () =>
            {
                UITopManager.ClosePopup();
                if (Instance.m_objPauseCount.activeInHierarchy)
                    GameManager.ResumeGame();
                else
                    Instance.m_objPauseCount.SetActivate(true);
            },
            "EXIT", () =>
            {
                // 확인 팝업. 
                UITopManager.OpenPopup(true, "ARE YOU SURE YOU\nWANT TO EXIT?",
                "RETURN", OpenPausePopup,
                "EXIT", () =>
                {
                    UITopManager.ClosePopup();
                    GameManager.GameResult();
                },
                UIPopup.ENUM_BUTTON_COLOR.NORMAL,
                UIPopup.ENUM_BUTTON_COLOR.RED);
            },
            UIPopup.ENUM_BUTTON_COLOR.NORMAL,
            UIPopup.ENUM_BUTTON_COLOR.YELLOW);
        }

        /// <summary>
        /// 스크린샷 공유. 
        /// </summary>
        public void ShareScreenShot(string szTitle, string szContent, float fStartDelay = 1f)
        {
            if (_crtShareScreenShot != null)
                StopCoroutine(_crtShareScreenShot);

            _crtShareScreenShot = StartCoroutine(_CrtMakeScreenShot(szTitle, szContent, fStartDelay));
        }

        private IEnumerator _CrtMakeScreenShot(string szTitle, string szContent, float fStartDelay = 1f)
        {
            // 터치 방지 커버 표시. 
            ShowCover(new Color(1f, 1f, 1f, 0.004f));

            yield return new WaitForSeconds(fStartDelay);

            #if !UNITY_EDITOR && UNITY_ANDROID
            try
            {
                Texture2D tex = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
                tex.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0, false);

                byte[] bytes = tex.EncodeToPNG();
                Destroy(tex);

                string szSavePath = System.IO.Path.Combine(Application.persistentDataPath, "screenshot.png");
                System.IO.File.WriteAllBytes(szSavePath, bytes);

                // 스크린샷 공유. 
                string szChooser = "Sharing a screen shot";
                EtceteraAndroid.shareWithNativeShareIntent(szContent, szTitle, szChooser, szSavePath);
            }
            catch
            {
                ShowToast("Cannot Take Screenshot");
            }
            #endif 

            // 터치 방지. 
            SetActiveWaitCover(true, 10f);

            yield return new WaitForSeconds(1f);

            SetActiveWaitCover(false);

            HideCover();
        }

        /// <summary>
        /// 크레딧 표시. 
        /// </summary>
        public void ShowCredits(bool bActive)
        {
            if (bActive)
            {
                GameManager.Instance.m_UIGameTitle.CachedGameObject.SetActivate(false);
                Instance.m_mainCamBlur.enabled = false;

                ShowToast("THANK YOU FOR PLAYING!", ENUM_UI_POSITION.TOP);
            }
            else
            {
                GameManager.Instance.m_UIGameTitle.CachedGameObject.SetActivate(true);
                Instance.m_mainCamBlur.enabled = true;
            }

            for (int i = 0, cnt = m_lstCreditObjects.Count; i < cnt; i++)
            {
                m_lstCreditObjects[i].SetActivate(bActive);
            }
        }
#endregion Game Methods
    }
}