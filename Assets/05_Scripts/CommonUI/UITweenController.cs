﻿using System;
using System.Collections;
using UnityEngine;

namespace Topping
{
    public class UITweenController : MonoBehaviour
    {
        #region Public Variables
        public bool bActiveTween = false;
        public bool bResetToBegin = true;
        public UITweener[] m_arrTweenTarget;
        #endregion Public Variables

        #region Private Variables
        private GameObject _cachedGameObject = null;
        private Transform _cachedTransform = null;

        private UITweener _finishTween = null;
        private Action _callBackFinished = null;
        #endregion Private Variables

        #region Properties
        public GameObject CachedGameObject { get { return _cachedGameObject ?? (_cachedGameObject = gameObject); } }
        public Transform CachedTransform { get { return _cachedTransform ?? (_cachedTransform = transform); } }
        public bool IsInitialized { get { return m_arrTweenTarget != null && m_arrTweenTarget.Length > 0; } }
        #endregion Properties

        #region Mono
        private void Awake()
        {
            if (m_arrTweenTarget == null || m_arrTweenTarget.Length < 1)
                m_arrTweenTarget = GetComponents<UITweener>();
        }

        private void OnEnable()
        {
            if (bActiveTween)
                PlayForward(bResetToBegin);
        }
        #endregion Mono

        #region Game Methods
        /// <summary>
        /// 트윈 재생. 
        /// </summary>
        public void PlayForward(bool bResetToBegining = true, Action callback = null)
        {
            if (!IsInitialized)
                return;

            CachedGameObject.SetActivate(true);

            _finishTween = null;
            for (int i = 0, cnt = m_arrTweenTarget.Length; i < cnt; i++)
            {
                if (m_arrTweenTarget[i] != null)
                {
                    // 딜리게이트 삭제. 
                    m_arrTweenTarget[i].ClearOnFinished();

                    if (_finishTween == null)
                        _finishTween = m_arrTweenTarget[i];
                    else if (_finishTween.duration < m_arrTweenTarget[i].duration)
                        _finishTween = m_arrTweenTarget[i];

                    if (bResetToBegining)
                        m_arrTweenTarget[i].ResetToBeginning();
                    else
                        m_arrTweenTarget[i].Sample(0f, false);

                    m_arrTweenTarget[i].PlayForward();
                }
            }

            _callBackFinished = callback;

            if (_finishTween != null)
                _finishTween.SetOnFinished(_OnTweenFinished);
            else
                _OnTweenFinished();
        }

        /// <summary>
        /// 트윈 역재생. 
        /// </summary>
        public void PlayReverse(bool bResetToBegining = true, Action callback = null)
        {
            if (!IsInitialized)
                return;

            CachedGameObject.SetActivate(true);

            _finishTween = null;
            for (int i = 0, cnt = m_arrTweenTarget.Length; i < cnt; i++)
            {
                if (m_arrTweenTarget[i] != null)
                {
                    // 딜리게이트 삭제. 
                    m_arrTweenTarget[i].ClearOnFinished();

                    if (_finishTween == null)
                        _finishTween = m_arrTweenTarget[i];
                    else if (_finishTween.duration < m_arrTweenTarget[i].duration)
                        _finishTween = m_arrTweenTarget[i];

                    if (bResetToBegining)
                        m_arrTweenTarget[i].ResetToBeginning();
                    else
                        m_arrTweenTarget[i].Sample(1f, false);

                    m_arrTweenTarget[i].PlayReverse();
                }
            }

            _callBackFinished = callback;

            if (_finishTween != null)
                _finishTween.SetOnFinished(_OnTweenFinished);
            else
                _OnTweenFinished();
        }

        public void ResetToBeginning()
        {
            for (int i = 0, cnt = m_arrTweenTarget.Length; i < cnt; i++)
            {
                if (m_arrTweenTarget[i] != null)
                {
                    m_arrTweenTarget[i].ResetToBeginning();
                }
            }
        }

        public void Finish(bool bDisable = false)
        {
            for (int i = 0, cnt = m_arrTweenTarget.Length; i < cnt; i++)
            {
                if (m_arrTweenTarget[i] != null)
                {
                    m_arrTweenTarget[i].Sample(1f, true);
                    m_arrTweenTarget[i].enabled = false;
                }
            }

            CachedGameObject.SetActivate(!bDisable);
        }
        #endregion Game Methods

        #region Event
        private void _OnTweenFinished()
        {
            if (_callBackFinished != null)
            {
                _callBackFinished();
                _callBackFinished = null;
            }

            if (_finishTween != null)
                _finishTween.ClearOnFinished();
        }
        #endregion Event
    }

    public static partial class ExtensionMethods
    {
        #region Extension Methods
        /// <summary>
        /// UITweenController 확장 메소드. 
        /// (Null 체크)
        /// </summary>
        public static bool ForcePlayForward(this UITweenController target, bool bResetToBegining = true, Action callback = null)
        {
            bool bResult = false;

            if (target != null)
            {
                target.PlayForward(bResetToBegining, callback);
                bResult = true;
            }

            return bResult;
        }

        /// <summary>
        /// UITweenController 확장 메소드. 
        /// (Null 체크)
        /// </summary>
        public static bool ForcePlayReverse(this UITweenController target, bool bResetToBegining = true, Action callback = null)
        {
            bool bResult = false;

            if (target != null)
            {
                target.PlayReverse(bResetToBegining, callback);
                bResult = true;
            }

            return bResult;
        }

        /// <summary>
        /// UITweenController 확장 메소드. 
        /// (Null 체크)
        /// </summary>
        public static bool ForceReset(this UITweenController target)
        {
            bool bResult = false;

            if (target != null)
            {
                target.ResetToBeginning();
                bResult = true;
            }

            return bResult;
        }
        #endregion Extension Methods
    }
}