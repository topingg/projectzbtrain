﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AvoEx;

namespace Topping
{
    public class UITitleScene : MonoBehaviour
    {
        #region Public Variables
        public GameObject m_objStartButton;
        public UILabel m_lblProgress;
        public UISprite m_sprProgress;
        public UISprite m_sprCoverBlack;
        public Transform m_trLoadingTrain;
        #endregion Public Variables

        #region Private Variables
        private float _fTrainPositionValue = 0f;
        #endregion Private Variables

        #region Mono
        void Awake()
        {
            _fTrainPositionValue = 0f;

            _SetStartButtonActive(false);
            _SetProgressBar(0);

            TitleScene.onInitProgress += _OnChangeInitProgress;
            TitleScene.onLoginProgress += _OnChangeLoginProgress;
        }

        void Update()
        {
            _UpdateProgress();
        }

        void OnDestroy()
        {
            TitleScene.onInitProgress -= _OnChangeInitProgress;
            TitleScene.onLoginProgress -= _OnChangeLoginProgress;
        }
        #endregion Mono

        #region Game Methods
        private void _UpdateProgress()
        {
            if (TitleScene.initProgress == TitleScene.ENUM_INIT_PROGRESS.UPDATE_BUNDLE || TitleScene.initProgress == TitleScene.ENUM_INIT_PROGRESS.ASK_DOWNLOAD)
            {
                if (!BundleManager.isInitialized)
                {
                    switch (BundleManager.currentStatus)
                    {
                        case BundleManager.STATUS.INITIALIZE_STARTED:
                            _SetProgressLable(FixedStringData.GetLocalizedString("BundleManager.INITIALIZE_STARTED"));
                            break;
                        case BundleManager.STATUS.LOADING_MANIFEST:
                            _SetProgressLable(FixedStringData.GetLocalizedString("BundleManager.LOADING_MANIFEST"));
                            break;
                        case BundleManager.STATUS.LOADED_BUNDLEINFO:
                            _SetProgressLable(FixedStringData.GetLocalizedString("BundleManager.LOADED_BUNDLEINFO"));
                            break;
                        case BundleManager.STATUS.PREDOWNLOADING:
                            {
                                //_SetProgressBar(BundleManager.ManagedBundle.downloadingProgress * 0.3f);

                                string szBundleName = "Downloading..";
                                if (BundleManager.Instance.listPreloadRequests != null && BundleManager.Instance.listPreloadRequests.Count > 0)
                                    szBundleName = BundleManager.Instance.listPreloadRequests[0].name;

                                _SetProgressLable(string.Format(FixedStringData.GetLocalizedString("BundleManager.PREDOWNLOADING"), BundleManager.Instance.curPreloadCount, BundleManager.Instance.maxPreloadCount, szBundleName.ToUpper()));
                            }
                            break;
                    }
                }
            }
            else if (TitleScene.initProgress == TitleScene.ENUM_INIT_PROGRESS.LOAD_TABLE)
            {
                switch (GameDataManager.currentStatus)
                {
                    case GameDataManager.STATUS.LOADING:
                        {
                            string szTableName = "Table Loading..";
                            int curCount = GameDataManager.Instance.curLoadCount;
                            int maxCount = GameDataManager.Instance.maxLoadCount;
                            szTableName = GameDataManager.loadingTableName;
                            //_SetProgressBar(0.3f + (curCount / (float)maxCount));
                            _SetProgressBar(curCount / (float)maxCount);
                            _SetProgressLable(string.Format(FixedStringData.GetLocalizedString("DataManager.LOADING"), curCount, maxCount, szTableName));
                        }
                        break;

                    case GameDataManager.STATUS.FINISHED:
                        {
                            _SetProgressBar(1f);
                            _SetProgressLable(FixedStringData.GetLocalizedString("DataManager.FINISHED"));
                        }
                        break;
                }
            }
        }

        private void _SetProgressBar(float fValue)
        {
            if (m_sprProgress == null)
                return;

            fValue = Mathf.Clamp(fValue, 0f, 1f);

            m_sprProgress.cachedTransform.localScale = new Vector3(fValue, 1f, 1f);

            // 기차 위치 조정. 
            /*if (_fTrainPositionValue < fValue)
                _fTrainPositionValue = fValue;

            //m_trLoadingTrain.localPosition = new Vector3(Mathf.Lerp(-295f, 295f, _fTrainPositionValue), m_trLoadingTrain.localPosition.y, 0f);        // Right. 
            m_trLoadingTrain.localPosition = new Vector3(Mathf.Lerp(-182f, 182f, _fTrainPositionValue), m_trLoadingTrain.localPosition.y, 0f);          // Center.*/
        }

        private void _SetProgressLable(string szMessage)
        {
            return;
            m_lblProgress.SetText(szMessage);
        }

        private void _SetStartButtonActive(bool active)
        {
            m_objStartButton.SetActivate(active);
        }

        private IEnumerator _MoveNextScene()
        {
            if (m_sprCoverBlack != null)
            {
                /*Go.to(m_sprCoverBlack, 1f,
                    new GoTweenConfig()
                    .floatProp("alpha", 1));*/

                //yield return new WaitForSeconds(1f);
            }

            yield return null;

            TitleScene.MoveNextScene();
        }
        #endregion Game Methods

        #region OnClick
        public void OnClickGameStart()
        {
            TitleScene.StartLoginProgress();
        }
        #endregion OnClick

        #region OnEvent
        private void _OnChangeInitProgress(TitleScene.ENUM_INIT_PROGRESS eProgress)
        {
            switch (eProgress)
            {
                case TitleScene.ENUM_INIT_PROGRESS.UPDATE_BUNDLE_ERROR:
                    _SetProgressLable(string.Format(FixedStringData.GetLocalizedString("ENUM_INIT_PROGRESS.UPDATE_BUNDLE_ERROR"), BundleManager.error));
                    break;

                case TitleScene.ENUM_INIT_PROGRESS.LOAD_TABLE_ERROR:
                    _SetProgressLable(FixedStringData.GetLocalizedString("ENUM_INIT_PROGRESS.LOAD_TABLE_ERROR"));
                    break;

                case TitleScene.ENUM_INIT_PROGRESS.FINISHED:
                    _SetStartButtonActive(true);
                    _SetProgressBar(1f);
                    _SetProgressLable(FixedStringData.GetLocalizedString("ENUM_INIT_PROGRESS.FINISHED"));

                    // 바로 다음 씬으로 이동. 
                    TitleScene.MoveNextScene();
                    break;

                case TitleScene.ENUM_INIT_PROGRESS.START_LOGIN_PROCESS:
                    _SetStartButtonActive(false);
                    _SetProgressBar(0f);
                    _SetProgressLable("");
                    break;

                case TitleScene.ENUM_INIT_PROGRESS.LOGIN_FAILED:
                    _SetStartButtonActive(true);
                    _SetProgressBar(0f);
                    _SetProgressLable(FixedStringData.GetLocalizedString("ENUM_INIT_PROGRESS.LOGIN_FAILED"));
                    break;
            }
        }

        private void _OnChangeLoginProgress(TitleScene.ENUM_LOGIN_PROCESS eProgress)
        {
            switch (eProgress)
            {
                case TitleScene.ENUM_LOGIN_PROCESS.REQUEST_SERVER_LIST:
                    _SetProgressLable(FixedStringData.GetLocalizedString("ENUM_LOGIN_PROCESS.REQUEST_SERVER_LIST"));
                    break;

                case TitleScene.ENUM_LOGIN_PROCESS.RESPONSE_SERVER_LIST:
                    _SetProgressLable(FixedStringData.GetLocalizedString("ENUM_LOGIN_PROCESS.RESPONSE_SERVER_LIST"));
                    break;

                case TitleScene.ENUM_LOGIN_PROCESS.WAIT_LOGIN_ID:
                    _SetProgressLable(FixedStringData.GetLocalizedString("ENUM_LOGIN_PROCESS.WAIT_LOGIN_ID"));
                    break;

                case TitleScene.ENUM_LOGIN_PROCESS.WAIT_NICKNAME:
                    _SetProgressLable(FixedStringData.GetLocalizedString("ENUM_LOGIN_PROCESS.WAIT_NICKNAME"));
                    break;

                case TitleScene.ENUM_LOGIN_PROCESS.REQUEST_LOGIN:
                    _SetProgressLable(FixedStringData.GetLocalizedString("ENUM_LOGIN_PROCESS.REQUEST_LOGIN"));
                    break;

                case TitleScene.ENUM_LOGIN_PROCESS.RESPONSE_LOGIN:
                    _SetProgressLable(FixedStringData.GetLocalizedString("ENUM_LOGIN_PROCESS.RESPONSE_LOGIN"));
                    break;

                case TitleScene.ENUM_LOGIN_PROCESS.FINISHED:
                    _SetProgressLable(FixedStringData.GetLocalizedString("ENUM_LOGIN_PROCESS.FINISHED"));

                    StartCoroutine(_MoveNextScene());
                    break;

                default:
                    _SetProgressBar(0f);
                    _SetProgressLable("");
                    break;
            }
        }
        #endregion OnEvent
    }
}