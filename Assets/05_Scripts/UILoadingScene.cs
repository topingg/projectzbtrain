﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Topping
{
    public class UILoadingScene : MonoBehaviour
    {
        #region Public Variables
        public UISprite m_sprProgress;
        public Transform m_trLoadingTrain;
        public float m_fProgressCachingRate = 0.7f;
        #endregion Public Variables

        #region Private Variables
        private float _fTrainPositionValue = 0f;
        #endregion Private Variables

        #region Game Methods
        private void _SetProgressBar(float fValue)
        {
            if (m_sprProgress == null)
                return;

            fValue = Mathf.Clamp(fValue, 0f, 1f);

            m_sprProgress.cachedTransform.localScale = new Vector3(fValue, 1f, 1f);

            // 기차 위치 조정. 
            /*if (_fTrainPositionValue < fValue)
                _fTrainPositionValue = fValue;

            //m_trLoadingTrain.localPosition = new Vector3(Mathf.Lerp(-295f, 295f, _fTrainPositionValue), m_trLoadingTrain.localPosition.y, 0f);        // Right. 
            m_trLoadingTrain.localPosition = new Vector3(Mathf.Lerp(-182f, 182f, _fTrainPositionValue), m_trLoadingTrain.localPosition.y, 0f);          // Center.
            */
        }
        #endregion Game Methods

        #region MonoBehaviour
        void Awake()
        {
            _fTrainPositionValue = 0f;

            _SetProgressBar(0);
        }

        void Update()
        {
            if (LoadingScene.isPreLoadingCompleted)
            {
                float fSceneApplyRate = (1f - m_fProgressCachingRate) * LoadingScene.sceneLoadingRate;
                float fPreRate = m_fProgressCachingRate + LoadingScene.sceneLoadedRate;
                _SetProgressBar(fPreRate + (EngineManager.sceneLoadingProgress * fSceneApplyRate));

                //_SetProgressBar(m_fProgressCachingRate + (EngineManager.sceneLoadingProgress * (1f - m_fProgressCachingRate)));
            }
            else
            {
                _SetProgressBar(LoadingScene.currentLoadingRate);
            }
        }
        #endregion MonoBehaviour
    }
}