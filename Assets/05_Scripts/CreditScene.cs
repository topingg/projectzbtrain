﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Topping
{
    public class CreditScene : MonoBehaviour
    {
        public List<PlayerController> m_lstPlayerController;

        private List<bool> _lstSide = new List<bool>();
        private List<float> _lstDelay = new List<float>();

        private void OnEnable()
        {
            if (_lstSide == null)
                _lstSide = new List<bool>();
            else
                _lstSide.Clear();

            if (_lstDelay == null)
                _lstDelay = new List<float>();
            else
                _lstDelay.Clear();

            for (int i = 0, cnt = m_lstPlayerController.Count; i < cnt; i++)
            {
                _lstSide.Add(false);
                _lstDelay.Add(Random.Range(0.1f, 2f));
            }
        }

        private void Update()
        {
            _UpdateAnimation();
        }

        private void _UpdateAnimation()
        {
            for (int i = 0, cnt = m_lstPlayerController.Count; i < cnt; i++)
            {
                if (_lstDelay[i] <= 0f)
                {
                    _lstDelay[i] = Random.Range(0.1f, 2f);

                    if (_lstSide[i])
                        m_lstPlayerController[i].cachedTransform.localRotation = Quaternion.Euler(0f, 180f, 0f);
                    else
                        m_lstPlayerController[i].cachedTransform.localRotation = Quaternion.identity;

                    m_lstPlayerController[i].PlayerObject.cachedAnimator.SetTrigger("IsAttack");
                    _lstSide[i] = !_lstSide[i];
                }
                else
                {
                    _lstDelay[i] -= Time.deltaTime;
                }
            }
        }
    }
}