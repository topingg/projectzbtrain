﻿using UnityEngine;
using UnityEditor;
#if UNITY_IOS
    using UnityEditor.Callbacks;
    using UnityEditor.iOS.Xcode;
#endif

using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace Topping
{
    public class CustomBuilder : EditorWindow
    {
        #region Enums
        private enum ENUM_SERVER_TYPE
        {
            NONE,

            DEV,
            STAGE,
            LIVE,
        }

        private enum ENUM_IOS_ARCHITECTURE
        {
            NONE,

            ARM64,
            UNIVERSAL,

            INVALID,
        }

        private enum ENUM_APP_SIGN
        {
            DEV             = 0,
            GOOGLE          = 1,
        }

        private enum ENUM_IOS_PROVISIONING
        {
            DEV             = 0,
            ADHOC           = 1,
            DISTRIBUTION    = 2,
        }
        #endregion Enums

        #region Public Variables
        public static CustomBuilder curWindow = null;
        public static Exception globalPostProcessException = null;
        #endregion Public Variables

        #region Private Variables
        private const string PROVISIONING_FILE_PATH = "Keystore/iOSProvisioning";
        private static string APPLICATION_PATH = "";
        private static string BUILD_PATH = "";

        #if UNITY_ANDROID
        private readonly BuildTarget BUILD_TARGET = BuildTarget.Android;
        #elif UNITY_IOS
        private readonly  BuildTarget BUILD_TARGET = BuildTarget.iOS;
        #else
        private readonly BuildTarget BUILD_TARGET = BuildTarget.Android;
        #endif

        private ENUM_SERVER_TYPE _eServerType = ENUM_SERVER_TYPE.NONE;
        private string _szSVNRevision = null;
        private string _szBuildVersion = "";//KRPG5DataManager.BuildVersion;
        private string _szBundleIdentifier = "";

        private string _szVersion = "";
        private int _iVersionCode = 0;
        private AndroidBuildSystem _eAndroidBuildSystem = AndroidBuildSystem.Gradle;
        private ENUM_APP_SIGN _eAppSign = ENUM_APP_SIGN.DEV;

        private string _keyStorePath = "";
        private string _szKeyStorePassword = "";

        private string _sziOSBuildNumber = "";
        private iOSBuildType _eIOSBuildType = iOSBuildType.Release;
        private ENUM_IOS_PROVISIONING _eProvisioning = ENUM_IOS_PROVISIONING.DEV;
        private int _iPrevProvisioningIndex = -1;

        private string _szTeamIdentifier = "";
        private string _ProvisioningUUID = "";
        private string _ProvisioningName = "";

        private bool _bShowOption = false;

        private bool _bDevelopmentBuild = false;
        private bool _bUseIL2CPP = false;
        private bool _bConnectProfiler = false;
        private bool _bAllowDebugging = false;

        public static Stopwatch sw = new Stopwatch();

        // ENUM_APP_SIGN 인덱스와 동기화. 
        private string[] _arrAndroidBundleIdentifier = new string[]
        {
            "com.topping.zbtrain",
            "com.topping.zbtrain",
        };

        // ENUM_IOS_PROVISIONING 인덱스와 동기화. 
        private string[] _arrIOSBundleIdentifier = new string[]
        {
            "com.topping.zbtrain",
            "com.topping.zbtrain",
            "com.topping.zbtrain",
        };

        private string[] _arrIOSFileSuffix = new string[]
        {
            "_DEV",
            "_ADHOC",
            "_STORE",
        };

        private string[] _arrIOSExportOptionsPlist = new string[]
        {
            "exportOptions_dev.plist",
            "exportOptions_adhoc.plist",
            "exportOptions_store.plist",
        };

        private string[] _arrIOSProvisiningFilename = new string[]
        {
            "ZBT_DEV.mobileprovision",
            "ZBT_ADHOC.mobileprovision",
            "ZBT_STORE.mobileprovision",
        };

        // TODO : 인증서 갱신시 이름 갱신 필요
        private string[] _arrIOSCodeSignIdentityName = new string[]
        {
            "iPhone Developer: Min Seo (LSXUQP44B6)",                       // NEXON_DEV
            "iPhone Distribution: NEXON Korea Corporation (NDX844788Z)",    // NEXON_ADHOC
            "iPhone Distribution: NEXON Korea Corporation (NDX844788Z)",    // NEXON_APPSTORE
            "iPhone Distribution: NEXON Korea Corporation (NDX844788Z)",    // NEXON_INHOUSE,
            "iPhone Distribution: Movegames Co., Ltd (F6N9HPGLJ8)",         // MOVE_DEV
            "iPhone Distribution: Movegames Co., Ltd (F6N9HPGLJ8)",         // MOVE_DIST
        };
        #endregion Private Variables

        
        #region Mono
        [MenuItem("Tools/Project Builder %#z")]
        static void OpenCustomBuilderWindow()
        {
            CustomBuilder window = GetWindow<CustomBuilder>(false, "Project Builder", true);
            if (window != null)
            {
                window.position = new Rect(400, 400, 400, 400);
                window.minSize = new Vector2(550, 300);
            }
        }

        public CustomBuilder()
        {
            curWindow = this;
        }

        private void OnEnable()
        {
            LoadConfig();
        }

        private void OnDisable()
        {
            SaveConfig();
        }
        #endregion Mono

        #region Game Methods
        public string GetFilenameSuffix()
        {
            return _arrIOSFileSuffix[(int)_eProvisioning];
        }

        public string GetExportOptionsPlist()
        {
            return _arrIOSExportOptionsPlist[(int)_eProvisioning];
        }

        public string GetCodeSignIdentityName()
        {
            return _arrIOSCodeSignIdentityName[(int)_eProvisioning];
        }

        public bool IsIOSDevBuild()
        {
            if (_eProvisioning == ENUM_IOS_PROVISIONING.DEV)
                return true;

            return false;
        }

        public string GetSigningCetificateSimpleName()
        {
            return IsIOSDevBuild() == true ? "iPhone Developer" : "iPhone Distribution";
        }

        public void SaveConfig()
        {
            EditorPrefs.SetString("szBundleIdentifier", _szBundleIdentifier);

            EditorPrefs.SetString("szVersion", _szVersion);
            EditorPrefs.SetInt("iVersionCode", _iVersionCode);
            EditorPrefs.SetInt("eAndroidBuildSystem", (int)_eAndroidBuildSystem);
            EditorPrefs.SetInt("eAppSign", (int)_eAppSign);

            EditorPrefs.SetString("szKeyStorePassword", _szKeyStorePassword);

            EditorPrefs.SetString("sziOSBuildNumber", _sziOSBuildNumber);
            EditorPrefs.SetInt("eIOSBuildType", (int)_eIOSBuildType);
            EditorPrefs.SetInt("eProvisioning", (int)_eProvisioning);

            EditorPrefs.SetBool("bDevelopmentBuild", _bDevelopmentBuild);
            EditorPrefs.SetBool("bUseIL2CPP", _bUseIL2CPP);
            EditorPrefs.SetBool("bConnectProfiler", _bConnectProfiler);
            EditorPrefs.SetBool("bAllowDebugging", _bAllowDebugging);
        }

        public void LoadConfig()
        {
            if (BUILD_TARGET == BuildTarget.iOS)
                BUILD_PATH = APPLICATION_PATH + "/" + "XcodeBuild";
            else
                BUILD_PATH = APPLICATION_PATH + "/" + "Builds";

            APPLICATION_PATH = Application.dataPath.Replace("/Assets", "");

            _szBundleIdentifier = EditorPrefs.GetString("szBundleIdentifier", PlayerSettings.applicationIdentifier);

            _szVersion = EditorPrefs.GetString("szVersion", PlayerSettings.bundleVersion);
            _iVersionCode = EditorPrefs.GetInt("iVersionCode", PlayerSettings.Android.bundleVersionCode);
            _eAndroidBuildSystem = (AndroidBuildSystem)EditorPrefs.GetInt("eAndroidBuildSystem", (int)AndroidBuildSystem.Internal);
            _eAppSign = (ENUM_APP_SIGN)EditorPrefs.GetInt("eAppSign", (int)ENUM_APP_SIGN.DEV);

            _szKeyStorePassword = EditorPrefs.GetString("szKeyStorePassword", PlayerSettings.Android.keystorePass);

            _sziOSBuildNumber = EditorPrefs.GetString("sziOSBuildNumber", PlayerSettings.iOS.buildNumber);
            _eIOSBuildType = (iOSBuildType)EditorPrefs.GetInt("eIOSBuildType", (int)iOSBuildType.Debug);
            _eProvisioning = (ENUM_IOS_PROVISIONING)EditorPrefs.GetInt("eProvisioning", (int)ENUM_IOS_PROVISIONING.DEV);

            _bDevelopmentBuild = EditorPrefs.GetBool("bDevelopmentBuild", false);
            _bUseIL2CPP = EditorPrefs.GetBool("bUseIL2CPP", BUILD_TARGET == BuildTarget.iOS ? true : false);
            _bConnectProfiler = EditorPrefs.GetBool("bConnectProfiler", false);
            _bAllowDebugging = EditorPrefs.GetBool("bAllowDebugging", false);

            ApplyToPlayerSettings();
            _iPrevProvisioningIndex = -1;
        }

        void OnGUI()
        {
            try
            {

                GUIStyle foldoutStyle = CreateFoldoutGUI();

                var expand = '\u2261'.ToString();
                GUIContent expandContent = new GUIContent(expand, "Expand/Collapse");

                EditorGUILayout.BeginVertical(EditorStyles.objectFieldThumb, GUILayout.ExpandWidth(true));
                {
                    EditorGUILayout.BeginHorizontal();
                    {
                        GUI.color = Color.white;
                        EditorGUILayout.Foldout(true, new GUIContent("Build Settings"), foldoutStyle);
                        //_bShowOption = GUILayout.Toggle(_bShowOption, expandContent, EditorStyles.toolbarButton, GUILayout.Width(50f));
                    }
                    EditorGUILayout.EndHorizontal();
                }
                EditorGUILayout.EndVertical();

                EditorGUILayout.BeginVertical();
                EditorGUILayout.Space();
                EditorGUILayout.EndVertical();

                EditorGUILayout.BeginHorizontal();
                if (GUILayout.Button("Select Server", EditorStyles.toolbarDropDown, GUILayout.Width(100f)))
                {
                    GUI.FocusControl(null);
                    GenericMenu menuURLs = new GenericMenu();
                    menuURLs.AddItem(new GUIContent("None"), false, () => { _eServerType = ENUM_SERVER_TYPE.NONE; });
                    menuURLs.AddItem(new GUIContent("Develop"), false, () => { _eServerType = ENUM_SERVER_TYPE.DEV; });
                    menuURLs.AddItem(new GUIContent("Stage"), false, () => { _eServerType = ENUM_SERVER_TYPE.STAGE; });
                    menuURLs.AddItem(new GUIContent("Live"), false, () => { _eServerType = ENUM_SERVER_TYPE.LIVE; });

                    menuURLs.DropDown(new Rect(5f, 10f, Screen.width, 16));
                }
                EditorGUILayout.LabelField(_eServerType.ToString(), GUILayout.Width(150f));
                EditorGUILayout.Space();

                if (GUILayout.Button("Apply", GUILayout.Width(100f)))
                {
                    ApplyChanges();
                }
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginVertical();
                EditorGUILayout.Space();
                EditorGUILayout.EndVertical();

                EditorGUILayout.BeginHorizontal();
                //EditorGUILayout.LabelField(, GUILayout.Width(100));
                _szBuildVersion = EditorGUILayout.TextField("Build Name", _szBuildVersion, GUILayout.Width(400f));
                EditorGUILayout.Space();
                GUI.backgroundColor = Color.green;
                if (GUILayout.Button("Build", GUILayout.Width(100f)))
                {
                    if (EditorUtility.DisplayDialog("삽질방지", "잠깐!! AssetManager 설정은 잘 하셨나요?", "네", "아니오"))
                    {
                        // EditorWindow.GetWindow(Type.GetType("UnityEditor.BuildPlayerWindow,UnityEditor"));
                        BuildPlayer();
                    }
                    else
                    {
                        AvoEx.AssetManagerEditor window = GetWindow<AvoEx.AssetManagerEditor>(false, "AssetManager", true);
                        if (window != null)
                        {
                            window.position = new Rect(220, 220, 800, 400);
                            window.minSize = new Vector2(515f, 200f);
                        }
                    }
                }
                GUI.backgroundColor = Color.white;
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginVertical();
                EditorGUILayout.Space();
                EditorGUILayout.EndVertical();

                EditorGUILayout.BeginVertical();
                {
                    //--> Build Settings
                    GUILayout.Label("Build Settings", EditorStyles.boldLabel);
                    EditorGUILayout.BeginVertical();
                    {
                        EditorGUILayout.LabelField("Bundle Identifier", _szBundleIdentifier, GUILayout.Width(400f));
                        _szVersion = EditorGUILayout.TextField("Version", _szVersion, GUILayout.Width(400f));
                        if (BUILD_TARGET == BuildTarget.iOS)
                        {
                            _sziOSBuildNumber = EditorGUILayout.TextField("Build Number", _sziOSBuildNumber, GUILayout.Width(400f));
                            _eIOSBuildType = (iOSBuildType)EditorGUILayout.EnumPopup("Build Type", _eIOSBuildType, GUILayout.Width(400f));
                        }
                        else if (BUILD_TARGET == BuildTarget.Android)
                        {
                            _iVersionCode = EditorGUILayout.IntField("Version Code", _iVersionCode, GUILayout.Width(400f));
                            _eAndroidBuildSystem = (AndroidBuildSystem)EditorGUILayout.EnumPopup("Build System", _eAndroidBuildSystem, GUILayout.Width(400f));
                        }
                        EditorGUILayout.Space();
                    }
                    EditorGUILayout.EndVertical();
                    //<--

                    //--> BUILD TARGET
                    if (BUILD_TARGET == BuildTarget.Android)
                    {
                        GUILayout.Label("Keystore settings", EditorStyles.boldLabel);
                        EditorGUILayout.BeginVertical();
                        {
                            _eAppSign = (ENUM_APP_SIGN)EditorGUILayout.EnumPopup("KeyStore", (Enum)_eAppSign, GUILayout.Width(400f));
                            _keyStorePath = APPLICATION_PATH + "/";
                            if (_eAppSign == ENUM_APP_SIGN.DEV)
                            {
                                _keyStorePath = _keyStorePath + "Keystore/dev.keystore";
                                PlayerSettings.Android.keyaliasName = "dev";
                            }
                            else if (_eAppSign == ENUM_APP_SIGN.GOOGLE)
                            {
                                _keyStorePath = _keyStorePath + "Keystore/google_release_key.jks";
                                PlayerSettings.Android.keyaliasName = "google";
                            }
                            else
                            {
                                _keyStorePath = "";
                                PlayerSettings.Android.keyaliasName = "";
                            }

                            _szBundleIdentifier = _arrAndroidBundleIdentifier[(int)_eAppSign];

                            EditorGUILayout.LabelField("Keystore Name", PlayerSettings.Android.keystoreName, GUILayout.Width(600f));
                            _szKeyStorePassword = EditorGUILayout.PasswordField("Keystore Password", _szKeyStorePassword, GUILayout.Width(400f));
                            EditorGUILayout.LabelField("Key Alias Name", PlayerSettings.Android.keyaliasName, GUILayout.Width(400f));
                            EditorGUILayout.Space();
                        }
                        EditorGUILayout.EndVertical();
                    }
                    else if (BUILD_TARGET == BuildTarget.iOS)
                    {
                        GUILayout.Label("Provisioning settings", EditorStyles.boldLabel);
                        EditorGUILayout.BeginVertical();
                        {
                            _eProvisioning = (ENUM_IOS_PROVISIONING)EditorGUILayout.EnumPopup("Provisioning", (Enum)_eProvisioning, GUILayout.Width(400f));

                            if (_iPrevProvisioningIndex != (int)_eProvisioning)
                            {
                                string provisioningFilename = APPLICATION_PATH + "/" + PROVISIONING_FILE_PATH + "/" + _arrIOSProvisiningFilename[(int)_eProvisioning];
                                string provisioningText = File.ReadAllText(provisioningFilename);

                                // Team Identifier
                                int startIndex = provisioningText.IndexOf("<key>TeamIdentifier</key>");
                                int endIndex = provisioningText.IndexOf("</string>", startIndex);
                                string resultText = provisioningText.Substring(startIndex, endIndex - startIndex);
                                int lastIndex = resultText.LastIndexOf("<string>");
                                _szTeamIdentifier = resultText.Substring(lastIndex).Replace("<string>", "");

                                // Provisioning UUID
                                startIndex = provisioningText.IndexOf("<key>UUID</key>");
                                endIndex = provisioningText.IndexOf("</string>", startIndex);
                                resultText = provisioningText.Substring(startIndex, endIndex - startIndex);
                                lastIndex = resultText.LastIndexOf("<string>");
                                _ProvisioningUUID = resultText.Substring(lastIndex).Replace("<string>", "");

                                // Provisioning Name
                                startIndex = provisioningText.IndexOf("<key>Name</key>");
                                endIndex = provisioningText.IndexOf("</string>", startIndex);
                                resultText = provisioningText.Substring(startIndex, endIndex - startIndex);
                                lastIndex = resultText.LastIndexOf("<string>");
                                _ProvisioningName = resultText.Substring(lastIndex).Replace("<string>", "");

                                _iPrevProvisioningIndex = (int)_eProvisioning;

                                PlayerSettings.iOS.appleEnableAutomaticSigning = false;
                                PlayerSettings.iOS.appleDeveloperTeamID = "";
                            }

                            _szBundleIdentifier = _arrIOSBundleIdentifier[(int)_eProvisioning];

                            EditorGUILayout.LabelField("Team Identifier", _szTeamIdentifier, GUILayout.Width(500f));
                            EditorGUILayout.LabelField("Provisioning Name", _ProvisioningName, GUILayout.Width(500f));
                            EditorGUILayout.LabelField("Provisioning UUID", PlayerSettings.iOS.iOSManualProvisioningProfileID, GUILayout.Width(500f));
                            EditorGUILayout.LabelField("Coed Sign Identity", _arrIOSCodeSignIdentityName[(int)_eProvisioning]);
                            EditorGUILayout.LabelField("Architecture", ((ENUM_IOS_ARCHITECTURE)PlayerSettings.GetArchitecture(BuildTargetGroup.iOS)).ToString());
                            EditorGUILayout.Space();

                        }
                        EditorGUILayout.EndVertical();
                        //<--
                    }

                    EditorGUILayout.BeginVertical();
                    EditorGUILayout.Space();
                    EditorGUILayout.EndVertical();
                    EditorGUILayout.BeginVertical();
                    EditorGUILayout.Space();
                    EditorGUILayout.EndVertical();

                    EditorGUILayout.BeginVertical(EditorStyles.objectFieldThumb, GUILayout.ExpandWidth(true));
                    {
                        EditorGUILayout.BeginHorizontal();
                        {
                            GUI.color = Color.white;
                            _bShowOption = EditorGUILayout.Foldout(_bShowOption, new GUIContent("Build Options"), foldoutStyle);
                            _bShowOption = GUILayout.Toggle(_bShowOption, expandContent, EditorStyles.toolbarButton, GUILayout.Width(50f));
                        }
                        EditorGUILayout.EndHorizontal();
                    }
                    EditorGUILayout.EndVertical();

                    if (_bShowOption)
                    {
                        //--> Build options
                        GUILayout.Label("Build Options", EditorStyles.boldLabel);
                        EditorGUILayout.BeginVertical();
                        {
                            EditorGUI.BeginDisabledGroup(BUILD_TARGET == BuildTarget.iOS ? true : false);
                            _bUseIL2CPP = EditorGUILayout.Toggle("Use IL2CPP", BUILD_TARGET == BuildTarget.iOS ? true : _bUseIL2CPP);
                            EditorGUI.EndDisabledGroup();
                            _bDevelopmentBuild = EditorGUILayout.Toggle("Development Build", _bDevelopmentBuild);
                            _bConnectProfiler = EditorGUILayout.Toggle("Connect Profiler", _bConnectProfiler);
                            _bAllowDebugging = EditorGUILayout.Toggle("Allow Debugging", _bAllowDebugging);
                        }
                        EditorGUILayout.EndVertical();
                        //<--
                    }
                }
                EditorGUILayout.EndVertical();

                ApplyToPlayerSettings();
            }
            catch { }
        }

        private GUIStyle CreateFoldoutGUI()
        {
            GUIStyle myFoldoutStyle = new GUIStyle(EditorStyles.foldout);
            Color myStyleColor = Color.white;
            myFoldoutStyle.fontStyle = FontStyle.Bold;
            myFoldoutStyle.normal.textColor = myStyleColor;
            myFoldoutStyle.onNormal.textColor = myStyleColor;
            myFoldoutStyle.hover.textColor = myStyleColor;
            myFoldoutStyle.onHover.textColor = myStyleColor;
            myFoldoutStyle.focused.textColor = myStyleColor;
            myFoldoutStyle.onFocused.textColor = myStyleColor;
            myFoldoutStyle.active.textColor = myStyleColor;
            myFoldoutStyle.onActive.textColor = myStyleColor;

            return myFoldoutStyle;
        }

        void BuildPlayer()
        {
            globalPostProcessException = null;

            sw.Reset ();
			sw.Start ();

			SaveConfig();

            string buildFilename = _szBuildVersion + "_" + DateTime.Now.ToString("HHmmss");
            string ext = "";
			if (BUILD_TARGET == BuildTarget.Android) 
			{
				buildFilename += ".apk";
				ext = "apk";
            }

            string buildPath = EditorUtility.SaveFilePanel("Build " + BUILD_TARGET.ToString(), BUILD_PATH, buildFilename, ext);

            if (string.IsNullOrEmpty(buildPath))
                return;

            List<string> levelList = new List<string>();
            levelList.Add("Assets/04_Scenes/IntroScene.unity"); // 기본
            levelList.Add("Assets/04_Scenes/TitleScene.unity"); // 기본

            string[] levels = levelList.ToArray();

            // Build Player.
            BuildOptions buildOptions = BuildOptions.None;
            if (_bDevelopmentBuild == true)
                buildOptions |= BuildOptions.Development;

            if (_bUseIL2CPP == true || BUILD_TARGET == BuildTarget.iOS)
                buildOptions |= BuildOptions.Il2CPP;

            if (_bConnectProfiler == true)
                buildOptions |= BuildOptions.ConnectWithProfiler;

            if (_bAllowDebugging == true)
                buildOptions |= BuildOptions.AllowDebugging;

            BuildPipeline.BuildPlayer(levels, buildPath, BUILD_TARGET, buildOptions);

            EditorUtility.RevealInFinder(buildPath);
        }

        void ApplyToPlayerSettings()
        {
            PlayerSettings.applicationIdentifier = _szBundleIdentifier;
            PlayerSettings.bundleVersion = _szVersion;

            if (BUILD_TARGET == BuildTarget.iOS)
                PlayerSettings.iOS.buildNumber = _sziOSBuildNumber;
            else
                PlayerSettings.Android.bundleVersionCode = _iVersionCode;

            PlayerSettings.Android.keystoreName = _keyStorePath;
            PlayerSettings.Android.keystorePass = _szKeyStorePassword;
            PlayerSettings.Android.keyaliasPass = _szKeyStorePassword;

            PlayerSettings.iOS.iOSManualProvisioningProfileID = _ProvisioningUUID;

            EditorUserBuildSettings.development = _bDevelopmentBuild;
            EditorUserBuildSettings.allowDebugging = _bAllowDebugging;
            EditorUserBuildSettings.connectProfiler = _bConnectProfiler;
            EditorUserBuildSettings.iOSBuildConfigType = _eIOSBuildType;
            EditorUserBuildSettings.androidBuildSystem = _eAndroidBuildSystem;

            if (BUILD_TARGET == BuildTarget.iOS)
            {
                _bUseIL2CPP = true;
                PlayerSettings.SetArchitecture(BuildTargetGroup.iOS, (int)ENUM_IOS_ARCHITECTURE.UNIVERSAL);
            }

            PlayerSettings.SetScriptingBackend(
                BUILD_TARGET == BuildTarget.Android ? BuildTargetGroup.Android : BuildTargetGroup.iOS, 
                _bUseIL2CPP == true ? ScriptingImplementation.IL2CPP : ScriptingImplementation.Mono2x);


            //--> !! 현재 무조건 끄게 되어있음. 필요하면 추가 !!
            PlayerSettings.stripEngineCode = false;
            PlayerSettings.strippingLevel = StrippingLevel.Disabled;
            //<--
        }

        void ApplyChanges()
        {
            SaveConfig();
            //ApplyTapjoySetting();
            //GetSVNRevision();
            //EditAndroidManifest();

            //_szBuildVersion = _eServerType.ToString() + "." + DateTime.Today.ToString("yyMMdd") + ".r" + _szSVNRevision;

            string szPublishType = "";
            if (BUILD_TARGET == BuildTarget.Android)
            {
                switch (_eAppSign)
                {
                    case ENUM_APP_SIGN.DEV:
                        szPublishType = "_DEV";
                        _szKeyStorePassword = "topping";
                        break;
                    case ENUM_APP_SIGN.GOOGLE:
                        szPublishType = "_PUB";
                        _szKeyStorePassword = "!toPPing@gameS&";
                        break;
                }
            }

            _szBuildVersion = "ZBT" + szPublishType + "_" + _szVersion + "." + _iVersionCode + "." + DateTime.Now.ToString("yyMMdd");
            //SaveBuildVersionCode();
        }

        /*public void ApplyTapjoySetting()
        {
            TapjoyUnity.Internal.TapjoySettings settings = TapjoyEditor.TapjoySettingsEditor.Load();
            TapjoyUnity.Internal.PlatformSettings platformSettings = null;

            if (BUILD_TARGET == BuildTarget.iOS)
                platformSettings = settings.IosSettings;
            else
                platformSettings = settings.AndroidSettings;

            // SDK 키
            if (SelectedServer == SELECTED_SERVER.Dev_Server)
            {
                platformSettings.SdkKey = KRPGDefines.TAPJOY_SDK_KEY_DEV;
                platformSettings.PushKey = "603860994338";
            }
            else if (SelectedServer == SELECTED_SERVER.Stage_Server)
            {
                platformSettings.SdkKey = KRPGDefines.TAPJOY_SDK_KEY_STAGE;
            }
            else if (SelectedServer == SELECTED_SERVER.Live_Server)
            {
                platformSettings.SdkKey = KRPGDefines.TAPJOY_SDK_KEY_LIVE;
            }
            else
            {
                platformSettings.SdkKey = KRPGDefines.TAPJOY_SDK_KEY_CBT;
            }

            // 디버그 모드
            if (SelectedServer == SELECTED_SERVER.Dev_Server || SelectedServer == SELECTED_SERVER.Stage_Server)
                settings.DebugEnabled = true;
            else
                settings.DebugEnabled = false;

            TapjoyEditor.TapjoySettingsEditor.Save(settings);
        }*/

        void GetSVNRevision()
        {
            try
            {
                string command = "svn info";
                #if UNITY_EDITOR_WIN
                System.Diagnostics.ProcessStartInfo procStartInfo =
                new System.Diagnostics.ProcessStartInfo("cmd", "/c " + command);
                procStartInfo.UseShellExecute = false;
                procStartInfo.RedirectStandardOutput = true;
                procStartInfo.RedirectStandardError = true;
                procStartInfo.CreateNoWindow = true;
                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.StartInfo = procStartInfo;
                #elif UNITY_EDITOR_OSX
		        System.Diagnostics.Process proc = new System.Diagnostics.Process (); 
		        proc.StartInfo.FileName = "/bin/bash"; 
		        proc.StartInfo.Arguments = "-c \" " + command + " \""; 
		        proc.StartInfo.UseShellExecute = false; 
		        proc.StartInfo.RedirectStandardOutput = true; 
		        proc.StartInfo.RedirectStandardError = true; 
		        proc.StartInfo.CreateNoWindow = true; 
                #endif

                proc.Start();
                // Get the output into a string 
                proc.WaitForExit();

                string revision = proc.StandardOutput.ReadToEnd();
                string error = proc.StandardError.ReadToEnd();

                revision = revision.Replace("Revision: ", ";");
                #if UNITY_EDITOR_WIN
                revision = revision.Replace("\r\nNode Kind", ";");
                #elif UNITY_EDITOR_OSX
		        revision = revision.Replace("\nNode Kind", ";");
                #endif
                string[] parsedRevision = revision.Split(';');

                _szSVNRevision = parsedRevision[1];
            }
            catch (System.Exception e)
            {
                // Log the exception 
                UnityEngine.Debug.Log("Got exception: " + e.Message);
            }
        }

        void SaveBuildVersionCode()
        {
            try
            {
                StringBuilder FixedCode = new StringBuilder();
                string ScriptPath = "./Assets/500_Scripts/Game/Table/KRPG5DataManager.cs";

                StreamReader Reader = new StreamReader(ScriptPath, Encoding.UTF8, true);

                while (!Reader.EndOfStream)
                {
                    string curLine = Reader.ReadLine();

                    if (curLine.IndexOf("public const string BuildVersion = ") >= 0)
                    {
                        curLine = "        public const string BuildVersion = \"" + _szBuildVersion + "\";";
                    }
                    FixedCode.AppendLine(curLine);
                }

                Reader.Close();

                StreamWriter Writer = new StreamWriter(ScriptPath, false, Encoding.UTF8);

                byte[] UTF8Bytes = Encoding.UTF8.GetBytes(FixedCode.ToString());
                char[] ConvertedString = Encoding.UTF8.GetChars(UTF8Bytes);

                Writer.Write(ConvertedString);
                Writer.Close();

                AssetDatabase.Refresh();
                AssetDatabase.ImportAsset(ScriptPath, ImportAssetOptions.ForceUpdate);
            }
            catch (System.Exception ex)
            {
                UnityEngine.Debug.LogError("Set ServerDefineScriptFile Error. " + ex.Message);
            }
        }

        void EditAndroidManifest()
        {
            try
            {
                StringBuilder FixedCode = new StringBuilder();
                string ScriptPath = "./Assets/Plugins/Android/AndroidManifest.xml";

                StreamReader Reader = new StreamReader(ScriptPath, Encoding.UTF8, true);

                while (!Reader.EndOfStream)
                {
                    string curLine = Reader.ReadLine();

                    if (curLine.IndexOf("NPAServiceID") >= 0)
                    {
                        switch (_eServerType)
                        {
                            case ENUM_SERVER_TYPE.DEV:
                                curLine = "    <meta-data android:name=\"NPAServiceID\" android:value=\"NPA_1341\" />";
                                break;
                            case ENUM_SERVER_TYPE.STAGE:
                                curLine = "    <meta-data android:name=\"NPAServiceID\" android:value=\"NPA_1342\" />";
                                break;
                            case ENUM_SERVER_TYPE.LIVE:
                                curLine = "    <meta-data android:name=\"NPAServiceID\" android:value=\"NPA_1343\" />";
                                break;
                        }
                    }
                    if (curLine.IndexOf("com.nexon.platform.store.NexonStoreClientId") >= 0)
                    {
                        switch (_eServerType)
                        {
                            case ENUM_SERVER_TYPE.DEV:
                                curLine = "    <meta-data android:name=\"com.nexon.platform.store.NexonStoreClientId\" android:value = \"MTg2NTYwODI2Mw\" />";
                                break;
                            case ENUM_SERVER_TYPE.STAGE:
                                curLine = "    <meta-data android:name=\"com.nexon.platform.store.NexonStoreClientId\" android:value = \"MjEwNzI0MzE1Mw\" />";
                                break;
                            case ENUM_SERVER_TYPE.LIVE:
                                curLine = "    <meta-data android:name=\"com.nexon.platform.store.NexonStoreClientId\" android:value = \"MTAxNzY1NDk5\" />";
                                break;
                        }
                    }
                    FixedCode.AppendLine(curLine);
                }

                Reader.Close();

                StreamWriter Writer = new StreamWriter(ScriptPath, false, Encoding.UTF8);

                byte[] UTF8Bytes = Encoding.UTF8.GetBytes(FixedCode.ToString());
                char[] ConvertedString = Encoding.UTF8.GetChars(UTF8Bytes);

                Writer.Write(ConvertedString);
                Writer.Close();

                EditPostProcessor();

            }
            catch (System.Exception ex)
            {
                UnityEngine.Debug.LogError("Set ServerDefineScriptFile Error. " + ex.Message);
            }
        }

        void EditPostProcessor()
        {
            try
            {
                StringBuilder FixedCode = new StringBuilder();
                string ScriptPath = "./Assets/Editor/PostProcessBuildPlayer";

                StreamReader Reader = new StreamReader(ScriptPath, Encoding.Default, true);

                while (!Reader.EndOfStream)
                {
                    string curLine = Reader.ReadLine();

                    if (curLine.IndexOf("service_id =") >= 0)
                    {
                        switch (_eServerType)
                        {
                            case ENUM_SERVER_TYPE.DEV:
                                curLine = "	service_id = '1341'";
                                break;
                            case ENUM_SERVER_TYPE.STAGE:
                                curLine = "	service_id = '1342'";
                                break;
                            case ENUM_SERVER_TYPE.LIVE:
                                curLine = "	service_id = '1343'";
                                break;
                        }
                    }
                    if (curLine.IndexOf("nexon_store_client_id = ") >= 0)
                    {
                        switch (_eServerType)
                        {
                            case ENUM_SERVER_TYPE.DEV:
                                curLine = "	nexon_store_client_id = 'MTg2NTYwODI2Mw'";
                                break;
                            case ENUM_SERVER_TYPE.STAGE:
                                curLine = "	nexon_store_client_id = 'MjEwNzI0MzE1Mw'";
                                break;
                            case ENUM_SERVER_TYPE.LIVE:
                                curLine = "	nexon_store_client_id = 'MTAxNzY1NDk5'";
                                break;
                        }
                    }
                    FixedCode.AppendLine(curLine);
                }

                Reader.Close();

                StreamWriter Writer = new StreamWriter(ScriptPath, false, Encoding.Default);

                byte[] ANSIBytes = Encoding.Default.GetBytes(FixedCode.ToString());
                char[] ConvertedString = Encoding.Default.GetChars(ANSIBytes);

                Writer.Write(ConvertedString);
                Writer.Close();

                EditServerDefine();

            }
            catch (System.Exception ex)
            {
                UnityEngine.Debug.LogError("Set ServerDefineScriptFile Error. " + ex.Message);
            }
        }
        void EditServerDefine()
        {
            try
            {
                StringBuilder FixedCode = new StringBuilder();
                string ScriptPath = "./Assets/500_Scripts/Network/PRRPGNetwork.cs";
                bool isChanged = false;

                StreamReader Reader = new StreamReader(ScriptPath, Encoding.UTF8, true);

                while (!Reader.EndOfStream)
                {
                    string curLine = Reader.ReadLine();

                    if ((curLine.IndexOf("#define DEV_SERVER") >= 0 ||
                        curLine.IndexOf("#define STAGE_SERVER") >= 0 ||
                        curLine.IndexOf("#define LIVE_SERVER") >= 0) && !isChanged)
                    {
                        switch (_eServerType)
                        {
                            /*
                                                       case SELECTED_SERVER.Beta:
                                                           curLine = "//#define DEV_SERVER";
                                                           isChanged = true;
                                                           break;
                            */
                            case ENUM_SERVER_TYPE.DEV:
                                curLine = "#define DEV_SERVER";
                                isChanged = true;
                                break;
                            case ENUM_SERVER_TYPE.STAGE:
                                curLine = "#define STAGE_SERVER";
                                isChanged = true;
                                break;
                            case ENUM_SERVER_TYPE.LIVE:
                                curLine = "#define LIVE_SERVER";
                                isChanged = true;
                                break;
                        }
                    }
                    FixedCode.AppendLine(curLine);
                }

                Reader.Close();

                StreamWriter Writer = new StreamWriter(ScriptPath, false, Encoding.UTF8);

                byte[] UTF8Bytes = Encoding.UTF8.GetBytes(FixedCode.ToString());
                char[] ConvertedString = Encoding.UTF8.GetChars(UTF8Bytes);

                Writer.Write(ConvertedString);
                Writer.Close();

                SaveBuildVersionCode();
            }
            catch (System.Exception ex)
            {
                UnityEngine.Debug.LogError("Set ServerDefineScriptFile Error. " + ex.Message);
            }

        }

#if UNITY_IOS
        public enum EAPSEntitlement
        {
            DEVELOPMENT,
            PRODUCTION,
            INHOUSE,
        }

        public static PBXProject CreatePBXProject(string workPath, out string projPath, out string targetGUID)
        {
            // Get path to the PBX project
            projPath = workPath + "/Unity-iPhone.xcodeproj/project.pbxproj";

            // Read the pbx project
            PBXProject proj = new PBXProject();
            proj.ReadFromString(File.ReadAllText(projPath));

            // Get the build target
            targetGUID = proj.TargetGuidByName(PBXProject.GetUnityTargetName());

            return proj;
        }

        // Xcode 프로젝트 빌드 전 pbxproj 파일 및 info.plist 파일 수정 PostProcess
        [PostProcessBuild(90000)]
        public static void OnPostprocessBuild(BuildTarget buildTarget, string workPath)
        {
            if (buildTarget == BuildTarget.iOS)
            {
                try
                {
                    string projPath = workPath + "/Unity-iPhone.xcodeproj/project.pbxproj";

                    // Add APNS Capabilities with UnityEditor.iOS.XcodeAPI
                    ProjectCapabilityManager projectCapabilityManager = new ProjectCapabilityManager(projPath, "pr.entitlements", PBXProject.GetUnityTargetName());
                    projectCapabilityManager.AddPushNotifications(CustomBuilder.curWindow.IsIPhoneDeveloper());
                    projectCapabilityManager.AddGameCenter();
                    projectCapabilityManager.AddInAppPurchase();
                    projectCapabilityManager.WriteToFile();

                    // .xcodeproj/project.pbxproj
                    string targetGUID = "";
                    PBXProject proj = CreatePBXProject(workPath, out projPath, out targetGUID);

                    // set ENABLE_BITCODE to false (this shouldn't be required in Unity 5.2+)
                    proj.SetBuildProperty(targetGUID, "ENABLE_BITCODE", "false");

                    // set CODE_SIGN_IDENTITY to iPhone Distribution.
                    proj.SetBuildProperty(targetGUID, "CODE_SIGN_IDENTITY", CustomBuilder.curWindow.GetCodeSignIdentityName());
                    if (CustomBuilder.curWindow.eProvisioning == EProvisioning.NEXON_DEV ||
                        CustomBuilder.curWindow.eProvisioning == EProvisioning.MOVE_DEV)
                    {
                        proj.SetBuildProperty(targetGUID, "CODE_SIGN_IDENTITY[sdk=iphoneos*]", "iPhone Developer");
                    }
                    else
                    {
                        proj.SetBuildProperty(targetGUID, "CODE_SIGN_IDENTITY[sdk=iphoneos*]", "iPhone Distribution");
                    }

                    proj.SetBuildProperty(targetGUID, "CODE_SIGN_STYLE", "Manual");
                    proj.SetBuildProperty(targetGUID, "PROVISIONING_PROFILE_SPECIFIER", CustomBuilder.curWindow.provisioningName);

                    // Add APNS Capabilities with UnityEditor.iOS.XcodeAPI
                    proj.AddCapability(targetGUID, PBXCapabilityType.InAppPurchase);
                    proj.AddCapability(targetGUID, PBXCapabilityType.GameCenter);
                    proj.AddCapability(targetGUID, PBXCapabilityType.PushNotifications, "pr.entitlements", true);

                    // Add a TeamId for sigining.
                    proj.SetTeamId(targetGUID, CustomBuilder.curWindow.teamIdentifier);

                    // Write back out the PBX project
                    File.WriteAllText(projPath, proj.WriteToString());

                    // Info.plist
                    // Read the Info.plist file
                    string plistPath = workPath + "/Info.plist";
                    PlistDocument plist = new PlistDocument();
                    plist.ReadFromString(File.ReadAllText(plistPath));

                    // Get root of plist
                    PlistElementDict rootDict = plist.root;

                    //Set Requires full screen = true. This was needed only with Xcode 9.
                    rootDict.SetBoolean("UIRequiresFullScreen", true);
                    rootDict.SetBoolean("ITSAppUsesNonExemptEncryption", false);
                    rootDict.SetString("NSCameraUsageDescription", "Allow camera access so that you can upload image to contact help center.");
                    rootDict.SetString("NSPhotoLibraryUsageDescription", "Allow photo access so that you can upload image to contact help center.");
                    rootDict.SetBoolean("NPAccountGuestLoginAlert", true);

                    /* PlayerSetting에서 설정
                    //Adding background modes for remote notification (For Push notifications - OneSignal)
                    var bgModes = rootDict.CreateArray("UIBackgroundModes");
                    bgModes.AddString("remote-notification");
                    bgModes.AddString("fetch");
                    */

                    //Write out the Info.plist file
                    plist.WriteToFile(plistPath);
                }
                catch (Exception e)
                {
                    globalPostProcessException = e;
                }
            }
        }

        // 실제 프로젝트 빌드 및 ipa 익스포트
        public static class XCodeProjectBuildAndExport
        {
            [PostProcessBuild(999999)]
            public static void OnPostprocessBuild(BuildTarget buildTarget, string workPath)
            {
                if (buildTarget == BuildTarget.iOS)
                {
                    string archivePath = "";
                    string ipaPath = "";

                    UnityEngine.Debug.Log("GetFilenameSuffix: " + CustomBuilder.curWindow.GetFilenameSuffix());
                    UnityEngine.Debug.Log("GetExportOptionsPlist: " + CustomBuilder.curWindow.GetExportOptionsPlist());

                    // IPA
                    string exportOptionPlistPathFilename = APPLICATION_PATH + "/Keystore/" + CustomBuilder.curWindow.GetExportOptionsPlist();

                    // Xcode9 추가 작업
                    PlistDocument plist = new PlistDocument();
                    plist.ReadFromString(File.ReadAllText(exportOptionPlistPathFilename));
                    PlistElementDict rootDict = plist.root;
                    PlistElementDict addDict = rootDict.CreateDict("provisioningProfiles");
                    addDict.SetString(PlayerSettings.bundleIdentifier, PlayerSettings.iOS.iOSManualProvisioningProfileID);
                    rootDict.SetString("signingCertificate", CustomBuilder.curWindow.GetSigningCetificateSimpleName());
                    rootDict.SetString("signingStyle", "manual");
                    //rootDict.SetBoolean("uploadBitcode", false);
                    rootDict.SetBoolean("compileBitcode", false);
                    rootDict.SetBoolean("stripSwiftSymbols", true);
                    rootDict.SetString("thinning", "<none>");
                    plist.WriteToFile(exportOptionPlistPathFilename);

                    try
                    {
                        // 이전 포스트 프로레스 예외처리가 있으면 빌드를 하지 않는다.
                        if (globalPostProcessException != null)
                            throw globalPostProcessException;

                        // 빌드
                        BuildXcodeProject(workPath, true, CustomBuilder.curWindow.GetFilenameSuffix(), out archivePath);

                        // Export
                        ExportXArchive(workPath, archivePath, exportOptionPlistPathFilename, CustomBuilder.curWindow.GetFilenameSuffix(), out ipaPath);
                    }
                    catch (Exception e)
                    {
                        UnityEngine.Debug.LogError("Export failed with exception : " + e.ToString());
                    }

                    EditorUtility.RevealInFinder(ipaPath);
                }

                globalPostProcessException = null;

                CustomBuilder.sw.Stop();
                UnityEngine.Debug.Log("<color=green>Build Elapsed Time" + CustomBuilder.sw.Elapsed + "</color>");
            }

            public static void BuildXcodeProject(string workPath, bool isCleanBuild, string suffix, out string archivePath)
            {
                archivePath = workPath + "/" + "xcarchive" + suffix + ".xcarchive";
                //if (Directory.Exists(archivePath) == false)
                 //   Directory.CreateDirectory(archivePath);

				string args = string.Format ("-project {0} -scheme {1} -configuration {2} {3} archive -archivePath {4} DEVELOPMENT_TEAM={5} CODE_SIGN_IDENTITY='{6}'",
					              "Unity-iPhone.xcodeproj",
					              "Unity-iPhone",
					              CustomBuilder.curWindow.eIOSBuildType.ToString (),
					              isCleanBuild == true ? "clean" : "",
					              archivePath,
                    			  CustomBuilder.curWindow.teamIdentifier,
					              CustomBuilder.curWindow.GetSigningCetificateSimpleName());

                UnityEngine.Debug.Log("[BuildXcodeProject] workPath: " + workPath);
                UnityEngine.Debug.Log("[BuildXcodeProject] isCleanBuild: " + isCleanBuild);
                UnityEngine.Debug.Log("[BuildXcodeProject] archivePath: " + archivePath);
                UnityEngine.Debug.Log("[BuildXcodeProject] teamIdentifier: " + CustomBuilder.curWindow.teamIdentifier);
                UnityEngine.Debug.Log("[BuildXcodeProject] GetSigningCetificateSimpleName: " + CustomBuilder.curWindow.GetSigningCetificateSimpleName());
                UnityEngine.Debug.Log("[BuildXcodeProject] args: " + args);

                ProcessStartInfo psi = new ProcessStartInfo();
                psi.FileName = "xcodebuild";
                psi.WorkingDirectory = workPath;
                psi.Arguments = args;
                psi.WindowStyle = ProcessWindowStyle.Normal;
                psi.CreateNoWindow = false;
                psi.UseShellExecute = false;
                psi.RedirectStandardOutput = true;
                psi.RedirectStandardError = true;

                using (Process process = Process.Start(psi))
                {
                    string outputString = process.StandardOutput.ReadToEnd();
                    string errorString = process.StandardError.ReadToEnd();
                    process.WaitForExit();

                    UnityEngine.Debug.Log(outputString);
                    UnityEngine.Debug.LogError(errorString);
                }
            }

            public static void ExportXArchive(string workPath, string xarchivePath, string exportOptionPlistPathFilename, string suffix, out string ipaPath)
            {
                ipaPath = workPath + "/" + "ipa";
                if (Directory.Exists(ipaPath) == false)
                    Directory.CreateDirectory(ipaPath);

                // ipa 
				string srcFilename = ipaPath + "/" + "Unity-iPhone.ipa";
				string destFilename = ipaPath + "/" + "PowerRanger_" + PlayerSettings.bundleVersion + "_" + PlayerSettings.iOS.buildNumber + suffix + ".ipa";

                string args = string.Format("-exportArchive -archivePath {0} -exportOptionsPlist {1} -exportPath {3}",
                    xarchivePath,
                    exportOptionPlistPathFilename,
                    CustomBuilder.curWindow.eIOSBuildType.ToString(),
                    ipaPath);

                UnityEngine.Debug.Log("[ExportXArchive] workPath: " + workPath);
                UnityEngine.Debug.Log("[ExportXArchive] xarchivePath: " + xarchivePath);
                UnityEngine.Debug.Log("[ExportXArchive] exportOptionPlistPathFilename: " + exportOptionPlistPathFilename);
                UnityEngine.Debug.Log("[ExportXArchive] ipaPath: " + ipaPath);
                UnityEngine.Debug.Log("[ExportXArchive] args: " + args);
				UnityEngine.Debug.Log("[ExportXArchive] srcFilename: " + srcFilename);
				UnityEngine.Debug.Log("[ExportXArchive] destFilename: " + destFilename);
				                
				ProcessStartInfo psi = new ProcessStartInfo();
                psi.FileName = "xcodebuild";
                psi.WorkingDirectory = workPath;
                psi.Arguments = args;
                psi.WindowStyle = ProcessWindowStyle.Normal;
                psi.CreateNoWindow = false;
                psi.UseShellExecute = false;
                psi.RedirectStandardOutput = true;
                psi.RedirectStandardError = true;

                using (Process process = Process.Start(psi))
                {
                    string outputString = process.StandardOutput.ReadToEnd();
                    string errorString = process.StandardError.ReadToEnd();
                    process.WaitForExit();

                    UnityEngine.Debug.Log(outputString);
                    UnityEngine.Debug.LogError(errorString);
                }

				File.Move (srcFilename, destFilename);
            }
        }

        //--> 현재 사용하지 않음.
        /*
        public static void AddCodeSignEntitlement(PBXProject proj, string target, string workPath, EAPSEntitlement eAPSEntitlement)
        {
            // copy entitlement file
            string entitlementPath = APPLICATION_PATH + "/" + "Keystore/";
            string developmentEntitlementFilename = "development.entitlements";
            string productionEntitlementFilename = "production.entitlements";
            string inhouseEntitlementFilename = "inhouse.entitlements";

            string developmentEntitlementPath = entitlementPath + developmentEntitlementFilename;
            string productionEntitlementPath = entitlementPath + productionEntitlementFilename;
            string inhouseEntitlementPath = entitlementPath + inhouseEntitlementFilename;

            string developmentRelativeDestination = target + "/" + developmentEntitlementFilename;
            string productionRelativeDestination = target + "/" + productionEntitlementFilename;
            string inhouseRelativeDestination = target + "/" + inhouseEntitlementFilename;

            UnityEngine.Debug.Log("[AddCodeSignEntitlement] workPath:" + workPath);
            if (eAPSEntitlement == EAPSEntitlement.DEVELOPMENT)
            {
                UnityEngine.Debug.Log("[AddCodeSignEntitlement] developmentEntitlementFilename:" + developmentEntitlementFilename);
                UnityEngine.Debug.Log("[AddCodeSignEntitlement] developmentEntitlementPath:" + developmentEntitlementPath);
                UnityEngine.Debug.Log("[AddCodeSignEntitlement] developmentRelativeDestination:" + developmentRelativeDestination);
            }
            else if (eAPSEntitlement == EAPSEntitlement.PRODUCTION)
            {
                UnityEngine.Debug.Log("[AddCodeSignEntitlement] productionEntitlementFilename:" + productionEntitlementFilename);
                UnityEngine.Debug.Log("[AddCodeSignEntitlement] productionEntitlementPath:" + productionEntitlementPath);
                UnityEngine.Debug.Log("[AddCodeSignEntitlement] productionRelativeDestination:" + productionRelativeDestination);
            }
            else
            {
                UnityEngine.Debug.Log("[AddCodeSignEntitlement] inhouseEntitlementFilename:" + inhouseEntitlementFilename);
                UnityEngine.Debug.Log("[AddCodeSignEntitlement] inhouseEntitlementPath:" + inhouseEntitlementPath);
                UnityEngine.Debug.Log("[AddCodeSignEntitlement] inhouseRelativeDestination:" + inhouseRelativeDestination);
            }

            if (File.Exists(developmentEntitlementPath) == true)
                File.Copy(developmentEntitlementPath, workPath + "/" + developmentRelativeDestination, true);
            else
                UnityEngine.Debug.LogError("[OnPostprocessBuild] " + developmentEntitlementPath + " not found.");

            if (File.Exists(productionEntitlementPath) == true)
                File.Copy(productionEntitlementPath, workPath + "/" + productionRelativeDestination, true);
            else
                UnityEngine.Debug.LogError("[OnPostprocessBuild] " + productionEntitlementPath + " not found.");

            if (File.Exists(inhouseEntitlementPath) == true)
                File.Copy(inhouseEntitlementPath, workPath + "/" + inhouseRelativeDestination, true);
            else
                UnityEngine.Debug.LogError("[OnPostprocessBuild] " + inhouseEntitlementPath + " not found.");

            // Add APNS Capabilities
            proj.AddFile(developmentRelativeDestination, developmentEntitlementFilename);
            proj.AddFile(productionRelativeDestination, productionEntitlementFilename);
            proj.AddFile(inhouseRelativeDestination, inhouseEntitlementFilename);

            var tagetGUID = proj.TargetGuidByName(target);
            proj.SetBuildProperty(tagetGUID, "CODE_SIGN_ENTITLEMENTS",
                eAPSEntitlement == EAPSEntitlement.DEVELOPMENT ? developmentRelativeDestination :
                eAPSEntitlement == EAPSEntitlement.PRODUCTION ? productionRelativeDestination : inhouseRelativeDestination);

            //proj.SetBuildProperty (tagetGUID, "SystemCapabilities", "{com.apple.Push = {enabled = 1;}; com.apple.GameCenter = {enabled = 1;}; com.apple.InAppPurchase  = {enabled = 1;};}"); 
        }
        */
        //<--
#endif
    }
    #endregion Game Methods
}
