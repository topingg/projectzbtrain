﻿using UnityEngine;
using System.Collections;
using AvoEx;

namespace Topping
{
    public class IntroScene : SceneObject
    {
        public UITexture m_texLogo;

        protected override void Start()
        {
            if (Time.timeScale < 1f)
                Time.timeScale = 1f;

            base.Start();
            SoundManager.MuteSFX(EncryptPrefs.GetBool(ProjectDefines.KEY_OPTION_MUTE_SFX));
            SoundManager.MuteMusic(EncryptPrefs.GetBool(ProjectDefines.KEY_OPTION_MUTE_MUSIC));

            /*if (m_texLogo != null)
                m_texLogo.alpha = 0f;*/

            StartCoroutine(_PlayIntroSound());

            // 구글 플레이 로그인. 
            GPGSManager.Instance.Login();
        }

        private IEnumerator _PlayIntroSound()
        {
            yield return new WaitForSeconds(0.5f);
            SoundManager.PlaySFX("Intro");
        }

        // NGUI 트윈 종료 후 호출. 
        public void OnFinishedTween()
        {
            // TitleScene 은 Build 에 포함되어 있어야 한다. 
            EngineManager.RequestSceneAsync(ProjectDefines.SCENE_NAME_TITLE);
        }
    }
}