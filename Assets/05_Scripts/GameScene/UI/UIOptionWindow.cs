﻿using UnityEngine;
using AvoEx;

namespace Topping
{
    public class UIOptionWindow : UIWindow
    {
        #region Public Variables
        public UISprite m_sprSoundIcon;
        public UISprite m_sprMusicIcon;

        public GameObject m_objTestWindow;
        #endregion Public Variables

        #region Mono
        protected override void OnEnable()
        {
            base.OnEnable();
            
            if (UITopManager.IsInstantiated)
            {
                UITopManager.ShowUIBlur();
                UITopManager.ShowBackCover();
            }

            m_objTestWindow.SetActivate(false);
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            if (UITopManager.IsInstantiated)
            {
                UITopManager.HideUIBlur();
                UITopManager.HideBackCover();
                UITopManager.HideToast();
            }
        }
        #endregion Mono

        #region Game Methods
        public override void InitWindow()
        {
            base.InitWindow();

            m_sprSoundIcon.SetSprite(SoundManager.IsSFXMuted() ? "zt_ui_08_sound_off" : "zt_ui_08_btn_sound");
            m_sprMusicIcon.SetSprite(SoundManager.IsMusicMuted() ? "zt_ui_08_music_off" : "zt_ui_08_btn_music");
        }
        #endregion Game Methods

        #region OnClick
        public void OnClickSound()
        {
            if (SoundManager.IsSFXMuted())
            {
                SoundManager.MuteSFX(false);
                UITopManager.ShowToast("Sound On");
            }
            else
            {
                SoundManager.MuteSFX(true);
                UITopManager.ShowToast("Sound Off");
            }

            EncryptPrefs.SetBool(ProjectDefines.KEY_OPTION_MUTE_SFX, SoundManager.IsSFXMuted());

            InitWindow();
        }

        public void OnClickMusic()
        {
            if (SoundManager.IsMusicMuted())
            {
                SoundManager.MuteMusic(false);
                UITopManager.ShowToast("Music On");
            }
            else
            {
                SoundManager.MuteMusic(true);
                UITopManager.ShowToast("Music Off");
            }

            EncryptPrefs.SetBool(ProjectDefines.KEY_OPTION_MUTE_MUSIC, SoundManager.IsMusicMuted());

            InitWindow();
        }

        public void OnClickRank()
        {
            if (GPGSManager.Instance.IsLogin)
            {
                GPGSManager.ShowRank(true);
            }
            else
            {
                UITopManager.ShowToast("Please Login To Google");
            }
        }

        public void OnClickAchieve()
        {
            if (GPGSManager.Instance.IsLogin)
            {
                GPGSManager.ShowAchieve();
            }
            else
            {
                UITopManager.ShowToast("Please Login To Google");
            }
        }

        public void OnClickCredit()
        {
            CloseWindow(false);
            UITopManager.Instance.ShowCredits(true);
        }

        /// <summary>
        /// Back 버튼 클릭. 
        /// </summary>
        public void OnClickBack()
        {
            if (_isBackPressed)
                return;

            _isBackPressed = true;

            CloseWindow(true, false);
        }

        /// <summary>
        /// Back Key 클릭. 
        /// </summary>
        public override void OnBackPressed()
        {
            base.OnBackPressed();

            if (UITopManager.Instance.m_objWaitCover.activeInHierarchy)
                return;

            SoundManager.PlaySFX("tick");

            OnClickBack();
        }

        //////////////////// TEST MODE /////////////////////
        public void OnClickOpenTestMode()
        {
            m_objTestWindow.SetActivate(true);
        }

        public void OnClickLogOut()
        {
            GPGSManager.Instance.LogOut();
        }

        public void OnClickCheckLoginID(string szType)
        {
            switch (szType)
            {
                case "google":
                    UITopManager.OpenPopup(string.Format("GOOGLE  ID\n{0}", GPGSManager.Instance.UserID), "OK");
                    break;

                case "facebook":
                    UITopManager.OpenPopup(string.Format("FACEBOOK  ID\n{0}", FBManager.Instance.UserID), "OK");
                    break;

                case "facebook_token":
                    UITopManager.OpenPopup(string.Format("GOOGLE  TOKEN\n{0}", FBManager.Instance.Token), "OK");
                    break;

                case "system":
                    UITopManager.OpenPopup(string.Format("DEVICE  ID\n{0}", UserData.FakeUserName), "OK");
                    break;
            }
        }

        public void OnClickCharSkillBlurOnOff()
        {
            // 캐릭터 선택창 스킬 이펙트시 블러 ON/OFF.
            bool isTestBlurOn = EncryptPrefs.GetBool("TEST_CHAR_SEL_BG_BLUR");
            if (isTestBlurOn)
            {
                EncryptPrefs.SetBool("TEST_CHAR_SEL_BG_BLUR", false);
                UITopManager.ShowToast("Character Select Effect Blur [ff0000]Off[-]");
            }
            else
            {
                EncryptPrefs.SetBool("TEST_CHAR_SEL_BG_BLUR", true);
                UITopManager.ShowToast("Character Select Effect Blur [ff0000]On[-]");
            }
        }

        public void OnClickAllCharUnlock()
        {
            UserData.UnlockAllCharacter();
            UITopManager.ShowToast("All Character Unlocked");
        }

        public void OnClickAllCharLock()
        {
            UserData.LockAllCharacter();
            UITopManager.ShowToast("All Character Locked");
        }

        public void OnClickResetUserData()
        {
            UITopManager.OpenPopup(false, "ARE YOU SURE YOU\nWANT TO REMOVE USER DATA?", "NO", null, "YES", () =>
            {
                UserData.ResetUserData();
                UITopManager.ClosePopup();
                UITopManager.ShowToast("User Data Is Removed");
            }, UIPopup.ENUM_BUTTON_COLOR.NORMAL, UIPopup.ENUM_BUTTON_COLOR.RED);
        }

        public void OnClickPlayerInvincible()
        {
            bool isTestInvincible = EncryptPrefs.GetBool(ProjectDefines.KEY_TEST_PLAYER_INVINCIBLE);
            if (isTestInvincible)
            {
                EncryptPrefs.SetBool(ProjectDefines.KEY_TEST_PLAYER_INVINCIBLE, false);
                UITopManager.ShowToast("Player Invicible [ff0000]Off[-]");
            }
            else
            {
                EncryptPrefs.SetBool(ProjectDefines.KEY_TEST_PLAYER_INVINCIBLE, true);
                UITopManager.ShowToast("Player Invincible [ff0000]On[-]");
            }
        }

        public void OnClickGoogleSave()
        {
            GPGSManager.SaveToGoogle();
        }

        public void OnClickGoogleLoad()
        {
            GPGSManager.LoadFromGoogle();
        }

        public void OnClickFacebookLogout()
        {
            FBManager.Instance.LogOut();
        }


        public void OnClickCloseTestMode()
        {
            m_objTestWindow.SetActivate(false);
        }

        public void OnClickShowUnityAD()
        {
            AdManager.Instance.ShowAds(true, AdManager.ENUM_AD_TYPE.UNITY_ADS, AdManager.ENUM_AD_PLACEMENT.REVIVE_MENU);
        }

        public void OnClickShowVungleAD()
        {
            AdManager.Instance.ShowAds(true, AdManager.ENUM_AD_TYPE.VUNGLE, AdManager.ENUM_AD_PLACEMENT.REVIVE_MENU);
        }

        public void OnClickShowTapjoyAD()
        {
            //AdManager.Instance.ShowAds(true, AdManager.ENUM_AD_TYPE.TAPJOY, AdManager.ENUM_AD_PLACEMENT.REVIVE_MENU);
        }

        public void OnClickIsTapjoyCon()
        {
            UITopManager.ShowToast(TapjoyManager.Instance.IsConnected.ToString());

            if (!TapjoyManager.Instance.IsConnected)
            {
                TapjoyManager.Instance.Init();
            }
        }
        //////////////////// TEST MODE /////////////////////
        #endregion OnClick
    }
}