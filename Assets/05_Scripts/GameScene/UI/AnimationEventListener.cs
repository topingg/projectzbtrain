﻿using UnityEngine;

namespace Topping
{
    public class AnimationEventListener : MonoBehaviour
    {
        public bool bAutoPlay = false;

        private Animator _cachedAnimator;
        private GameObject _cachedGameObject;

        private void Start()
        {
            _cachedAnimator = GetComponent<Animator>();
            _cachedGameObject = gameObject;
        }

        private void OnEnable()
        {
            if (bAutoPlay && _cachedAnimator != null)
                _cachedAnimator.Play(0);
        }

        /// <summary>
        /// PauseCount 애니메이션에서 호출. 
        /// </summary>
        public void PauseCountFinished()
        {
            GameManager.ResumeGame();

            _cachedGameObject.SetActivate(false);
        }

        /// <summary>
        /// 효과음 재생. 
        /// </summary>
        public void PlaySoundSFX(string szSound)
        {
            if (!szSound.IsNull())
                SoundManager.PlaySFX(szSound);
        }

        /// <summary>
        /// Skill - TinyBearStomp 연출. 
        /// </summary>
        public void Skill_TinyBearStomp()
        {
            if (GameManager.Instance.IsPlaying)
                SoundManager.PlaySFX("stomp");

            // 카메라 흔들기. 
            GameManager.Instance.m_shakeMainCam.Shake(0.1f, 0.3f);

            // 모든 적 제거. 
            PlayerManager.Instance.KillAllEnemys(true);
        }
    }
}