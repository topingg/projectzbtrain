﻿using UnityEngine;

namespace Topping
{
    public class UIReviveWindow : UIWindow
    {
        #region Public Variables
        public UISprite m_sprTimeGauge;
        public float m_fRemainTime;
        #endregion Public Variables

        #region Private Variables
        private bool _isPlayingAd = false;
        private float _fBufferTime = 1f;
        #endregion Private Variables

        #region Mono
        protected override void OnEnable()
        {
            base.OnEnable();

            if (UITopManager.IsInstantiated)
            {
                UITopManager.ShowUIBlur();
                UITopManager.ShowBackCover(new Color(0.23f, 0f, 0f, 0.7f));
            }

            if (AdManager.IsInstantiated)
                AdManager.OnAdFinished += _OnAdFinished;
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            if (UITopManager.IsInstantiated)
            {
                UITopManager.HideUIBlur();
                UITopManager.HideBackCover();
            }

            if (AdManager.IsInstantiated)
                AdManager.OnAdFinished -= _OnAdFinished;
        }

        private void Update()
        {
            // 현재 Pause 상태이므로 Time.unscaledDeltaTime 을 사용함. 
            // 그러므로 홈키를 눌러서 나가거나, 광고를 취소 해서 TIME_REVIVE_LIMIT_SEC을 넘길 경우 카운트가 이어지지 않고 바로 결과화면이 보여진다. 

            // 광고 재생중에는 리턴. 
            if (_isPlayingAd)
            {
                _fBufferTime = 0.5f;
                return;
            }

            // 카운트가 시작되기 전 유예시간. 
            if (_fBufferTime > 0f)
            {
                _fBufferTime -= Time.unscaledDeltaTime;
                return;
            }

            // 카운트 시작. 
            if (m_fRemainTime > 0f)
            {
                m_fRemainTime -= Time.unscaledDeltaTime;
                m_sprTimeGauge.SetFillAmount(Mathf.Clamp01(m_fRemainTime / ProjectDefines.TIME_REVIVE_LIMIT_SEC));

                if (m_fRemainTime <= 0f)
                {
                    m_fRemainTime = 0f;
                    _TimesUp();
                }
            }
        }
        #endregion Mono

        #region Game Methods
        public override void InitWindow()
        {
            base.InitWindow();

            SoundManager.PlaySFX("game_over");

            // 최고 점수 갱신. 
            UserData.CheckBestScore();
            UserData.CheckBestKillCount();
            UserData.CheckBestKillNpcCount();
            UserData.CheckBestComboCount();

            // 유저 데이터 저장. 
            UserData.Save();

            m_sprTimeGauge.SetFillAmount(1f);
            m_fRemainTime = ProjectDefines.TIME_REVIVE_LIMIT_SEC;

            _isPlayingAd = false;
            _fBufferTime = 1f;
        }

        /// <summary>
        /// 부활가능 시간 초과. 
        /// </summary>
        private void _TimesUp()
        {
            _ReviveFailed();
        }

        /// <summary>
        /// 게임 이어하기. 
        /// </summary>
        private void _ReviveSuccess()
        {
            // 유저 데이터 저장. 
            UserData.AddWatchAdCount(1);
            UserData.Save();

            // GPGS Event 전송. 
            GPGSManager.SendStepAchieve(GPGSManager.ENUM_GPGS_TYPE.ACHIEVE_IM_NOT_DYING_10, 1);
            GPGSManager.SendStepAchieve(GPGSManager.ENUM_GPGS_TYPE.ACHIEVE_GRIM_REAPER_100, 1);

            GameManager.GameRevive();
            CloseWindow(false);
        }

        /// <summary>
        /// 결과화면 표시. 
        /// </summary>
        private void _ReviveFailed()
        {
            CloseWindow(false);
            GameManager.GameResult();
        }

        public override void OnBackPressed()
        {
            base.OnBackPressed();

            SoundManager.PlaySFX("tick");

            _ReviveFailed();
        }
        #endregion Game Methods

        #region OnClick
        /// <summary>
        /// 광고보기 클릭. 성공적으로 완료시 이어하기. 
        /// </summary>
        public void OnClickWatchAd()
        {
            if (_isPlayingAd)
                return;

            _isPlayingAd = true;

            // 랜덤 광고. 
            //int iRndNum = Random.Range(1, (int)AdManager.ENUM_AD_TYPE.END);
            //AdManager.Instance.ShowAds(true, (AdManager.ENUM_AD_TYPE)iRndNum);

            //AdManager.Instance.ShowAds(true, AdManager.ENUM_AD_TYPE.TAPJOY);
            AdManager.Instance.ShowAds(true, AdManager.ENUM_AD_TYPE.VUNGLE);
        }
        #endregion OnClick

        #region Event
        private void _OnAdFinished(bool bResult)
        {
            _isPlayingAd = false;

            if (bResult)
                _ReviveSuccess();
            else
                _ReviveFailed();
        }
        #endregion Event
    }
}