﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AvoEx.ObservableType;

namespace Topping
{
    public class UIGameTitle : MonoBehaviour
    {
        #region Public Variables
        public List<UITweenController> m_lstTweenController;
        #endregion Public Variables

        #region Private Variables
        private GameObject _cachedGameObject;
        
        // 숨겨진 캐릭터 데이터. 
        private ConditionData _hiddenConditionData = null;
        private int _iTitleTouchCount = 0;
        #endregion Private Variables

        #region Properties
        public GameObject CachedGameObject { get { return _cachedGameObject ?? (_cachedGameObject = gameObject); } }
        #endregion Properties

        #region Mono
        private void OnEnable()
        {
            GameManager.ResumeGame();

            InitUI();
        }
        #endregion Mono

        #region Game Methods
        /// <summary>
        /// UI 초기화. 
        /// </summary>
        public void InitUI()
        {
            SoundManager.SetCrossDuration(1f);
            SoundManager.PlayConnection("TitleTheme");

            // 메인캠 블러. 
            UITopManager.Instance.m_mainCamBlur.InitBlur(3f, 1f);

            if (_hiddenConditionData == null)
                _hiddenConditionData = ConditionData.GetConditionData((ConditionManager.ENUM_UNLOCK_CONDITION)ProjectDefines.HIDDEN_TYPE_TITLE_SCENE);
            _iTitleTouchCount = 0;
            
            ShowUI();

            TapjoyUnity.Tapjoy.TrackEvent("UserFlow", "TitleScene", 0);
        }

        /// <summary>
        /// UI 표시 트윈. 
        /// </summary>
        public void ShowUI()
        {
            if (m_lstTweenController != null)
            {
                for (int i = 0, cnt = m_lstTweenController.Count; i < cnt; i++)
                {
                    m_lstTweenController[i].ForcePlayForward(false);
                }
            }
        }

        /// <summary>
        /// UI 숨김 트윈. 
        /// </summary>
        public void HideUI()
        {
            if (m_lstTweenController != null)
            {
                bool bRegEvent = false;
                for (int i = 0, cnt = m_lstTweenController.Count; i < cnt; i++)
                {
                    if (!bRegEvent)
                    {
                        m_lstTweenController[i].ForcePlayReverse(false, () => { CachedGameObject.SetActivate(false); });
                        bRegEvent = true;
                    }
                    else
                    {
                        m_lstTweenController[i].ForcePlayReverse(false);
                    }
                }
            }
        }
        #endregion Game Methods

        #region OnClick
        public void OnClickGameStart()
        {
            // 배경 Offset 초기화. 
            StageManager.Instance.SetBgOffsetTime(0f);

            HideUI();
            GameManager.Instance.SetUIState(GameManager.ENUM_GAME_SCENE_TYPE.GAME, false);
            GameManager.Instance.m_UIGameScene.WaitForGameStart();
        }

        public void OnClickOption()
        {
            UITopManager.GetUIWindow(typeof(UIOptionWindow)).ForceOpen();
        }

        public void OnClickRank()
        {
            if (GPGSManager.Instance.IsLogin)
            {
                GPGSManager.ShowRank();
            }
            else
            {
                UITopManager.ShowToast("Please Login To Google");
            }
        }

        public void OnClickCharacter()
        {
            GameManager.Instance.SetUIState(GameManager.ENUM_GAME_SCENE_TYPE.CHAR_SELECT);

            UITopManager.GetUIWindow(typeof(UICharSelectWindow)).ForceOpen();
        }

        public void OnClickLogo()
        {
            if (_hiddenConditionData == null)
                return;

            _iTitleTouchCount++;

            if (_iTitleTouchCount > _hiddenConditionData.m_iAmount)
                return;

            // 이미 보유하고 있다면 리턴. 
            if (UserData.ListUnLockCharId.Contains(_hiddenConditionData.m_iCharId))
                return;

            // 히든 캐릭터 획득. 
            if (_iTitleTouchCount >= _hiddenConditionData.m_iAmount)
            {
                if (ConditionManager.CharacterUnlock(_hiddenConditionData.m_iCharId, true, true, true))
                {
                    GPGSManager.SendUnlockAchieve(GPGSManager.ENUM_GPGS_TYPE.ACHIEVE_TREASURE_HUNT);
                }
            }
        }
        #endregion OnClick
    }
}