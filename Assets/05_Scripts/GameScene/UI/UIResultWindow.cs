﻿using System.Collections.Generic;
using UnityEngine;
using AvoEx;

namespace Topping
{
    public class UIResultWindow : UIWindow
    {
        #region Public Variables
        public UILabel m_lblScore;
        public UILabel m_lblKills;
        public UILabel m_lblMaxCombo;

        public UILabel m_lblScoreBest;
        public UILabel m_lblKillsBest;
        public UILabel m_lblMaxComboBest;

        public GameObject m_objShareMenu;
        #endregion Public Variables

        #region Private Variables
        private float _fStartDelay = 1f;
        private float _fStartDelayCheck = 0f;
        private float _fTweenDuration = 1f;

        private float _fTweenScore = 0f;
        private float _fTweenKills = 0f;
        private float _fTweenMaxCombo = 0f;

        private float _fTweenScoreBest = 0f;
        private float _fTweenKillsBest = 0f;
        private float _fTweenComboBest = 0f;

        private float _fDestScore = 0f;
        private float _fDestKills = 0f;
        private float _fDestMaxCombo = 0f;

        private float _fDestScoreBest = 0f;
        private float _fDestKillsBest = 0f;
        private float _fDestComboBest = 0f;
        #endregion Private Variables

        #region Mono
        protected override void OnEnable()
        {
            base.OnEnable();

            if (UITopManager.IsInstantiated)
            {
                UITopManager.ShowUIBlur();
                UITopManager.ShowBackCover();
            }

            m_objShareMenu.SetActivate(false);
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            if (UITopManager.IsInstantiated)
            {
                UITopManager.HideUIBlur();
                UITopManager.HideBackCover();
            }
        }

        private void Update()
        {
            if (_fStartDelayCheck < _fStartDelay)
            {
                _fStartDelayCheck += Time.unscaledDeltaTime;
                return;
            }

            if (_fTweenScore < _fDestScore)
                _fTweenScore = Mathf.MoveTowards(_fTweenScore, _fDestScore, Time.unscaledDeltaTime * (_fDestScore / _fTweenDuration));
            m_lblScore.SetText(string.Format("{0:N0} m", Mathf.FloorToInt(_fTweenScore)));

            if (_fTweenKills < _fDestKills)
                _fTweenKills = Mathf.MoveTowards(_fTweenKills, _fDestKills, Time.unscaledDeltaTime * (_fDestKills / _fTweenDuration));
            m_lblKills.SetText(string.Format("{0:N0} KILL", Mathf.FloorToInt(_fTweenKills)));

            if (_fTweenMaxCombo < _fDestMaxCombo)
                _fTweenMaxCombo = Mathf.MoveTowards(_fTweenMaxCombo, _fDestMaxCombo, Time.unscaledDeltaTime * (_fDestMaxCombo / _fTweenDuration));
            m_lblMaxCombo.SetText(string.Format("{0:N0} COMBO", Mathf.FloorToInt(_fTweenMaxCombo)));


            if (_fTweenScoreBest < _fDestScoreBest)
                _fTweenScoreBest = Mathf.MoveTowards(_fTweenScoreBest, _fDestScoreBest, Time.unscaledDeltaTime * (_fDestScoreBest / _fTweenDuration));
            m_lblScoreBest.SetText(string.Format("{0:N0} m", Mathf.FloorToInt(_fTweenScoreBest)));

            if (_fTweenKillsBest < _fDestKillsBest)
                _fTweenKillsBest = Mathf.MoveTowards(_fTweenKillsBest, _fDestKillsBest, Time.unscaledDeltaTime * (_fDestKillsBest / _fTweenDuration));
            m_lblKillsBest.SetText(string.Format("{0:N0} KILL", Mathf.FloorToInt(_fTweenKillsBest)));

            if (_fTweenComboBest < _fDestComboBest)
                _fTweenComboBest = Mathf.MoveTowards(_fTweenComboBest, _fDestComboBest, Time.unscaledDeltaTime * (_fDestComboBest / _fTweenDuration));
            m_lblMaxComboBest.SetText(string.Format("{0:N0} COMBO", Mathf.FloorToInt(_fTweenComboBest)));
        }
        #endregion Mono

        #region Game Methods
        public override void InitWindow()
        {
            base.InitWindow();

            SoundManager.SetCrossDuration(1f);
            SoundManager.PlayConnection("TitleTheme");

            SoundManager.PlaySFX("game_result");

            // 변수 초기화. 
            /*if (GameManager.GameSceneType == GameManager.ENUM_GAME_SCENE_TYPE.GAME)
                _fStartDelayCheck = 0f;*/

            // 플레이어 제거. 
            GameManager.Instance.DestroyPlayer();

            // 트윈 표시용. 
            _fTweenScore = 0f;
            _fTweenKills = 0f;
            _fTweenMaxCombo = 0f;
            _fTweenScoreBest = 0f;
            _fTweenKillsBest = 0;
            _fTweenComboBest = 0f;

            // 최고 점수 갱신. 
            UserData.CheckBestScore();
            UserData.CheckBestKillCount();
            UserData.CheckBestKillNpcCount();
            UserData.CheckBestComboCount();

            // 유저 데이터 저장. 
            UserData.Save();

            // 해금 캐릭터 확인. 
            ConditionManager.CheckUnlockCharacter();

            // 유저 데이터 조회. 
            _fDestScore = UserData.Score;
            _fDestKills = UserData.KillCount;
            _fDestMaxCombo = UserData.MaxComboCount;
            _fDestScoreBest = UserData.BestScore;
            _fDestKillsBest = UserData.BestKillCount;
            _fDestComboBest = UserData.BestComboCount;

            // UI 초기화. 
            m_lblScore.SetText("0 m");
            m_lblKills.SetText("0 KILL");
            m_lblMaxCombo.SetText("0 COMBO");
            m_lblScoreBest.SetText("0 m");
            m_lblKillsBest.SetText("0 KILL");
            m_lblMaxComboBest.SetText("0 COMBO");

            TapjoyUnity.Tapjoy.TrackEvent("UserFlow", "GameResult", 0);
        }
        #endregion Game Methods

        #region OnClick
        public void OnClickBack()
        {
            if (_isBackPressed)
                return;

            _isBackPressed = true;

            GameManager.Instance.InitGame();
            CloseWindow(true, false);
        }

        public override void OnBackPressed()
        {
            base.OnBackPressed();

            SoundManager.PlaySFX("tick");

            OnClickBack();
        }

        public void OnClickShare()
        {
            // 공유 메뉴 버튼 표시. 
            m_objShareMenu.SetActivate(!m_objShareMenu.activeInHierarchy);
        }

        public void OnClickShareToFacebook()
        {
            OnClickShare();
            FBManager.Instance.ShareToFacebook("My Score", "This is my score.");
        }

        public void OnClickShareToScreenShot()
        {
            OnClickShare();
            UITopManager.Instance.ShareScreenShot("My Score", "This is my score.", 0.1f);
        }

        public void OnClickRank()
        {
            if (GPGSManager.Instance.IsLogin)
            {
                GPGSManager.ShowRank();
            }
            else
            {
                UITopManager.ShowToast("Please Login To Google");
            }
        }

        public void OnClickPlay()
        {
            CloseWindow(true, false);

            GameManager.GameReStart();
        }

        public void OnClickCharacter()
        {
            CachedGameObject.SetActivate(false);

            GameManager.Instance.SetUIState(GameManager.ENUM_GAME_SCENE_TYPE.CHAR_SELECT);
            UITopManager.GetUIWindow(typeof(UICharSelectWindow)).ForceOpen();
        }
        #endregion OnClick
    }
}