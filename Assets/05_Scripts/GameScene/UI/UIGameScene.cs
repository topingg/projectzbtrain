﻿using UnityEngine;
using System.Collections;
using AvoEx;
using AvoEx.ObservableType;

namespace Topping
{

    public class UIGameScene : MonoBehaviour
    {
        #region Public Variables
        public UILabel m_lblDistance;
        public UISprite m_sprHpGauge;
        public UISprite m_sprFeverGauge;

        // 탭 가이드. 
        public GameObject m_objTapGuide;

        // UI 이펙트. 
        public UITweenController m_tweenEffectFool;     // NPC 처치시 Fool 말풍선. 
        public GameObject m_objEffectLowHp;             // 낮은 HP 경고. 

        // 피버 버튼 이펙트. 
        public GameObject m_objEffectFever;

        // 피버 마크 트윈. 
        public UITweener m_tweenFever;

        // 피버 마크. 
        public UISprite m_sprFeverMark;

        // 콤보 표시. 
        public GameObject m_objCombo;
        public UILabel m_lblCombo;
        public UITweenController m_tweenCombo;
        #endregion Public Variables

        #region Private Variables
        private GameObject _cachedGameObject;

        private float _fMaxPlayerHp = 0f;
        private float _fMaxPlayerFever = 0f;

        private float _fShotDelayTime = 0f;
        private string _szPressedBtn = null;
        private bool _bIsPressAttack = false;
        #endregion Private Variables

        #region Properties
        public GameObject CachedGameObject { get { return _cachedGameObject ?? (_cachedGameObject = gameObject); } }
        public bool IsPressAttack { get { return _bIsPressAttack; } }
        #endregion Properties

        #region Mono
        private void OnEnable()
        {
            GameManager.OnGameStarted += _OnGameStarted;
            StageManager.OnChangeAppearSpeedUpRate += _OnChangedSpeedUpRate;
            PlayerController.OnPlayerFever += _OnFeverPlayer;

            UserData.AddObservScore(_OnChangedUserScore);
            UserData.AddObservCombo(_OnChangedComboCount);

            // 플레이어 이벤트. 
            if (GameManager.Instance.UserPlayerCtrl != null)
            {
                _fMaxPlayerHp = GameManager.Instance.UserPlayerCtrl.PlayerInfo.PlayerData.m_fMaxHP;
                GameManager.Instance.UserPlayerCtrl.PlayerInfo.AddObservHP(_OnChangedPlayerHp);

                _fMaxPlayerFever = GameManager.Instance.UserPlayerCtrl.PlayerInfo.PlayerData.m_fMaxFever;
                GameManager.Instance.UserPlayerCtrl.PlayerInfo.AddObservFever(_OnChangedPlayerFever);
            }

            InitUI();
        }

        private void OnDisable()
        {
            GameManager.OnGameStarted -= _OnGameStarted;
            StageManager.OnChangeAppearSpeedUpRate -= _OnChangedSpeedUpRate;
            PlayerController.OnPlayerFever -= _OnFeverPlayer;

            if (!GameManager.IsInstantiated)
                return;

            UserData.DelObservScore(_OnChangedUserScore);
            UserData.DelObservCombo(_OnChangedComboCount);

            // 플레이어 이벤트. 
            if (GameManager.Instance.UserPlayerCtrl != null)
            {
                GameManager.Instance.UserPlayerCtrl.PlayerInfo.DelObservHP(_OnChangedPlayerHp);
                GameManager.Instance.UserPlayerCtrl.PlayerInfo.DelObservFever(_OnChangedPlayerFever);
            }
        }

        private void Update()
        {
            if (_bIsPressAttack && !_szPressedBtn.IsNull())
                _ProcessShoot();

            // 어택 딜레이 계산용. 
            _fShotDelayTime -= Time.deltaTime;
        }
        #endregion Mono

        #region Game Methods
        /// <summary>
        /// UI 초기화. 
        /// </summary>
        public void InitUI()
        {
            SoundManager.PlayConnection("InGame");
            UITopManager.Instance.m_mainCamBlur.TweenOpacity(0.5f, 0f);
            UITopManager.SetActivePauseCount(false);

            m_lblDistance.SetText("0");
            m_sprHpGauge.SetFillAmount(1f);
            m_sprFeverGauge.SetFillAmount();

            m_objTapGuide.SetActivate(true);

            m_tweenEffectFool.CachedGameObject.SetActivate(false);
            m_objEffectLowHp.SetActivate(false);
            m_objEffectFever.SetActivate(false);

            m_objCombo.SetActivate(false);
            m_lblCombo.SetText("0");

            m_tweenFever.enabled = false;
            m_sprFeverMark.alpha = 1f;

            _fShotDelayTime = 0f;
            _szPressedBtn = null;
            _bIsPressAttack = false;

            TapjoyUnity.Tapjoy.TrackEvent("UserFlow", "GameScene", 0);
        }

        /// <summary>
        /// 탭 가이드를 터치하지 않아도 게임 시작. 
        /// </summary>
        public void WaitForGameStart()
        {
            StartCoroutine(_CrtGameStart());
        }

        /// <summary>
        /// 게임 시작. 탭 가이드를 터치하지 않으면 자동으로 게임 시작. 
        /// </summary>
        private IEnumerator _CrtGameStart()
        {
            yield return new WaitForEndOfFrame();

            yield return new WaitForSeconds(ProjectDefines.TIME_START_GUIDE_DISPLAY_SEC);

            if (GameManager.Instance.IsPlaying || GameManager.Instance.IsPaused)
                yield break;

            GameManager.GameStart();
        }

        // UI 이펙트 표시. 
        public void ShowUIEffectFool()
        {
            m_tweenEffectFool.ForcePlayForward();
        }

        /// <summary>
        /// Shoot 클릭 처리. 
        /// </summary>
        private void _ProcessShoot()
        {
            if (_szPressedBtn.IsNull())
                return;

            if (!GameManager.Instance.IsPlaying || GameManager.Instance.IsPaused || GameManager.Instance.IsFeverMode)
                return;

            if (GameManager.Instance.UserPlayerCtrl == null || GameManager.Instance.UserPlayerCtrl.IsDead || GameManager.Instance.UserPlayerCtrl.IsFever)
                return;

            ENUM_PLAYER_SIDE eAttackSide = ENUM_PLAYER_SIDE.NONE;
            if (_szPressedBtn.Equals("Left"))
                eAttackSide = ENUM_PLAYER_SIDE.LEFT;
            else if (_szPressedBtn.Equals("Right"))
                eAttackSide = ENUM_PLAYER_SIDE.RIGHT;

            if (eAttackSide == ENUM_PLAYER_SIDE.NONE)
                return;

            // 발사 딜레이 체크. 
            float fShotDelay = ProjectDefines.TIME_DEFAULT_SHOT_DELAY;
            if (GameManager.Instance.UserPlayerCtrl.CachedPlayerData != null)
                fShotDelay = GameManager.Instance.UserPlayerCtrl.CachedPlayerData.m_fAttackDelay;

            if (_fShotDelayTime > 0f)
                return;
            else
                _fShotDelayTime = fShotDelay;

            if (eAttackSide == ENUM_PLAYER_SIDE.LEFT)
                GameManager.Instance.UserPlayerCtrl.cachedTransform.localRotation = Quaternion.identity;
            else if (eAttackSide == ENUM_PLAYER_SIDE.RIGHT)
                GameManager.Instance.UserPlayerCtrl.cachedTransform.localRotation = Quaternion.Euler(0f, 180f, 0f);


            // 피버 상태일때도 좌우 전환은 가능하도록.. (위에 피버모드일때 리턴하도록 수정)
            if (GameManager.Instance.IsFeverMode || GameManager.Instance.UserPlayerCtrl.IsFever)
                return;


            // 터치 이펙트. 
            string szEffectFire = GameManager.Instance.UserPlayerCtrl.CachedPlayerData.m_szAttackEffect;
            if (szEffectFire.IsNull())
                szEffectFire = ProjectDefines.PATH_EFFECT_TOUCH;

            /*ManagedObject objEffect = ObjectPoolManager.PopInstance(szEffectFire);
            if (objEffect != null)
            {
                Vector3 vEffectPos = GameManager.Instance.CachedCamera.ScreenToWorldPoint(UICamera.lastEventPosition);
                objEffect.cachedTransform.position = new Vector3(vEffectPos.x, vEffectPos.y, -2f);
                ObjectPoolManager.PushInstance(objEffect, 0.5f);
            }*/

            ManagedObject objEffect = ObjectPoolManager.PopInstance(szEffectFire);
            if (objEffect != null)
            {
                float xPos = -1000f;
                float xRnd = Random.Range(-0.5f, 0.5f);
                if (eAttackSide == ENUM_PLAYER_SIDE.LEFT)
                    xPos = -ProjectDefines.ATTACK_POSITION.x + xRnd;
                else if (eAttackSide == ENUM_PLAYER_SIDE.RIGHT)
                    xPos = ProjectDefines.ATTACK_POSITION.x + xRnd;

                objEffect.cachedTransform.position = new Vector3(xPos, ProjectDefines.ATTACK_POSITION.y, -2f);
                ObjectPoolManager.PushInstance(objEffect, 0.5f);
            }

            ConditionManager.Instance.AddAttackCount(1);

            // 공격 시작. 
            GameManager.Instance.UserPlayerCtrl.Attack();

            // 공격 대상 찾기. 
            PlayerController target = PlayerManager.Instance.GetAttackTarget(eAttackSide);
            if (target != null)
                target.ProcessHit(GameManager.Instance.UserPlayerCtrl);
            else
            {
                // 맨 땅을 쏘면 콤보 리셋. 
                UserData.SetComboCount(0);
            }
        }
        #endregion Game Methods

        #region OnClick
        /// <summary>
        /// 탭 가이드 터치. 터치 안하면 코루틴에서 자동으로 게임시작. 
        /// </summary>
        public void OnClickTapGuide(string szBtn)
        {
            if (GameManager.Instance.IsPlaying || GameManager.Instance.IsPaused)
                return;

            GameManager.GameStart();
        }

        /// <summary>
        /// 버튼을 Press, Drag in 했을 때 호출. 
        /// </summary>
        public void OnPressShootBtn(string szBtn)
        {
            _bIsPressAttack = true;
            _szPressedBtn = szBtn;
        }

        /// <summary>
        /// 버튼을 Release, Drag out 했을 때 호출. 
        /// </summary>
        public void OnReleaseShootBtn()
        {
            _bIsPressAttack = false;
            _szPressedBtn = "";

            // 연타 할 때는 딜레이 없음. 
            _fShotDelayTime = 0f;
        }

        /// <summary>
        /// 일시 정지. 
        /// </summary>
        public void OnClickPause()
        {
            if (GameManager.PauseGame())
            {
                if (GameManager.Instance.IsAppFocused)
                    SoundManager.PlaySFX("pause");

                UITopManager.OpenPausePopup();
            }
        }

        /// <summary>
        /// 피버 버튼 클릭. 
        /// </summary>
        public void OnClickFever()
        {
            if (GameManager.Instance.IsFeverMode)
                return;

            GameManager.Instance.UserPlayerCtrl.Fever();
        }
        #endregion OnClick

        #region Event
        private void _OnGameStarted()
        {
            m_objTapGuide.SetActivate(false);

            // 알림 메시지 표시. 
            /*UITopManager.SetAnnounce("[ff0000]Zombie Is Coming..[-]");
            UITopManager.SetAnnounce("Shoot The Zombies");
            UITopManager.ShowAnnounce(2f);*/
        }

        private void _OnChangedUserScore(ObEncFloat fDist)
        {
            m_lblDistance.SetText(string.Format("{0:N0}", Mathf.FloorToInt(fDist.Value)));
        }

        private void _OnChangedComboCount(ObEncInt iCombo)
        {
            int iValue = iCombo.Value;

            if (iValue <= 1)
                m_objCombo.SetActivate(false);
            else
            {
                m_objCombo.SetActivate(true);
                m_lblCombo.SetText(iValue.ToString());
                m_tweenCombo.PlayForward(true);
            }
        }

        private void _OnChangedSpeedUpRate(float fRate)
        {
            if (fRate <= 0f)
                return;

            /*UITopManager.SetAnnounce(string.Format("They're coming faster. [ff0000](x {0:0.#})[-]", fRate));
            UITopManager.ShowAnnounce();*/
        }

        private void _OnChangedPlayerHp(ObEncFloat fHp)
        {
            m_sprHpGauge.SetFillAmount(fHp.Value / _fMaxPlayerHp);
            m_objEffectLowHp.SetActivate(m_sprHpGauge.fillAmount <= 0.2f);
        }

        private void _OnChangedPlayerFever(ObEncFloat fFever)
        {
            m_sprFeverGauge.SetFillAmount(fFever.Value / _fMaxPlayerFever);
            m_objEffectFever.SetActivate(m_sprFeverGauge.fillAmount >= 1f);

            if (m_sprFeverGauge.fillAmount >= 1f)
            {
                m_tweenFever.enabled = true;

                SoundManager.PlaySFX("fever_full");
            }
            else
            {
                bool bTweenOff = true;
                // 피버모드일때는 게이지가 다 소진되어야 트윈을 종료함. 
                if (GameManager.Instance.IsFeverMode)
                {
                    if (m_sprFeverGauge.fillAmount > 0.01f)
                        bTweenOff = false;
                }

                if (bTweenOff)
                {
                    m_tweenFever.enabled = false;
                    m_sprFeverMark.alpha = 1f;
                }
            }
        }

        private void _OnFeverPlayer(PlayerController player, bool isFever)
        {
            if (player != null && player != GameManager.Instance.UserPlayerCtrl)
                return;

            // 피버의 시작과 끝에 반짝이는 효과. 
            UITopManager.ShowCover(new Color(1f, 1f, 1f, 1f), 0.02f);
        }
        #endregion Event
    }
}