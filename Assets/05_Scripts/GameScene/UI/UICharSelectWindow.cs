﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using AvoEx;
using AvoEx.ObservableType;

namespace Topping
{
    public class UICharSelectWindow : UIWindow
    {
        #region Public Variables
        // 말풍선. 
        public UILabel m_lblSpeech;
        public UISprite m_sprSpeechBubble;
        public UITweenController m_tweenSpeechBubble;

        // 캐릭터 이름. 
        public UILabel m_lblCharName;
        public UITweenController m_tweenCharName;
        // 스킬 이름. 
        public UILabel m_lblSkillName;
        public UITweenController m_tweenSkillName;

        // 언락 조건 설명. 
        public UILabel m_lblConditionDesc;
        // 언락 조건 수치. 
        public UILabel m_lblConditionAmount;

        // 선택 버튼. 
        public GameObject m_objBtnSelect;
        // 잠김 버튼. 
        public GameObject m_objBtnLock;

        // 캐릭터 컨테이너. 
        public Transform m_trCharContainer;
        // 공유 버튼 서브 메뉴. 
        public GameObject m_objShareMenu;

        // 모두 언락 버튼. 
        public GameObject m_objBtnUnlockAll;
        #endregion Public Variables

        #region Private Variables
        // 처음 창을 열었을 때 캐릭터선택 증감값이 적용되지 않도록 하기 위한 변수. 
        private bool _isFirstOpen = true;

        // 현재 캐릭터 데이터 캐싱. 
        private PlayerData _CurPlayerData = null;
        private SkillData _CurSkillData = null;
        // 현재 캐릭터 언락 여부. 
        private bool _isCurUnLock = false;

        // 다음 캐릭터가 나오는 방향. 
        private ENUM_PLAYER_SIDE _eSide = ENUM_PLAYER_SIDE.NONE;
        private float _fSpawnPosX = 600f;
        // 스폰된 캐릭터. 
        private ManagedObject _objCurChar = null;
        private ManagedObject _objPrevChar = null;
        // 스폰된 이펙트. 
        private ManagedObject _objSkillEffect = null;

        // 캐릭터 공격 모션 코루틴. 
        private Coroutine _crtIdleAttack = null;
        // 캐릭터 스킬 이펙트 코루틴. 
        private Coroutine _crtSkillEffect = null;

        // 캐릭터 대사 인덱스. 
        private int _iSpeechIdx = 0;

        private Vector2 _vSpeechOriginalPos = default(Vector2);
        private Vector2 _vDragStart = default(Vector2);
        private Vector2 _vDragDelta = default(Vector2);
        #endregion Private Variables

        #region Mono
        protected override void Start()
        {
            base.Start();

            IAPManager.OnPurchaseSuccess += _OnPurchaseSuccess;
        }

        private void OnDestroy()
        {
            IAPManager.OnPurchaseSuccess -= _OnPurchaseSuccess;
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            if (_vSpeechOriginalPos == default(Vector2))
                _vSpeechOriginalPos = m_tweenSpeechBubble.CachedTransform.localPosition;

            if (UITopManager.IsInstantiated)
            {
                UITopManager.ShowUIBlur();
                UITopManager.ShowBackCover();
            }

            m_objShareMenu.SetActivate(false);
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            if (UITopManager.IsInstantiated)
            {
                UITopManager.HideUIBlur();
                UITopManager.HideBackCover();
                UITopManager.HideToast();
            }

            _ClearObject();
        }

        public void Update()
        {
            // Resize Freely + Scale Tween 을 했더니 자꾸 좌표가 움직여서 고정함. 
            if (_vSpeechOriginalPos != default(Vector2))
                m_tweenSpeechBubble.CachedTransform.SetPosition(_vSpeechOriginalPos);
        }

        private void LateUpdate()
        {
            // 현재 캐릭터 이동. (중앙으로)
            if (_objCurChar != null)
                _objCurChar.cachedTransform.localPosition = Vector3.Lerp(_objCurChar.cachedTransform.localPosition, Vector3.zero, Time.deltaTime * 10f);

            // 이전 캐릭터 이동. 
            if (_objPrevChar != null)
            {
                Vector3 vDest = Vector3.zero;
                // 오른쪽 클릭했을 경우 왼쪽으로 이동. 
                if (_eSide == ENUM_PLAYER_SIDE.RIGHT)
                    vDest = Vector3.left * _fSpawnPosX;
                // 왼쪽 클릭했을 경우 오른쪽으로 이동. 
                else if (_eSide == ENUM_PLAYER_SIDE.LEFT)
                    vDest = Vector3.right * _fSpawnPosX;

                _objPrevChar.cachedTransform.localPosition = Vector3.Lerp(_objPrevChar.cachedTransform.localPosition, vDest, Time.deltaTime * 10f);
            }
        }
        #endregion Mono

        #region Game Methods
        public override void InitWindow()
        {
            base.InitWindow();

            _DespawnObjects();

            // 해금 캐릭터 확인. 
            ConditionManager.CheckUnlockCharacter();

            // 현재 선택한 캐릭터로 설정. 없으면 기본 캐릭터로 설정. 
            if (UserData.SelectedCharId > 0)
                _CurPlayerData = PlayerData.GetTableData(UserData.SelectedCharId);
            else
                _CurPlayerData = PlayerData.GetTableData(ProjectDefines.BASE_PLAYER_ID);

            _CurSkillData = null;
            _isFirstOpen = true;

            InitCharacter();

            TapjoyUnity.Tapjoy.TrackEvent("UserFlow", "CharacterSelect", 0);
        }

        /// <summary>
        /// 캐릭터 표시. 
        /// </summary>
        /// <param name="eSide">다음 캐릭터가 등장하는 방향 및 ID 증감</param>
        public void InitCharacter(ENUM_PLAYER_SIDE eSide = ENUM_PLAYER_SIDE.RIGHT)
        {
            m_lblCharName.SetText();
            m_lblSkillName.SetText();

            m_lblConditionDesc.SetText(ProjectDefines.BASE_PLAYER_CONDITION_DESC);
            m_lblConditionAmount.SetText();

            m_objBtnSelect.SetActivate(false);
            m_objBtnLock.SetActivate(true);
            m_objBtnUnlockAll.SetActivate(true);

            // 아이들링 어택 제거. 
            _ClearIdleAttack();

            // 스킬 이펙트 제거. 
            _ClearSkillEffect();

            // 대사 인덱스 초기화. 
            _iSpeechIdx = 0;

            _eSide = eSide;

            // Data Dictionary 를 List 로 변환. 
            List<PlayerData> dataList = new List<PlayerData>();
            var itorPlayerData = PlayerData.dataDictionary.GetEnumerator();
            while (itorPlayerData.MoveNext())
            {
                if (itorPlayerData.Current.Value == null)
                    continue;

                if (itorPlayerData.Current.Value.UID == ProjectDefines.BASE_PLAYER_ID)
                    dataList.Insert(0, itorPlayerData.Current.Value);
                else
                    dataList.Add(itorPlayerData.Current.Value);
            }

            // 현재 캐릭터의 인덱스. 
            int iCurIdx = dataList.IndexOf(_CurPlayerData);

            // 처음 열었다면 증감값 적용 안함. 
            if (_isFirstOpen)
            {
                _eSide = ENUM_PLAYER_SIDE.NONE;
                _isFirstOpen = false;
            }

            // 이전 캐릭터. (인덱스 -1)
            if (_eSide == ENUM_PLAYER_SIDE.LEFT)
            {
                if (iCurIdx == 0)
                    _CurPlayerData = dataList[dataList.Count - 1];
                else
                    _CurPlayerData = dataList[iCurIdx - 1];
            }
            // 다음 캐릭터. (인덱스 +1)
            else if (_eSide == ENUM_PLAYER_SIDE.RIGHT)
            {
                if (iCurIdx == dataList.Count - 1)
                    _CurPlayerData = dataList[0];
                else
                    _CurPlayerData = dataList[iCurIdx + 1];
            }
            // 현재 캐릭터. 
            else
            {
                _CurPlayerData = dataList[iCurIdx];
            }

            // 오류시 기본 캐릭터를 보여줌.
            if (_CurPlayerData == null)
                _CurPlayerData = PlayerData.GetTableData(ProjectDefines.BASE_PLAYER_ID);

            // 현재 캐릭터를 보유중인지 체크. 
            _isCurUnLock = UserData.ListUnLockCharId.Contains(_CurPlayerData.UID);

            // 해금 조건 체크. 
            ConditionData conditionData = ConditionData.GetConditionData(_CurPlayerData.UID);
            if (conditionData != null)
            {
                m_lblConditionDesc.SetText(conditionData.m_szTitle);

                // 히든은 표시 안함. 
                if ((int)conditionData.m_eCondition < 1000)
                {
                    string szAmount = "";
                    int iDestAmount = conditionData.m_iAmount;
                    int iCurAmount = 0;

                    if (_isCurUnLock)
                        iCurAmount = iDestAmount;
                    else
                        iCurAmount = ConditionManager.GetConditionValue(conditionData.m_eCondition);

                    // 시간 관련 텍스트 추가. 
                    if ((int)conditionData.m_eCondition >= 40 && (int)conditionData.m_eCondition < 50)
                    {
                        DateTime dt1 = new DateTime().AddSeconds(iCurAmount);
                        DateTime dt2 = new DateTime().AddSeconds(iDestAmount);

                        szAmount = string.Format("[ff0000]{0:D2}:{1:D2}:{2:D2} / {3:D2}:{4:D2}:{5:D2}[-]",
                            dt1.Hour, dt1.Minute, dt1.Second, dt2.Hour, dt2.Minute, dt2.Second);
                    }
                    else
                        szAmount = string.Format("[ff0000]{0:N0} / {1:N0}[-]", iCurAmount, iDestAmount);

                    m_lblConditionAmount.SetText(szAmount);
                }
            }

            // 버튼 설정. 
            m_objBtnSelect.SetActivate(_isCurUnLock);
            m_objBtnLock.SetActivate(!_isCurUnLock);
            m_objBtnUnlockAll.SetActivate(!_isCurUnLock);

            // 이전 캐릭터 삭제. 
            if (_objPrevChar != null)
                ObjectPoolManager.PushInstance(_objPrevChar);

            // 현재 캐릭터를 이전 캐릭터로 지정하여 원하는 방향으로 움직임 LateUpdate(). 
            if (_objCurChar != null)
                _objPrevChar = _objCurChar;

            // 캐릭터 정보 표시. 
            if (_CurPlayerData == null)
                return;

            // 캐릭터 이름. 
            m_lblCharName.SetText(_CurPlayerData.m_szName);

            // 캐릭터 오브젝트. 
            _objCurChar = ObjectPoolManager.PopInstance(_CurPlayerData.m_szPrefab);
            if (_objCurChar != null)
            {
                _objCurChar.cachedTransform.SetParent(m_trCharContainer);
                _objCurChar.SetLayer(LayerMask.NameToLayer("UITop"));
                _objCurChar.cachedTransform.localRotation = Quaternion.identity;
                _objCurChar.cachedTransform.localScale = Vector3.one * 100f;
                // 지정한 위치에서 가운데로 움직임 LateUpdate().
                // 처음 열었을 때는 Right 로 지정됨. 
                _objCurChar.cachedTransform.SetPosition(_eSide == ENUM_PLAYER_SIDE.LEFT ? Vector3.left * _fSpawnPosX : Vector3.right * _fSpawnPosX);

                // 흑백 쉐이더 적용. 
                Utilities.AdjustGrayScale(_objCurChar, !_isCurUnLock);

                // 아이들 어택 표시. 
                if (CachedGameObject.activeInHierarchy)
                    _crtIdleAttack = StartCoroutine(_CrtIdleAttack());
            }

            // 스킬 정보 표시. 
            _CurSkillData = SkillData.GetTableData(_CurPlayerData.m_iSkillID);
            if (_CurSkillData != null)
            {
                // 스킬 이름. 
                m_lblSkillName.SetText(_CurSkillData.m_szName);
            }

            // Label 트위닝. 
            m_tweenCharName.ForcePlayForward();
            m_tweenSkillName.ForcePlayForward();
        }

        /// <summary>
        /// 사용했던 오브젝트 디스폰. 
        /// </summary>
        private void _DespawnObjects()
        {
            if (_objCurChar != null)
            {
                ObjectPoolManager.PushInstance(_objCurChar);
                _objCurChar = null;
            }
            if (_objPrevChar != null)
            {
                ObjectPoolManager.PushInstance(_objPrevChar);
                _objPrevChar = null;
            }
        }

        /// <summary>
        /// 아이들링 어택. 
        /// </summary>
        private IEnumerator _CrtIdleAttack()
        {
            yield return new WaitForSeconds(1f);
            try
            {
                Animator ac = ((PlayerObject)_objCurChar).cachedAnimator;
                if (ac != null)
                {
                    ac.SetInteger("PlayerState", 0);

                    while (_objCurChar != null)
                    {
                        // 왼쪽 2발. 
                        _objCurChar.cachedTransform.localRotation = Quaternion.identity;
                        ac.SetTrigger("IsAttack");
                        yield return new WaitForSeconds(0.3f);
                        ac.SetTrigger("IsAttack");

                        yield return new WaitForSeconds(0.5f);

                        // 스킬 시연중이 아니면 말풍선 표시. 
                        //if (_objSkillEffect == null)
                        //{
                        _ShowSpeechBubble();
                        //}

                        yield return new WaitForSeconds(2.5f);

                        // 오른쪽 2발. 
                        _objCurChar.cachedTransform.localRotation = Quaternion.Euler(new Vector3(0f, 180f, 0f));
                        ac.SetTrigger("IsAttack");
                        yield return new WaitForSeconds(0.3f);
                        ac.SetTrigger("IsAttack");

                        yield return new WaitForSeconds(0.5f);

                        // 스킬 시연중이 아니면 말풍선 표시. 
                        //if (_objSkillEffect == null)
                        //{
                            _ShowSpeechBubble();
                        //}

                        yield return new WaitForSeconds(2.5f);
                    }
                }
            }
            finally { _ClearIdleAttack(); }
        }

        /// <summary>
        /// 아이들링 어택 제거. 
        /// </summary>
        private void _ClearIdleAttack()
        {
            if (_crtIdleAttack != null)
                StopCoroutine(_crtIdleAttack);

            _crtIdleAttack = null;
            m_tweenSpeechBubble.Finish();
        }

        /// <summary>
        /// 말풍선 표시. 
        /// </summary>
        private void _ShowSpeechBubble()
        {
            // 대사가 있다면 말풍선 표시. 
            if (_CurPlayerData.m_arrSpeech == null || _CurPlayerData.m_arrSpeech.Length < 1)
                return;

            m_sprSpeechBubble.alpha = 0f;
            m_tweenSpeechBubble.CachedTransform.rotation = Quaternion.identity;
            m_tweenSpeechBubble.CachedTransform.SetPosition(new Vector3(60f, 232f, 0f));
            m_tweenSpeechBubble.CachedTransform.localScale = Vector3.one;

            // 현재 인덱스의 대사. 
            string szSpeech = _CurPlayerData.m_arrSpeech[_iSpeechIdx];

            // 인덱스 증가. 
            _iSpeechIdx++;
            if (_iSpeechIdx >= _CurPlayerData.m_arrSpeech.Length)
                _iSpeechIdx = 0;

            // Label 설정. 
            m_lblSpeech.SetText(szSpeech);

            // 말풍선 표시. 
            m_tweenSpeechBubble.ForcePlayForward();
        }

        /// <summary>
        /// 스킬 이펙트 표시. 
        /// </summary>
        /// <returns></returns>
        private IEnumerator _CrtSkillEffect()
        {
            if (_CurPlayerData == null || _CurSkillData == null)
            {
                _ClearSkillEffect();
                yield break;
            }
            
            if (_CurSkillData.IsHaveSkill())
            {
                // Test Mode. 
                if (!EncryptPrefs.GetBool("TEST_CHAR_SEL_BG_BLUR"))
                {
                    UITopManager.Instance.m_mainCamBlur.enabled = false;
                    UITopManager.Instance.m_UICamBlur.enabled = false;
                }
            }

            // 시작 프리팹 출력. 
            if (!_CurSkillData.m_szStartPrefab.IsNull())
            {
                _objSkillEffect = ObjectPoolManager.PopInstance(_CurSkillData.m_szStartPrefab);

                // 흑백 쉐이더 적용. 
                Utilities.AdjustGrayScale(_objSkillEffect, !_isCurUnLock);

                Animator anim = _objSkillEffect.GetComponent<Animator>();
                if (anim != null && anim.runtimeAnimatorController.animationClips.Length > 0)
                    yield return new WaitForSeconds(anim.runtimeAnimatorController.animationClips[0].length);
            }

            // 시작 딜레이. 
            if (_CurSkillData.m_fStartDelay > 0f)
                yield return new WaitForSeconds(_CurSkillData.m_fStartDelay);

            // 시작 프리팹 제거. 
            if (_objSkillEffect != null)
                ObjectPoolManager.PushInstance(_objSkillEffect);

            // 스킬 프리팹 출력. 
            int iLayerOrigin = 0;
            if (!_CurSkillData.m_szEffectPrefab.IsNull())
            {
                _objSkillEffect = ObjectPoolManager.PopInstance(_CurSkillData.m_szEffectPrefab);
                if (_objSkillEffect != null)
                {
                    iLayerOrigin = _objSkillEffect.gameObject.layer;
                    _objSkillEffect.SetLayer(0);

                    // 흑백 쉐이더 적용. 
                    Utilities.AdjustGrayScale(_objSkillEffect, !_isCurUnLock);

                    // 스킬 시간동안 기다림. 
                    yield return new WaitForSeconds(_CurPlayerData.m_fFeverDuration);

                    _objSkillEffect.SetLayer(iLayerOrigin);
                }
            }

            // 끝 딜레이. 
            if (_CurSkillData.m_fEndDelay > 0f)
                yield return new WaitForSeconds(_CurSkillData.m_fEndDelay);

            // 스킬 프리팹 제거. 
            if (_objSkillEffect != null)
                ObjectPoolManager.PushInstance(_objSkillEffect);

            // 끝 프리팹 출력. 
            if (!_CurSkillData.m_szEndPrefab.IsNull())
            {
                _objSkillEffect = ObjectPoolManager.PopInstance(_CurSkillData.m_szEndPrefab);

                // 흑백 쉐이더 적용. 
                Utilities.AdjustGrayScale(_objSkillEffect, !_isCurUnLock);

                Animator anim = _objSkillEffect.GetComponent<Animator>();
                if (anim != null && anim.runtimeAnimatorController.animationClips.Length > 0)
                    yield return new WaitForSeconds(anim.runtimeAnimatorController.animationClips[0].length);
            }

            // 끝 프리팹 제거. 
            if (_objSkillEffect != null)
                ObjectPoolManager.PushInstance(_objSkillEffect);

            // 스킬 종료. 
            _ClearSkillEffect();
        }

        /// <summary>
        /// 스킬 이펙트 제거. 
        /// </summary>
        private void _ClearSkillEffect()
        {
            UITopManager.Instance.m_mainCamBlur.enabled = true;
            UITopManager.Instance.m_UICamBlur.enabled = true;

            if (_crtSkillEffect != null)
                StopCoroutine(_crtSkillEffect);

            _crtSkillEffect = null;

            if (_objSkillEffect != null)
                ObjectPoolManager.PushInstance(_objSkillEffect);

            _objSkillEffect = null;
        }

        /// <summary>
        /// 오브젝트 정리. 
        /// </summary>
        private void _ClearObject()
        {
            StopAllCoroutines();
            _ClearIdleAttack();
            _DespawnObjects();
        }
        #endregion Game Methods

        #region OnClick
        public void OnPress()
        {
            _vDragStart = UICamera.lastEventPosition;
        }

        public void OnRelease()
        {
            _vDragDelta = UICamera.lastEventPosition;

            float _fDistDelta = Vector2.Distance(_vDragStart, _vDragDelta);
            if (_fDistDelta < 50f)
            {
                OnClickCharacter();
            }
            else
            {
                SoundManager.PlaySFX("tick");

                if (_vDragStart.x - _vDragDelta.x > 0f)
                    OnClickRight();
                else
                    OnClickLeft();
            }
        }

        /// <summary>
        /// 캐릭터 터치. Fever 이펙트 보여주기. 
        /// </summary>
        public void OnClickCharacter()
        {
            if (_crtSkillEffect != null)
                return;

            // 드래그일 때는 리턴. 
            if (Vector2.Distance(_vDragStart, _vDragDelta) > 20f)
                return;

            _crtSkillEffect = StartCoroutine(_CrtSkillEffect());
        }

        public void OnClickLeft()
        {
            _OnClickArrow(ENUM_PLAYER_SIDE.LEFT);
        }

        public void OnClickRight()
        {
            _OnClickArrow(ENUM_PLAYER_SIDE.RIGHT);
        }

        private void _OnClickArrow(ENUM_PLAYER_SIDE eSide)
        {
            InitCharacter(eSide);
        }

        /// <summary>
        /// 캐릭터 선택 버튼 클릭. 
        /// </summary>
        public void OnClickSelect()
        {
            UserData.SelectCharacter(_CurPlayerData.UID);

            // 바로 게임 시작하도록 수정. 
            //OnClickBack();

            // 오브젝트 정리. 
            _ClearObject();

            // 스킬 이펙트 정리. 
            _ClearSkillEffect();

            // 배경 Offset 초기화. 
            StageManager.Instance.SetBgOffsetTime(0f);

            // 게임 초기화. 
            GameManager.Instance.InitGame(GameManager.ENUM_GAME_SCENE_TYPE.GAME);

            // 게임 시작. 
            GameManager.Instance.m_UIGameScene.WaitForGameStart();

            // 창 닫기. 
            CloseWindow(true, false);
        }

        public void OnClickLock()
        {
            // Do nothing. 
        }

        public void OnClickUnlockAll()
        {

        #if UNITY_ANDROID
            IAPManager.Instance.IAPBuy(IAPManager.IAP_ID_UNLOCK_ALL_CHAR_ANDROID);
        #elif UNITY_IOS
            IAPManager.Instance.IAPBuy(IAPManager.IAP_ID_UNLOCK_ALL_CHAR_IOS);
        #endif
        }

        public void OnClickShare()
        {
            // 공유 메뉴 버튼 표시. 
            m_objShareMenu.SetActivate(!m_objShareMenu.activeInHierarchy);
        }

        public void OnClickShareToFacebook()
        {
            OnClickShare();
            FBManager.Instance.ShareToFacebook("My Characters", "This is my character.");
        }

        public void OnClickShareToScreenShot()
        {
            OnClickShare();
            UITopManager.Instance.ShareScreenShot("My Characters", "This is my character.", 0.1f);
        }

        /// <summary>
        /// Back 버튼 클릭. 
        /// </summary>
        public void OnClickBack()
        {
            if (_isBackPressed)
                return;

            _isBackPressed = true;

            _ClearSkillEffect();

            // Title 화면에서 이 창을 열었을 때, 트윈없이 바로 이 창을 닫음. 
            if (GameManager.PrevSceneType == GameManager.ENUM_GAME_SCENE_TYPE.TITLE)
            {
                GameManager.Instance.SetUIState(GameManager.PrevSceneType);
                CloseWindow(false);
            }
            // UI를 닫고 Title 로 돌아감. 
            else
            {
                GameManager.Instance.InitGame();
                CloseWindow(true, false);
            }
        }

        /// <summary>
        /// Back Key 클릭. 
        /// </summary>
        public override void OnBackPressed()
        {
            base.OnBackPressed();

            SoundManager.PlaySFX("tick");

            OnClickBack();
        }
#endregion OnClick

#region Event
        private void _OnPurchaseSuccess(string szPid)
        {
            InitWindow();
        }
#endregion Event
    }
}