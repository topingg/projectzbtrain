﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Topping
{
    [Serializable]
    public class DamageInfo
    {
        // 추후 속성이나 다른 타입이 추가될 수 있음. 
        public enum ENUM_DAMAGE_TYPE
        {
            NORMAL,
        }

        private ENUM_DAMAGE_TYPE _eDmgType = ENUM_DAMAGE_TYPE.NORMAL;
        private float _fDmgValue = 1f;
        private float _fDmgDuration = 0f;

        public ENUM_DAMAGE_TYPE DamageType { get { return _eDmgType; } }
        public float DamageValue { get { return _fDmgValue; } }
        public float DamageDuration { get { return _fDmgDuration; } }

        public DamageInfo()
        {
            _eDmgType = ENUM_DAMAGE_TYPE.NORMAL;
            _fDmgValue = 1f;
            _fDmgDuration = 0f;
        }

        public void SetDamageType(ENUM_DAMAGE_TYPE eType) { _eDmgType = eType; }
        public void SetDamageValue(float fValue) { _fDmgValue = fValue; }
        public void SetDamageDuration(float fValue) { _fDmgDuration = fValue; }
    }

    [Serializable]
    public class BuffInfo
    {
        public enum ENUM_BUFF_TYPE
        {
            NONE,
            JUMP            = 1,        // Duration 시간 후에 점프. 
            ACCELERATE      = 2,        // Duration 시간 만큼 가속. 

            HP              = 10,       // Duration 시간 동안 적용. 
            FEVER_POINT     = 11,       // Duration 시간 동안 적용. 
        }

        public ENUM_BUFF_TYPE m_eBuffType { get; set; }
        public float m_fBuffValue { get; set; }
        public float m_fBuffDuration { get; set; }
        public bool m_bOverlap { get; set; }
    }
}