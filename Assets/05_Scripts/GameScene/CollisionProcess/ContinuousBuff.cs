﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Topping
{
    public class ContinuousBuff : MonoBehaviour, IContinuousBuff
    {
        #region Private Variables
        private PlayerController _attackerCtrl = null;
        private PlayerController _targetCtrl = null;
        private BuffInfo _buffInfo = null;

        private bool _isPaused = false;

        private float _fPastTime = 0f;
        private bool _isInitialized = false;
        private bool _isActive = false;
        #endregion Private Variables

        #region Properties
        //-------------- Interface -------------------//
        public PlayerController Attacker { get { return _attackerCtrl; } }
        public PlayerController Target { get { return _targetCtrl; } }
        public BuffInfo BuffInfo { get { return _buffInfo; } }

        public bool IsPaused { get { return _isPaused; } }
        //-------------- Interface -------------------//
        #endregion Properties

        #region Mono
        private void Update()
        {
            if (!_isInitialized)
                return;

            if (_isPaused)
                return;

            _ProcessBuff(Time.deltaTime);
        }
        #endregion Mono

        #region Interface Methods
        public void InitComponent(BuffInfo buffInfo, PlayerController targetCtrl, PlayerController attackerCtrl)
        {
            if (buffInfo == null || targetCtrl == null)
            {
                Stop();
                return;
            }

            _buffInfo = buffInfo;
            _targetCtrl = targetCtrl;
            _attackerCtrl = attackerCtrl;

            if (_buffInfo.m_fBuffDuration <= 0f)
            {
                Stop();
                return;
            }

            _fPastTime = 0f;
            _isActive = false;

            /*if (_buffInfo.m_eBuffType == BuffInfo.ENUM_BUFF_TYPE.ACCELERATE)
                GameManager.Instance.UserPlayerCtrl.BuffAccelStack.Push(this);*/

            _isInitialized = true;
        }

        public void Reset()
        {
            _fPastTime = 0f;
        }

        public void Pause()
        {
            _isPaused = true;
        }

        public void Resume()
        {
            _isPaused = false;
        }

        public void Stop()
        {
            /*if (_buffInfo.m_eBuffType == BuffInfo.ENUM_BUFF_TYPE.ACCELERATE)
                GameManager.Instance.UserPlayerCtrl.BuffAccelStack.Pop();

            if (GameManager.Instance.UserPlayerCtrl.BuffAccelStack.Count <= 0 && !GameManager.Instance.IsFeverMode)
                StageManager.Instance.SetScrollSpeed(ProjectDefines.BASE_SCROLL_SPEED);*/

            Destroy(this);
        }
        #endregion Interface Methods

        #region Game Methods
        private void _ProcessBuff(float fDeltaTime)
        {
            if (!GameManager.Instance.IsPlaying)
                return;

            if (_targetCtrl == null)
                Stop();

            if (_targetCtrl.IsDead)
                Stop();

            if (!_isActive)
            {
                _isActive = true;
                //_targetCtrl.SetBuffInfo(_buffInfo);
            }

            _fPastTime += fDeltaTime;

            if (_fPastTime >= _buffInfo.m_fBuffDuration)
                Stop();
        }

        public void SetPastTime(float fValue)
        {
            _fPastTime = fValue;
        }
        #endregion Game Methods
    }
}