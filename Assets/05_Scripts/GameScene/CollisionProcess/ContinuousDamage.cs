﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Topping
{
    public class ContinuousDamage : MonoBehaviour, IContinuousDamage
    {
        #region Private Variables
        private PlayerController _attackerCtrl = null;
        private PlayerController _targetCtrl = null;
        private DamageInfo _damageInfo = null;

        private bool _isPaused = false;

        private float _fPastTime = 0f;
        private float _fTotalDmg = 0f;
        private float _fFinalDmg = 0f;
        private bool _isInitialized = false;
        #endregion Private Variables

        #region Properties
        //-------------- Interface -------------------//
        public PlayerController Attacker { get { return _attackerCtrl; } }
        public PlayerController Target { get { return _targetCtrl; } }
        public DamageInfo AttackInfo { get { return _damageInfo; } }

        public bool IsPaused { get { return _isPaused; } }
        //-------------- Interface -------------------//
        #endregion Properties

        #region Mono
        private void Update()
        {
            if (!_isInitialized)
                return;

            if (_isPaused)
                return;

            _ProcessHit(Time.deltaTime);
        }
        #endregion Mono

        #region Interface Methods
        public void InitComponent(DamageInfo damageInfo, PlayerController targetCtrl, PlayerController attackerCtrl = null)
        {
            if (damageInfo == null || targetCtrl == null)
            {
                Stop();
                return;
            }

            _damageInfo = damageInfo;
            _targetCtrl = targetCtrl;
            _attackerCtrl = attackerCtrl;

            if (_targetCtrl.PlayerInfo.IsUnbeatable())
            {
                //Debug.LogError("{0} is Unbeatable".SetFormat(_targetCtrl.PlayerInfo.PlayerData.m_szName));
                Stop();
                return;
            }

            if (_damageInfo.DamageDuration <= 0f)
            {
                Stop();
                return;
            }
            else
            {
                _targetCtrl.CurrentState.Hit(_targetCtrl);
            }

            _fPastTime = 0f;
            _fTotalDmg = 0f;

            _fFinalDmg = _damageInfo.DamageValue;

            /*PlayerInfo.Stats playerStat;

            // 스탯 적용. 
            if (_attackerCtrl != null)
                playerStat = _attackerCtrl.PlayerInfo.PlayerStat;
            else
                playerStat = PlayerInfo.Stats.One;

            _fFinalDmg = playerStat.m_fAttackDmg * _damageInfo.DamageValue;*/

            _isInitialized = true;
        }

        public void Reset()
        {
            _fPastTime = 0f;
        }

        public void Pause()
        {
            _isPaused = true;
        }

        public void Resume()
        {
            _isPaused = false;
        }

        public void Stop()
        {
            //Debug.LogError("Time : {0} / Total Dmg : {1}".SetFormat(_fPastTime, _fTotalDmg));
            Destroy(this);
        }
        #endregion Interface Methods

        #region Game Methods
        private void _ProcessHit(float fDeltaTime)
        {
            if (!GameManager.Instance.IsPlaying)
                return;

            if (_targetCtrl == null)
                Stop();

            if (_targetCtrl.IsDead)
                Stop();

            if (_targetCtrl.PlayerInfo.IsUnbeatable())
            {
                //Debug.LogError("{0} is Unbeatable".SetFormat(_targetCtrl.PlayerInfo.PlayerData.m_szName));
                Stop();
                return;
            }

            float fDmg = Mathf.Lerp(0f, _fFinalDmg, fDeltaTime * (1f / _damageInfo.DamageDuration));

            _targetCtrl.AddPlayerHP(-fDmg);

            _fPastTime += fDeltaTime;
            _fTotalDmg += fDmg;

            if (_fPastTime >= _damageInfo.DamageDuration || _fTotalDmg >= _fFinalDmg - 0.01f)
                Stop();
        }
        #endregion Game Methods
    }
}