﻿using UnityEngine;

namespace Topping
{
    public interface IContinuousBuff
    {
        PlayerController Attacker { get; }
        PlayerController Target { get; }
        BuffInfo BuffInfo { get; }

        bool IsPaused { get; }

        void InitComponent(BuffInfo buffInfo, PlayerController targetCtrl, PlayerController attackerCtrl);
        void Reset();
        void Pause();
        void Resume();
        void Stop();
    }
}