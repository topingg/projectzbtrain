﻿using UnityEngine;

namespace Topping
{
    public interface IContinuousDamage
    {
        PlayerController Attacker { get; }
        PlayerController Target { get; }
        DamageInfo AttackInfo { get; }

        bool IsPaused { get; }

        void InitComponent(DamageInfo damageInfo, PlayerController targetCtrl, PlayerController attackerCtrl);
        void Reset();
        void Pause();
        void Resume();
        void Stop();
    }
}