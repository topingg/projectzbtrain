﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Topping
{
    public class SpawnFactory : MonoBehaviour
    {
        #region Public Variables
        // Sorting 레이어 설정을 위한 스폰 인덱스 전역변수. 
        public static int s_iSpawnIdx;

        // 캐릭터가 겹치지 않도록 하기위한 전역변수. 
        public static bool s_bSpawning = false;
        #endregion Public Variables

        #region Private Variables
        private ENUM_OBJECT_TYPE _eDataType = ENUM_OBJECT_TYPE.NONE;
        private IPlayerData _charData = null;

        //private int _iSpawnIdx = 0;
        private bool _isWorking = false;
        private float _fSpawnDelayTime = 0f;
        private float _fSpawnWaitDelayTime = 0f;
        #endregion Private Variables

        #region Mono
        private void Awake()
        {
            StageManager.OnStartSpawnFactory += _OnStartFactory;
            StageManager.OnDestroySpawnFactory += _OnDestroyFactory;
        }

        private void OnDestroy()
        {
            StageManager.OnStartSpawnFactory -= _OnStartFactory;
            StageManager.OnDestroySpawnFactory -= _OnDestroyFactory;
        }

        private void Update()
        {
            if (!_isWorking || _charData == null)
                return;

            if (_fSpawnDelayTime <= 0f)
            {
                if (s_bSpawning)
                {
                    // 스폰이 막히는 것을 대비하는 안전장치. 
                    if (_fSpawnWaitDelayTime <= 0f)
                    {
                        _fSpawnWaitDelayTime = 0.5f;
                        s_bSpawning = false;
                    }
                    else
                    {
                        _fSpawnWaitDelayTime += Time.unscaledDeltaTime;
                    }
                    return;
                }

                s_bSpawning = true;

                // 생성 방향 지정용 모수. 
                ENUM_PLAYER_SIDE eSide = ENUM_PLAYER_SIDE.RIGHT;
                int iRndNum = Random.Range(0, 1000000);
                if (iRndNum % 2 == 0)
                    eSide = ENUM_PLAYER_SIDE.LEFT;

                // 생성 위치 조정용 모수. 
                //float fRndNum = Random.Range(-0.5f, 0.5f);
                // 위치 고정. 
                float fRndNum = 0f;

                float fPosX;
                if (eSide == ENUM_PLAYER_SIDE.LEFT)
                    fPosX = -2f + fRndNum;
                else
                    fPosX = 2f + fRndNum;

                PatternObject objPlayer = (PatternObject)PlayerManager.Instance.CreateObject(_charData, _eDataType);
                if (objPlayer != null)
                {
                    objPlayer.cachedTransform.SetPosition(new Vector2(fPosX, ProjectDefines.OBJECT_SPAWN_HEIGHT));
                    objPlayer.cachedTransform.SetRotation(Vector3.zero);

                    objPlayer.playerController.SetPlayerSide(eSide);
                    objPlayer.playerController.CurrentState.Run(objPlayer.playerController);

                    // 스폰 속도에 따른 이동 속도 보정. 
                    objPlayer.playerController.PlayerInfo.SetVelocity(Vector2.up * _charData.m_fMoveSpeed * StageManager.Instance.EnemyMoveSpeedRate);
                    objPlayer.playerController.SetAnimatorActive(true);

                    // 정렬 순서 정의. 
                    if (objPlayer.cachedSpriteRenderer != null)
                        objPlayer.cachedSpriteRenderer.sortingOrder = s_iSpawnIdx;

                    s_iSpawnIdx++;
                }

                _fSpawnDelayTime = _GetAppearDelay();
            }
            else
            {
                // 생성 속도 증가. (몬스터 및 NPC 같이 증가)
                if (StageManager.Instance.AppearSpeedUpRate > 0f)
                    _fSpawnDelayTime -= Time.deltaTime * StageManager.Instance.AppearSpeedUpRate;
                else
                    _fSpawnDelayTime -= Time.deltaTime;
            }
        }

        /// <summary>
        /// 등장 딜레이 계산. 
        /// </summary>
        private float _GetAppearDelay()
        {
            float fResult = 0f;

            switch (_eDataType)
            {
                case ENUM_OBJECT_TYPE.ENEMY:
                    {
                        EnemyData enemyData = _charData as EnemyData;
                        if (enemyData != null)
                            fResult = Random.Range(enemyData.m_fAppearDelayMin, enemyData.m_fAppearDelayMax);
                    }
                    break;

                case ENUM_OBJECT_TYPE.NPC:
                    {
                        NpcData npcData = _charData as NpcData;
                        if (npcData != null)
                            fResult = Random.Range(npcData.m_fAppearDelayMin, npcData.m_fAppearDelayMax);
                    }
                    break;
            }

            return fResult;
        }
        #endregion Mono

        #region Game Methods
        public void InitFactory(IPlayerData tbData) 
        {
            _isWorking = false;
            s_iSpawnIdx = 0;
            s_bSpawning = false;

            if (tbData == null)
                Destroy(this);

            _charData = tbData;

            if (_charData.GetType().Equals(typeof(EnemyData)))
                _eDataType = ENUM_OBJECT_TYPE.ENEMY;
            else if (_charData.GetType().Equals(typeof(NpcData)))
                _eDataType = ENUM_OBJECT_TYPE.NPC;

            _fSpawnDelayTime = _GetAppearDelay();
        }

        public static void InitFactory()
        {
            s_iSpawnIdx = 0;
            s_bSpawning = false;
        }
        #endregion Game Methods

        #region Event
        private void _OnStartFactory()
        {
            _isWorking = true;
        }

        private void _OnDestroyFactory()
        {
            Destroy(this);
        }
        #endregion Event
    }
}