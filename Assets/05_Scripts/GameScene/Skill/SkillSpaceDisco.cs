﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AvoEx;

namespace Topping
{
    public class SkillSpaceDisco : PlayerSkill
    {
        #region Const
        public readonly int SKILL_ID = 1000012;
        #endregion Const

        #region Private Variables
        private float _fUpdateTime = 0f;
        private PlayerController _skillTarget = null;

        private List<PlayerController> _lstTarget = null;
        #endregion Private Variables

        #region Override Methods
        public override void InitSkill(PlayerController player, int iSkillId)
        {
            base.InitSkill(player, iSkillId);
            _ClearTargetList();
        }

        public override void StartSkill()
        {
            base.StartSkill();
            _ClearTargetList();
        }

        public override void ClearSkill()
        {
            base.ClearSkill();
            _ClearTargetList();
        }

        private void _ClearTargetList()
        {
            if (_lstTarget == null)
                _lstTarget = new List<PlayerController>();
            else
                _lstTarget.Clear();
        }

        protected override void SubUpdate()
        {
            if (CachedData == null)
                return;

            // 날려버림. 
            for (int i = 0, cnt = _lstTarget.Count; i < cnt; i++)
            {
                if (_lstTarget[i] != null && !_lstTarget[i].IsDead)
                {
                    _lstTarget[i].SetPlayerState(PlayerStatePool.GetState(ENUM_PLAYER_STATE.IDLE));
                    _lstTarget[i].PlayerInfo.SetVelocity();
                    _lstTarget[i].cachedTransform.position = Vector3.Lerp(_lstTarget[i].cachedTransform.position, new Vector3(0f, 8f, 0f), Time.deltaTime * 1.5f);

                    if (_lstTarget[i].cachedTransform.position.y >= ProjectDefines.TOP_SCENE_IN_HEIGHT - 2f)
                    {
                        _lstTarget[i].AddPlayerHP(-CachedData.m_fAttackDmg);
                        _lstTarget[i] = null;
                    }
                }
            }

            // 타겟 찾기. 
            if (_fUpdateTime > 0f)
            {
                _fUpdateTime -= Time.deltaTime;
                return;
            }

            // 적 찾는 간격. 
            _fUpdateTime = CachedData.m_fSearchDelay;
            _skillTarget = _GetSkillTarget();

            if (_skillTarget != null && !_lstTarget.Contains(_skillTarget))
                _lstTarget.Add(_skillTarget);
        }

        private PlayerController _GetSkillTarget()
        {
            PlayerController target = null;

            // 시트에 타있는 적 조회. 
            for (int i = 0, cnt = GameManager.Instance.m_lstTrainSeat.Count; i < cnt; i++)
            {
                PlayerController player = GameManager.Instance.m_lstTrainSeat[i];
                if (player != null && !player.IsDead && player.cachedTransform.position.y > ProjectDefines.BOT_SCENE_IN_HEIGHT && !_lstTarget.Contains(player))
                {
                    target = player;
                    break;
                }
            }

            // 시트 조회가 다 끝난 후 조회. 
            if (target == null)
            {
                // 적 리스트. 
                for (int i = 0, cnt = PlayerManager.Instance.CreatedEnemyList.Count; i < cnt; i++)
                {
                    if (PlayerManager.Instance.CreatedEnemyList[i] == null)
                        continue;

                    if (!((PatternObject)PlayerManager.Instance.CreatedEnemyList[i]).isActivated)
                        continue;

                    PlayerController player = PlayerManager.Instance.CreatedEnemyList[i].playerController;
                    if (player != null && !player.IsDead && player.cachedTransform.position.y > ProjectDefines.BOT_SCENE_IN_HEIGHT + 4f && !_lstTarget.Contains(player))
                    {
                        target = player;
                        break;
                    }
                }
            }

            return target;
        }
        #endregion Override Methods
    }
}