﻿using System.Collections;
using UnityEngine;
using AvoEx;

namespace Topping
{
    public class CommonSkill : PlayerSkill
    {
        #region Const
        public readonly int SKILL_ID = 1000000;
        #endregion Const

        #region Private Variables
        private float _fUpdateTime = 0f;
        private PlayerController _skillTarget = null;
        #endregion Private Variables

        #region Override Methods
        protected override void SubUpdate()
        {
            if (CachedData == null)
                return;

            // 별도로 처리가 된 스킬은 여기서 타겟을 찾지 않음. 
            if (CachedData.m_fSearchDelay < 0)
                return;

            // 타겟 찾기. 
            if (_fUpdateTime > 0f)
            {
                _fUpdateTime -= Time.deltaTime;
                return;
            }

            // 적 찾는 간격. 
            _fUpdateTime = CachedData.m_fSearchDelay;
            _skillTarget = PlayerManager.Instance.GetFeverTarget(CachedData.m_fSearchHeight);

            // 적 공격. 
            if (_skillTarget != null)
                _skillTarget.AddPlayerHP(-CachedData.m_fAttackDmg);
        }
        #endregion Override Methods
    }
}