﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AvoEx;

namespace Topping
{
    public abstract class PlayerSkill : MonoBehaviour
    {
        #region Enum
        public enum ENUM_PLAYER_SKILL_ID
        {
            COMMON = 1000000,
            SOUL_SHOUTING = 1000001,
            TINY_BEAR_STOMP = 1000002,
            EASTER_BUNNY = 1000003,
            FLAME_THROWER = 1000004,
            SOL_AZTECA = 1000005,
            MUSCLE_TORNADO = 1000006,
            GATLING_SHOWER = 1000007,
            CALL_TO_ARMS = 1000008,
            TESLA_SPECIAL = 1000009,
            ART_OF_WAR = 1000010,
            GRAND_CROSS = 1000011,
            SPACE_DISCO = 1000012,
        }

        protected enum ENUM_SOUND_TYPE
        {
            START,
            EFFECT,
            END,

            ALL,
        }
        #endregion Enum

        #region Private Variables
        private PlayerController _cachedPlayer = null;
        private SkillData _cachedData = null;

        private ManagedObject _objSkillEffect;
        private bool _bSkillActivated = false;

        private Coroutine _crtSkillProcess = null;
        private AudioSource _audioStart = null;
        //private AudioSource _audioEffect = null;
        private AudioSource _audioEnd = null;

        private float _fTweenSpeed = 0f;
        #endregion Private Variables

        #region Properties
        public PlayerController CachedPlayer { get { return _cachedPlayer; } }
        public SkillData CachedData { get { return _cachedData; } }
        public bool IsSkillActivated { get { return _bSkillActivated; } }
        #endregion Properties

        #region Mono
        private void Update()
        {
            if (!_bSkillActivated)
                return;

            if (_cachedPlayer == null || _cachedData == null)
                return;

            // 게임 Pause 일 때 일시정지하면 스킬 효과음도 일시정지. 
            if (GameManager.Instance.IsPaused)
                GameManager.Instance.StopSkillSound(true);
            else
                GameManager.Instance.ResumeSkillSound();

            // 피버 게이지 줄이기. 
            _cachedPlayer.PlayerInfo.SetFever(Mathf.MoveTowards(_cachedPlayer.PlayerInfo.CurFever, 0f, Time.deltaTime * _fTweenSpeed));

            SubUpdate();
        }
        #endregion Mono

        #region Abstract Methods
        protected abstract void SubUpdate();
        #endregion Abstract Methods

        #region Game Methods
        /// <summary>
        /// 스킬 초기화. 
        /// </summary>
        public virtual void InitSkill(PlayerController player, int iSkillId)
        {
            _cachedPlayer = player;
            _cachedData = SkillData.GetTableData(iSkillId);

            if (_cachedPlayer == null || _cachedData == null)
                Destroy(this);

            _fTweenSpeed = _cachedPlayer.PlayerInfo.PlayerData.m_fMaxFever / _cachedPlayer.CachedPlayerData.m_fFeverDuration;    // 지정된 시간동안 모든 게이지가 소모되게 만든다. 
        }

        /// <summary>
        /// 스킬 시작. 
        /// </summary>
        public virtual void StartSkill()
        {
            if (!GameManager.Instance.IsPlaying || GameManager.Instance.IsPaused)
                return;

            if (GameManager.Instance.IsFeverMode || _cachedPlayer.IsFever)
                return;

            SoundManager.PlaySFX("start_fever");

            ClearSkill();
            _crtSkillProcess = StartCoroutine(_StartSkill());
        }

        private IEnumerator _StartSkill()
        {
            if (CachedPlayer.CurrentState != null)
                CachedPlayer.CurrentState.Fever(CachedPlayer);

            ConditionManager.Instance.AddFeverCount(1);

            // 시작 사운드 재생. 
            PlaySkillSound(ENUM_SOUND_TYPE.START, CachedData.m_szStartSound);

            // 시작 프리팹 출력. 
            ManagedObject objStartEffect = CreateSkillEffect(CachedData.m_szStartPrefab);
            if (objStartEffect != null)
            {
                // 흑백 쉐이더 적용. 
                Utilities.AdjustGrayScale(objStartEffect);

                Animator anim = objStartEffect.GetComponent<Animator>();
                if (anim != null && anim.runtimeAnimatorController.animationClips.Length > 0)
                    yield return new WaitForSeconds(anim.runtimeAnimatorController.animationClips[0].length);
            }

            // 시작 딜레이. 
            if (CachedData.m_fStartDelay > 0f)
                yield return new WaitForSeconds(CachedData.m_fStartDelay);

            // 시작 사운드 중지. 
            //StopSkillSound(ENUM_SOUND_TYPE.START);

            // 시작 프리팹 제거. 
            RemoveSkillEffect();

            //////////////////////////////////////////////////////////////////////////////////

            // 스킬 프리팹 출력. 
            ManagedObject objSkillEffect = CreateSkillEffect(CachedData.m_szEffectPrefab);
            if (objSkillEffect != null)
            {
                // 흑백 쉐이더 적용. 
                Utilities.AdjustGrayScale(objSkillEffect);
            }

            // 스킬 사운드 재생. 
            GameManager.Instance.PlaySkillSound(CachedData.m_szEffectSound,
                SoundManager.Instance.PropBaseVolumes.ContainsKey(CachedData.m_szEffectSound) ?
                SoundManager.Instance.PropBaseVolumes[CachedData.m_szEffectSound] : 1f,
                CachedData.m_iEffectSoundLoop == -1 ? true : false);

            // 스킬 활성화. 
            SetSkillStatus(true);

            // 스킬 시간동안 기다림. 
            yield return new WaitForSeconds(CachedPlayer.PlayerInfo.PlayerData.m_fFeverDuration);

            // 스킬 비활성화. 
            SetSkillStatus(false);

            // 스킬 사운드 중지. 
            StopSkillSound(ENUM_SOUND_TYPE.EFFECT);

            // 끝 딜레이. 
            if (CachedData.m_fEndDelay > 0f)
                yield return new WaitForSeconds(CachedData.m_fEndDelay);

            // 스킬 프리팹 제거. 
            RemoveSkillEffect();

            //////////////////////////////////////////////////////////////////////////////////

            // 끝 사운드 재생. 
            PlaySkillSound(ENUM_SOUND_TYPE.END, CachedData.m_szEndSound);

            // 끝 프리팹 출력. 
            if (!CachedData.m_szEndPrefab.IsNull())
            {
                ManagedObject objEndEffect = CreateSkillEffect(CachedData.m_szEndPrefab);
                if (objEndEffect != null)
                {
                    // 흑백 쉐이더 적용. 
                    Utilities.AdjustGrayScale(objEndEffect);

                    Animator anim = objEndEffect.GetComponent<Animator>();
                    if (anim != null && anim.runtimeAnimatorController.animationClips.Length > 0)
                        yield return new WaitForSeconds(anim.runtimeAnimatorController.animationClips[0].length);
                }
            }

            // 끝 사운드 중지. 
            StopSkillSound(ENUM_SOUND_TYPE.END);

            // 끝 프리팹 제거. 
            RemoveSkillEffect();

            // 스킬 종료. 
            ClearSkill();
        }

        /// <summary>
        /// 스킬 종료. 
        /// </summary>
        public virtual void ClearSkill()
        {
            if (_crtSkillProcess != null)
                StopCoroutine(_crtSkillProcess);

            // 효과음 중지. 
            StopSkillSound(ENUM_SOUND_TYPE.ALL);

            // 스킬 프리팹 제거. 
            RemoveSkillEffect();

            // 피버 종료 처리. 
            SetSkillStatus(false);
            _cachedPlayer.IsFever = false;

            if (_cachedPlayer.CurrentState != null)
                _cachedPlayer.CurrentState.Idle(_cachedPlayer);
        }

        /// <summary>
        /// 스킬 효과음 재생. 
        /// </summary>
        protected void PlaySkillSound(ENUM_SOUND_TYPE eType, string szSound)
        {
            if (szSound.IsNull())
                return;

            switch (eType)
            {
                case ENUM_SOUND_TYPE.START:
                    _audioStart = SoundManager.PlaySFX(szSound);
                    break;

                case ENUM_SOUND_TYPE.EFFECT:
                    //_audioEffect = SoundManager.PlaySFX(CachedData.m_szEndSound);
                    break;

                case ENUM_SOUND_TYPE.END:
                    _audioEnd = SoundManager.PlaySFX(CachedData.m_szEndSound);
                    break;
            }
        }

        /// <summary>
        /// 스킬 효과음 중지. 
        /// </summary>
        protected void StopSkillSound(ENUM_SOUND_TYPE eType)
        {
            switch (eType)
            {
                case ENUM_SOUND_TYPE.START:
                    if (_audioStart != null)
                        _audioStart.Stop();
                    break;

                case ENUM_SOUND_TYPE.EFFECT:
                    /*if (_audioEffect != null)
                        _audioEffect.Stop();*/

                    GameManager.Instance.StopSkillSound();
                    break;

                case ENUM_SOUND_TYPE.END:
                    if (_audioEnd != null)
                        _audioEnd.Stop();
                    break;

                case ENUM_SOUND_TYPE.ALL:
                    StopSkillSound(ENUM_SOUND_TYPE.START);
                    StopSkillSound(ENUM_SOUND_TYPE.EFFECT);
                    StopSkillSound(ENUM_SOUND_TYPE.END);
                    break;
            }
        }

        /// <summary>
        /// 스킬 이펙트 생성. 
        /// </summary>
        protected ManagedObject CreateSkillEffect(string szPrefab)
        {
            if (szPrefab.IsNull())
                return null;

            _objSkillEffect = ObjectPoolManager.PopInstance(szPrefab);
            return _objSkillEffect;
        }

        /// <summary>
        /// 스킬 프리팹 제거. 
        /// </summary>
        protected void RemoveSkillEffect()
        {
            if (_objSkillEffect != null)
                ObjectPoolManager.PushInstance(_objSkillEffect);
        }

        /// <summary>
        /// 스킬 상태 설정. 
        /// </summary>
        protected void SetSkillStatus(bool bActive)
        {
            _bSkillActivated = bActive;
            GameManager.Instance.IsFeverMode = bActive;
        }
        #endregion Game Methods
    }
}