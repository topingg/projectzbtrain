﻿using UnityEngine;
using System.Collections.Generic;
using AvoEx;

namespace Topping
{
    public class PlayerSkillPool : MonoSingleton<PlayerSkillPool>
    {
        public Dictionary<int, System.Type> PlayerSkillDic;
        
        PlayerSkillPool()
        {
            if (PlayerSkillDic == null)
                PlayerSkillDic = new Dictionary<int, System.Type>();
            else
                PlayerSkillDic.Clear();

            PlayerSkillDic.Add((int)PlayerSkill.ENUM_PLAYER_SKILL_ID.COMMON, typeof(CommonSkill));
            PlayerSkillDic.Add((int)PlayerSkill.ENUM_PLAYER_SKILL_ID.SPACE_DISCO, typeof(SkillSpaceDisco));
        }

        public static System.Type GetSkill(PlayerSkill.ENUM_PLAYER_SKILL_ID eSkillId)
        {
            try
            {
                return GetSkill((int)eSkillId);
            }
            catch
            {
                return typeof(CommonSkill);
            }
        }

        public static System.Type GetSkill(int stateIdx)
        {
            try
            {
                return Instance.PlayerSkillDic[stateIdx].GetType();
            }
            catch
            {
                return typeof(CommonSkill);
            }
        }
    }
}