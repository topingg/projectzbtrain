﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AvoEx;
using AvoEx.ObservableType;

using cakeslice;

namespace Topping
{
    public class StageManager : MonoSingleton<StageManager>
    {
        #region Event Delegate
        public delegate void DeleFloat(float fSpeed);

        public static event DeleFloat OnChangeScrollSpeed;
        public static event DeleFloat OnChangeAppearSpeedUpRate;
        public static event DeleFloat OnChangeEnemyMoveSpeedUpRate;

        public static event Action OnStartSpawnFactory;
        public static event Action OnDestroySpawnFactory;
        #endregion Event Delegate

        #region Public Variables
        public Renderer m_rdrBackground;

        public Renderer m_rdrBackgroundTunnel;
        public GameObject m_objBackGroundTunnel;
        public GameObject m_objBackGroundTunnelEntrance;
        public Transform m_trBackGroundTunnelEntrance;
        public GameObject m_objBackGroundTunnelExit;
        public Transform m_trBackGroundTunnelExit;
        public GameObject m_objBackGroundFever;

        public SpriteRenderer m_rdrTrain;
        public Transform m_trGroupTrain;

        public OutlineController m_compOutlineCtrl;

        #if UNITY_EDITOR
        // 인스펙터에서 배경 스크롤 속도 조절 테스트용. 
        public float m_fTestScrollSpeed = -1f;
        #endif
        #endregion Public Variables

        #region Private Variables
        private StageData _stageData = null;
        private StageData _nextStageData = null;
        private int _iStageNo = 0;

        private ObEncFloat _fScrollSpeed = new ObEncFloat();
        private float _fPrevScrollSpeed = 0f;
        private float _fCachedScrollSpeed = 0f;

        // 시작시 배경 스크롤 가속 관련. 
        private bool _bAccelScroll = false;
        private float _fAccelScrollTime = 0f;

        private Material _matBackground = null;
        private Material _matBackgroundTunnel = null;
        private float _fBgOffsetTime = 0f;

        // 등장속도 증가 배율.
        private ObEncFloat _fAppearSpeedUpRate = new ObEncFloat();
        private float _fCachedAppearSpeedUpRate = 0f;       // ObEncFloat 에 자주 엑세스 하지 않도록. 
        private float _fPrevAppearSpeedUpRate = 0f;         // 사용하지 않지만 혹시 모름. 

        // 이동속도 증가 배율.
        private float _fEnemyMoveSpeedUpRate = 0f;
        private float _fPrevEnemyMoveSpeedUpRate = 0f;     // 사용하지 않지만 혹시 모름. 

        // 등장속도 데이터 리스트. 
        private List<AppearanceData> _lstAppearData = new List<AppearanceData>();

        // 다음에 적용될 인덱스. 
        private int _iNextAppearIdx = 0;

        // 등장속도 증가 마지막 값. (값이 존재하면 무한 증가)
        private int _iLastAppearNextDist = 0;
        private int _iLastAppearSpeedUpInterval = 0;
        private float _fLastAppearSpeedUpRate = 0f;
        private float _fLastMoveSpeedUpRate = 0f;

        private Coroutine _crtStartSpawnFactory = null;
        private Coroutine _crtChangeBackGround = null;
        #endregion Private Variables

        #region Properties
        public StageData StageData { get { return _stageData; } }
        public int StageNo { get { return _iStageNo; } }

        /// <summary>
        /// 스크롤 속도. 
        /// </summary>
        public float ScrollSpeed { get { return _fCachedScrollSpeed; } }

        /// <summary>
        /// 스폰 속도 스피드업 배율. 
        /// </summary>
        public float AppearSpeedUpRate { get { return _fCachedAppearSpeedUpRate; } }
        public float PrevAppearSpeedUpRate { get { return _fPrevAppearSpeedUpRate; } }

        /// <summary>
        /// 이동 속도 증가 배율. 
        /// </summary>
        public float EnemyMoveSpeedRate { get { return _fEnemyMoveSpeedUpRate; } }
        public float PrevEnemyMoveSpeedRate { get { return _fPrevEnemyMoveSpeedUpRate; } }
        #endregion Properties

        #region Mono
        private void Awake()
        {
            PlayerController.OnPlayerFever += _OnFeverPlayer;
            UserData.AddObservScore(_OnChangedScore);
        }

        private void OnDestroy()
        {
            PlayerController.OnPlayerFever -= _OnFeverPlayer;
            UserData.DelObservScore(_OnChangedScore);
        }

        private void Update()
        {
            _UpdateScrollSpeed(Time.deltaTime);
            _UpdateCheckSpeedUp();
        }

        private void LateUpdate()
        {
            if (ScrollSpeed <= 0f || GameManager.Instance.IsPaused)
                return;

            if (_matBackground != null)
            {
                float fSpeed = ScrollSpeed;

                #if UNITY_EDITOR
                if (m_fTestScrollSpeed > -1f)
                    fSpeed = m_fTestScrollSpeed;
                #endif

                // 피버모드시 속도 조정. 
                /*if (GameManager.Instance.UserPlayerCtrl.IsFever)
                    fSpeed = ScrollSpeed * 3f;*/

                _fBgOffsetTime += fSpeed * 0.001f;
                float y = Mathf.Repeat(_fBgOffsetTime * fSpeed * 0.05f, 1f);
                _matBackground.mainTextureOffset = new Vector2(0f, y);

                if (m_objBackGroundTunnel.activeInHierarchy)
                    _matBackgroundTunnel.mainTextureOffset = new Vector2(0f, y * 2f);

                if (_fBgOffsetTime > 10f)
                    _fBgOffsetTime = 0f;
            }
        }
        #endregion Mono

        #region Game Methods
        public void InitStage()
        {
            if (_stageData == null)
                InitStage(ProjectDefines.DEFAULT_STAGE_ID);
            else
                InitStage(_stageData);
        }

        public void InitStage(int iStageId)
        {
            InitStage(StageData.GetTableData(iStageId));
        }

        public void InitStage(StageData stageData)
        {
            if (stageData == null)
                return;

            _stageData = stageData;
            _nextStageData = _GetNextStageData(_stageData.UID);
            _iStageNo = _stageData.m_iStageNo;

            _fBgOffsetTime = 0f;
            _bAccelScroll = false;
            _fAccelScrollTime = 0f;

            if (_crtChangeBackGround != null)
                StopCoroutine(_crtChangeBackGround);
            _crtChangeBackGround = null;

            // 터널 숨김. 
            m_objBackGroundTunnel.SetActivate(false);
            m_objBackGroundTunnelEntrance.SetActivate(false);
            m_objBackGroundTunnelExit.SetActivate(false);

            // 아웃라인 숨김. 
            //m_compOutlineCtrl.ActiveOutline(false);
            m_compOutlineCtrl.ActiveOutline(true);
            m_compOutlineCtrl.HideOutlines(1f);

            SetScrollSpeed(ProjectDefines.BACKGROUND_SCROLL_SPEED_IN_UI);

            SetAppearSpeedUpRate(1f);
            SetEnemyMoveSpeedRate(1f);

            if (_lstAppearData != null)
                _lstAppearData.Clear();
            else
                _lstAppearData = new List<AppearanceData>();

            _iNextAppearIdx = 0;

            _iLastAppearNextDist = 0;
            _iLastAppearSpeedUpInterval = 0;
            _fLastAppearSpeedUpRate = 0f;
            _fLastMoveSpeedUpRate = 0f;

            // 등장 속도 리스트 추출. 
            var itorData = AppearanceData.dataDictionary.GetEnumerator();
            while (itorData.MoveNext())
            {
                if (itorData.Current.Value.m_iStageId == _stageData.UID)
                {
                    if (itorData.Current.Value.m_iSpeedUpDist < 0)
                    {
                        _iLastAppearSpeedUpInterval = -itorData.Current.Value.m_iSpeedUpDist;
                        _fLastAppearSpeedUpRate = itorData.Current.Value.m_fSpeedUpAdd;
                        _fLastMoveSpeedUpRate = itorData.Current.Value.m_fEnemyMoveSpeedAdd;
                    }
                    else
                    {
                        _lstAppearData.Add(itorData.Current.Value);
                    }
                }
            }

            if (_lstAppearData.Count > 0)
            {
                // 적용 거리 순으로 리스트 정렬. 
                _lstAppearData.Sort((lhs, rhs) => { return lhs.m_iSpeedUpDist.CompareTo(rhs.m_iSpeedUpDist); });

                // 마지막 거리 증가. 
                _iLastAppearNextDist = _lstAppearData[_lstAppearData.Count - 1].m_iSpeedUpDist + _iLastAppearSpeedUpInterval;

                //Debug.LogError("Last Appear Next Dist = "+ _iLastAppearNextDist);
            }


            if (m_rdrBackground != null)
            {
                // 배경 위치 리셋. 
                _matBackground = m_rdrBackground.material;
                if (_matBackground != null)
                {
                    _matBackground.mainTextureOffset = Vector2.zero;

                    // 배경 텍스쳐 설정. 
                    Texture texBackground = AssetManager.LoadAsset<Texture>(_stageData.m_szBgPath);
                    if (texBackground != null)
                        _matBackground.mainTexture = texBackground;
                }
            }

            if (m_rdrBackgroundTunnel != null)
            {
                _matBackgroundTunnel = m_rdrBackgroundTunnel.material;
            }

            if (m_rdrTrain != null)
            {
                // 기차 텍스쳐 설정. 
                Sprite sprTrain = AssetManager.LoadAsset<Sprite>(_stageData.m_szTrainPath);
                if (sprTrain != null)
                    m_rdrTrain.sprite = sprTrain;
            }

            // 붙어있는 팩토리 컴포넌트 삭제. 
            DestroySpawnFactory();

            // SpawnFactory 생성. 
            SpawnFactory sf = null;

            // 적 데이터. 
            if (_stageData.m_lstAppearEnemy != null)
            {
                for (int i = 0, cnt = _stageData.m_lstAppearEnemy.Count; i < cnt; i++)
                {
                    if (_stageData.m_lstAppearEnemy[i] < 1)
                        continue;

                    EnemyData tData = EnemyData.GetTableData(_stageData.m_lstAppearEnemy[i]);
                    if (tData == null)
                        continue;

                    sf = gameObject.AddComponent<SpawnFactory>();
                    sf.InitFactory(tData);
                }
            }

            // NPC 데이터. 
            if (_stageData.m_lstAppearNpc != null)
            {
                for (int i = 0, cnt = _stageData.m_lstAppearNpc.Count; i < cnt; i++)
                {
                    if (_stageData.m_lstAppearNpc[i] < 1)
                        continue;

                    NpcData tData = NpcData.GetTableData(_stageData.m_lstAppearNpc[i]);
                    if (tData == null)
                        continue;

                    sf = gameObject.AddComponent<SpawnFactory>();
                    sf.InitFactory(tData);
                }
            }
        }

        /// <summary>
        /// 다음 맵 데이터 조회. 
        /// </summary>
        private StageData _GetNextStageData(int iCurStageId)
        {
            if (iCurStageId < 1)
                return null;

            StageData stageData = StageData.GetTableData(iCurStageId);
            if (stageData == null)
                return null;

            StageData result = null;
            var itorStage = StageData.dataDictionary.GetEnumerator();
            while (itorStage.MoveNext())
            {
                if (itorStage.Current.Value == null ||
                    itorStage.Current.Value.m_fStartDistance == -1 ||
                    itorStage.Current.Value.UID == iCurStageId)
                    continue;

                if (itorStage.Current.Value.m_iStageNo > stageData.m_iStageNo)
                {
                    result = itorStage.Current.Value;
                    break;
                }
            }

            return result;
        }

        /// <summary>
        /// 배경의 Offset 값을 초기화 한다. 
        /// </summary>
        public void SetBgOffsetTime(float fValue)
        {
            _fBgOffsetTime = fValue;
        }

        /// <summary>
        /// 배경 스크롤 설정. 
        /// </summary>
        public void SetBackgroundScroll(bool bActive)
        {
            if (bActive)
            {
                // 메인에서 흘러가는 배경 덕분에, UV가 중간쯤 위치했을 때 스타트 되면 과도하게 빨라진다. 
                // _fBgOffsetTime 변수를 0으로 바꾸면 화면이 뚝 끊겨 보임. 
                //_fBgOffsetTime = 0f;
                _fAccelScrollTime = 0f;
            }

            _bAccelScroll = bActive;
        }

        /// <summary>
        /// 스크롤 속도 설정. 
        /// </summary>
        public void SetScrollSpeed(float fValue)
        {
            if (_fScrollSpeed.Value != fValue)
            {
                _fPrevScrollSpeed = _fScrollSpeed.Value;
                _fScrollSpeed.Value = fValue;
                _fCachedScrollSpeed = fValue;

                if (OnChangeScrollSpeed != null)
                    OnChangeScrollSpeed(fValue);

                //Debug.LogError(string.Format("OnChangeScrollSpeed {0}->{1}", _fPrevScrollSpeed, value));
            }
        }

        /// <summary>
        /// 배경 스크롤 속도 체크. 
        /// </summary>
        private void _UpdateScrollSpeed(float fDeltaTime)
        {
            if (!_bAccelScroll)
                return;

            if (!GameManager.Instance.IsPlaying)
                return;

            _fAccelScrollTime += fDeltaTime * 0.01f;
            float fSpeed = Mathf.Lerp(ScrollSpeed, ProjectDefines.BACKGROUND_SCROLL_SPEED_MAX, Mathf.Tan(_fAccelScrollTime));

            if (fSpeed >= ProjectDefines.BACKGROUND_SCROLL_SPEED_MAX - 0.01f)
            {
                SetScrollSpeed(ProjectDefines.BACKGROUND_SCROLL_SPEED_MAX);
                _bAccelScroll = false;
                _fAccelScrollTime = 0f;
                return;
            }

            SetScrollSpeed(fSpeed);
        }

        /// <summary>
        /// 스폰 팩토리 시작. 
        /// </summary>
        public void StartSpawnFactory()
        {
            if (_crtStartSpawnFactory != null)
                StopCoroutine(_crtStartSpawnFactory);

            _crtStartSpawnFactory = StartCoroutine(_StartFactory());
        }

        /// <summary>
        /// 스폰 팩토리 시작. 
        /// </summary>
        private IEnumerator _StartFactory()
        {
            float fSpawnDelay = 0f;
            if (_stageData != null)
                fSpawnDelay= _stageData.m_fSpawnStartDelay;

            yield return new WaitForSeconds(fSpawnDelay);

            if (OnStartSpawnFactory != null)
                OnStartSpawnFactory();
        }

        /// <summary>
        /// 스폰 팩토리 삭제. 
        /// </summary>
        public static void DestroySpawnFactory()
        {
            if (OnDestroySpawnFactory != null)
                OnDestroySpawnFactory();
        }

        /// <summary>
        /// 등장속도 증가 배율 설정. 
        /// </summary>
        public void SetAppearSpeedUpRate(float fValue)
        {
            if (_fAppearSpeedUpRate.Value != fValue)
            {
                _fPrevAppearSpeedUpRate = _fAppearSpeedUpRate.Value;
                _fAppearSpeedUpRate.Value = fValue;
                _fCachedAppearSpeedUpRate = fValue;

                if (OnChangeAppearSpeedUpRate != null)
                    OnChangeAppearSpeedUpRate(fValue);
            }
        }

        /// <summary>
        /// 이동속도 증가 배율 설정. 
        /// </summary>
        public void SetEnemyMoveSpeedRate(float fValue)
        {
            if (_fEnemyMoveSpeedUpRate != fValue)
            {
                _fPrevEnemyMoveSpeedUpRate = _fEnemyMoveSpeedUpRate;
                _fEnemyMoveSpeedUpRate = fValue;

                if (OnChangeEnemyMoveSpeedUpRate != null)
                    OnChangeEnemyMoveSpeedUpRate(fValue);
            }
        }

        /// <summary>
        /// 적 등장 속도 체크. 
        /// </summary>
        private void _UpdateCheckSpeedUp()
        {
            if (!GameManager.Instance.IsPlaying)
                return;

            if (_lstAppearData == null || _lstAppearData.Count < 1)
                return;

            // 한계치까지 올랐음. 
            if (_iNextAppearIdx >= _lstAppearData.Count)
            {
                if (_iLastAppearNextDist > 0)
                {
                    // 조건이 맞으면 무한 증가. 
                    int iCurScore = Mathf.FloorToInt(UserData.Score);
                    if (_iLastAppearNextDist < iCurScore)
                    {
                        _iLastAppearNextDist = iCurScore + _iLastAppearSpeedUpInterval;
                        SetAppearSpeedUpRate(_fCachedAppearSpeedUpRate + _fLastAppearSpeedUpRate);
                        SetEnemyMoveSpeedRate(_fEnemyMoveSpeedUpRate + _fLastMoveSpeedUpRate);

                        //Debug.LogError("SpeedUp : "+ AppearSpeedUpRate + " / Enemy Move Speed : "+ EnemyMoveSpeedRate);
                    }
                }
            }
            // 일반 증가. 
            else
            {
                int iCurScore = Mathf.FloorToInt(UserData.Score);
                // 등장속도 증가. 
                if (_lstAppearData[_iNextAppearIdx].m_iSpeedUpDist < iCurScore)
                {
                    SetAppearSpeedUpRate(_fCachedAppearSpeedUpRate + _lstAppearData[_iNextAppearIdx].m_fSpeedUpAdd);
                    SetEnemyMoveSpeedRate(_fEnemyMoveSpeedUpRate + _lstAppearData[_iNextAppearIdx].m_fEnemyMoveSpeedAdd);
                    _iNextAppearIdx++;

                    //Debug.LogError("SpeedUp : " + AppearSpeedUpRate + " / Enemy Move Speed : " + EnemyMoveSpeedRate);
                }
            }
        }

        /// <summary>
        /// 백그라운드 테마 변경. 
        /// </summary>
        public void ChangeBackGroundTheme(StageData stageData)
        {
            if (stageData == null)
                return;

            if (_crtChangeBackGround != null)
                return;

            _crtChangeBackGround = StartCoroutine(_ChangeBackGroundTheme(stageData));
        }

        private IEnumerator _ChangeBackGroundTheme(StageData stageData)
        {
            if (stageData == null)
                yield break;

            // 터널 입구 통과. 
            m_trBackGroundTunnelEntrance.SetPosition(Vector3.up * ProjectDefines.BACKGROUND_TUNNEL_START_HEIGHT);
            m_objBackGroundTunnelEntrance.SetActivate(true);
            while (m_trBackGroundTunnelEntrance.localPosition.y > ProjectDefines.BACKGROUND_TUNNEL_END_HEIGHT)
            {
                //m_trBackGroundTunnelEntrance.Translate(Vector3.down * ScrollSpeed * Time.deltaTime);
                m_trBackGroundTunnelEntrance.Translate(Vector3.down * ScrollSpeed * 0.16f * Time.deltaTime);      // UI. 

                // 터널 배경 표시. 
                if (m_trBackGroundTunnelEntrance.localPosition.y < ProjectDefines.BACKGROUND_TUNNEL_CHANGE_HEIGHT)
                    m_objBackGroundTunnel.SetActivate(true);

                // 좀비 아웃라인 표시. 
                //if (m_trBackGroundTunnelEntrance.localPosition.y < ProjectDefines.ATTACK_POSITION.y)
                if (m_trBackGroundTunnelEntrance.localPosition.y < 1000f)        // UI. 
                {
                    //m_compOutlineCtrl.ShowOutlines(0.3f);
                    m_compOutlineCtrl.ActiveOutline(true);
                }

                yield return null;
            }
            m_objBackGroundTunnelEntrance.SetActivate(false);

            // 아웃라인 숨김. 
            //m_compOutlineCtrl.HideOutlines(0.5f);
            m_compOutlineCtrl.ActiveOutline(false);

            // 배경 텍스쳐 설정. 
            Texture texBackground = AssetManager.LoadAsset<Texture>(stageData.m_szBgPath);
            if (texBackground != null)
                _matBackground.mainTexture = texBackground;

            yield return new WaitForSeconds(ProjectDefines.TUNNEL_TIME_SEC);

            // 터널 출구 통과. 
            m_trBackGroundTunnelExit.SetPosition(Vector3.up * ProjectDefines.BACKGROUND_TUNNEL_START_HEIGHT);
            m_objBackGroundTunnelExit.SetActivate(true);
            while (m_trBackGroundTunnelExit.localPosition.y > ProjectDefines.BACKGROUND_TUNNEL_END_HEIGHT)
            {
                m_trBackGroundTunnelExit.Translate(Vector3.down * ScrollSpeed * 0.16f * Time.deltaTime);      // UI. 
                //m_trBackGroundTunnelExit.Translate(Vector3.down * ScrollSpeed * Time.deltaTime);

                // 터널 배경 숨김. 
                if (m_trBackGroundTunnelExit.localPosition.y < ProjectDefines.BACKGROUND_TUNNEL_CHANGE_HEIGHT)
                    m_objBackGroundTunnel.SetActivate(false);

                // 좀비 아웃라인 표시. 
                //if (m_trBackGroundTunnelExit.localPosition.y < ProjectDefines.ATTACK_POSITION.y)
                if (m_trBackGroundTunnelExit.localPosition.y < 1000f)        // UI. 
                {
                    //m_compOutlineCtrl.ShowOutlines(0.3f);
                    m_compOutlineCtrl.ActiveOutline(true);
                }

                yield return null;
            }
            m_objBackGroundTunnelExit.SetActivate(false);

            // 아웃라인 숨김. 
            //m_compOutlineCtrl.HideOutlines(0.5f);
            m_compOutlineCtrl.ActiveOutline(false);

            _nextStageData = _GetNextStageData(stageData.UID);
            _crtChangeBackGround = null;
        }
        #endregion Game Methods

        #region Event
        private void _OnChangedScore(ObEncFloat fValue)
        {
            if (_nextStageData == null)
                return;

            // 테마 변경. 
            if (fValue.Value >= _nextStageData.m_fStartDistance)
            {
                ChangeBackGroundTheme(_nextStageData);
            }
        }

        private void _OnFeverPlayer(PlayerController player, bool isFever)
        {
            if (player != GameManager.Instance.UserPlayerCtrl)
                return;

            //m_objBackgroundFever.SetActivate(isFever);
        }
        #endregion Event

#if UNITY_EDITOR
        private void _DrawGizmoLabel(Vector3 pos, string strLabel, Color color = default(Color), int fontSize = 12)
        {
            GUIStyle style = new GUIStyle();
            style.normal.textColor = color;
            style.fontSize = fontSize;
            style.fontStyle = FontStyle.Normal;
            UnityEditor.Handles.Label(pos, strLabel, style);
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(new Vector2(-10f, ProjectDefines.TOP_DESPAWN_HEIGHT), new Vector2(10f, ProjectDefines.TOP_DESPAWN_HEIGHT));
            _DrawGizmoLabel(new Vector2(6f, ProjectDefines.TOP_DESPAWN_HEIGHT), "Despawn Line", Color.red);

            Gizmos.DrawLine(new Vector2(-10f, ProjectDefines.BOT_DESPAWN_HEIGHT), new Vector2(10f, ProjectDefines.BOT_DESPAWN_HEIGHT));
            _DrawGizmoLabel(new Vector2(6f, ProjectDefines.BOT_DESPAWN_HEIGHT), "Despawn Line", Color.red);

            Gizmos.color = Color.yellow;
            Gizmos.DrawLine(new Vector2(-10f, ProjectDefines.TOP_SCENE_IN_HEIGHT), new Vector2(10f, ProjectDefines.TOP_SCENE_IN_HEIGHT));
            _DrawGizmoLabel(new Vector2(6f, ProjectDefines.TOP_SCENE_IN_HEIGHT), "Out of Scene", Color.yellow);

            Gizmos.DrawLine(new Vector2(-10f, ProjectDefines.BOT_SCENE_IN_HEIGHT), new Vector2(10f, ProjectDefines.BOT_SCENE_IN_HEIGHT));
            _DrawGizmoLabel(new Vector2(6f, ProjectDefines.BOT_SCENE_IN_HEIGHT), "Start of Scene", Color.yellow);

            Gizmos.color = Color.cyan;
            Gizmos.DrawLine(new Vector2(-10f, ProjectDefines.OBJECT_SPAWN_HEIGHT), new Vector2(10f, ProjectDefines.OBJECT_SPAWN_HEIGHT));
            _DrawGizmoLabel(new Vector2(6f, ProjectDefines.OBJECT_SPAWN_HEIGHT), "Spawn Line", Color.cyan);

            Gizmos.color = Color.green;
            Gizmos.DrawLine(new Vector2(-10f, ProjectDefines.ATTACK_POSITION.y), new Vector2(10f, ProjectDefines.ATTACK_POSITION.y));
            _DrawGizmoLabel(new Vector2(6f, ProjectDefines.ATTACK_POSITION.y), "Attack Position", Color.green);
        }
#endif
    }
}