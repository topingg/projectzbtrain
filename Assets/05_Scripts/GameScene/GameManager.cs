﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using AvoEx;
using AvoEx.ObservableType;

namespace Topping
{
    public class GameManager : MonoSingleton<GameManager>
    {
        /// <summary>
        /// 씬 로드 되자마자 해당 UI 를 표시하기 위함. 
        /// </summary>
        public enum ENUM_GAME_SCENE_TYPE
        {
            NONE,
            TITLE,
            GAME,
            CHAR_SELECT,
        }

        #region Delegates
        public static event Action OnGameStarted;
        #endregion Delegates

        #region Public Variables
        // Scene 이 로드 될 때 반영함. 
        public int m_iScreenWidth = 720;
        public int m_iScreenHeight = 1280;
        public int m_iFrameRate = 30;

        // UI Objects. 
        public UIGameTitle m_UIGameTitle;
        public UIGameScene m_UIGameScene;
        public GameObject m_objUIGameSceneUnder;

        // 좀비가 탈 기차 포지션 배열. 
        public Transform[] m_arrTrainSeat;
        // 기차에 탄 좀비 리스트. (0~5번 인덱스까지 유효함)
        public List<PlayerController> m_lstTrainSeat = new List<PlayerController>();
        // 기차에 타지 못한 좀비는 디스폰 되므로 데미지 계산을 위한 리스트. 
        public List<float> m_lstAddDamage = new List<float>();

        // SoundManager 사용시 스킬 사운드를 음소거하면 오브젝트가 사라지는 문제 때문에 만듦. 
        public AudioSource m_audioSkill;

        // 카메라 흔들기 컴포넌트. 
        public ObjectShake m_shakeMainCam;
        #endregion Public Variables

        #region Private Variables
        private static ENUM_GAME_SCENE_TYPE _ePrevSceneType = ENUM_GAME_SCENE_TYPE.NONE;
        private static ENUM_GAME_SCENE_TYPE _eGameSceneType = ENUM_GAME_SCENE_TYPE.TITLE;

        private bool _isPlaying = false;
        private bool _isFactoryStarted = false;

        private Camera _cachedCamera = null;
        private PlayerController _userPlayerCtrl = null;
        private ObEncFloat _drainTrainHp = new ObEncFloat();
        private ObEncFloat _restoreTrainFever = new ObEncFloat();

        private bool _isAppFocused = true;

        private Coroutine _crtCalcPlayerStatus = null;

        private float _fPlayTime = 0f;

        private bool _bReviveCount = false;
        private float _fReviveTime = 0f;

        // 게임을 벗어난 시간 저장. 
        private DateTime _dtLastUnFocusedTime = default(DateTime);
        #endregion Private Variables

        #region Properties
        public static ENUM_GAME_SCENE_TYPE PrevSceneType { get { return _ePrevSceneType; } }
        public static ENUM_GAME_SCENE_TYPE GameSceneType { get { return _eGameSceneType; } }

        public bool IsPlaying { get { return _isPlaying; } }
        public bool IsFactoryStarted { get { return _isFactoryStarted; } }

        public bool IsPaused { get { return Time.timeScale <= 0f; } }
        public bool IsFeverMode { get; set; }

        public Camera CachedCamera { get { return _cachedCamera; } }
        public PlayerController UserPlayerCtrl { get { return _userPlayerCtrl; } }
        public ObEncFloat DrainTrainHp { get { return _drainTrainHp; } }
        public ObEncFloat RestoreTrainFever { get { return _restoreTrainFever; } }

        public bool IsAppFocused { get { return _isAppFocused; } }
        #endregion Properties

        #region Mono
        private void Awake()
        {
            Caching.CleanCache();
            Screen.sleepTimeout = SleepTimeout.NeverSleep;

            Screen.SetResolution(m_iScreenWidth, m_iScreenHeight, true);
            Application.targetFrameRate = m_iFrameRate;

            #if UNITY_EDITOR && TEST_MODE
            GameDataManager.Instance.LoadTables(false);
            #endif

            _cachedCamera = Camera.main;

            GPGSManager.Instance.Login();
            FBManager.Instance.InitFB();
        }

        private void Start()
        {
            StageManager.OnStartSpawnFactory += _OnStartSpawnFactory;
            GPGSManager.OnCheckSaveDataResult += _OnCheckSaveDataResult;

            SoundManager.MuteSFX(EncryptPrefs.GetBool(ProjectDefines.KEY_OPTION_MUTE_SFX));
            SoundManager.MuteMusic(EncryptPrefs.GetBool(ProjectDefines.KEY_OPTION_MUTE_MUSIC));

            // 폴더가 존재하는지 확인. 
            if (!Directory.Exists(UserData.PathDirectory))
                Directory.CreateDirectory(UserData.PathDirectory);

            // 파일이 존재하는지 확인. 
            if (!File.Exists(UserData.SavePath))
            {
                FileStream fs = File.Create(UserData.SavePath);
                fs.Close();
            }

            // 유저 데이터 로드. 
            bool bLoad = UserData.Load();
            if (!bLoad) UserData.Save();

            // 클라우드 데이터 체크. 
            if (!EncryptPrefs.GetBool(ProjectDefines.KEY_CLOUD_DATA_CHECK, false))
                GPGSManager.CheckExistSaveData();

            // 출석 체크. 
            UserData.CheckUserAttend();

            //Debug.LogError(string.Format("Attend Time : {0}", UserDataManager.UserData.m_iAttendDateTime.Value));
            //Debug.LogError(string.Format("Attend Sunday : {0}", UserDataManager.UserData.m_iAttendSundayCount.Value));

            // 게임 초기화. 
            InitGame(_eGameSceneType);
        }

        private void OnDestroy()
        {
            StageManager.OnStartSpawnFactory -= _OnStartSpawnFactory;
            GPGSManager.OnCheckSaveDataResult -= _OnCheckSaveDataResult;
        }

        private void Update()
        {
            _UpdateCalcDistance(Time.deltaTime);
            _UpdateGameRevive(Time.unscaledDeltaTime);

            if (_isPlaying && !IsPaused)
                _fPlayTime += Time.deltaTime;

            // SFX 음소거일때 스킬 사운드 제어. 
            if (m_audioSkill != null)
                m_audioSkill.mute = SoundManager.IsSFXMuted();

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                UIWindow curWindow = UIWindow.GetCurrentWindow();

                // 열린 팝업이 있을 때. 
                if (UITopManager.Instance.IsPopupOpend)
                {
                    UITopManager.OnBackPressedPopup();
                }
                // 열린 창이 있을 때. 
                else if (curWindow != null && curWindow.CachedGameObject.activeInHierarchy)
                {
                    curWindow.OnBackPressed();
                }
                else
                {
                    switch (_eGameSceneType)
                    {
                        case ENUM_GAME_SCENE_TYPE.GAME:
                            {
                                m_UIGameScene.OnClickPause();
                            }
                            break;

                        case ENUM_GAME_SCENE_TYPE.TITLE:
                            {
                                // 게임 종료 팝업.
                                UITopManager.OpenPopup(false, "ARE YOU SURE YOU\nWANT TO EXIT?", 
                                    "NO", null, 
                                    "YES", () => 
                                    {
                                        #if UNITY_EDITOR
                                        ObjectPoolManager.PushAllInstaces();
                                        SceneManager.LoadScene(ProjectDefines.SCENE_NAME_GAME);
                                        #else
                                        EngineManager.Quit();
                                        #endif
                                    }, 
                                    UIPopup.ENUM_BUTTON_COLOR.NORMAL, UIPopup.ENUM_BUTTON_COLOR.RED);
                            }
                            break;
                    }
                }
            }


#if UNITY_EDITOR
            // Test Mode. 
            if (Input.GetMouseButtonDown(1))
            {
                UIWindow curWindow = UIWindow.GetCurrentWindow();

                // 열린 팝업이 있을 때. 
                if (UITopManager.Instance.IsPopupOpend)
                {
                    UITopManager.OnBackPressedPopup();
                }
                // 열린 창이 있을 때. 
                else if (curWindow != null && curWindow.CachedGameObject.activeInHierarchy)
                {
                    curWindow.OnBackPressed();
                }
                else
                {
                    switch (_eGameSceneType)
                    {
                        case ENUM_GAME_SCENE_TYPE.GAME:
                            {
                                m_UIGameScene.OnClickPause();
                            }
                            break;

                        case ENUM_GAME_SCENE_TYPE.TITLE:
                            {
                                // 게임 종료 팝업.
                                UITopManager.OpenPopup(false, "ARE YOU SURE YOU\nWANT TO EXIT?",
                                    "NO", null,
                                    "YES", () => 
                                    {
                                        #if UNITY_EDITOR
                                        ObjectPoolManager.PushAllInstaces();
                                        SceneManager.LoadScene(ProjectDefines.SCENE_NAME_GAME);
                                        #else
                                        EngineManager.Quit();
                                        #endif
                                    }, 
                                    UIPopup.ENUM_BUTTON_COLOR.NORMAL, UIPopup.ENUM_BUTTON_COLOR.RED);
                            }
                            break;
                    }
                }
            }

            if (Input.GetKeyDown(KeyCode.F1))
            {
                UserData.UnlockAllCharacter();

                UITopManager.ShowToast("All Character Unlocked");
            }

            if (Input.GetKeyDown(KeyCode.F2))
            {
                ObjectPoolManager.PushAllInstaces();
                _eGameSceneType = ENUM_GAME_SCENE_TYPE.TITLE;
                SceneManager.LoadScene(ProjectDefines.SCENE_NAME_INTRO);
            }

            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                if (_userPlayerCtrl != null)
                    _userPlayerCtrl.AddPlayerFever(50f);
            }

            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                if (_userPlayerCtrl != null)
                    _userPlayerCtrl.AddPlayerFever(-50f);
            }

            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                if (_userPlayerCtrl != null)
                    _userPlayerCtrl.AddPlayerHP(-50f);
            }

            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                if (_userPlayerCtrl != null)
                    _userPlayerCtrl.AddPlayerHP(50f);
            }

            if (Input.GetKeyDown(KeyCode.N))
            {
                UITopManager.SetNoti("Test Notification");
                UITopManager.ShowNoti();
            }

            if (Input.GetKeyDown(KeyCode.M))
            {
                UITopManager.HideNoti();
            }

            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                UITopManager.OpenPopup(false, "1 Button", "Btn1", () => 
                {
                    UITopManager.OpenPopup(true, "1 Button (2)", "Close", null, UIPopup.ENUM_BUTTON_COLOR.NORMAL);
                }, UIPopup.ENUM_BUTTON_COLOR.NORMAL);
            }

            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                UITopManager.OpenPopup(true, "2 Button", "Btn1", () => { Debug.LogError("Btn1"); }, "Btn2", UIPopup.ENUM_BUTTON_COLOR.GREEN);
                UITopManager.SetBackActionTypePopup(UIPopup.ENUM_BACK_KEY_ACTION.BUTTON_1);
            }

            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                UITopManager.SetBackActionTypePopup(UIPopup.ENUM_BACK_KEY_ACTION.BUTTON_1);
                UITopManager.OpenPopup(false, "2 Button", "Btn1", () => 
                {
                    UITopManager.SetBackActionTypePopup(UIPopup.ENUM_BACK_KEY_ACTION.CLOSE);
                    UITopManager.OpenPopup(true, "2 Button", "Btn1", () => { Debug.LogError("Btn1"); }, "Btn2", () => { Debug.LogError("Btn2"); }, UIPopup.ENUM_BUTTON_COLOR.NORMAL, UIPopup.ENUM_BUTTON_COLOR.GREEN);
                }, "Btn2", UIPopup.ENUM_BUTTON_COLOR.GREEN);
            }

            if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                
            }

            if (Input.GetKeyDown(KeyCode.D))
            {
                if (_userPlayerCtrl != null)
                    _userPlayerCtrl.AddPlayerHP(-999999f);
            }
#endif
        }

        private void OnApplicationFocus(bool focus)
        {
            _isAppFocused = focus;

            if (_isAppFocused)
            {
                if (_eGameSceneType != ENUM_GAME_SCENE_TYPE.GAME)
                    EngineManager.ResumeGame();

                // 일정시간 경과 후 클라이언트 리셋. 
                /*if (_dtLastUnFocusedTime != default(DateTime))
                {
                    long ticks = DateTime.Now.Ticks - _dtLastUnFocusedTime.Ticks;
                    TimeSpan elapsedSpan = new TimeSpan(ticks);

                    if (elapsedSpan.TotalSeconds > ProjectDefines.TIME_RECONNECT_TIME)
                    {
                        _dtLastUnFocusedTime = default(DateTime);

                        // 배경 음악 정지. 
                        SoundManager.StopMusic();
                        // 게임 시간 원래대로. 
                        EngineManager.ResumeGame();
                        // 게임 초기화. 
                        InitGame();
                        // 모든 오브젝트 반환. 
                        ObjectPoolManager.PushAllInstaces();

                        _eGameSceneType = ENUM_GAME_SCENE_TYPE.TITLE;

                        SceneManager.LoadScene(ProjectDefines.SCENE_NAME_INTRO);
                    }
                }*/

                if (!GPGSManager.Instance.IsLogin)
                    GPGSManager.Instance.Login();

                if (AdManager.Instance.IsVungleShowing)
                    Vungle.onResume();
            }
            else
            {
                _dtLastUnFocusedTime = DateTime.Now;

                if (_eGameSceneType == ENUM_GAME_SCENE_TYPE.GAME)
                    m_UIGameScene.OnClickPause();
                else
                    EngineManager.PauseGame();

                if (AdManager.Instance.IsVungleShowing)
                    Vungle.onPause();
            }
        }
        #endregion Mono

        #region Game Methods
        /// <summary>
        /// 게임 초기화. 
        /// 배경, 시트, 점수, 플레이어, UI 등. 
        /// </summary>
        public void InitGame(ENUM_GAME_SCENE_TYPE eType = ENUM_GAME_SCENE_TYPE.TITLE)
        {
            Caching.CleanCache();

            ResumeGame();

            // 배경 초기화. 
            StageManager.Instance.InitStage(ProjectDefines.DEFAULT_STAGE_ID);

            _isPlaying = false;
            _isFactoryStarted = false;
            _fPlayTime = 0f;

            ClearTrainSeat();

            UserData.SetScore(0);
            UserData.SetKillCount(0);
            UserData.SetKillNpcCount(0);
            UserData.SetComboCount(0, true);

            PlayerManager.Instance.Init();

            // 씬 타입 설정.
            SetUIState(eType);
        }

        /// <summary>
        /// Scene Type 설정 및 UI 활성화. 
        /// </summary>
        public void SetUIState(ENUM_GAME_SCENE_TYPE eType, bool bDisableUI = true)
        {
            // 닫기 트윈을 사용하지 않을 때 바로 숨김. 
            if (bDisableUI)
            {
                m_UIGameTitle.CachedGameObject.SetActivate(false);
                m_UIGameScene.CachedGameObject.SetActivate(false);
                m_objUIGameSceneUnder.SetActivate(false);
            }

            _ePrevSceneType = _eGameSceneType;
            _eGameSceneType = eType;

            switch (_eGameSceneType)
            {
                case ENUM_GAME_SCENE_TYPE.TITLE:
                    m_UIGameTitle.CachedGameObject.SetActivate(true);
                    break;

                case ENUM_GAME_SCENE_TYPE.GAME:
                    // 플레이어 생성. 
                   CreatePlayer();

                    m_UIGameScene.CachedGameObject.SetActivate(true);
                    m_objUIGameSceneUnder.SetActivate(true);
                    break;
            }
        }

        /// <summary>
        /// 로딩이 필요한 프리팹 캐싱. 
        /// </summary>
        private void _CachingPrefab()
        {
            if (_userPlayerCtrl == null)
                return;

            string szEffectFire = _userPlayerCtrl.PlayerInfo.PlayerData.m_szAttackEffect;
            if (szEffectFire.IsNull())
                szEffectFire = ProjectDefines.PATH_EFFECT_TOUCH;

            CachingPrefab(szEffectFire, 2);

            SkillData skillData = SkillData.GetTableData(_userPlayerCtrl.CachedPlayerData.m_iSkillID);
            if (skillData != null)
            {
                CachingPrefab(skillData.m_szStartPrefab);
                CachingPrefab(skillData.m_szEffectPrefab);
                CachingPrefab(skillData.m_szEndPrefab);
            }
        }

        /// <summary>
        /// 프리팹 캐싱. 
        /// </summary>
        public void CachingPrefab(string szPrefab, int iCount = 1)
        {
            if (szPrefab.IsNull())
                return;

            for (int i = 0; i < iCount; i++)
            {
                ManagedObject cachedObject = ObjectPoolManager.PopInstance(szPrefab);
                if (cachedObject != null)
                {
                    cachedObject.cachedTransform.position = Vector2.one * 2000f;
                    ObjectPoolManager.PushInstance(cachedObject);
                }
            }
        }

        /// <summary>
        /// 플레이어 생성. 
        /// </summary>
        public void CreatePlayer()
        {
            if (_userPlayerCtrl != null)
            {
                _userPlayerCtrl.PlayerObject.DespawnObject();
                _userPlayerCtrl = null;
            }

            int iCharId = UserData.SelectedCharId;
            if (iCharId <= 0)
                iCharId = ProjectDefines.BASE_PLAYER_ID;

            PlayerData playerData = PlayerData.GetTableData(iCharId);
            if (playerData == null)
                return;

            _drainTrainHp.Value = playerData.m_fDrainHpPerSec;
            _restoreTrainFever.Value = playerData.m_fRestoreFeverPerSec;

            IPatternObject objPlayer = PlayerManager.Instance.CreateObject(playerData, ENUM_OBJECT_TYPE.PLAYER);
            if (objPlayer == null)
                return;

            // 흑백 쉐이더 해제. 
            objPlayer.cachedSpriteRenderer.material.SetFloat("_EffectAmount", 0f);
            _userPlayerCtrl = objPlayer.playerController;

            _CachingPrefab();
        }

        /// <summary>
        /// 플레이어 제거. 
        /// </summary>
        public void DestroyPlayer()
        {
            if (_userPlayerCtrl == null)
                return;

            // 피버 종료 처리. 
            _userPlayerCtrl.PlayerSkill.ClearSkill();

            if (_userPlayerCtrl.PlayerObject != null)
                _userPlayerCtrl.PlayerObject.DespawnObject();

            _userPlayerCtrl = null;
        }

        /// <summary>
        /// 게임 일시정지. 
        /// </summary>
        public static bool PauseGame()
        {
            if (Instance.IsPaused)
                return false;

            if (Instance.UserPlayerCtrl == null || Instance.UserPlayerCtrl.IsDead)
                return false;

            // 배경음 볼륨 줄임. 
            SoundManager.SetVolumeMusic(ProjectDefines.PAUSE_VOLUME_MUSIC);

            EngineManager.PauseGame();

            return true;
        }

        /// <summary>
        /// 게임 재개. 
        /// </summary>
        public static void ResumeGame()
        {
            // 배경음 볼륨 복원. 
            SoundManager.SetVolumeMusic(1f);

            EngineManager.ResumeGame();
        }

        /// <summary>
        /// 게임 시작. 
        /// </summary>
        public static void GameStart()
        {
            if (Instance._isPlaying)
                return;

            // 게임 시작. 
            Instance._isPlaying = true;

            // 플레이어 아이들링. 
            if (Instance._userPlayerCtrl != null)
                Instance._userPlayerCtrl.CurrentState.Idle(Instance._userPlayerCtrl);

            // 해금 조건 초기화. 
            ConditionManager.Instance.Init();

            // 배경 스크롤링 시작. 
            StageManager.Instance.SetBackgroundScroll(true);

            // 스폰 팩토리 시작. 
            StageManager.Instance.StartSpawnFactory();

            // 캐릭터 체력 감소 등 플레이어 상태 계산. 
            if (Instance._crtCalcPlayerStatus != null)
                Instance.StopCoroutine(Instance._crtCalcPlayerStatus);

            Instance._crtCalcPlayerStatus = Instance.StartCoroutine(Instance._CrtCalcPlayerStatus());

            if (OnGameStarted != null)
                OnGameStarted();
        }

        /// <summary>
        /// 게임 오버. 
        /// </summary>
        public static void GameOver()
        {
            Instance.StartCoroutine(Instance._GameOver());
        }

        /// <summary>
        /// 잠시 기다린 후 게임 오버 처리. 
        /// 여러가지 효과를 보여주고, 이펙트 등 디스폰 될 오브젝트 시간을 기다려주는 역할. 
        /// </summary>
        private IEnumerator _GameOver()
        {
            yield return new WaitForSeconds(1f);

            EngineManager.PauseGame();

            // 아나운서 메시지, 토스트 메시지 숨김. 
            UITopManager.HideAnnounce();
            UITopManager.HideToast();

            // 경고 이펙트 숨김. 
            m_UIGameScene.m_objEffectLowHp.SetActivate(false);


            // 리바이브 창 열기. 
            if (Application.internetReachability != NetworkReachability.NotReachable)
                UITopManager.GetUIWindow(typeof(UIReviveWindow)).ForceOpen();

            // 인터넷 연결이 되어있지 않으면 바로 결과창 표시. 
            else
                GameResult();
        }

        /// <summary>
        /// 게임 완전 종료. 
        /// </summary>
        public static void GameResult()
        {
            // 아나운서 메시지, 토스트 메시지 숨김. 
            UITopManager.ClearAnnounce();
            UITopManager.HideToast();

            // 게임 UI 숨김. 
            Instance.m_UIGameScene.CachedGameObject.SetActivate(false);
            Instance.m_objUIGameSceneUnder.SetActivate(false);
            Instance.m_UIGameScene.m_objEffectFever.SetActivate(false);

            // 게임 오버. 
            Instance.StopAllCoroutines();
            Instance._isPlaying = false;

            // 해금 조건 적용. 
            ConditionManager.ProcessGameOver(Mathf.FloorToInt(Instance._fPlayTime));


            // GPGS Event 전송. 
            if (UserData.KillCountAccum >= 33000)
                GPGSManager.SendUnlockAchieve(GPGSManager.ENUM_GPGS_TYPE.ACHIEVE_ZOMBIE_SLAYER);
            else if (UserData.KillCountAccum >= 3000)
                GPGSManager.SendUnlockAchieve(GPGSManager.ENUM_GPGS_TYPE.ACHIEVE_ZOMBIE_HUNTER);
            else if (UserData.KillCountAccum >= 1000)
                GPGSManager.SendUnlockAchieve(GPGSManager.ENUM_GPGS_TYPE.ACHIEVE_ZOMBIE_TRACKER);

            if (UserData.GameOverCount <= 20)
                GPGSManager.SendStepAchieve(GPGSManager.ENUM_GPGS_TYPE.ACHIEVE_NOVICE_HUNTER_10, 1);
            if (UserData.KillNpcCountAccum <= 20)
                GPGSManager.SendStepAchieve(GPGSManager.ENUM_GPGS_TYPE.ACHIEVE_BE_CAREFUL_10, (int)UserData.KillNpcCountAccum);

            if (UserData.ListUnLockCharCount >= 10 && UserData.BestScore >= 50000 && UserData.PlayTimeAccum >= 10800 && UserData.PlayTimeAccum < 14400)
                GPGSManager.SendUnlockAchieve(GPGSManager.ENUM_GPGS_TYPE.ACHIEVE_THE_CONQUEROR);

            GPGSManager.SendLeader(GPGSManager.ENUM_GPGS_TYPE.LEADER_KILL_ZOMBIE, UserData.KillCountAccum);
            GPGSManager.SendLeader(GPGSManager.ENUM_GPGS_TYPE.LEADER_KILL_NPC, UserData.KillNpcCountAccum);
            GPGSManager.SendLeader(GPGSManager.ENUM_GPGS_TYPE.LEADER_PLAY_TIME, UserData.PlayTimeAccum * 1000);     // 구글 업적은 밀리초 단위라서 1000 곱함. 

            GPGSManager.SendEvent(GPGSManager.ENUM_GPGS_TYPE.EVENT_KILL_ZOMBIE, UserData.KillCount);
            GPGSManager.SendEvent(GPGSManager.ENUM_GPGS_TYPE.EVENT_GAME_OVER, 1);

            if (UserData.BestScore > 40000)
                GPGSManager.RevealAchieve(GPGSManager.ENUM_GPGS_TYPE.ACHIEVE_THE_CONQUEROR);


            // 배경 스크롤 속도 조정. 
            StageManager.Instance.SetScrollSpeed(ProjectDefines.BACKGROUND_SCROLL_SPEED_IN_UI);
            
            // 스폰 팩토리 삭제. 
            StageManager.DestroySpawnFactory();

            // 화면의 모든 오브젝트 삭제. 
            ObjectPoolManager.PushAllInstaces();

            // 배경이 다시 흐르도록. 
            ResumeGame();

            // 결과 창 열기. 
            UIWindow winResult = UITopManager.GetUIWindow(typeof(UIResultWindow));
            if (winResult != null)
            {
                winResult.ForceOpen();
                winResult.InitWindow();
            }
        }

        /// <summary>
        /// 게임 재시작. (부활)
        /// 현재 게임 상황이 유지되며, 플레이어 정보만 변경. 
        /// </summary>
        public static void GameRevive()
        {
            // 현재 생성된 리스트 디스폰. 
            PlayerManager.Instance.DespawnAllObject();

            // 기차 비우기. 
            Instance.ClearTrainSeat();

            // 플레이어 상태 초기화. 
            Instance._userPlayerCtrl.PlayerInfo.InitStats();
            Instance._userPlayerCtrl.IsDead = false;

            // 플레이어 아이들링. 
            if (Instance._userPlayerCtrl != null)
                Instance._userPlayerCtrl.CurrentState.Idle(Instance._userPlayerCtrl);

            // Game UI 정리. (Fool 이펙트)
            Instance.m_UIGameScene.m_tweenEffectFool.Finish(true);

            Instance._fReviveTime = 1f;
            Instance._bReviveCount = true;

            // 스폰 팩토리 초기화. 
            SpawnFactory.InitFactory();
        }

        private void _UpdateGameRevive(float fDeltaTime)
        {
            if (!_bReviveCount)
                return;

            if (_fReviveTime > 0f)
            {
                _fReviveTime -= fDeltaTime;
                return;
            }
            else
            {
                // Resume 카운트. 
                UITopManager.SetActivePauseCount(true);
                _bReviveCount = false;
            }
        }

        /// <summary>
        /// 게임 재시작. 
        /// </summary>
        public static void GameReStart()
        {
            Instance.InitGame(ENUM_GAME_SCENE_TYPE.GAME);
            Instance.m_UIGameScene.WaitForGameStart();
        }

        /// <summary>
        /// 거리(Score) 계산. 
        /// </summary>
        private void _UpdateCalcDistance(float deltaTime)
        {
            // 게임 시작 전에는 돌리지 않음. 
            if (!_isPlaying)
                return;

            if (_userPlayerCtrl == null || _userPlayerCtrl.IsDead)
                return;

            float fSpeedDelta = 0f;
            if (StageManager.Instance.ScrollSpeed > 1f)
                fSpeedDelta = 1f;
            else if (StageManager.Instance.ScrollSpeed > 0f)
                fSpeedDelta = 1 / StageManager.Instance.ScrollSpeed;

            float fAddScore = fSpeedDelta * (ProjectDefines.TRAIN_SPEED * deltaTime);

            // 속도에 따른 점수 증가. 
            UserData.AddScore(fAddScore);
        }

        /// <summary>
        /// 플레이어 상태 계산. 
        /// </summary>
        private IEnumerator _CrtCalcPlayerStatus()
        {
            float fDrainTrainHp = _drainTrainHp.Value;
            float fGetTrainFever = _restoreTrainFever.Value;
            float fInterval = 0.1f;

            while (true)
            {
                yield return new WaitForSeconds(fInterval);

                // 게임 시작 전이나 피버 때는 돌리지 않음. 
                if (!IsFactoryStarted || _userPlayerCtrl.IsFever || IsPaused)
                    continue;

                // 추가 체력 감소량. 
                float fAddDamage = 0f;
                for (int i = 0, cnt = m_lstAddDamage.Count; i < cnt; i++)
                {
                    fAddDamage += m_lstAddDamage[i];
                }

                // 플레이어 체력 감소. 
                _userPlayerCtrl.AddPlayerHP(-(fDrainTrainHp + fAddDamage) * fInterval);

                // 플레이어 피버 충전. (차후 추가 획득량이 생길 수 있음)
                _userPlayerCtrl.AddPlayerFever(fGetTrainFever * fInterval);

                if (!_isPlaying)
                    break;
            }
        }

        /// <summary>
        /// 기차 태우기. 
        /// </summary>
        public void SetTrainSeat(PlayerController target)
        {
            if (target == null)
                return;

            if (target.m_compOutline != null)
                Destroy(target.m_compOutline);

            // 기차에 타면 콤보 리셋. 
            if (!_userPlayerCtrl.IsFever)
                UserData.SetComboCount(0);

            ConditionManager.Instance.AddZombieOnTrain(1);

            // 0~5까지 6자리만 탑승 가능. 나머지는 리스트만 늘림. 
            m_lstTrainSeat.Add(target);
            m_lstAddDamage.Add(target.PlayerInfo.PlayerData.m_fAttackDmg);

            if (m_lstTrainSeat.Count > m_arrTrainSeat.Length)
            {
                target.PlayerObject.DespawnObject();
            }
            else
            {
                target.cachedTransform.position = m_arrTrainSeat[m_lstTrainSeat.Count - 1].position;
                target.cachedTransform.rotation = m_arrTrainSeat[m_lstTrainSeat.Count - 1].rotation;
            }
        }

        /// <summary>
        /// 기차 비우기. 
        /// </summary>
        public void ClearTrainSeat()
        {
            for (int i = 0, cnt = m_lstTrainSeat.Count; i < cnt; i++)
            {
                if (m_lstTrainSeat[i] == null)
                    continue;

                if (!m_lstTrainSeat[i].IsDead)
                    m_lstTrainSeat[i].PlayerObject.DespawnObject();
            }

            m_lstTrainSeat.Clear();
            m_lstAddDamage.Clear();
        }

        /// <summary>
        /// 스킬 효과음 재생. 
        /// </summary>
        public void PlaySkillSound(string szSound, float fVolume = 1f, bool bLoop = false)
        {
            if (!szSound.IsNull() && m_audioSkill != null)
            {
                AudioClip skillAudioClip = SoundManager.Load(szSound);
                if (skillAudioClip != null)
                {
                    m_audioSkill.clip = skillAudioClip;
                    m_audioSkill.volume = fVolume;
                    m_audioSkill.loop = bLoop;
                    m_audioSkill.Play();
                }
            }
        }

        /// <summary>
        /// 스킬 효과음 중지. 
        /// </summary>
        public void StopSkillSound(bool isPause = false)
        {
            if (m_audioSkill != null)
            {
                if (isPause && m_audioSkill.isPlaying)
                {
                    m_audioSkill.Pause();
                }
                else if (!isPause)
                {
                    m_audioSkill.Stop();
                    m_audioSkill.clip = null;
                }
            }
        }

        /// <summary>
        /// 스팀 효과음 다시 재생. 
        /// </summary>
        public void ResumeSkillSound()
        {
            if (m_audioSkill != null && !m_audioSkill.isPlaying)
                m_audioSkill.UnPause();
        }
        #endregion Game Methods

        #region Event
        private void _OnStartSpawnFactory()
        {
            _isFactoryStarted = true;
        }

        private void _OnCheckSaveDataResult(bool bExist)
        {
            EncryptPrefs.SetBool(ProjectDefines.KEY_CLOUD_DATA_CHECK, true);

            if (bExist)
            {
                UITopManager.OpenPopup(false, "YOUR  DATA  EXISTS\nIN  THE  CLOUD\nWANT  TO  LOAD  IT ?", "YES", () =>
                {
                    GPGSManager.LoadFromGoogle();
                    UITopManager.ClosePopup();
                }, "NO", null, UIPopup.ENUM_BUTTON_COLOR.GREEN, UIPopup.ENUM_BUTTON_COLOR.NORMAL);
            }
        }
        #endregion Event
    }
}