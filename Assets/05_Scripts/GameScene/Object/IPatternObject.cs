﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Topping
{
    public enum ENUM_OBJECT_TYPE
    {
        ENEMY,
        NPC,
        OBSTACLE,
        ITEM,

        PLAYER = 100,

        NONE = 999,
    }

    public interface IPatternObject
    {
        PlayerController playerController { get; }
        ENUM_OBJECT_TYPE ObjectType { get; }
        IPlayerData ObjectData { get; }

        Animator cachedAnimator { get; }
        Animation cachedAnimation { get; }

        // 모델 트랜스폼. 
        Transform trModelObject { get; }
        // 그림자가 붙을 위치. 
        Transform trShadowObject { get; }
        // 이펙트가 붙을 위치. 
        Transform trEffectObject { get; }

        // 모델 오브젝트. 
        GameObject cachedModelObject { get; }
        GameObject cachedShadowObject { get; }

        SpriteRenderer cachedSpriteRenderer { get; }

        Quaternion originRotLeft { get; }
        Quaternion originRotRight { get; }


        /// <summary>
        /// 오브젝트 생성. 
        /// </summary>
        void InitObject(Transform trParent = null, IPlayerData playerData = null);
        void DespawnObject();
    }
}