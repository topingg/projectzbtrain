﻿using UnityEngine;
using AvoEx;

namespace Topping
{
    public class PatternObject : ManagedObject, IPatternObject
    {
        #region Private Variables
        private PlayerController _playerCtrl = null;
        private ENUM_OBJECT_TYPE _eObjectType = ENUM_OBJECT_TYPE.NONE;
        private IPlayerData _objectData = null;

        private Animator _cachedAnimator = null;
        private Animation _cachedAnimation = null;

        private Transform _trModelObject = null;
        private Transform _trShadowObject = null;
        private Transform _trEffectObject = null;

        private GameObject _cachedModelObject = null;
        private GameObject _cachedShadowObject = null;

        private SpriteRenderer _cachedSpriteRenderer = null;

        private Quaternion _originRotLeft = Quaternion.identity;
        private Quaternion _originRotRight = Quaternion.identity;

        private string _szHitAnim = null;
        #endregion Private Variables

        #region Properties
        //-------------- Interface -------------------//
        public PlayerController playerController { get { return _playerCtrl; } }
        public ENUM_OBJECT_TYPE ObjectType { get { return _eObjectType; } }
        public IPlayerData ObjectData { get { return _objectData; } }

        public Animator cachedAnimator { get { return _cachedAnimator; } }
        public Animation cachedAnimation { get { return _cachedAnimation; } }
        public Transform trModelObject { get { return _trModelObject; } }
        public Transform trShadowObject { get { return _trShadowObject; } }
        public Transform trEffectObject { get { return _trEffectObject; } }

        public GameObject cachedModelObject { get { return _cachedModelObject; } }
        public GameObject cachedShadowObject { get { return _cachedShadowObject; } }

        public SpriteRenderer cachedSpriteRenderer { get { return _cachedSpriteRenderer; } }

        public Quaternion originRotLeft { get { return _originRotLeft; } }
        public Quaternion originRotRight { get { return _originRotRight; } }
        //-------------- Interface -------------------//
        #endregion Properties

        #region Mono
        protected override void Awake()
        {
            base.Awake();

            if (_cachedAnimator == null)
                _cachedAnimator = GetComponent<Animator>();

            if (_cachedAnimator == null)
                _cachedAnimator = GetComponentInChildren<Animator>();

            if (_cachedAnimation == null)
                _cachedAnimation = GetComponent<Animation>();

            if (_cachedAnimation == null)
                _cachedAnimation = GetComponentInChildren<Animation>();

            _trModelObject = cachedTransform.Find("ModelObject");
            _trShadowObject = cachedTransform.Find("ShadowObject");

            if (_trModelObject != null)
                _cachedModelObject = _trModelObject.gameObject;

            _cachedSpriteRenderer = GetComponentInChildren<SpriteRenderer>();

            _playerCtrl = GetComponent<PlayerController>();
        }

        private void OnEnable()
        {
            if (_cachedAnimator != null)
                _cachedAnimator.enabled = false;

            // 애니메이션 초기화. 
            if (_cachedAnimation != null && !_szHitAnim.IsNull())
            {
                _cachedAnimation[_szHitAnim].time = 0f;
                _cachedAnimation[_szHitAnim].enabled = true;
                _cachedAnimation[_szHitAnim].weight = 1;
                _cachedAnimation.Sample();
                _cachedAnimation[_szHitAnim].enabled = false;
            }
        }
        #endregion Mono

        #region Interface Methods
        public void InitObject(Transform trParent, IPlayerData playerData)
        {
            _objectData = playerData;

            if (trParent != null)
                cachedTransform.SetParent(trParent);

            //cachedTransform.localPosition = ProjectDefines.PLAYER_BASE_POSITION;

            _originRotRight = Quaternion.identity;

            if (_trModelObject != null)
                _trModelObject.localRotation = Quaternion.identity;

            if (_trShadowObject != null)
            {
                _trShadowObject.localRotation = Quaternion.identity;
                _cachedShadowObject.SetActive(true);
            }

            if (_playerCtrl != null && playerData != null)
                _playerCtrl.InitController(playerData);
        }

        public void DespawnObject()
        {
            try
            {
                ObjectPoolManager.PushInstance(this);
            }
            catch
            {
                Destroy(cachedObject);
            }

            if (PlayerManager.Instance != null)
                PlayerManager.Instance.RemoveObject(this);
        }
        #endregion Interface Methods
    }
}