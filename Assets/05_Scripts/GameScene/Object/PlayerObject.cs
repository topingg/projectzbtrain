﻿using UnityEngine;
using System;
using AvoEx;

namespace Topping
{
    /// <summary>
    /// 유저 오브젝트. 
    /// </summary>
    public class PlayerObject : ManagedObject, IPatternObject
    {
        #region Public Variables
        public Transform m_trEffectObject;
        #endregion Public Variables

        #region Private Variables
        private PlayerController _playerCtrl = null;
        private ENUM_OBJECT_TYPE _eObjectType = ENUM_OBJECT_TYPE.PLAYER;
        private IPlayerData _objectData = null;

        private Animator _cachedAnimator = null;
        private Animation _cachedAnimation = null;

        private Transform _trModelObject = null;
        private Transform _trShadowObject = null;

        private GameObject _cachedModelObject = null;
        private GameObject _cachedShadowObject = null;

        private SpriteRenderer _cachedSpriteRenderer = null;

        private Quaternion _originRotLeft = Quaternion.identity;
        private Quaternion _originRotRight = Quaternion.identity;
        #endregion Private Variables

        #region Properties
        //-------------- Interface -------------------//
        public PlayerController playerController { get { return _playerCtrl; } }
        public ENUM_OBJECT_TYPE ObjectType { get { return _eObjectType; } }
        public IPlayerData ObjectData { get { return _objectData; } }

        public Animator cachedAnimator { get { return _cachedAnimator; } }
        public Animation cachedAnimation { get { return _cachedAnimation; } }

        public Transform trModelObject { get { return _trModelObject; } }
        public Transform trShadowObject { get { return _trShadowObject; } }
        public Transform trEffectObject { get { return m_trEffectObject; } }

        public GameObject cachedModelObject { get { return _cachedModelObject; } }
        public GameObject cachedShadowObject { get { return _cachedShadowObject; } }

        public SpriteRenderer cachedSpriteRenderer { get { return _cachedSpriteRenderer; } }

        public Quaternion originRotLeft { get { return _originRotLeft; } }
        public Quaternion originRotRight { get { return _originRotRight; } }
        //-------------- Interface -------------------//
        #endregion Properties

        #region Mono
        protected override void Awake()
        {
            base.Awake();

            if (_cachedAnimator == null)
                _cachedAnimator = GetComponent<Animator>();

            if (_cachedAnimator == null)
                _cachedAnimator = GetComponentInChildren<Animator>();

            _trModelObject = cachedTransform.Find("ModelObject");
            _trShadowObject = cachedTransform.Find("ShadowObject");
            if (_trShadowObject != null)
                _cachedShadowObject = _trShadowObject.gameObject;
            if (m_trEffectObject == null)
                m_trEffectObject = cachedTransform.Find("EffectObject");

            if (_trModelObject != null)
                _cachedModelObject = _trModelObject.gameObject;

            _cachedSpriteRenderer = GetComponentInChildren<SpriteRenderer>();

            _playerCtrl = GetComponent<PlayerController>();
        }

        private void OnEnable()
        {
            /*if (_cachedAnimator != null)
                _cachedAnimator.enabled = false;*/

            PlayerController.OnPlayerDead += _OnPlayerDead;
            PlayerController.OnPlayerFever += _OnPlayerFever;
            StageManager.OnChangeScrollSpeed += _OnScrollAnimatorSpeed;
        }

        private void OnDisable()
        {
            PlayerController.OnPlayerDead -= _OnPlayerDead;
            PlayerController.OnPlayerFever -= _OnPlayerFever;
            StageManager.OnChangeScrollSpeed -= _OnScrollAnimatorSpeed;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (_playerCtrl == null)
                return;

            if (_playerCtrl.IsDead)
                return;

            if (other.gameObject.CompareTag("Enemy"))
            {
                PlayerController enemyCtrl = other.GetComponent<PlayerController>();
                if (enemyCtrl == null)
                    return;

                if (enemyCtrl.IsDead)
                    return;

                // 무적. 
                if (_playerCtrl.PlayerInfo.HasCondition(ENUM_PLAYER_CONDITION.INVINCIBLE))
                {
                    //enemyCtrl.ProcessBuff(_playerCtrl);
                    enemyCtrl.ProcessHit(_playerCtrl);
                    return;
                }
            }
        }
        #endregion Mono

        #region Interface Methods
        public void InitObject(Transform trParent, IPlayerData playerData)
        {
            _objectData = playerData;

            if (trParent != null)
                cachedTransform.SetParent(trParent);

            cachedTransform.localPosition = ProjectDefines.PLAYER_BASE_POSITION;

            _originRotRight = Quaternion.identity;

            if (_trModelObject != null)
                _trModelObject.localRotation = Quaternion.identity;

            if (_trShadowObject != null)
            {
                _trShadowObject.localRotation = Quaternion.identity;
                _cachedShadowObject.SetActive(true);
            }

            if (_playerCtrl != null && playerData != null)
                _playerCtrl.InitController(playerData);
        }

        public void DespawnObject()
        {
            try
            {
                if (_playerCtrl != null)
                    ObjectPoolManager.PushInstance(_playerCtrl);
                else
                    ObjectPoolManager.PushInstance(this);
            }
            catch
            {
                Destroy(cachedObject);
            }
        }
        #endregion Interface Methods

        #region Event
        private void _OnPlayerDead(PlayerController player, bool isDead)
        {
            if (player == null && player != this)
                return;

            if (_cachedShadowObject != null)
                _cachedShadowObject.SetActive(!isDead);
        }

        private void _OnPlayerFever(PlayerController player, bool isFever)
        {
            if (player == null || player != _playerCtrl)
                return;

            if (isFever)
            {
                if (_cachedShadowObject != null)
                    _cachedShadowObject.SetActive(!isFever);
            }
        }

        /// <summary>
        /// 스크롤 속도에 맞춰 애니메이션 속도 조정. 
        /// </summary>
        private void _OnScrollAnimatorSpeed(float fSpeed)
        {
            if (cachedAnimator == null)
                return;

            //cachedAnimator.speed = Mathf.Clamp(fSpeed * 0.185f, 0.5f, 2.5f);
        }
        #endregion Event
    }
}