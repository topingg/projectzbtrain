﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AvoEx;

using cakeslice;

namespace Topping
{
    public enum ENUM_PLAYER_SIDE
    {
        NONE,
        LEFT,
        RIGHT,
    }

    /// <summary>
    /// 주의 : 인스펙터에서 PatternObject 컴포넌트보다 위에 있으면 생성시 오류 발생.
    /// </summary>
    [RequireComponent(typeof(IPatternObject))]
    public class PlayerController : ManagedObject
    {
        #region Delegate
        public delegate void DelePlayerBoolean(PlayerController player, bool bBoolean);
        public static event DelePlayerBoolean OnPlayerDead;
        public static event DelePlayerBoolean OnPlayerFever;
        public static event System.Action OnUserDead;
        #endregion Delegate

        #region Public Variables
        /// <summary>
        /// 피버시 활성화 시킬 오브젝트. 
        /// </summary>
        public GameObject m_objFeverEffectModel;

        /// <summary>
        /// 아웃라인 컴포넌트. 
        /// </summary>
        public Outline m_compOutline;
        public GameObject m_objSprite;
        #endregion Public Variables

        #region Private Variables
        [SerializeField]
        private PlayerInfo _playerInfo = null;
        private IPatternObject _playerObject = null;

        private IPlayerState _reservedState = null;
        private IPlayerState _currentState = null;
        [SerializeField]
        private ENUM_PLAYER_STATE _ePlayerState = ENUM_PLAYER_STATE.NONE;
        private ENUM_PLAYER_SIDE _ePlayerSide = ENUM_PLAYER_SIDE.NONE;

        private bool _isDead = false;
        private bool _isFever = false;

        private PlayerData _cachedPlayerData = null;
        private EnemyData _cachedEnemyData = null;
        private NpcData _cachedNpcData = null;
        private PlayerSkill _playerSkill = null;

        private bool _isItemCreated = false;
        private ManagedObject _objCreatedItem = null;

        private bool _isOutOfScreen = false;

        // 스폰되었는지 체크용. 
        private bool _isSpawnChecked = false;
        // 스폰 체크 높이 (랜덤)
        private float _fSpawnCheckHeight = 1f;
        #endregion Private Variables

        #region Properties
        public PlayerInfo PlayerInfo { get { return _playerInfo; } }
        public IPatternObject PlayerObject { get { return _playerObject; } }

        public IPlayerState ReservedState { get { return _reservedState; } }
        public IPlayerState CurrentState { get { return _currentState; } }

        public ENUM_PLAYER_STATE CurrentStateType { get { return _ePlayerState; } }
        public ENUM_PLAYER_SIDE CurrentSideType { get { return _ePlayerSide; } }

        public bool IsDead
        {
            get
            {
                return _isDead;
            }
            set
            {
                if (_isDead != value)
                    _isDead = value;

                if (_isDead == true)
                {
                    if (_currentState != null)
                        _currentState.Die(this);

                    if (OnPlayerDead != null)
                        OnPlayerDead(this, _isDead);

                    if (this == GameManager.Instance.UserPlayerCtrl)
                    {
                        // 피버 종료 처리. 
                        if (this == GameManager.Instance.UserPlayerCtrl)
                            GameManager.Instance.UserPlayerCtrl.PlayerSkill.ClearSkill();

                        // 게임 오버. 
                        GameManager.GameOver();

                        if (OnUserDead != null)
                            OnUserDead();
                    }
                }
            }
        }

        public bool IsFever
        {
            get
            {
                return _isFever;
            }
            set
            {
                if (_isFever != value)
                    _isFever = value;

                if (OnPlayerFever != null)
                    OnPlayerFever(this, _isFever);
            }
        }

        public bool IsAttacking { get; set; }

        public PlayerData CachedPlayerData { get { return _cachedPlayerData; } }
        public EnemyData CachedEnemyData { get { return _cachedEnemyData; } }
        public NpcData CachedNpcData { get { return _cachedNpcData; } }
        public PlayerSkill PlayerSkill { get { return _playerSkill; } }
        public ManagedObject CachedCreatedItem { get { return _objCreatedItem; } }
        #endregion Properties

        #region Mono
        protected override void Awake()
        {
            base.Awake();

            if (_playerObject == null)
                _playerObject = GetComponent<IPatternObject>();
        }

        private void Update()
        {
            if (!GameManager.Instance.IsPlaying)
                return;

            // 플레이어 이동. 
            //if (_playerObject.cachedAnimator != null && _playerObject.cachedAnimator.enabled)
                cachedTransform.Translate(_playerInfo.Velocity * Time.deltaTime);

            if (IsDead)
                return;

            if (_currentState != null)
                _currentState.Update(this, Time.deltaTime);

            // 캐릭터 타입별 액션. 
            if (_playerInfo.PlayerType == ENUM_PLAYER_TYPE.ENEMY || _playerInfo.PlayerType == ENUM_PLAYER_TYPE.NPC)
                _UpdateCharacterAction(Time.deltaTime);
        
            // 화면 밖으로 나가면 디스폰. 
            if (_playerObject != null &&
                (cachedTransform.position.y > ProjectDefines.TOP_DESPAWN_HEIGHT ||
                cachedTransform.position.y < ProjectDefines.BOT_DESPAWN_HEIGHT))
            {
                _playerObject.DespawnObject();
            }

            // 스폰 완료. 
            if (!_isSpawnChecked)
            {
                if (cachedTransform.position.y > ProjectDefines.OBJECT_SPAWN_HEIGHT + _fSpawnCheckHeight)
                {
                    _isSpawnChecked = true;
                    SpawnFactory.s_bSpawning = false;
                }
            }
        }
        #endregion Mono

        #region Game Methods
        public void InitController(IPlayerData playerData, ENUM_PLAYER_SIDE eSide = ENUM_PLAYER_SIDE.NONE)
        {
            _InitVariables();

            if (m_objSprite != null)
            {
                m_compOutline = m_objSprite.GetComponent<Outline>();
                if (m_compOutline == null)
                    m_compOutline = m_objSprite.AddComponent<Outline>();
            }

            _playerInfo = PlayerInfo.CreatePlayer(playerData);

            if (playerData.GetType().Equals(typeof(PlayerData)))
            {
                _cachedPlayerData = playerData as PlayerData;
                if (m_compOutline != null) m_compOutline.color = 1;
            }
            else if (playerData.GetType().Equals(typeof(EnemyData)))
            {
                _cachedEnemyData = playerData as EnemyData;
                if (m_compOutline != null) m_compOutline.color = 0;
            }
            else if (playerData.GetType().Equals(typeof(NpcData)))
            {
                _cachedNpcData = playerData as NpcData;
                if (m_compOutline != null) m_compOutline.color = 1;
            }

            if (_cachedPlayerData != null)
                InitPlayerSkill(_cachedPlayerData.m_iSkillID);

            _ePlayerSide = eSide;

            _currentState = PlayerStatePool.GetState(ENUM_PLAYER_STATE.IDLE);
            SetPlayerState(PlayerStatePool.GetState(ENUM_PLAYER_STATE.IDLE));

            StartCoroutine(_CheckAnimationState());
        }

        public void InitPlayerSkill(int iSkillId)
        {
            _playerSkill = null;

            if (iSkillId < 1)
                return;

            _playerSkill = cachedObject.AddComponent(PlayerSkillPool.GetSkill(iSkillId)) as PlayerSkill;
            if (_playerSkill == null)
                return;

            _playerSkill.InitSkill(this, iSkillId);
        }

        private void _InitVariables()
        {
            IsDead = false;

            IsFever = false;
            IsAttacking = false;

            _cachedPlayerData = null;
            _cachedEnemyData = null;
            _cachedNpcData = null;
            _playerSkill = null;

            _isItemCreated = false;
            _objCreatedItem = null;

            _isOutOfScreen = false;

            _isSpawnChecked = false;
            _fSpawnCheckHeight = Random.Range(1f, 1.5f);
        }

        public void SetAnimatorActive(bool bActive)
        {
            if (_playerObject != null && _playerObject.cachedAnimator != null)
                _playerObject.cachedAnimator.enabled = bActive;
        }

        public void AddPlayerHP(float fValue)
        {
            if (IsDead)
                return;

            // Test Mode. 
            if (_playerInfo.PlayerType == ENUM_PLAYER_TYPE.PLAYER && EncryptPrefs.GetBool(ProjectDefines.KEY_TEST_PLAYER_INVINCIBLE))
                return;

            float fCurHP = _playerInfo.CurHP;
            float fNextHP = Mathf.Clamp(fCurHP + fValue, 0f, _playerInfo.PlayerData.m_fMaxHP);
            _playerInfo.SetHP(fNextHP);

            if (fNextHP <= 0f)
            {
                if (m_compOutline != null)
                    Destroy(m_compOutline);

                IsDead = true;

                if (_playerInfo.PlayerType == ENUM_PLAYER_TYPE.ENEMY)
                {
                    UserData.AddKillCount(1);
                    UserData.AddComboCount(1);
                }
                else if (_playerInfo.PlayerType == ENUM_PLAYER_TYPE.NPC)
                {
                    UserData.AddKillNpcCount(1);

                    // NPC 를 쏘면 콤보 리셋. 
                    UserData.SetComboCount(0);
                }
            }
        }

        public void AddPlayerFever(float fValue)
        {
            if (IsDead)
                return;

            float fCurFever = _playerInfo.CurFever;
            float fNextFever = Mathf.Clamp(fCurFever + fValue, 0f, _playerInfo.PlayerData.m_fMaxFever);
            _playerInfo.SetFever(fNextFever);
        }
        #endregion Game Methods

        #region Damage Process
        /// <summary>
        /// 피격 처리. (공격자 정보)
        /// </summary>
        public void ProcessHit(PlayerController attackerCtrl)
        {
            if (attackerCtrl == null)
                return;

            if (attackerCtrl.PlayerInfo == null || attackerCtrl.PlayerInfo.PlayerData == null)
                return;

            ProcessHit(attackerCtrl.PlayerInfo.PlayerData.m_lstDamage, attackerCtrl);
        }

        /// <summary>
        /// 피격 처리. (데미지 리스트)
        /// </summary>
        public void ProcessHit(List<DamageInfo> lstDamageInfo, PlayerController attackerCtrl = null)
        {
            if (lstDamageInfo == null)
                return;

            for (int i = 0, cnt = lstDamageInfo.Count; i < cnt; i++)
            {
                ProcessHit(lstDamageInfo[i], attackerCtrl);
            }
        }

        /// <summary>
        /// 피격 처리. (데미지 클래스)
        /// </summary>
        public void ProcessHit(DamageInfo damageInfo, PlayerController attackerCtrl = null)
        {
            if (!GameManager.Instance.IsPlaying)
                return;

            if (IsDead)
                return;

            if (damageInfo == null)
                return;

            // 지속 데미지. 
            if (damageInfo.DamageDuration > 0f)
            {
                ContinuousDamage prevComp = cachedObject.GetComponent<ContinuousDamage>();
                if (prevComp != null)
                {
                    if (prevComp.AttackInfo.DamageType == damageInfo.DamageType)
                    {
                        prevComp.Reset();
                        return;
                    }
                }

                ContinuousDamage comp = cachedObject.AddComponent<ContinuousDamage>();
                comp.InitComponent(damageInfo, this, attackerCtrl);
            }

            // 일반 데미지. 
            else
            {
                // 플레이어 타격음 재생. 
                if (attackerCtrl == GameManager.Instance.UserPlayerCtrl)
                {
                    if (attackerCtrl.PlayerInfo.PlayerData.m_arrHitSound != null)
                    {
                        int iHitSoundCount = attackerCtrl.PlayerInfo.PlayerData.m_arrHitSound.Length;
                        if (iHitSoundCount > 0)
                        {
                            string szHitSound = attackerCtrl.PlayerInfo.PlayerData.m_arrHitSound[Random.Range(0, iHitSoundCount)];
                            if (!szHitSound.IsNull())
                                SoundManager.PlaySFX(szHitSound);
                        }
                    }
                }

                // 피격 효과음 재생. 
                if (this != GameManager.Instance.UserPlayerCtrl)
                {
                    if (_playerInfo.PlayerData.m_arrHitSound != null)
                    {
                        int iHitSoundCount = _playerInfo.PlayerData.m_arrHitSound.Length;
                        if (iHitSoundCount > 0)
                        {
                            string szHitSound = _playerInfo.PlayerData.m_arrHitSound[Random.Range(0, iHitSoundCount)];
                            if (!szHitSound.IsNull())
                                SoundManager.PlaySFX(szHitSound);
                        }
                    }
                }

                if (_playerInfo.IsUnbeatable())
                {
                    //Debug.LogError("{0} is Unbeatable".SetFormat(_playerInfo.PlayerData.m_szName));
                    return;
                }
                else
                {
                    if (_currentState != null)
                        _currentState.Hit(this);
                }

                AddPlayerHP(-damageInfo.DamageValue);

                /*PlayerInfo.Stats playerStat;
                float fFinalDmg = 0f;

                // 스탯 적용. 
                if (attackerCtrl != null)
                    playerStat = attackerCtrl.PlayerInfo.PlayerStat;
                else
                    playerStat = PlayerInfo.Stats.One;

                _fFinalDmg = playerStat.m_fAttackDmg * _damageInfo.DamageValue;

                AddPlayerHP(-fFinalDmg);*/

                //Debug.LogError("{0} - HP {1}".SetFormat(_playerInfo.PlayerData.m_szName, _playerInfo.m_fHP.Value));
            }
        }
        #endregion Damage Process

        #region Control
        public void Attack()
        {
            if (!GameManager.Instance.IsPlaying || GameManager.Instance.IsPaused)
                return;

            if (IsFever || IsDead)
                return;
            
            if (_currentState != null)
                _currentState.Attack(this);
        }

        public void Fever()
        {
            bool bImpossible = false;

            if (!GameManager.Instance.IsPlaying || GameManager.Instance.IsPaused)
                bImpossible = true;

            if (IsFever || IsDead)
                bImpossible = true;

            if (_playerInfo.CurFever < _playerInfo.PlayerData.m_fMaxFever)
                bImpossible = true;

            if (bImpossible)
            {
                // Test Mode. 
                AddPlayerFever(20f);

                SoundManager.PlaySFX("Impossible");
                return;
            }

            if (_playerSkill != null)
                _playerSkill.StartSkill();
        }
        #endregion Control

        #region State
        /// <summary>
        /// 애니메이션 상태 체크. 
        /// </summary>
        private IEnumerator _CheckAnimationState()
        {
            if (_playerObject.cachedAnimator == null)
                yield break;

            while (true)
            {
                yield return null;

                //if (_reservedState != null && !_playerObject.cachedAnimator.IsInTransition(0))
                if (_reservedState != null)
                {
                    if (_currentState != null)
                        _currentState.End(this);

                    _currentState = _reservedState;
                    _ePlayerState = _currentState.GetCurrentState();
                    _reservedState = null;
                    _currentState.Start(this);
                }

                if (_playerObject.cachedModelObject.activeInHierarchy)
                {
                    if (_playerObject.cachedAnimator.parameters.Length > 0)
                        _playerObject.cachedAnimator.SetInteger("PlayerState", (int)_ePlayerState);
                }
            }
        }

        /// <summary>
        /// 플레이어 상태 설정 예약. 
        /// </summary>
        public void SetPlayerState(IPlayerState nextState)
        {
            _reservedState = nextState;
        }

        /// <summary>
        /// 플레이어 위치 설정. 
        /// </summary>
        public void SetPlayerSide(ENUM_PLAYER_SIDE eSide)
        {
            if (_ePlayerSide != eSide)
                _ePlayerSide = eSide;
        }
        #endregion State

        #region Character Control
        private void _UpdateCharacterAction(float deltaTime)
        {
            switch (_playerInfo.PlayerType)
            {
                case ENUM_PLAYER_TYPE.ENEMY:
                    {
                        if (_cachedEnemyData == null)
                            return;

                        if (cachedTransform.position.y >= 0f && CurrentStateType != ENUM_PLAYER_STATE.ATTACK && CurrentStateType != ENUM_PLAYER_STATE.IDLE)
                            Attack();
                    }
                    break;

                case ENUM_PLAYER_TYPE.NPC:
                    {
                        if (_cachedNpcData == null)
                            return;
                        
                        if (cachedTransform.position.y >= ProjectDefines.TOP_SCENE_IN_HEIGHT && !_isOutOfScreen)
                        {
                            _isOutOfScreen = true;

                            // NPC 생존시 피버 획득. 
                            if (_cachedNpcData.m_fAliveGetFever > 0f && !GameManager.Instance.IsFeverMode)
                                GameManager.Instance.UserPlayerCtrl.AddPlayerFever(_cachedNpcData.m_fAliveGetFever);
                        }

                        switch (_cachedNpcData.m_eNpcType)
                        {
                            case NpcData.ENUM_NPC_TYPE.SHERIFF:
                                {
                                    // 보안관일때 아이템 던져서 연료 회복. 
                                    if (_cachedNpcData.m_fAliveGetGas > 0f && !_isItemCreated)
                                    {
                                        if (cachedTransform.position.y >= 1.5f)
                                        {
                                            // 아이템 생성. 
                                            if (_objCreatedItem == null)
                                            {
                                                string szItemPath = null;
                                                if (CurrentSideType == ENUM_PLAYER_SIDE.LEFT)
                                                    szItemPath = ProjectDefines.PATH_ITEM_GAS_L;
                                                else if (CurrentSideType == ENUM_PLAYER_SIDE.RIGHT)
                                                    szItemPath = ProjectDefines.PATH_ITEM_GAS_R;

                                                //_objCreatedItem = PlayerManager.Instance.CreateObject(null, ENUM_OBJECT_TYPE.ITEM);

                                                _objCreatedItem = ObjectPoolManager.PopInstance(szItemPath);
                                                if (_objCreatedItem != null)
                                                    _objCreatedItem.cachedTransform.position = cachedTransform.position;
                                            }

                                            // 아이템 이동. 
                                            if (_objCreatedItem != null)
                                            {
                                                _objCreatedItem.cachedTransform.position = Vector2.MoveTowards(_objCreatedItem.cachedTransform.position, ProjectDefines.ITEM_DEST_POSITION, deltaTime * 5f);

                                                if (Vector2.Distance(_objCreatedItem.cachedTransform.position, ProjectDefines.ITEM_DEST_POSITION) < 0.1f)
                                                {
                                                    _isItemCreated = true;
                                                    GameManager.Instance.UserPlayerCtrl.AddPlayerHP(_cachedNpcData.m_fAliveGetGas);
                                                    ObjectPoolManager.PushInstance(_objCreatedItem);
                                                }
                                            }
                                        }
                                    }
                                }
                                break;
                        }
                    }
                    break;
            }
        }
        #endregion Character Control

        #region Event
        /*private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision == null)
                return;
            
            switch (_playerInfo.PlayerType)
            {
                case ENUM_PLAYER_TYPE.ENEMY:
                case ENUM_PLAYER_TYPE.NPC:
                    {
                        if (!GameManager.Instance.UserPlayerCtrl.IsAttacking)
                            return;

                        if (GameManager.Instance.UserPlayerCtrl.IsDead || IsDead)
                            return;

                        // 사용자 터치 공격. 
                        if (collision.gameObject.CompareTag(ProjectDefines.TAG_TOUCH_EFFECT))
                            AddPlayerHP(-GameManager.Instance.UserPlayerCtrl.PlayerInfo.PlayerData.m_fAttackDmg);
                    }
                    break;
            }
        }*/
        #endregion Event
    }
}