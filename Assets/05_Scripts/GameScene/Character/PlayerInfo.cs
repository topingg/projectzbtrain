﻿using UnityEngine;
using System;
using AvoEx.ObservableType;

namespace Topping
{
    public enum ENUM_PLAYER_TYPE
    {
        NONE,
        PLAYER,
        ENEMY,
        OBSTACLE,
        NPC,
    }

    [Serializable]
    public partial class PlayerInfo
    {
        #region Private Variables
        private int _UID = 0;
        private IPlayerData _playerData = null;
        private ENUM_PLAYER_TYPE _ePlayerType = ENUM_PLAYER_TYPE.NONE;
        private bool _isFriendly = false;

        [SerializeField]
        private Vector3 _velocity = Vector3.zero;

        private ObEncInt _iLevel = new ObEncInt(1);
        private ObEncFloat _fCurHP = new ObEncFloat();
        private ObEncFloat _fCurFever = new ObEncFloat();

        /// <summary>
        /// HP 자연 감소율. 
        /// </summary>
        private ObEncFloat _fHPReduceRate = new ObEncFloat();
        /// <summary>
        /// Fever 게이지 충전률. 
        /// </summary>
        private ObEncFloat _fFeverChargeRate = new ObEncFloat();
        private ObEncFloat _fFeverDuration = new ObEncFloat();

        private ObEncFloat _fMoveSpeed = new ObEncFloat();
        private ObEncFloat _fJumpSpeed = new ObEncFloat();

        // 캐릭터 순수 스탯. 
        private Stats _playerStat;
        // 버프에 의해서 더해지는 스탯. 
        private Stats _statBuffAdd;
        // 버프에 의해서 곱해지는 스탯. 
        private Stats _statBuffMul;
        #endregion Private Variables 

        #region Properties
        public int UID { get { return _UID; } }
        public IPlayerData PlayerData { get { return _playerData; } }
        public ENUM_PLAYER_TYPE PlayerType { get { return _ePlayerType; } }
        public bool IsFriendly { get { return _isFriendly; } }

        public Vector3 Velocity { get { return _velocity; } }

        // 기본 좌표들. 
        public float PositionX { get; private set; }
        public float PositionY { get; private set; }
        public float PositionZ { get; private set; }

        public int Level { get { return _iLevel.Value; } }
        public float CurHP { get { return _fCurHP.Value; } }
        public float CurFever { get { return _fCurFever.Value; } }

        /// <summary>
        /// HP 자연 감소율. 
        /// </summary>
        public float HPReduceRate { get { return _fHPReduceRate.Value; } }
        /// <summary>
        /// Fever 게이지 충전률. 
        /// </summary>
        public float FeverChargeRate { get { return _fFeverChargeRate.Value; } }
        public float FeverDuration { get { return _fFeverDuration.Value; } }

        public float MoveSpeed { get { return _fMoveSpeed.Value; } }
        public float JumpSpeed { get { return _fJumpSpeed.Value; } }

        public Stats PlayerStat { get { return _playerStat; } }
        #endregion Properties

        #region Player Info
        /// <summary>
        /// 생성자는 Private 이며 CreateCharcter() 메소드를 통해서만 생성이 가능하다.
        /// CharacterData 가 null 이 될 수 없다.
        /// </summary>
        private PlayerInfo(int iServerUID, IPlayerData charData)
        {
            if (charData == null)
                return;

            //_UID = iServerUID;
            _UID = charData.UID;
            _playerData = charData;

            InitStats();
        }

        /// <summary>
        /// 캐릭터 생성. 
        /// </summary>
        public static PlayerInfo CreatePlayer(IPlayerData playerData)
        {
            if (playerData == null)
                return null;

            PlayerInfo newPlayer = new PlayerInfo(0, playerData);

            if (playerData.GetType().Equals(typeof(PlayerData)))
            {
                newPlayer._ePlayerType = ENUM_PLAYER_TYPE.PLAYER;
                newPlayer._isFriendly = true;
            }
            else if (playerData.GetType().Equals(typeof(EnemyData)))
            {
                newPlayer._ePlayerType = ENUM_PLAYER_TYPE.ENEMY;
                newPlayer._isFriendly = false;
            }
            else if (playerData.GetType().Equals(typeof(NpcData)))
            {
                newPlayer._ePlayerType = ENUM_PLAYER_TYPE.NPC;
                newPlayer._isFriendly = true;
            }

            return newPlayer;
        }

        public void SetVelocity(Vector3 velocity = default(Vector3))
        {
            if (velocity == default(Vector3))
                _velocity = Vector3.zero;
            else
                _velocity = velocity;
        }

        public void SetPositionX(float fValue) { PositionX = fValue; }
        public void SetPositionY(float fValue) { PositionY = fValue; }
        public void SetPositionZ(float fValue) { PositionZ = fValue; }

        public void SetHP(float fValue) { _fCurHP.Value = fValue; }
        public void AddHP(float fValue) { _fCurHP.Value += fValue; }

        public void SetFever(float fValue) { _fCurFever.Value = fValue; }
        public void AddFever(float fValue) { _fCurFever.Value += fValue; }

        /// <summary>
        /// 캐릭터 생성. 
        /// </summary>
        public static PlayerInfo CreatePlayer<TKey, TValue>(TKey iPlayerId) where TValue : TableData<TKey, TValue>
        {
            TValue playerData = TableData<TKey, TValue>.GetTableData(iPlayerId);
            if (playerData == null)
            {
                Debug.LogError("Error!! PlayerData is null. ID : " + iPlayerId);
            }
            return CreatePlayer((IPlayerData)playerData);
        }

        /// <summary>
        /// 스탯 초기화 및 갱신. 
        /// </summary>
        public void InitStats()
        {
            _playerStat.m_fHP = _playerData.m_fMaxHP;
            _playerStat.m_fFever = 0f;
            _playerStat.m_fAttackRate = 1f;
            _playerStat.m_fDefenseRate = 0f;

            _playerStat.GrowStats(0, _iLevel);

            _RefreshPlayerStats();
        }

        /// <summary>
        /// 최종 스탯을 계산하여 캐릭터 데이터에 입력. 
        /// </summary>
        private void _RefreshPlayerStats()
        {
            Stats finalStats = (_playerStat * (Stats.One + _statBuffMul)) + _statBuffAdd;

            _fCurHP.Value = finalStats.m_fHP;
            _fCurFever.Value = finalStats.m_fFever;
        }

        /// <summary>
        /// 캐릭터 정렬. 
        /// </summary>
        public static int Compare(PlayerInfo lhs, PlayerInfo rhs)
        {
            // 1. 진화 단계.
            /*if (lhs.m_iEvolution > rhs.m_iEvolution)
                return -1;
            else if (lhs.m_iEvolution < rhs.m_iEvolution)
                return 1;

            // 2. 강화 단계.
            if (lhs.m_iUpgrade > rhs.m_iUpgrade)
                return -1;
            else if (lhs.m_iUpgrade < rhs.m_iUpgrade)
                return 1;

            // 3. 경험치.
            if (lhs.m_iExp > rhs.m_iExp)
                return -1;
            else if (lhs.m_iExp < rhs.m_iExp)
                return 1;

            // 4. 우선순위.
            if (lhs.charID < rhs.charID) // 테이블에 낮은 번호가 먼저 노출.
                return -1;
            else if (lhs.charID > rhs.charID)
                return 1;*/

            return 0;
        }
        #endregion Player Info

        #region Stats
        public struct Stats
        {
            public float m_fHP;
            public float m_fFever;
            public float m_fAttackRate;
            public float m_fDefenseRate;

            private static Stats _statZero;
            private static Stats _statOne;

            public static Stats Zero { get { return _statZero; } }
            public static Stats One { get { return _statOne; } }

            static Stats()
            {
                _statZero._Make(0f);
                _statOne._Make(1f);
            }

            private void _Make(float fValue)
            {
                m_fHP = fValue;
                m_fFever = fValue;
                m_fAttackRate = fValue;
                m_fDefenseRate = fValue;
            }

            #region Operator
            public static Stats operator +(Stats lhs, Stats rhs)
            {
                Stats result = new Stats()
                {
                    m_fHP = lhs.m_fHP + rhs.m_fHP,
                    m_fFever = lhs.m_fFever + rhs.m_fFever,
                    m_fAttackRate = lhs.m_fAttackRate + rhs.m_fAttackRate,
                    m_fDefenseRate = lhs.m_fDefenseRate + rhs.m_fDefenseRate,
                };

                return result;
            }

            public static Stats operator -(Stats lhs, Stats rhs)
            {
                Stats result = new Stats()
                {
                    m_fHP = lhs.m_fHP - rhs.m_fHP,
                    m_fFever = lhs.m_fFever - rhs.m_fFever,
                    m_fAttackRate = lhs.m_fAttackRate - rhs.m_fAttackRate,
                    m_fDefenseRate = lhs.m_fDefenseRate - rhs.m_fDefenseRate,
                };

                return result;
            }

            public static Stats operator *(Stats lhs, Stats rhs)
            {
                Stats result = new Stats()
                {
                    m_fHP = lhs.m_fHP * rhs.m_fHP,
                    m_fFever = lhs.m_fFever * rhs.m_fFever,
                    m_fAttackRate = lhs.m_fAttackRate * rhs.m_fAttackRate,
                    m_fDefenseRate = lhs.m_fDefenseRate * rhs.m_fDefenseRate,
                };

                return result;
            }
            #endregion Operator

            public void GrowStats(int iGrowDataID, int iLevel)
            {
                // Level 에 따른 스탯 증가량 산출. 
            }
        }
        #endregion Stats

        #region Event
        public void AddObservHP(Observable<ObEncFloat>.EventChanged callback, bool bCallbackNow = false)
        {
            _fCurHP.AddObserver(callback, bCallbackNow);
        }

        public void DelObservHP(Observable<ObEncFloat>.EventChanged callback)
        {
            _fCurHP.DelObserver(callback);
        }

        public void AddObservFever(Observable<ObEncFloat>.EventChanged callback, bool bCallbackNow = false)
        {
            _fCurFever.AddObserver(callback, bCallbackNow);
        }

        public void DelObservFever(Observable<ObEncFloat>.EventChanged callback)
        {
            _fCurFever.DelObserver(callback);
        }
        #endregion Event
    }
}