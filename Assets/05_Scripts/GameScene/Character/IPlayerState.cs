﻿using UnityEngine;

namespace Topping
{
    public enum ENUM_PLAYER_STATE
    {
        NONE        = 0,

        IDLE        = 1,
        RUN         = 2,
        ATTACK      = 3,
        HIT         = 4,
        DIE         = 5,

        FEVER       = 6,
    }

    public abstract class IPlayerState
    {
        public virtual void Update(PlayerController player, float deltaTime) { }

        // 상태 시작 & 끝. 
        public virtual void Start(PlayerController player) { }
        public virtual void End(PlayerController player) { }

        // 기본 동작. 
        public virtual void Idle(PlayerController player) { }
        public virtual void Run(PlayerController player) { }
        public virtual void Attack(PlayerController player) { }
        public virtual void Hit(PlayerController player) { }
        public virtual void Die(PlayerController player) { }

        // 특수 동작. 
        public virtual void Fever(PlayerController player) { }

        public abstract ENUM_PLAYER_STATE GetCurrentState();
    }
}
