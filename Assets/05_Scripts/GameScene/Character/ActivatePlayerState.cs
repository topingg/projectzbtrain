﻿using UnityEngine;

namespace Topping
{
    public class ActivatePlayerState : MonoBehaviour
    {
        /// <summary>
        /// 참조 대상. 
        /// </summary>
        public PlayerController m_targetController = null;
        /// <summary>
        /// 참조할 상태. 
        /// </summary>
        public ENUM_PLAYER_STATE m_targetState = ENUM_PLAYER_STATE.NONE;
        /// <summary>
        /// 선택한 상태가 참일 경우 적용 값. 
        /// </summary>
        public bool m_bTargetActivate = false;

        /// <summary>
        /// 적용할 대상. 
        /// </summary>
        public GameObject m_targetObject = null;

        private void LateUpdate()
        {
            if (m_targetController == null)
                return;

            switch (m_targetState)
            {
                case ENUM_PLAYER_STATE.DIE:
                    {
                        if (m_targetObject == null)
                            _Destroy();

                        if (m_targetController.IsDead)
                            m_targetObject.SetActivate(m_bTargetActivate);
                        else
                            m_targetObject.SetActivate(!m_bTargetActivate);
                    }
                    break;
            }
        }

        private void _Destroy()
        {
            Destroy(this);
        }
    }
}