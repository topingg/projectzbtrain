﻿using UnityEngine;

namespace Topping
{
    public enum ENUM_PLAYER_CONDITION
    {
        NORMAL = 0,

        INVINCIBLE = 1 << 1,
        PASS_THROUGH = 1 << 2,

        BURN = 1 << 10,
    }

    public partial class PlayerInfo
    {
        #region Private Variables
        private ENUM_PLAYER_CONDITION _eCondition = ENUM_PLAYER_CONDITION.NORMAL;
        #endregion Private Variables

        #region Properties
        public ENUM_PLAYER_CONDITION Condition { get { return _eCondition; } }
        #endregion

        #region Game Methods
        public bool IsUnbeatable()
        {
            return HasCondition(ENUM_PLAYER_CONDITION.INVINCIBLE);
        }

        public bool HasCondition(ENUM_PLAYER_CONDITION eCon)
        {
            return (_eCondition & eCon) == eCon;
        }

        public void AddCondition(ENUM_PLAYER_CONDITION eCon)
        {
            _eCondition = _eCondition | eCon;
        }

        public void RemoveCondition(ENUM_PLAYER_CONDITION eCon)
        {
            _eCondition = (ENUM_PLAYER_CONDITION)(_eCondition - (_eCondition & eCon));
        }
        #endregion Game Methods
    }
}