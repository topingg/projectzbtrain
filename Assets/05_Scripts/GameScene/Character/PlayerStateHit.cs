﻿using UnityEngine;
using System.Collections;

namespace Topping
{
    public class PlayerStateHit : IPlayerState
    {
        public override void Idle(PlayerController player)
        {
            player.SetPlayerState(PlayerStatePool.GetState(ENUM_PLAYER_STATE.IDLE));
        }

        public override void Run(PlayerController player)
        {
            player.SetPlayerState(PlayerStatePool.GetState(ENUM_PLAYER_STATE.RUN));
        }

        public override void Hit(PlayerController player)
        {
            player.SetPlayerState(PlayerStatePool.GetState(ENUM_PLAYER_STATE.HIT));
        }

        public override void Die(PlayerController player)
        {
            player.SetPlayerState(PlayerStatePool.GetState(ENUM_PLAYER_STATE.DIE));
        }

        public override void Fever(PlayerController player)
        {
            player.SetPlayerState(PlayerStatePool.GetState(ENUM_PLAYER_STATE.FEVER));
        }


        public override ENUM_PLAYER_STATE GetCurrentState()
        {
            return ENUM_PLAYER_STATE.HIT;
        }
    }
}