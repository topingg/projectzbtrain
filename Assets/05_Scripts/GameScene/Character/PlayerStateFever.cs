﻿using UnityEngine;
using System.Collections;

namespace Topping
{
    public class PlayerStateFever : IPlayerState
    {
        public override void Start(PlayerController player)
        {
            if (player.PlayerObject.cachedModelObject != null)
                player.PlayerObject.cachedModelObject.SetActivate(false);

            if (player.m_objFeverEffectModel != null)
            {
                player.m_objFeverEffectModel.SetActivate(false);
                player.m_objFeverEffectModel.SetActivate(true);
            }

            player.IsFever = true;
        }

        public override void End(PlayerController player)
        {
            if (player.PlayerObject.cachedModelObject != null)
                player.PlayerObject.cachedModelObject.SetActivate(true);

            if (player.m_objFeverEffectModel != null)
            {
                player.m_objFeverEffectModel.SetActivate(false);
                player.m_objFeverEffectModel.SetActivate(true);
            }

            // 피버 종료 당시 플레이어가 죽지 않았을 때만 실행. 
            if (!player.IsDead)
            {
                // 모든 적 죽이기. 
                PlayerManager.Instance.KillAllEnemys();
                // 시트 비우기. 
                GameManager.Instance.ClearTrainSeat();
            }

            // 피버 게이지 초기화. 
            player.PlayerInfo.SetFever(0f);

            SpawnFactory.s_bSpawning = false;
        }

        public override void Idle(PlayerController player)
        {
            player.SetPlayerState(PlayerStatePool.GetState(ENUM_PLAYER_STATE.IDLE));
        }


        public override ENUM_PLAYER_STATE GetCurrentState()
        {
            return ENUM_PLAYER_STATE.FEVER;
        }
    }
}