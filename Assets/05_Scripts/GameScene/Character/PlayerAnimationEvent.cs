﻿using UnityEngine;
using System.Collections;

namespace Topping
{
    public class PlayerAnimationEvent : MonoBehaviour
    {
        private PlayerController _userPlayer = null;
        private int _iCheckCnt = 100;

        private void Start()
        {
            StartCoroutine(_FindPlayer());
        }

        private IEnumerator _FindPlayer()
        {
            int iCnt = 0;
            while (_userPlayer == null)
            {
                yield return new WaitForSeconds(0.1f);

                if (_userPlayer == null)
                    _userPlayer = GameManager.Instance.UserPlayerCtrl;

                iCnt++;
                if (iCnt >= _iCheckCnt)
                    break;
            }

            //Debug.LogError("FindCount : " + iCnt);
        }

        public void AttackEnd()
        {
            if (_userPlayer != null)
                _userPlayer.CurrentState.Idle(_userPlayer);
        }
    }
}