﻿using UnityEngine;
using System.Collections;
using AvoEx;

namespace Topping
{
    public class PlayerStateDie : IPlayerState
    {
        public override void Start(PlayerController player)
        {
            string szDieEffect = player.PlayerInfo.PlayerData.m_szDieEffect;
            if (!szDieEffect.IsNull())
            {
                ManagedObject objEffect = ObjectPoolManager.PopInstance(szDieEffect);
                if (objEffect != null)
                {
                    objEffect.cachedTransform.position = player.cachedTransform.position;
                    ObjectPoolManager.PushInstance(objEffect, 1f);
                }
            }

            if (player.PlayerInfo.PlayerData.m_arrDieSound != null)
            {
                int iDieSoundCount = player.PlayerInfo.PlayerData.m_arrDieSound.Length;
                if (iDieSoundCount > 0)
                {
                    string szDieSound = player.PlayerInfo.PlayerData.m_arrDieSound[Random.Range(0, iDieSoundCount)];
                    if (!szDieSound.IsNull())
                        SoundManager.PlaySFX(szDieSound);
                }
            }

            player.PlayerInfo.SetVelocity();

            float fAddGas = 0f;
            float fAddFever = 0f;

            switch (player.PlayerInfo.PlayerType)
            {
                case ENUM_PLAYER_TYPE.ENEMY:
                    {
                        if (player.CachedEnemyData != null)
                        {
                            fAddGas = player.CachedEnemyData.m_fGetGas - player.CachedEnemyData.m_fLostGas;
                            fAddFever = player.CachedEnemyData.m_fGetFever - player.CachedEnemyData.m_fLostFever;
                        }
                    }
                    break;
                case ENUM_PLAYER_TYPE.NPC:
                    {
                        if (player.CachedNpcData != null)
                        {
                            fAddGas = player.CachedNpcData.m_fGetGas - player.CachedNpcData.m_fLostGas;
                            fAddFever = player.CachedNpcData.m_fGetFever - player.CachedNpcData.m_fLostFever;
                        }

                        // 죽으면 하강. 
                        player.PlayerInfo.SetVelocity(Vector3.down * StageManager.Instance.ScrollSpeed * 0.5f);

                        // 생성했던 아이템 디스폰. 
                        if (player.CachedCreatedItem != null)
                            ObjectPoolManager.PushInstance(player.CachedCreatedItem);

                        // UI 표시. 
                        GameManager.Instance.m_UIGameScene.ShowUIEffectFool();

                        // 카메라 흔들기. 
                        GameManager.Instance.m_shakeMainCam.Shake(0.2f, 0.3f);
                    }
                    break;
            }

            // 기름 변화. 
            if (fAddGas != 0f)
                GameManager.Instance.UserPlayerCtrl.AddPlayerHP(fAddGas);

            // 피버 변화. 
            if (fAddFever != 0f && !GameManager.Instance.IsFeverMode)
                GameManager.Instance.UserPlayerCtrl.AddPlayerFever(fAddFever);

            // 플레이어 오브젝트 디스폰. 
            if (player != GameManager.Instance.UserPlayerCtrl)
                ObjectPoolManager.PushInstance(player.PlayerObject as ManagedObject, 2f);
        }

        public override void Idle(PlayerController player)
        {
            player.SetPlayerState(PlayerStatePool.GetState(ENUM_PLAYER_STATE.IDLE));
        }

        public override void Run(PlayerController player)
        {
            player.SetPlayerState(PlayerStatePool.GetState(ENUM_PLAYER_STATE.RUN));
        }


        public override ENUM_PLAYER_STATE GetCurrentState()
        {
            return ENUM_PLAYER_STATE.DIE;
        }
    }
}