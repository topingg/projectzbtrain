﻿using UnityEngine;
using System.Collections;

namespace Topping
{
    public class PlayerStateRun : IPlayerState
    {
        public override void Start(PlayerController player)
        {
            if (player != null && player.PlayerObject != null && player.PlayerObject.cachedAnimator != null)
                player.PlayerObject.cachedAnimator.speed = StageManager.Instance.EnemyMoveSpeedRate;
        }

        public override void End(PlayerController player)
        {
            if (player != null && player.PlayerObject != null && player.PlayerObject.cachedAnimator != null)
                player.PlayerObject.cachedAnimator.speed = 1f;
        }


        public override void Attack(PlayerController player)
        {
            player.SetPlayerState(PlayerStatePool.GetState(ENUM_PLAYER_STATE.ATTACK));
        }

        public override void Idle(PlayerController player)
        {
            player.SetPlayerState(PlayerStatePool.GetState(ENUM_PLAYER_STATE.IDLE));
        }

        public override void Hit(PlayerController player)
        {
            player.SetPlayerState(PlayerStatePool.GetState(ENUM_PLAYER_STATE.HIT));
        }

        public override void Die(PlayerController player)
        {
            player.SetPlayerState(PlayerStatePool.GetState(ENUM_PLAYER_STATE.DIE));
        }

        public override void Fever(PlayerController player)
        {
            player.SetPlayerState(PlayerStatePool.GetState(ENUM_PLAYER_STATE.FEVER));
        }


        public override ENUM_PLAYER_STATE GetCurrentState()
        {
            return ENUM_PLAYER_STATE.RUN;
        }
    }
}