﻿using UnityEngine;
using System.Collections;

namespace Topping
{
    public class PlayerStateAttack : IPlayerState
    {
        public override void Update(PlayerController player, float deltaTime)
        {
            if (player == null)
                return;

            switch (player.PlayerInfo.PlayerType)
            {
                case ENUM_PLAYER_TYPE.ENEMY:
                    {
                        int iSeatIdx = GameManager.Instance.m_lstTrainSeat.Count;
                        if (iSeatIdx >= GameManager.Instance.m_arrTrainSeat.Length)
                            iSeatIdx = 4;

                        // 이동 속도를 멈추고, 지정된 자리로 이동. 
                        player.PlayerInfo.SetVelocity();
                        player.cachedTransform.position = Vector3.MoveTowards(player.cachedTransform.position, GameManager.Instance.m_arrTrainSeat[iSeatIdx].position, deltaTime * 10f);

                        // 기차 탑승. 
                        if (Vector2.Distance(player.cachedTransform.position, GameManager.Instance.m_arrTrainSeat[iSeatIdx].position) < 0.1f)
                        {
                            GameManager.Instance.SetTrainSeat(player);
                            Idle(player);
                        }
                    }
                    break;
            }
        }

        public override void Start(PlayerController player)
        {
            player.IsAttacking = true;

            player.PlayerObject.cachedAnimator.SetTrigger("IsAttack");

            // 공격 효과음 재생. 
            if (player.PlayerInfo.PlayerData.m_arrAttackSound != null)
            {
                int iAttackSoundCount = player.PlayerInfo.PlayerData.m_arrAttackSound.Length;
                if (iAttackSoundCount > 0)
                {
                    string szAttackSound = player.PlayerInfo.PlayerData.m_arrAttackSound[Random.Range(0, iAttackSoundCount)];
                    if (!szAttackSound.IsNull())
                        SoundManager.PlaySFX(szAttackSound);
                }
            }
        }

        public override void End(PlayerController player)
        {
            player.IsAttacking = false;
        }


        public override void Attack(PlayerController player)
        {
            player.SetPlayerState(PlayerStatePool.GetState(ENUM_PLAYER_STATE.ATTACK));
        }

        public override void Idle(PlayerController player)
        {
            player.SetPlayerState(PlayerStatePool.GetState(ENUM_PLAYER_STATE.IDLE));
        }

        public override void Run(PlayerController player)
        {
            player.SetPlayerState(PlayerStatePool.GetState(ENUM_PLAYER_STATE.RUN));
        }

        public override void Hit(PlayerController player)
        {
            player.SetPlayerState(PlayerStatePool.GetState(ENUM_PLAYER_STATE.HIT));
        }

        public override void Die(PlayerController player)
        {
            player.SetPlayerState(PlayerStatePool.GetState(ENUM_PLAYER_STATE.DIE));
        }

        public override void Fever(PlayerController player)
        {
            player.SetPlayerState(PlayerStatePool.GetState(ENUM_PLAYER_STATE.FEVER));
        }


        public override ENUM_PLAYER_STATE GetCurrentState()
        {
            return ENUM_PLAYER_STATE.ATTACK;
        }
    }
}