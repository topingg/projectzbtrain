﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using AvoEx;

namespace Topping
{
    public class PlayerManager : MonoSingleton<PlayerManager>
    {
        #region Private Variables
        private List<IPatternObject> _createdEnemyList = new List<IPatternObject>();
        private List<IPatternObject> _createdNpcList = new List<IPatternObject>();
        private List<IPatternObject> _createdObstacleList = new List<IPatternObject>();
        private List<IPatternObject> _createdItemList = new List<IPatternObject>();
        #endregion Private Variables

        #region Properties
        public List<IPatternObject> CreatedEnemyList { get { return _createdEnemyList; } }
        #endregion Properties

        #region Game Methods
        public void Init()
        {
            if (_createdEnemyList != null) _createdEnemyList.Clear(); else _createdEnemyList = new List<IPatternObject>();
            if (_createdNpcList != null) _createdNpcList.Clear(); else _createdNpcList = new List<IPatternObject>();
            if (_createdObstacleList != null) _createdObstacleList.Clear(); else _createdObstacleList = new List<IPatternObject>();
            if (_createdItemList != null) _createdItemList.Clear(); else _createdItemList = new List<IPatternObject>();

            ObjectPoolManager.PushAllInstaces();
        }

        /// <summary>
        /// 오브젝트 생성. 
        /// </summary>
        public IPatternObject CreateObject(IPlayerData playerData, ENUM_OBJECT_TYPE eType = ENUM_OBJECT_TYPE.NONE, Transform trParent = null)
        {
            string szPrefab = null;
            IPlayerData getPlayerData = playerData;

            if (szPrefab.IsNull() && getPlayerData != null)
                szPrefab = getPlayerData.m_szPrefab;

            if (szPrefab.IsNull())
                return null;

            IPatternObject instObject = ObjectPoolManager.PopInstance(szPrefab) as IPatternObject;
            if (instObject != null)
            {
                instObject.InitObject(trParent, getPlayerData);
                AddListObject(eType, instObject);
            }

            return instObject;
        }

        /// <summary>
        /// 생성 리스트에 담기. 
        /// </summary>
        private void AddListObject(ENUM_OBJECT_TYPE eType, IPatternObject pObject)
        {
            if (pObject == null)
                return;

            switch (eType)
            {
                case ENUM_OBJECT_TYPE.ENEMY:
                    {
                        if (_createdEnemyList.Contains(pObject))
                            return;
                        else
                            _createdEnemyList.Add(pObject);
                    }
                    break;

                case ENUM_OBJECT_TYPE.NPC:
                    {
                        if (_createdNpcList.Contains(pObject))
                            return;
                        else
                            _createdNpcList.Add(pObject);
                    }
                    break;

                case ENUM_OBJECT_TYPE.OBSTACLE:
                    {
                        if (_createdObstacleList.Contains(pObject))
                            return;
                        else
                            _createdObstacleList.Add(pObject);
                    }
                    break;

                case ENUM_OBJECT_TYPE.ITEM:
                    {
                        if (_createdItemList.Contains(pObject))
                            return;
                        else
                            _createdItemList.Add(pObject);
                    }
                    break;
            }
        }

        /// <summary>
        /// 화면상의 적을 모두 없앤다. (버프 장애물만 남김)
        /// </summary>
        public void KillAllEnemys(bool bOnScreen = false)
        {
            // 적 제거. 
            for (int i = 0, cnt = _createdEnemyList.Count; i < cnt; i++)
            {
                if (_createdEnemyList[i] != null && _createdEnemyList[i].playerController != null)
                {
                    // 화면 안에 들어온 적만 제거. 
                    if (bOnScreen && _createdEnemyList[i].trModelObject.position.y < ProjectDefines.BOT_SCENE_IN_HEIGHT)
                        continue;

                    _createdEnemyList[i].playerController.AddPlayerHP(-999999);
                }
            }
        }

        /// <summary>
        /// 오브젝트 제거. 
        /// </summary>
        public bool RemoveObject(IPatternObject target)
        {
            bool bResult = false;

            if (target != null)
            {
                if (!bResult && _createdEnemyList.Contains(target))
                {
                    _createdEnemyList.Remove(target);
                    bResult = true;
                }

                if (!bResult && _createdNpcList.Contains(target))
                {
                    _createdNpcList.Remove(target);
                    bResult = true;
                }

                if (!bResult && _createdObstacleList.Contains(target))
                {
                    _createdObstacleList.Remove(target);
                    bResult = true;
                }

                if (!bResult && _createdItemList.Contains(target))
                {
                    _createdItemList.Remove(target);
                    bResult = true;
                }
            }

            return bResult;
        }

        /// <summary>
        /// 생성된 모든 오브젝트 디스폰. 
        /// </summary>
        public void DespawnAllObject()
        {
            DespawnTypeObject(ENUM_OBJECT_TYPE.ENEMY);
            DespawnTypeObject(ENUM_OBJECT_TYPE.NPC);
            DespawnTypeObject(ENUM_OBJECT_TYPE.OBSTACLE);
            DespawnTypeObject(ENUM_OBJECT_TYPE.ITEM);
        }

        /// <summary>
        /// 타입별 오브젝트 디스폰. 
        /// </summary>
        public void DespawnTypeObject(ENUM_OBJECT_TYPE eType)
        {
            switch (eType)
            {
                case ENUM_OBJECT_TYPE.ENEMY:
                    {
                        int iCnt = _createdEnemyList.Count;
                        int iIdx = 0;

                        while (_createdEnemyList.Count > 0)
                        {
                            if (_createdEnemyList[0] == null)
                                _createdEnemyList.RemoveAt(0);
                            else
                                _createdEnemyList[0].DespawnObject();

                            // 무한루프 안전장치. 
                            iIdx++;
                            if (iIdx > iCnt) break;
                        }

                        _createdEnemyList.Clear();
                    }
                    break;

                case ENUM_OBJECT_TYPE.NPC:
                    {
                        int iCnt = _createdNpcList.Count;
                        int iIdx = 0;

                        while (_createdNpcList.Count > 0)
                        {
                            if (_createdNpcList[0] == null)
                                _createdNpcList.RemoveAt(0);
                            else
                            {
                                // 생성한 아이템이 있는지 확인. 
                                if (_createdNpcList[0].playerController.CachedCreatedItem != null)
                                    ObjectPoolManager.PushInstance(_createdNpcList[0].playerController.CachedCreatedItem);

                                _createdNpcList[0].DespawnObject();
                            }

                            // 무한루프 안전장치. 
                            iIdx++;
                            if (iIdx > iCnt) break;
                        }

                        _createdNpcList.Clear();
                    }
                    break;

                case ENUM_OBJECT_TYPE.OBSTACLE:
                    {
                        int iCnt = _createdObstacleList.Count;
                        int iIdx = 0;

                        while (_createdObstacleList.Count > 0)
                        {
                            if (_createdObstacleList[0] == null)
                                _createdObstacleList.RemoveAt(0);
                            else
                                _createdObstacleList[0].DespawnObject();

                            // 무한루프 안전장치. 
                            iIdx++;
                            if (iIdx > iCnt) break;
                        }

                        _createdObstacleList.Clear();
                    }
                    break;

                case ENUM_OBJECT_TYPE.ITEM:
                    {
                        int iCnt = _createdItemList.Count;
                        int iIdx = 0;

                        while (_createdItemList.Count > 0)
                        {
                            if (_createdItemList[0] == null)
                                _createdItemList.RemoveAt(0);
                            else
                                _createdItemList[0].DespawnObject();

                            // 무한루프 안전장치. 
                            iIdx++;
                            if (iIdx > iCnt) break;
                        }

                        _createdItemList.Clear();
                    }
                    break;
            }
        }

        /// <summary>
        /// 공격 가능한 타겟 조회. 
        /// </summary>
        public PlayerController GetAttackTarget(ENUM_PLAYER_SIDE eSide)
        {
            PlayerController target = null;

            // 적 리스트. 
            for (int i = 0, cnt = _createdEnemyList.Count; i < cnt; i++)
            {
                if (_createdEnemyList[i] == null)
                    continue;

                PlayerController player = _createdEnemyList[i].playerController;

                if (player == null || player.IsDead)
                    continue;

                if (player.CurrentSideType != eSide)
                    continue;

                if (Mathf.Abs(ProjectDefines.ATTACK_POSITION.y - player.cachedTransform.position.y) < ProjectDefines.ATTACK_RANGE)
                {
                    if (target == null)
                        target = player;
                    else
                    {
                        if (target.cachedTransform.position.y < player.cachedTransform.position.y)
                            target = player;
                    }
                }
            }

            // NPC 리스트. (피버모드에는 조회하지 않음)
            if (!GameManager.Instance.IsFeverMode)
            {
                for (int i = 0, cnt = _createdNpcList.Count; i < cnt; i++)
                {
                    if (_createdNpcList[i] == null)
                        continue;

                    PlayerController player = _createdNpcList[i].playerController;

                    if (player == null || player.IsDead)
                        continue;

                    if (player.CurrentSideType != eSide)
                        continue;

                    // NPC 는 타격 판정에 약간의 여유를 둔다. 
                    if (player.cachedTransform.position.y > 0f)
                        continue;

                    if (Mathf.Abs(ProjectDefines.ATTACK_POSITION.y - player.cachedTransform.position.y) < ProjectDefines.ATTACK_RANGE)
                    {
                        if (target == null)
                            target = player;
                        else
                        {
                            if (target.cachedTransform.position.y < player.cachedTransform.position.y)
                                target = player;
                        }
                    }
                }
            }

            return target;
        }

        /// <summary>
        /// 피버 공격 타겟 조회. 
        /// </summary>
        public PlayerController GetFeverTarget(float fSearchHeight = 0)
        {
            PlayerController target = null;

            // 시트에 타있는 적 조회. 
            for (int i = 0, cnt = GameManager.Instance.m_lstTrainSeat.Count; i < cnt; i++)
            {
                PlayerController player = GameManager.Instance.m_lstTrainSeat[i];
                if (player != null && !player.IsDead && player.cachedTransform.position.y > ProjectDefines.BOT_SCENE_IN_HEIGHT)
                {
                    target = player;
                    break;
                }
            }

            // 시트 조회가 다 끝난 후 조회. 
            if (target == null)
            {
                // 적 리스트. 
                for (int i = 0, cnt = _createdEnemyList.Count; i < cnt; i++)
                {
                    if (_createdEnemyList[i] == null)
                        continue;

                    if (!((PatternObject)_createdEnemyList[i]).isActivated)
                        continue;

                    PlayerController player = _createdEnemyList[i].playerController;
                    if (player != null && !player.IsDead && player.cachedTransform.position.y > ProjectDefines.BOT_SCENE_IN_HEIGHT + fSearchHeight)
                    {
                        target = player;
                        break;
                    }
                }
            }

            return target;
        }
        #endregion Game Methods
    }
}