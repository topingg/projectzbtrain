﻿using UnityEngine;
using System.Collections.Generic;
using AvoEx;

namespace Topping
{
    public class PlayerStatePool : MonoSingleton<PlayerStatePool>
    {
        public Dictionary<int, IPlayerState> PlayerStateDic;

        PlayerStatePool()
        {
            if (PlayerStateDic == null)
                PlayerStateDic = new Dictionary<int, IPlayerState>(10);
            else
                PlayerStateDic.Clear();

            PlayerStateDic.Add((int)ENUM_PLAYER_STATE.ATTACK, new PlayerStateAttack());
            PlayerStateDic.Add((int)ENUM_PLAYER_STATE.IDLE, new PlayerStateIdle());
            PlayerStateDic.Add((int)ENUM_PLAYER_STATE.RUN, new PlayerStateRun());
            PlayerStateDic.Add((int)ENUM_PLAYER_STATE.HIT, new PlayerStateHit());
            PlayerStateDic.Add((int)ENUM_PLAYER_STATE.DIE, new PlayerStateDie());
            PlayerStateDic.Add((int)ENUM_PLAYER_STATE.FEVER, new PlayerStateFever());
        }

        public static void AddState(ENUM_PLAYER_STATE playerState, IPlayerState state)
        {
            Instance.PlayerStateDic.Add((int)playerState, state);
        }

        public static void UpdateState(ENUM_PLAYER_STATE playerState, IPlayerState state)
        {
            Instance.PlayerStateDic[(int)playerState] = state;
        }

        public static IPlayerState GetState(ENUM_PLAYER_STATE state)
        {
            try
            {
                return GetState((int)state);
            }
            catch
            {
                return null;
            }
        }

        public static IPlayerState GetState(int stateIdx)
        {
            try
            {
                return Instance.PlayerStateDic[stateIdx];
            }
            catch
            {
                return null;
            }
        }

    }
}