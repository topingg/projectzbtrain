﻿using UnityEngine;
using AvoEx;

namespace Topping
{
    public class ObjectShake : MonoBehaviour
    {
        public bool m_bStartShake = false;
        public float m_fShakeTime = 0.2f;
        public float m_fShakeAmount = 0.5f;
        public float m_fDecreaseFactor = 1f;

        private Transform _cachedTransform = null;
        private Vector3 _vOriginalPos = Vector3.zero;
        private float _fRemainTime = 0f;

        private void Awake()
        {
            _cachedTransform = transform;
            _vOriginalPos = _cachedTransform.localPosition;
        }

        private void Update()
        {
            if (!m_bStartShake)
                return;

            if (_fRemainTime == 0f)
                _fRemainTime = m_fShakeTime;

            if (_fRemainTime > 0f)
            {
                Vector3 vRandom = Random.insideUnitSphere;
                _cachedTransform.localPosition = _vOriginalPos + vRandom * m_fShakeAmount;
                _fRemainTime -= Time.unscaledDeltaTime * m_fDecreaseFactor;
            }
            else
            {
                _fRemainTime = 0f;
                _cachedTransform.localPosition = _vOriginalPos;
                m_bStartShake = false;
            }
        }
        
        public void Shake()
        {
            Shake(m_fShakeTime, m_fShakeAmount, m_fDecreaseFactor);
        }

        public void Shake(float fShakeTime)
        {
            Shake(fShakeTime, m_fShakeAmount, m_fDecreaseFactor);
        }

        public void Shake(float fShakeTime, float fShakeAmount)
        {
            Shake(fShakeTime, fShakeAmount, m_fDecreaseFactor);
        }

        public void Shake(float fShakeTime, float fShakeAmount, float fDecreaseFactor)
        {
            _fRemainTime = fShakeTime;
            m_fShakeAmount = fShakeAmount;
            m_fDecreaseFactor = fDecreaseFactor;
            m_bStartShake = true;
        }
    }
}