﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using cakeslice;

namespace Topping
{
    [RequireComponent(typeof(OutlineEffect))]
    public class OutlineController : MonoBehaviour
    {
        #region Public Variables
        public OutlineEffect m_compOutline;
        #endregion Public Variables

        #region Private Variables
        private Color _originColor0 = Color.black;
        private Color _originColor1 = Color.black;
        private Color _originColor2 = Color.black;

        private bool _bTween = false;
        private bool _bTweenShow = true;
        private float _fTweenSpeed = 1f;
        #endregion Private Variables

        #region Mono
        private void Start()
        {
            if (m_compOutline == null)
                m_compOutline = GetComponent<OutlineEffect>();

            if (m_compOutline != null)
            {
                _originColor0 = m_compOutline.lineColor0;
                _originColor1 = m_compOutline.lineColor1;
                _originColor2 = m_compOutline.lineColor2;
            }
        }

        private void Update()
        {
            if (!_bTween)
                return;

            if (_bTweenShow)
            {
                m_compOutline.lineColor0.a = Mathf.MoveTowards(m_compOutline.lineColor0.a, _originColor0.a, Time.deltaTime * _fTweenSpeed);
                m_compOutline.lineColor1.a = Mathf.MoveTowards(m_compOutline.lineColor1.a, _originColor1.a, Time.deltaTime * _fTweenSpeed);
                m_compOutline.lineColor2.a = Mathf.MoveTowards(m_compOutline.lineColor2.a, _originColor2.a, Time.deltaTime * _fTweenSpeed);
                m_compOutline.UpdateMaterialsPublicProperties();
            }
            else
            {
                m_compOutline.lineColor0.a = Mathf.MoveTowards(m_compOutline.lineColor0.a, 0f, Time.deltaTime * _fTweenSpeed);
                m_compOutline.lineColor1.a = Mathf.MoveTowards(m_compOutline.lineColor1.a, 0f, Time.deltaTime * _fTweenSpeed);
                m_compOutline.lineColor2.a = Mathf.MoveTowards(m_compOutline.lineColor2.a, 0f, Time.deltaTime * _fTweenSpeed);
                m_compOutline.UpdateMaterialsPublicProperties();

                if (m_compOutline.lineColor0.a == 0f)
                {
                    _StartTween(false);
                    ActiveOutline(false);
                }
            }
        }
        #endregion Mono

        #region Game Methods
        public void ActiveOutline(bool bActive)
        {
            if (m_compOutline == null)
                return;

            m_compOutline.enabled = bActive;

            m_compOutline.lineColor0.a = _originColor0.a;
            m_compOutline.lineColor1.a = _originColor1.a;
            m_compOutline.lineColor2.a = _originColor2.a;
            m_compOutline.UpdateMaterialsPublicProperties();
        }

        public void ShowOutlines(float fShowDuration = 1f)
        {
            if (fShowDuration <= 0f)
            {
                ActiveOutline(true);
                return;
            }

            if (_bTween)
                return;

            m_compOutline.lineColor0.a = 0f;
            m_compOutline.lineColor1.a = 0f;
            m_compOutline.lineColor2.a = 0f;
            m_compOutline.UpdateMaterialsPublicProperties();

            _fTweenSpeed = 1f / fShowDuration;
            _bTweenShow = true;

            ActiveOutline(true);
            _StartTween(true);
        }

        public void HideOutlines(float fHideDuration = 1f)
        {
            if (fHideDuration <= 0f)
            {
                ActiveOutline(false);
                return;
            }

            _fTweenSpeed = 1f / fHideDuration;
            _bTweenShow = false;

            _StartTween(true);
        }

        private void _StartTween(bool bStart)
        {
            _bTween = bStart;
        }
        #endregion Game Methods
    }
}