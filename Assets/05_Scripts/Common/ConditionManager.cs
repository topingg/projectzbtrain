﻿using System;
using UnityEngine;
using AvoEx;
using AvoEx.ObservableType;

namespace Topping
{
    public class ConditionManager : MonoSingleton<ConditionManager>
    {
        #region Enum
        public enum ENUM_UNLOCK_CONDITION
        {
            NONE                    = 0,        // 없음. 

            KILL_ZOMBIE             = 1,        // 좀비 킬 수. 
            KILL_ZOMBIE_ACCUM       = 2,        // 좀비 킬 수. (누적)

            DISTANCE                = 10,       // 최고 점수. 
            DISTANCE_ACCUM          = 11,       // 누적 점수. (누적)

            GAMEOVER                = 20,       // 게임 종료 횟수. 

            ATTEND                  = 30,       // 출석 횟수. 
            ATTEND_SUNDAY           = 31,       // 일요일 출석 횟수. 
            
            PLAY_TIME               = 40,       // 플레이 시간. (초)
            PLAY_TIME_ACCUM         = 41,       // 플레이 시간. (초, 누적)

            KILL_NPC                = 50,       // NPC 처치 수. 
            KILL_NPC_ACCUM          = 51,       // NPC 처치 수. (누적)

            ZOMBIE_ON_TRAIN         = 60,       // 열차에 탑승한 좀비 수. 
            ZOMBIE_ON_TRAIN_ACCUM   = 61,       // 열차에 탑승한 좀비 수. (누적)

            ATTACK_COUNT            = 70,       // 공격 터치 횟수. 
            ATTACK_COUNT_ACCUM      = 71,       // 공격 터치 횟수. (누적)
            FEVER_COUNT             = 72,       // 피버 사용 횟수. 
            FEVER_COUNT_ACCUM       = 73,       // 피버 사용 횟수. (누적)

            WATCH_AD_COUNT          = 80,       // 광고 시청 횟수. 

            TITLE_TOUCH             = 1000,     // 타이틀 이미지 터치 횟수. (히든)
        }
        #endregion Enum

        #region Private Variables
        private int _iZombieOnTrain = 0;
        private int _iAttackCount = 0;
        private int _iFeverCount = 0;
        #endregion Private Variables

        #region Mono
        private void OnEnable()
        {
            Init();
        }
        #endregion Mono

        #region Game Methods
        public void Init()
        {
            _iZombieOnTrain = 0;
            _iAttackCount = 0;
            _iFeverCount = 0;
        }

        public void AddZombieOnTrain(int iValue)
        {
            _iZombieOnTrain += iValue;
        }

        public void AddAttackCount(int iValue)
        {
            _iAttackCount += iValue;
        }

        public void AddFeverCount(int iValue)
        {
            _iFeverCount += iValue;
        }

        /// <summary>
        /// 현재까지 쌓인 스택을 UserData 에 적용. 
        /// </summary>
        public static void AdjustToUserData()
        {
            UserData.AddZombieOnTrain(Instance._iZombieOnTrain);
            UserData.AddAttackCount(Instance._iAttackCount);
            UserData.AddFeverCount(Instance._iFeverCount);
        }

        /// <summary>
        /// 게임 오버 프로세스. 
        /// </summary>
        public static void ProcessGameOver(int iPlayTime, bool bDataSave = true)
        {
            AdjustToUserData();

            // 게임 오버 카운트 증가. 
            UserData.AddGameOverCount(1);

            // 플레이 시간 저장. 
            UserData.AddPlayTime(iPlayTime);

            // 누적 데이터 적용. 
            UserData.AddAccumulationValue();

            if (bDataSave)
                UserData.Save();
        }

        /// <summary>
        /// 해금된 캐릭터가 있는지 확인. 
        /// </summary>
        public static void CheckUnlockCharacter()
        {
            bool bNeedSave = false;

            var itorData = ConditionData.dataDictionary.GetEnumerator();
            while (itorData.MoveNext())
            {
                if (itorData.Current.Value == null)
                    continue;

                int iUserAmount = GetConditionValue(itorData.Current.Value.m_eCondition);
                int iDestAmount = itorData.Current.Value.m_iAmount;
                int iCharId = itorData.Current.Value.m_iCharId;

                // 조건을 충족했다면 캐릭터 추가. 
                if (iUserAmount >= iDestAmount)
                {
                    CharacterUnlock(iCharId);
                    bNeedSave = true;
                }
            }

            if (bNeedSave)
            {
                UserData.Save();
                UITopManager.ShowNoti();
            }
        }

        /// <summary>
        /// 캐릭터 언락. 
        /// </summary>
        public static bool CharacterUnlock(int iCharId, bool bSave = false, bool bAddNoti = true, bool bShowNoti = false)
        {
            bool bResult = false;

            // 캐릭터 추가. 
            if (UserData.AddCharacter(iCharId, bSave))
            {
                // 첫 캐릭터 획득. 
                if (UserData.ListUnLockCharCount < 4)
                    GPGSManager.SendUnlockAchieve(GPGSManager.ENUM_GPGS_TYPE.ACHIEVE_NICE_TO_MEETYOU);

                GPGSManager.SendEvent(GPGSManager.ENUM_GPGS_TYPE.EVENT_GET_CHAR, 1);

                // 알림 추가. 
                if (bAddNoti)
                {
                    PlayerData pData = PlayerData.GetTableData(iCharId);
                    if (pData != null)
                    {
                        // 알림 설정. 
                        UITopManager.SetNoti(string.Format("New Character Unlocked\n\"{0}\"", pData.m_szName), pData.m_szIcon);
                    }
                }

                // 알림 표시. 
                if (bShowNoti) UITopManager.ShowNoti();

                bResult = true;
            }

            return bResult;
        }

        /// <summary>
        /// 해금 조건 타입별 현재값 조회. 
        /// </summary>
        public static int GetConditionValue(ENUM_UNLOCK_CONDITION eType)
        {
            int returnValue = 0;
            switch (eType)
            {
                case ENUM_UNLOCK_CONDITION.KILL_ZOMBIE:
                    returnValue = UserData.KillCount;
                    break;

                case ENUM_UNLOCK_CONDITION.KILL_ZOMBIE_ACCUM:
                    returnValue = Convert.ToInt32(UserData.KillCountAccum);
                    break;

                case ENUM_UNLOCK_CONDITION.DISTANCE:
                    returnValue = Mathf.FloorToInt(UserData.BestScore);
                    break;
                case ENUM_UNLOCK_CONDITION.DISTANCE_ACCUM:
                    returnValue = Convert.ToInt32(UserData.ScoreAccum);
                    break;

                case ENUM_UNLOCK_CONDITION.GAMEOVER:
                    returnValue = UserData.GameOverCount;
                    break;

                case ENUM_UNLOCK_CONDITION.ATTEND:
                    returnValue = UserData.AttendCount;
                    break;
                case ENUM_UNLOCK_CONDITION.ATTEND_SUNDAY:
                    returnValue = UserData.AttendSundayCount;
                    break;

                case ENUM_UNLOCK_CONDITION.PLAY_TIME:
                    returnValue = Convert.ToInt32(UserData.PlayTime);
                    break;
                case ENUM_UNLOCK_CONDITION.PLAY_TIME_ACCUM:
                    returnValue = Convert.ToInt32(UserData.PlayTimeAccum);
                    break;

                case ENUM_UNLOCK_CONDITION.KILL_NPC:
                    returnValue = UserData.KillNpcCount;
                    break;
                case ENUM_UNLOCK_CONDITION.KILL_NPC_ACCUM:
                    returnValue = Convert.ToInt32(UserData.KillNpcCountAccum);
                    break;

                case ENUM_UNLOCK_CONDITION.ZOMBIE_ON_TRAIN:
                    returnValue = UserData.ZombieOnTrain;
                    break;
                case ENUM_UNLOCK_CONDITION.ZOMBIE_ON_TRAIN_ACCUM:
                    returnValue = Convert.ToInt32(UserData.ZombieOnTrainAccum);
                    break;

                case ENUM_UNLOCK_CONDITION.ATTACK_COUNT:
                    returnValue = UserData.AttackCount;
                    break;
                case ENUM_UNLOCK_CONDITION.ATTACK_COUNT_ACCUM:
                    returnValue = Convert.ToInt32(UserData.AttackCountAccum);
                    break;
                case ENUM_UNLOCK_CONDITION.FEVER_COUNT:
                    returnValue = UserData.FeverCount;
                    break;
                case ENUM_UNLOCK_CONDITION.FEVER_COUNT_ACCUM:
                    returnValue = Convert.ToInt32(UserData.FeverCountAccum);
                    break;

                case ENUM_UNLOCK_CONDITION.WATCH_AD_COUNT:
                    returnValue = UserData.WatchAdCount;
                    break;
            }

            return returnValue;
        }
        #endregion Game Methods
    }
}