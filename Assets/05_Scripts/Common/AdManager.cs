﻿using UnityEngine;
using UnityEngine.Advertisements;
using System;
using System.Collections.Generic;
using AvoEx;

namespace Topping
{
    public class AdManager : MonoSingleton<AdManager>
    {
        #region Delegates
        public static Action<bool> OnAdFinished;
        #endregion Delegates

        #region Const
        private static readonly string UNITY_ADS_GAME_ID = "1689574";
        private static readonly string UNITY_ADS_PLACEMENT_REVIVE = "reviveMenu";
        private static readonly string UNITY_ADS_PLACEMENT_RESULT = "resultMenu";

        private static readonly string VUNGLE_ADS_APP_ID = "5a72ce3c6d797c6b710045bd";
        private static readonly string VUNGLE_ADS_PLACEMENT_REVIVE = "REVIVEM79101";
        private static readonly string VUNGLE_ADS_PLACEMENT_RESULT = "RESULTM38063";

        public static readonly string TAPJOY_ADS_SDK_KEY = "b3mvjRqySoi05uqa-Ht4FAECFZHHGzlpJmvNbvEHjFNT9lCU2ArTDf3unkDL";
        public static readonly string TAPJOY_ADS_PLACEMENT_REVIVE = "ReviveMenu";
        public static readonly string TAPJOY_ADS_PLACEMENT_RESULT = "ResultMenu";
        #endregion Const

        #region Enums
        public enum ENUM_AD_TYPE
        {
            NONE,

            UNITY_ADS,
            VUNGLE,
            //TAPJOY,

            END,
        }

        public enum ENUM_AD_PLACEMENT
        {
            NONE,

            REVIVE_MENU,
            RESULT_MENU,
        }
        #endregion Enums

        #region Private Variables
        private ENUM_AD_TYPE _eFirstAdType = ENUM_AD_TYPE.NONE;

        private ENUM_AD_TYPE _eCurAdType = ENUM_AD_TYPE.NONE;
        private ENUM_AD_PLACEMENT _eCurPlacement = ENUM_AD_PLACEMENT.NONE;

        private bool _isInitializedVungle = false;
        #endregion Private Variables

        #region Properties
        public bool IsVungleShowing { get; set; }
        public bool IsTapJoyShowing { get; set; }

        public bool IsAdShowing
        {
            get
            {
                return Advertisement.isShowing || IsVungleShowing || IsTapJoyShowing;
            }
        }
        #endregion Properties

        #region Mono
        private void OnEnable()
        {
            _isInitializedVungle = false;
            Vungle.onInitializeEvent += _OnInitVungle;
            Vungle.adPlayableEvent += _OnVungleAdsLoaded;
            Vungle.onAdStartedEvent += _OnVungleAdsStarted;
            Vungle.onAdFinishedEvent += _OnVungleAdsResult;

            InitUnityAds();
            InitVungleAds();
            TapjoyManager.Instance.Init();
        }

        private void OnDisable()
        {
            Vungle.onInitializeEvent -= _OnInitVungle;
            Vungle.adPlayableEvent -= _OnVungleAdsLoaded;
            Vungle.onAdStartedEvent -= _OnVungleAdsStarted;
            Vungle.onAdFinishedEvent -= _OnVungleAdsResult;
        }
        #endregion Mono

        #region Game Methods
        // Unity Ads. 
        public void InitUnityAds()
        {
            if (!Advertisement.isInitialized)
                Advertisement.Initialize(UNITY_ADS_GAME_ID);
        }

        private void _OnUnityAdsResult(ShowResult showResult)
        {
            bool bResult = false;

            switch (showResult)
            {
                case ShowResult.Finished:
                    bResult = true;
                    break;

                case ShowResult.Skipped:
                case ShowResult.Failed:
                    bResult = false;
                    break;
            }

            OnFinishAd(bResult);
        }


        // Vungle. 
        public void InitVungleAds()
        {
            Vungle.init(VUNGLE_ADS_APP_ID);
        }

        private void _OnInitVungle()
        {
            _isInitializedVungle = true;
            Vungle.loadAd(VUNGLE_ADS_PLACEMENT_REVIVE);
            Vungle.loadAd(VUNGLE_ADS_PLACEMENT_RESULT);
        }

        private void _OnVungleAdsLoaded(string szPlacementId, bool bPlayable)
        {
            /*if (bPlayable && Vungle.isAdvertAvailable(szPlacementId))
            {
                Vungle.playAd(szPlacementId);
            }
            else
            {
                // 다음 광고 재생. 
                ShowAds(false, _GetNextAdType(_eCurAdType), _eCurPlacement);
            }*/
        }

        private void _OnVungleAdsStarted(string szPlacementId)
        {
            IsVungleShowing = true;
        }

        private void _OnVungleAdsResult(string szPlacementId, AdFinishedEventArgs result)
        {
            IsVungleShowing = false;

            bool bResult = false;

            if (result.IsCompletedView)
            {
                bResult = true;
            }
            else if (result.WasCallToActionClicked)
            {
                bResult = false;
            }

            OnFinishAd(bResult);
        }


        /// <summary>
        /// 광고 재생. 
        /// </summary>
        public void ShowAds(bool isFirstStep = true, ENUM_AD_TYPE eAdType = ENUM_AD_TYPE.NONE, ENUM_AD_PLACEMENT ePlaceType = ENUM_AD_PLACEMENT.REVIVE_MENU)
        {
            SoundManager.Pause();

            if (IsAdShowing)
            {
                Debug.LogError("Ad is showing..");

                OnFinishAd(false);
                return;
            }

            if (isFirstStep)
            {
                _eFirstAdType = ENUM_AD_TYPE.NONE;
            }
            else
            {
                if (_eFirstAdType == eAdType)
                {
                    Debug.LogError("Prepared Ad is nothing..");

                    OnFinishAd(false);
                    return;
                }
            }

            ENUM_AD_TYPE prepareAd = ENUM_AD_TYPE.UNITY_ADS;
            string szPlacement = null;

            // 광고 우선 설정. 
            if (eAdType != ENUM_AD_TYPE.NONE)
                prepareAd = eAdType;
            else
                prepareAd = (ENUM_AD_TYPE)1;

            _eCurAdType = prepareAd;
            _eCurPlacement = ePlaceType;

            switch (prepareAd)
            {
                case ENUM_AD_TYPE.UNITY_ADS:
                    {
                        if (!Advertisement.isInitialized)
                            InitUnityAds();

                        if (_eCurPlacement == ENUM_AD_PLACEMENT.REVIVE_MENU)
                            szPlacement = UNITY_ADS_PLACEMENT_REVIVE;
                        else if (_eCurPlacement == ENUM_AD_PLACEMENT.RESULT_MENU)
                            szPlacement = UNITY_ADS_PLACEMENT_RESULT;

                        if (Advertisement.IsReady(szPlacement))
                        {
                            ShowOptions adOption = new ShowOptions();
                            adOption.resultCallback = _OnUnityAdsResult;

                            Advertisement.Show(szPlacement, adOption);
                        }
                        else
                        {
                            ShowAds(false, _GetNextAdType(_eCurAdType), _eCurPlacement);
                        }
                    }
                    break;

                case ENUM_AD_TYPE.VUNGLE:
                    {
                        if (!_isInitializedVungle)
                            InitVungleAds();

                        if (_eCurPlacement == ENUM_AD_PLACEMENT.REVIVE_MENU)
                            szPlacement = VUNGLE_ADS_PLACEMENT_REVIVE;
                        else if (_eCurPlacement == ENUM_AD_PLACEMENT.RESULT_MENU)
                            szPlacement = VUNGLE_ADS_PLACEMENT_RESULT;

                        if (Vungle.isAdvertAvailable(szPlacement))
                        {
                            Vungle.playAd(szPlacement);
                        }
                        else
                        {
                            Vungle.loadAd(szPlacement);
                            ShowAds(false, _GetNextAdType(_eCurAdType), _eCurPlacement);
                        }
                    }
                    break;

                /*case ENUM_AD_TYPE.TAPJOY:
                    {
                        if (_eCurPlacement == ENUM_AD_PLACEMENT.REVIVE_MENU)
                            szPlacement = TAPJOY_ADS_PLACEMENT_REVIVE;
                        else if (_eCurPlacement == ENUM_AD_PLACEMENT.RESULT_MENU)
                            szPlacement = TAPJOY_ADS_PLACEMENT_RESULT;

                        if (TapjoyManager.Instance.IsContentReady(szPlacement))
                        {
                            TapjoyManager.Instance.ShowPlacement(szPlacement);
                        }
                        else
                        {
                            ShowAds(false, _GetNextAdType(_eCurAdType), _eCurPlacement);
                        }
                    }
                    break;*/
            }
        }

        private ENUM_AD_TYPE _GetNextAdType(ENUM_AD_TYPE curType)
        {
            if (_eFirstAdType != ENUM_AD_TYPE.NONE)
                _eFirstAdType = curType;

            ENUM_AD_TYPE result = ENUM_AD_TYPE.NONE;

            if ((int)curType >= (int)ENUM_AD_TYPE.END - 1)
                result = (ENUM_AD_TYPE)1;
            else
                result = (ENUM_AD_TYPE)((int)curType + 1);

            return result;
        }

        public void OnFinishAd(bool bSuccess)
        {
            SoundManager.UnPause();

            if (OnAdFinished != null)
                OnAdFinished(bSuccess);
        }
        #endregion Game Methods
    }
}