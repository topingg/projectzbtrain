﻿using UnityEngine;
using System;
using System.Collections.Generic;
using AvoEx;

using Facebook.Unity;

namespace Topping
{
    public class FBManager : MonoSingleton<FBManager>
    {
        #region Enum
        private enum ENUM_LOGIN_ACTION
        {
            NONE,
            
            SHARE_LINK,
        }
        #endregion Enum

        #region Private Variables
        private List<string> _lstPerms = new List<string>() { "public_profile", "email", "user_friends" };
        private string _szShareLink = "https://play.google.com/apps/testing/com.topping.zbtrain";
        private string _szShareTitle = null;
        private string _szShareDesc = null;
        private ENUM_LOGIN_ACTION _eLoginAction = ENUM_LOGIN_ACTION.NONE;
        #endregion Private Variables

        #region Properties
        public bool IsInitialized { get { return FB.IsInitialized; } }
        public bool IsLogin { get { return FB.IsLoggedIn; } }

        public string Token
        {
            get
            {
                string szToken = null;
                if (IsLogin)
                {
                    szToken = AccessToken.CurrentAccessToken.TokenString;
                }
                return szToken;
            }
        }

        public string UserID
        {
            get
            {
                string szId = null;
                if (IsLogin)
                {
                    szId = AccessToken.CurrentAccessToken.UserId;
                }
                return szId;
            }
        }
        #endregion Properties

        #region Game Methods
        public void InitFB()
        {
            if (!IsInitialized)
                FB.Init(_InitCallback);
            else
                FB.ActivateApp();
       }

        private void _InitCallback()
        {
            if (IsInitialized)
                FB.ActivateApp();
            else
                UITopManager.ShowToast("SORRY. PLEASE TRY LATER");
        }

        public void Login()
        {
            if (!IsInitialized)
                InitFB();

            if (!IsLogin)
                FB.LogInWithReadPermissions(_lstPerms, _LoginCallback);
        }

        private void _LoginCallback(ILoginResult result)
        {
            if (IsLogin)
            {
                switch (_eLoginAction)
                {
                    case ENUM_LOGIN_ACTION.SHARE_LINK:
                        ShareToFacebook();
                        break;
                }

                /*AccessToken aToken = AccessToken.CurrentAccessToken;
                foreach (string perm in aToken.Permissions)
                {
                    Debug.Log(perm);
                }*/
            }
            else
            {
                UITopManager.ShowToast("CANNOT LOGIN TO FACEBOOK");
            }
        }

        public void LogOut()
        {
            if (IsLogin)
            {
                FB.LogOut();
                UITopManager.ShowToast("SUCCESSFULLY  LOGGED  OUT");
            }
        }

        public void SetShareContent(string szTitle, string szDesc)
        {
            _szShareTitle = szTitle;
            _szShareDesc = szDesc;
        }

        public void ShareToFacebook(string szTitle = null, string szDesc = null)
        {
            if (!szTitle.IsNull()) _szShareTitle = szTitle;
            if (!szDesc.IsNull()) _szShareDesc = szDesc;

            if (!IsLogin)
            {
                _eLoginAction = ENUM_LOGIN_ACTION.SHARE_LINK;
                Login();
                return;
            }

            UITopManager.SetActiveWaitCover(true, 10f);

            if (_szShareTitle.IsNull()) _szShareTitle = "Zombie Train";
            if (_szShareDesc.IsNull()) _szShareDesc = "Shoot the Zombies !!";

            FB.ShareLink(new Uri(_szShareLink), _szShareTitle, _szShareDesc, null, _ShareResult);
        }

        private void _ShareResult(IShareResult result)
        {
            UITopManager.SetActiveWaitCover(false);

            if (result.Cancelled)
            {
                UITopManager.OpenPopup("HAS  BEEN  CANCELED", "OK");
            }
            else if (!result.PostId.IsNull())
            {
                UITopManager.OpenPopup("SUCCESSFULLY\nSHARED", "OK");
            }
            else
            {
                UITopManager.OpenPopup("CAN  NOT  SHARE\nPLEASE TRY LATER", "OK");
            }
        }
        #endregion Game Methods
    }
}