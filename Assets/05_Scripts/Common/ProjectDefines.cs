﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Topping
{
    public class ProjectDefines
    {
        #region Layer
        public const int LAYER_DEFAULT = 0;
        #endregion Layer

        #region Tag
        public static readonly string TAG_TOUCH_EFFECT = "TouchEffect";
        #endregion Tag

        #region Scene Name
        public static readonly string BUNDLE_NAME_SCENE = "scenes";

        public static readonly string SCENE_NAME_INTRO = "IntroScene";
        public static readonly string SCENE_NAME_TITLE = "TitleScene";
        public static readonly string SCENE_NAME_GAME = "GameScene";
        public static readonly string SCENE_NAME_LOADING = "LoadingScene";
        #endregion Scene Name

        #region Path
        public static readonly string PATH_EFFECT_TOUCH = "03_Prefabs/Effect/TouchEffect.prefab";
        public static readonly string PATH_ITEM_GAS_L = "03_Prefabs/Item/Item_Gas_L.prefab";
        public static readonly string PATH_ITEM_GAS_R = "03_Prefabs/Item/Item_Gas_R.prefab";
        #endregion Path

        #region Time
        // 터치하고 있을 때 기본 발사 딜레이. 
        public const float TIME_DEFAULT_SHOT_DELAY = 0.1f;
        // 게임 시작시 가이드 터치 표시 시간. 
        public const float TIME_START_GUIDE_DISPLAY_SEC = 4f;
        // 부활 팝업 표시 시간. 
        public const float TIME_REVIVE_LIMIT_SEC = 3f;
        // 게임 재부팅 딜레이. 
        public const float TIME_RECONNECT_TIME = 1200f;
        #endregion Time

        #region Game Settings
        // 기본 캐릭터 ID. 
        public const int BASE_PLAYER_ID = 1000007;
        // 히든 캐릭터 조건값. (타이틀 화면)
        public const int HIDDEN_TYPE_TITLE_SCENE = 1000;
        // 캐릭터 선택창 기본 캐릭터 표시 문구. 
        public static readonly string BASE_PLAYER_CONDITION_DESC = "ON  YOUR  MARK";

        // 플레이어 스폰 위치. 
        public static readonly Vector2 PLAYER_BASE_POSITION = new Vector2(0f, 1.08f);
        // 아이템이 날라갈 목적지 좌표. 
        public static readonly Vector2 ITEM_DEST_POSITION = new Vector2(0f, 4.6f);

        // 공격 위치. 
        public static readonly Vector2 ATTACK_POSITION = new Vector2(2.2f, -0.5f);
        // 공격 포지션 기준으로 공격 가능한 범위. (상/하)
        public const float ATTACK_RANGE = 1f;

        // 기본 스테이지 ID. 
        public const int DEFAULT_STAGE_ID = 1000001;

        // 토스트 메시지 위치값. 
        public static readonly Vector2 TOAST_MSG_POSITION_TOP = new Vector2(0f, 515f);
        public static readonly Vector2 TOAST_MSG_POSITION_MID = Vector2.zero;
        public static readonly Vector2 TOAST_MSG_POSITION_BOT = new Vector2(0f, -515f);

        /// <summary>
        /// 배경 스크롤 속도. 
        /// </summary>
        public const float BACKGROUND_SCROLL_SPEED_MAX = 12f;

        /// <summary>
        /// 터널 시작 높이. 
        /// </summary>
        //public const float BACKGROUND_TUNNEL_START_HEIGHT = 7f;         // Texture. 
        public const float BACKGROUND_TUNNEL_START_HEIGHT = 1500f;    // UI. 

        /// <summary>
        /// 배경 변경 높이. 
        /// </summary>
        //public const float BACKGROUND_TUNNEL_CHANGE_HEIGHT = -9f;       // Texture. 
        public const float BACKGROUND_TUNNEL_CHANGE_HEIGHT = -35f;   // UI. 

        /// <summary>
        /// 터널 끝 높이. 
        /// </summary>
        //public const float BACKGROUND_TUNNEL_END_HEIGHT = -24f;         // Texture. 
        public const float BACKGROUND_TUNNEL_END_HEIGHT = -1500f;     // UI. 

        /// <summary>
        /// 터널 지속 시간. 
        /// </summary>
        public const float TUNNEL_TIME_SEC = 5f;

        /// <summary>
        /// UI 화면에서의 배경 스크롤 속도. 
        /// </summary>
        public const float BACKGROUND_SCROLL_SPEED_IN_UI = 2f;

        /// <summary>
        /// 기차 속도 = 1초당 기차가 가는 거리 (미터). 
        /// </summary>
        public const float TRAIN_SPEED = 100f;

        /// <summary>
        /// 오브젝트 스폰 라인. 
        /// </summary>
        public const float OBJECT_SPAWN_HEIGHT = -10f;

        /// <summary>
        /// 오브젝트 디스폰 라인. 
        /// </summary>
        public const float TOP_DESPAWN_HEIGHT = 10f;
        public const float BOT_DESPAWN_HEIGHT = -12f;

        /// <summary>
        /// 게임화면 상하 기준 라인. 
        /// </summary>
        public const float TOP_SCENE_IN_HEIGHT = 6f;
        public const float BOT_SCENE_IN_HEIGHT = -6f;

        /// <summary>
        /// Pause 시 음악 볼륨. 
        /// </summary>
        public const float PAUSE_VOLUME_MUSIC = 0.4f;
        #endregion Game Settings

        #region Player Data
        // 옵션 저장 키값. 
        public static readonly string KEY_OPTION_MUTE_SFX = "KeyMuteSfx";
        public static readonly string KEY_OPTION_MUTE_MUSIC = "KeyMuteMusic";

        public static readonly string KEY_TEST_PLAYER_INVINCIBLE = "KeyTestPlayerInvincible";
        public static readonly string KEY_CLOUD_DATA_CHECK = "KeyCloudDataCheck";
        #endregion Player Data
    }
}