﻿#if UNITY_PURCHASING
using System;
using UnityEngine;
using UnityEngine.Purchasing;
using AvoEx;

namespace Topping
{
    public class IAPManager : MonoSingleton<IAPManager>, IStoreListener
    {
        #region Const
        public const string IAP_ID_UNLOCK_ALL_CHAR_ANDROID = "consume.unlock.all.android";
        public const string IAP_ID_UNLOCK_ALL_CHAR_IOS = "consume.unlock.all.ios";
        #endregion Const

        #region Delegates
        public delegate void DelePurchaseSuccess(string szPid);
        public static event DelePurchaseSuccess OnPurchaseSuccess;
        #endregion Delegates

        #region Private Variables
        private static IStoreController m_StoreController = null;
        private static IExtensionProvider m_StoreExtProvider = null;
        #endregion Private Variables

        #region Properties
        private bool IsInitialized { get { return m_StoreController != null && m_StoreExtProvider != null; } }
        #endregion Properties


        #region Mono
        private void Start()
        {
            if (m_StoreController == null)
                InitIAP();
        }
        #endregion Mono

        #region Game Methods
        public void InitIAP()
        {
            if (IsInitialized)
                return;

            ConfigurationBuilder builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
            builder.AddProduct(IAP_ID_UNLOCK_ALL_CHAR_ANDROID, ProductType.Consumable, new IDs()
            {
                {IAP_ID_UNLOCK_ALL_CHAR_IOS, AppleAppStore.Name },
            });

            UnityPurchasing.Initialize(this, builder);
        }

        /// <summary>
        /// 상품 구매. 
        /// </summary>
        public bool IAPBuy(string szProductId)
        {
            bool bResult = false;

            if (!IsInitialized)
                return bResult;

            Product product = m_StoreController.products.WithID(szProductId);
            if (product != null && product.availableToPurchase)
            {
                m_StoreController.InitiatePurchase(product);

                bResult = true;
            }

            return bResult;
        }
        #endregion Game Methods

        #region Listener Event
        /// <summary>
        /// 초기화 완료. 
        /// </summary>
        public void OnInitialized(IStoreController controller, IExtensionProvider extension)
        {
            m_StoreController = controller;
            m_StoreExtProvider = extension;
        }

        /// <summary>
        /// 초기화 실패. 
        /// </summary>
        public void OnInitializeFailed(InitializationFailureReason reason)
        {
            UITopManager.OpenPopup(string.Format("IAP Init Error\n{0}", reason.ToString()), "CONFIRM");
        }

        /// <summary>
        /// 구매 완료. 
        /// </summary>
        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
        {
            string szPurchaseId = args.purchasedProduct.definition.id;
            switch (szPurchaseId)
            {
                case IAP_ID_UNLOCK_ALL_CHAR_ANDROID:
                case IAP_ID_UNLOCK_ALL_CHAR_IOS:
                    {
                        UserData.UnlockAllCharacter();
                        UITopManager.OpenPopup(false, "All Character Unlocked!!\nAre You Sure You\nSave to Cloud?", "NO", null, "YES", () => 
                        {
                            GPGSManager.SaveToGoogle();
                            UITopManager.ClosePopup();
                        }, UIPopup.ENUM_BUTTON_COLOR.NORMAL, UIPopup.ENUM_BUTTON_COLOR.GREEN);
                    }
                    break;

                default:
                    {
                        UITopManager.OpenPopup(string.Format("IAP Purchase Error\n{0}", szPurchaseId), "CONFIRM");
                    }
                    break;
            }

            if (OnPurchaseSuccess != null)
                OnPurchaseSuccess(szPurchaseId);

            return PurchaseProcessingResult.Complete;
        }

        /// <summary>
        /// 구매 실패. 
        /// </summary>
        public void OnPurchaseFailed(Product product, PurchaseFailureReason reason)
        {
            UITopManager.OpenPopup(string.Format("IAP Purchase Error\n{0}", reason.ToString()), "CONFIRM");
        }
        #endregion Listener Event
    }
}
#endif