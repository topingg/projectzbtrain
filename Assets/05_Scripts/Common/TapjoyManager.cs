﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AvoEx;
using TapjoyUnity;

namespace Topping
{
    public class TapjoyManager : MonoSingleton<TapjoyManager>
    {
        #region Public Variables
        public Dictionary<string, TJPlacement> m_dictPlacement = new Dictionary<string, TJPlacement>();
        #endregion Public Variables

        #region Private Variables

        #endregion Private Variables

        #region Properties
        public bool IsConnected { get { return Tapjoy.IsConnected; } }
        #endregion Properties

        #region Mono
        private void Awake()
        {
            Init();
        }

        private void OnEnable()
        {
            Tapjoy.OnConnectSuccess += _OnConnectSuccess;
            TJPlacement.OnVideoStart += _OnVideoStart;
            TJPlacement.OnVideoComplete += _OnVideoResult;
            TJPlacement.OnVideoError += _OnVideoError;
        }

        private void OnDisable()
        {
            Tapjoy.OnConnectSuccess -= _OnConnectSuccess;
            TJPlacement.OnVideoStart -= _OnVideoStart;
            TJPlacement.OnVideoComplete -= _OnVideoResult;
            TJPlacement.OnVideoError -= _OnVideoError;
        }
        #endregion Mono

        #region Game Methods
        public void Init()
        {
            if (!IsConnected)
            {
                /*Dictionary<string, object> connFlag = new Dictionary<string, object>();
                connFlag.Add("ALLOW_LEGACY_ID_FALLBACK", true);

                Tapjoy.Connect(AdManager.TAPJOY_ADS_SDK_KEY, connFlag);*/

                Tapjoy.Connect();
            }
        }

        public bool IsContentReady(string szPlacementId)
        {
            if (!IsConnected)
            {
                Init();
                return false;
            }

            if (szPlacementId.IsNull())
                return false;

            TJPlacement tp = m_dictPlacement[szPlacementId];
            if (tp == null)
                return false;
            
            bool resultValue = tp.IsContentReady();

            if (!resultValue)
                tp.RequestContent();

            return resultValue;
        }

        /// <summary>
        /// Placement 표시. 
        /// </summary>
        public void ShowPlacement(string szPlacementId)
        {
            if (!IsConnected)
            {
                Init();
                _OnVideoError(null, "Connection Error");
                return;
            }

            if (szPlacementId.IsNull())
            {
                _OnVideoError(null, "Service Error");
                return;
            }

            if (IsContentReady(szPlacementId))
            {
                m_dictPlacement[szPlacementId].ShowContent();
            }
            else
            {
                _OnVideoError(m_dictPlacement[szPlacementId], "Please Try Later");
            }
        }
        #endregion Game Methods

        #region Event
        private void _OnConnectSuccess()
        {
            if (m_dictPlacement == null)
                m_dictPlacement = new Dictionary<string, TJPlacement>();
            else
                m_dictPlacement.Clear();

            TJPlacement placeRevive = TJPlacement.CreatePlacement(AdManager.TAPJOY_ADS_PLACEMENT_REVIVE);
            m_dictPlacement.Add(AdManager.TAPJOY_ADS_PLACEMENT_REVIVE, placeRevive);
            placeRevive.RequestContent();

            TJPlacement placeResult = TJPlacement.CreatePlacement(AdManager.TAPJOY_ADS_PLACEMENT_RESULT);
            m_dictPlacement.Add(AdManager.TAPJOY_ADS_PLACEMENT_RESULT, placeResult);
        }

        private void _OnVideoStart(TJPlacement tp)
        {
            AdManager.Instance.IsTapJoyShowing = true;
        }

        private void _OnVideoResult(TJPlacement tp)
        {
            AdManager.Instance.IsTapJoyShowing = false;

            AdManager.Instance.OnFinishAd(true);

            if (tp != null)
                tp.RequestContent();
        }

        private void _OnVideoError(TJPlacement tp, string szMsg)
        {
            AdManager.Instance.IsTapJoyShowing = false;

            AdManager.Instance.OnFinishAd(false);

            if (tp != null)
                tp.RequestContent();

            UITopManager.ShowToast(szMsg);
        }
        #endregion Event
    }
}