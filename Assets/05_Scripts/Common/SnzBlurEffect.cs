﻿using UnityEngine;
using AvoEx;

namespace Topping
{
    /// <summary>
    /// 블러 효과를 위한 클래스. Image Effects/Blur 와 기본적으로 동일하지만
    /// Opacity 효과를 넣는 기능을 추가함.
    /// </summary>
    //[ExecuteInEditMode]
    public class SnzBlurEffect : MonoBehaviour
    {
        #region Public Variables
        [Range(0.0f, 30.0f)]
        public float m_fBlurSize = 1.0f;

        [Range(0.0f, 1.0f)]
        public float m_fOpacity = 0.5f;

        public Shader m_shader;
        #endregion Public Variables

        #region Private Variables
        private Material _mat = null;

        // Blur Tweening 관련. 
        private bool _bActiveTween = false;
        private float _fDuration = 0f;
        private float _fPastTime = 0f;
        private float _fDistValue = 0f;
        #endregion Private Variables

        #region Properties
        public float Opacity
        {
            get { return m_fOpacity; }
            set { m_fOpacity = value; }
        }
        #endregion Properties

        #region Mono
        public void Start()
        {
            if (m_shader != null)
            {
                _mat = new Material(m_shader);
            }
        }

        public void OnDestroy()
        {
            if (_mat != null)
            {
                DestroyImmediate(_mat);
                _mat = null;
            }
        }

        public void OnRenderImage(RenderTexture src, RenderTexture dst)
        {
            if (_mat == null || m_fBlurSize < 0.0001f || m_fOpacity < 0.0001f)
                Graphics.Blit(src, dst);
            else
            {
                RenderTexture rt = RenderTexture.GetTemporary(512, 512, 0, src.format);
                rt.filterMode = FilterMode.Bilinear;
                RenderTexture rt2 = RenderTexture.GetTemporary(512, 512, 0, src.format);
                rt2.filterMode = FilterMode.Bilinear;
                RenderTexture rt3 = RenderTexture.GetTemporary(512, 512, 0, src.format);
                rt3.filterMode = FilterMode.Bilinear;

                // Downsample
                _mat.SetVector("_Parameter", new Vector4(m_fBlurSize * (rt.width / (float)src.width), m_fBlurSize * (rt.height / (float)src.height), 0.0f, m_fOpacity));
                Graphics.Blit(src, rt, _mat, 0);

                // Blur
                Graphics.Blit(rt, rt2, _mat, 1);
                Graphics.Blit(rt2, rt3, _mat, 2);

                // Blend
                _mat.SetTexture("_DownTex", rt3);
                _mat.SetFloat("_Blend", m_fOpacity);
                Graphics.Blit(rt, dst, _mat, 3);

                RenderTexture.ReleaseTemporary(rt);
                RenderTexture.ReleaseTemporary(rt2);
                RenderTexture.ReleaseTemporary(rt3);
            }
        }

        private void Update()
        {
            if (_bActiveTween)
            {
                if (_fPastTime >= _fDuration)
                {
                    _bActiveTween = false;
                    return;
                }

                m_fOpacity = Mathf.MoveTowards(m_fOpacity, _fDistValue, Time.deltaTime * (1 / _fDuration));
                _fPastTime += Time.deltaTime;
            }
        }
        #endregion Mono

        #region Game Methods
        public void InitBlur(float fBlurSize = 1f, float fOpacity = 1f)
        {
            _bActiveTween = false;

            m_fBlurSize = fBlurSize;
            m_fOpacity = fOpacity;
        }

        /// <summary>
        /// 지정한 시간동안 원하는 수치로 트위닝. 
        /// </summary>
        public void TweenOpacity(float fDuration, float fDistValue)
        {
            _fDuration = fDuration;
            _fDistValue = fDistValue;
            _fPastTime = 0f;

            _bActiveTween = true;
        }
        #endregion Game Methods
    }
}