﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using GooglePlayGames;
using AvoEx;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;

namespace Topping
{
    public class GPGSManager : MonoSingleton<GPGSManager>
    {
        #region GPGS ID
        private const string ACHIEVE_NOVICE_HUNTER_10 = "CgkIvam0htAHEAIQEA";           // 10단계. 
        private const string ACHIEVE_NICE_TO_MEETYOU = "CgkIvam0htAHEAIQCA";            // 1회. 
        private const string ACHIEVE_ZOMBIE_TRACKER = "CgkIvam0htAHEAIQAg";             // 1회. 
        private const string ACHIEVE_ZOMBIE_HUNTER = "CgkIvam0htAHEAIQBQ";              // 1회. 
        private const string ACHIEVE_ZOMBIE_SLAYER = "CgkIvam0htAHEAIQBw";              // 1회. 
        private const string ACHIEVE_TREASURE_HUNT = "CgkIvam0htAHEAIQBg";              // 1회. 
        private const string ACHIEVE_BE_CAREFUL_10 = "CgkIvam0htAHEAIQEQ";              // 10단계. 
        private const string ACHIEVE_IM_NOT_DYING_10 = "CgkIvam0htAHEAIQEg";            // 10단계. 
        private const string ACHIEVE_GRIM_REAPER_100 = "CgkIvam0htAHEAIQEw";            // 100단계. 
        private const string ACHIEVE_THE_CONQUEROR = "CgkIvam0htAHEAIQFA";              // 1회. 숨김. 

        private const string LEADER_HIGH_SCORE = "CgkIvam0htAHEAIQAw";
        private const string LEADER_KILL_ZOMBIE = "CgkIvam0htAHEAIQBA";
        private const string LEADER_KILL_NPC = "CgkIvam0htAHEAIQFg";
        private const string LEADER_PLAY_TIME = "CgkIvam0htAHEAIQFw";

        private const string EVENT_KILL_ZOMBIE = "CgkIvam0htAHEAIQCg";
        private const string EVENT_GAME_OVER = "CgkIvam0htAHEAIQCQ";
        private const string EVENT_GET_CHAR = "CgkIvam0htAHEAIQFQ";
        #endregion GPGS ID

        #region Enum
        public enum ENUM_GPGS_TYPE
        {
            NONE,

            ACHIEVE_NOVICE_HUNTER_10,
            ACHIEVE_NICE_TO_MEETYOU,
            ACHIEVE_ZOMBIE_TRACKER,
            ACHIEVE_ZOMBIE_HUNTER,
            ACHIEVE_ZOMBIE_SLAYER,
            ACHIEVE_TREASURE_HUNT,
            ACHIEVE_BE_CAREFUL_10,
            ACHIEVE_IM_NOT_DYING_10,
            ACHIEVE_GRIM_REAPER_100,
            ACHIEVE_THE_CONQUEROR,

            LEADER_HIGH_SCORE,
            LEADER_KILL_ZOMBIE,
            LEADER_KILL_NPC,
            LEADER_PLAY_TIME,

            EVENT_KILL_ZOMBIE,
            EVENT_GAME_OVER,
            EVENT_GET_CHAR,
        }

        /// <summary>
        /// API 아이디 조회.
        /// </summary>
        private string GetAPIName(ENUM_GPGS_TYPE eType)
        {
            string returnValue = null;

            switch (eType)
            {
                case ENUM_GPGS_TYPE.ACHIEVE_NOVICE_HUNTER_10:
                    returnValue = ACHIEVE_NOVICE_HUNTER_10;
                    break;
                case ENUM_GPGS_TYPE.ACHIEVE_NICE_TO_MEETYOU:
                    returnValue = ACHIEVE_NICE_TO_MEETYOU;
                    break;
                case ENUM_GPGS_TYPE.ACHIEVE_ZOMBIE_TRACKER:
                    returnValue = ACHIEVE_ZOMBIE_TRACKER;
                    break;
                case ENUM_GPGS_TYPE.ACHIEVE_ZOMBIE_HUNTER:
                    returnValue = ACHIEVE_ZOMBIE_HUNTER;
                    break;
                case ENUM_GPGS_TYPE.ACHIEVE_ZOMBIE_SLAYER:
                    returnValue = ACHIEVE_ZOMBIE_SLAYER;
                    break;
                case ENUM_GPGS_TYPE.ACHIEVE_TREASURE_HUNT:
                    returnValue = ACHIEVE_TREASURE_HUNT;
                    break;
                case ENUM_GPGS_TYPE.ACHIEVE_BE_CAREFUL_10:
                    returnValue = ACHIEVE_BE_CAREFUL_10;
                    break;
                case ENUM_GPGS_TYPE.ACHIEVE_IM_NOT_DYING_10:
                    returnValue = ACHIEVE_IM_NOT_DYING_10;
                    break;
                case ENUM_GPGS_TYPE.ACHIEVE_GRIM_REAPER_100:
                    returnValue = ACHIEVE_GRIM_REAPER_100;
                    break;
                case ENUM_GPGS_TYPE.ACHIEVE_THE_CONQUEROR:
                    returnValue = ACHIEVE_THE_CONQUEROR;
                    break;

                case ENUM_GPGS_TYPE.LEADER_HIGH_SCORE:
                    returnValue = LEADER_HIGH_SCORE;
                    break;
                case ENUM_GPGS_TYPE.LEADER_KILL_ZOMBIE:
                    returnValue = LEADER_KILL_ZOMBIE;
                    break;
                case ENUM_GPGS_TYPE.LEADER_KILL_NPC:
                    returnValue = LEADER_KILL_NPC;
                    break;
                case ENUM_GPGS_TYPE.LEADER_PLAY_TIME:
                    returnValue = LEADER_PLAY_TIME;
                    break;

                case ENUM_GPGS_TYPE.EVENT_KILL_ZOMBIE:
                    returnValue = EVENT_KILL_ZOMBIE;
                    break;
                case ENUM_GPGS_TYPE.EVENT_GAME_OVER:
                    returnValue = EVENT_GAME_OVER;
                    break;
                case ENUM_GPGS_TYPE.EVENT_GET_CHAR:
                    returnValue = EVENT_GET_CHAR;
                    break;
            }

            return returnValue;
        }
        #endregion Enum

        #region Delegates
        public static event Action<bool> OnCheckSaveDataResult;
        #endregion Delegates

        #region Private Variables
        private struct SendLong
        {
            public ENUM_GPGS_TYPE m_eType;
            public long m_lValue;
        }

        private struct SendInt
        {
            public ENUM_GPGS_TYPE m_eType;
            public int m_iValue;
        }

        private Queue<SendLong> _queueRank = new Queue<SendLong>();
        private Queue<SendInt> _queueStepAchieve = new Queue<SendInt>();
        private Queue<string> _queueUnlockAchieve = new Queue<string>();
        private Queue<SendInt> _queueEvent = new Queue<SendInt>();

        private float _fSendDelay = 0.3f;
        private float _fSendTime = 0f;
        #endregion Private Variables

        #region Properties
        public bool IsLogin { get { return Social.localUser.authenticated; } }
        public PlayGamesPlatform PGP {
            get
            {
                PlayGamesPlatform pgp = null;
                if (IsLogin)
                {
                    pgp = Social.Active as PlayGamesPlatform;
                }
                return pgp;
            }
        }

        public string UserID
        {
            get
            {
                string szId = null;
                if (IsLogin)
                {
                    szId = Social.localUser.id;
                }
                return szId;
            }
        }
        #endregion Properties

        #region Mono
        private void Awake()
        {
            _InitPlugin();
        }

        private void Update()
        {
            _SendData(Time.unscaledDeltaTime);
        }
        #endregion Mono

        #region Game Methods
        public void Login()
        {
            if (IsLogin)
                return;

            PlayGamesPlatform.Activate();
            Social.localUser.Authenticate((bool bLogin) => 
            {
                /*if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name.Equals(ProjectDefines.SCENE_NAME_GAME))
                    UITopManager.ShowToast(string.Format("LOGINED : {0}", Social.localUser.id));*/

                // 클라우드 데이터 체크. 
                if (!EncryptPrefs.GetBool(ProjectDefines.KEY_CLOUD_DATA_CHECK, false))
                    GPGSManager.CheckExistSaveData();
            });
        }

        public void LogOut()
        {
            if (!IsLogin)
                return;

            if (PGP != null)
            {
                PGP.SignOut();
                UITopManager.ShowToast("SUCCESSFULLY  LOGGED  OUT");
            }
        }

        /// <summary>
        /// 랭킹 UI 표시. 
        /// </summary>
        public static void ShowRank(bool isMyRank = false)
        {
            if (!Instance.IsLogin)
                return;

            /*if (isMyRank)
            {
                if (Instance.PGP != null)
                    Instance.PGP.ShowLeaderboardUI(Instance.GetAPIName(ENUM_GPGS_TYPE.LEADER_HIGH_SCORE));
            }
            else
            {*/
                Social.ShowLeaderboardUI();
            //}
        }

        /// <summary>
        /// 업적 UI 표시. 
        /// </summary>
        public static void ShowAchieve()
        {
            if (!Instance.IsLogin)
                return;

            Social.ShowAchievementsUI();
        }

        /// <summary>
        /// 랭크 데이터 전송.
        /// </summary>
        public static void SendLeader(ENUM_GPGS_TYPE eType, long lValue = 0)
        {
            if (!Instance.IsLogin || lValue < 1)
                return;

            SendLong data = new SendLong() { m_eType = eType, m_lValue = lValue };
            Instance._queueRank.Enqueue(data);
        }

        /// <summary>
        /// 업적 데이터 전송. (단계 업적용)
        /// </summary>
        public static void SendStepAchieve(ENUM_GPGS_TYPE eType, int iValue = 1)
        {
            if (!Instance.IsLogin || iValue < 1)
                return;

            SendInt data = new SendInt() { m_eType = eType, m_iValue = iValue };
            Instance._queueStepAchieve.Enqueue(data);
        }

        /// <summary>
        /// 업적 데이터 전송. (즉시 완료용)
        /// </summary>
        public static void SendUnlockAchieve(ENUM_GPGS_TYPE eType)
        {
            if (!Instance.IsLogin)
                return;

            string szId = Instance.GetAPIName(eType);
            if (!szId.IsNull())
                Instance._queueUnlockAchieve.Enqueue(szId);
        }

        /// <summary>
        /// 숨김 업적 표시. 
        /// </summary>
        public static void RevealAchieve(ENUM_GPGS_TYPE eType)
        {
            if (Instance.PGP == null)
                return;

            Instance.PGP.RevealAchievement(Instance.GetAPIName(eType));
           
        }

        /// <summary>
        /// 이벤트 데이터 전송. 
        /// </summary>
        public static void SendEvent(ENUM_GPGS_TYPE eType, int iValue = 1)
        {
            if (!Instance.IsLogin || iValue < 1)
                return;

            SendInt data = new SendInt() { m_eType = eType, m_iValue = iValue };
            Instance._queueEvent.Enqueue(data);
        }

        /// <summary>
        /// 데이터 전송. 
        /// </summary>
        private void _SendData(float deltaTime)
        {
            if (_fSendTime > 0f)
            {
                _fSendTime -= deltaTime; return;
            }
            else
            {
                _fSendTime = _fSendDelay;
            }

            // 랭크 전송. 
            if (_queueRank.Count > 0)
            {
                if (Instance.PGP != null)
                {
                    SendLong data = _queueRank.Dequeue();
                    Instance.PGP.ReportScore(data.m_lValue, Instance.GetAPIName(data.m_eType), null);
                }
            }

            // 업적 전송. 
            if (_queueStepAchieve.Count > 0)
            {
                if (Instance.PGP != null)
                {
                    SendInt data = _queueStepAchieve.Dequeue();
                    Instance.PGP.IncrementAchievement(Instance.GetAPIName(data.m_eType), data.m_iValue, null);
                }
            }

            // 업적 완료. 
            if (_queueUnlockAchieve.Count > 0)
            {
                if (Instance.PGP != null)
                {
                    string szId = _queueUnlockAchieve.Dequeue();
                    Instance.PGP.UnlockAchievement(szId);
                }
            }

            // 이벤트 전송. 
            if (_queueEvent.Count > 0)
            {
                if (Instance.PGP != null)
                {
                    SendInt data = _queueEvent.Dequeue();
                    Instance.PGP.Events.IncrementEvent(Instance.GetAPIName(data.m_eType), (uint)data.m_iValue);
                }
            }
        }


        ///////////////////////////////////////////////////////////////////////////////////
        // Google Play Save
        ///////////////////////////////////////////////////////////////////////////////////

        private void _InitPlugin()
        {
            PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().EnableSavedGames().Build();
            PlayGamesPlatform.InitializeInstance(config);
            PlayGamesPlatform.DebugLogEnabled = false;
        }

        /// <summary>
        /// 구글 플레이에 유저 데이터 저장. 
        /// </summary>
        public static void SaveToGoogle()
        {
            if (!Instance.IsLogin)
            {
                UITopManager.ShowToast("Please Login to Google");
                return;
            }

            _OpenSavedGame(true);
        }

        /// <summary>
        /// 구글 플레이에서 유저 데이터 로드. 
        /// </summary>
        public static void LoadFromGoogle()
        {
            if (!Instance.IsLogin)
            {
                UITopManager.ShowToast("Please Login to Google");
                return;
            }

            _OpenSavedGame(false);
        }

        /// <summary>
        /// 구글 플레이 세이브 게임 열기. 
        /// </summary>
        private static void _OpenSavedGame(bool bSave)
        {
            UITopManager.SetActiveWaitCover(true, 10f);

            ISavedGameClient savedClient = PlayGamesPlatform.Instance.SavedGame;
            if (savedClient != null)
            {
                if (bSave)
                    savedClient.OpenWithAutomaticConflictResolution(UserData.FileName.Replace(".dat", ""), DataSource.ReadCacheOrNetwork, ConflictResolutionStrategy.UseLongestPlaytime, _OnSavedGameOpenedToSave);
                else
                    savedClient.OpenWithAutomaticConflictResolution(UserData.FileName.Replace(".dat", ""), DataSource.ReadCacheOrNetwork, ConflictResolutionStrategy.UseLongestPlaytime, _OnSavedGameOpenedToLoad);
            }

        }

        /// <summary>
        /// 세이브 파일 커밋. 
        /// </summary>
        private static void _OnSavedGameOpenedToSave(SavedGameRequestStatus status, ISavedGameMetadata meta)
        {
            try
            {
                byte[] bySaveFile = null;
                bySaveFile = File.ReadAllBytes(UserData.SavePath);

                if (bySaveFile == null)
                    throw new Exception("(FILE  ERROR)");

                SavedGameMetadataUpdate.Builder builder = new SavedGameMetadataUpdate.Builder();
                builder = builder.WithUpdatedPlayedTime(DateTime.Now.TimeOfDay).WithUpdatedDescription(string.Format("Saved at {0}", DateTime.Now));
                SavedGameMetadataUpdate updatedData = builder.Build();

                ISavedGameClient savedClient = PlayGamesPlatform.Instance.SavedGame;
                savedClient.CommitUpdate(meta, updatedData, bySaveFile, _OnGameSaved);

            }
            catch (Exception e)
            {
                UITopManager.SetActiveWaitCover(false);
                UITopManager.OpenPopup(string.Format("GOOGLE SAVE ERROR\nPLEASE TRY LATER\n{0}", e.Message.Substring(0, 20)), "OK");
            }
        }

        /// <summary>
        /// 세이브 결과. 
        /// </summary>
        private static void _OnGameSaved(SavedGameRequestStatus status, ISavedGameMetadata meta)
        {
            if (status == SavedGameRequestStatus.Success)
            {
                UITopManager.OpenPopup("SUCCESSFULLY\nSAVED", "OK");
            }
            else
            {
                UITopManager.OpenPopup("SAVE ERROR\nPLEASE TRY LATER", "OK");
            }
            UITopManager.SetActiveWaitCover(false);
        }

        /// <summary>
        /// 세이브 파일 로드. 
        /// </summary>
        private static void _OnSavedGameOpenedToLoad(SavedGameRequestStatus status, ISavedGameMetadata meta)
        {
            try
            {
                ISavedGameClient savedClient = PlayGamesPlatform.Instance.SavedGame;
                savedClient.ReadBinaryData(meta, _OnGameLoaded);
            }
            catch
            {
                UITopManager.SetActiveWaitCover(false);
                UITopManager.OpenPopup("GOOGLE LOAD ERROR\nPLEASE TRY LATER", "OK");
            }
        }

        /// <summary>
        /// 유저 데이터에 적용. 
        /// </summary>
        private static void _OnGameLoaded(SavedGameRequestStatus status, byte[] byData)
        {
            try
            {
                if (byData.Length < 1)
                {
                    UITopManager.OpenPopup("SAVED  FILE  IS\nNOT  EXIST", "OK");
                    return;
                }

                // 폴더가 존재하는지 확인. 
                if (!Directory.Exists(UserData.PathDirectory))
                    Directory.CreateDirectory(UserData.PathDirectory);

                // 파일이 존재하는지 확인. 
                if (!File.Exists(UserData.SavePath))
                {
                    FileStream fs = File.Create(UserData.SavePath);
                    fs.Close();
                }

                File.WriteAllBytes(UserData.SavePath, byData);
                UserData.Load();

                UITopManager.OpenPopup("SUCCESSFULLY\nLOADED", "OK");
            }
            catch (Exception e)
            {
                UITopManager.OpenPopup(string.Format("GOOGLE LOAD ERROR\nPLEASE TRY LATER\n{0}", e.Message.Substring(0, 20)), "OK");
            }
            finally
            {
                UITopManager.SetActiveWaitCover(false);
            }
        }

        /// <summary>
        /// 클라우드에 세이브파일이 있는지 체크. 
        /// </summary>
        public static void CheckExistSaveData()
        {
            if (!Instance.IsLogin)
                return;

            ISavedGameClient savedClient = PlayGamesPlatform.Instance.SavedGame;
            if (savedClient != null)
            {
                savedClient.OpenWithAutomaticConflictResolution(UserData.FileName.Replace(".dat", ""), DataSource.ReadCacheOrNetwork, ConflictResolutionStrategy.UseLongestPlaytime, _OnSaveGameOpendToCheck);
            }
            else
            {
                if (OnCheckSaveDataResult != null)
                    OnCheckSaveDataResult(false);
            }
        }

        private static void _OnSaveGameOpendToCheck(SavedGameRequestStatus status, ISavedGameMetadata meta)
        {
            try
            {
                ISavedGameClient savedClient = PlayGamesPlatform.Instance.SavedGame;
                savedClient.ReadBinaryData(meta, _OnCheckResult);
            }
            catch
            {
                if (OnCheckSaveDataResult != null)
                    OnCheckSaveDataResult(false);
            }
        }

        private static void _OnCheckResult(SavedGameRequestStatus status, byte[] byData)
        {
            bool bResult = false;
            if (status == SavedGameRequestStatus.Success && byData.Length > 0)
                bResult = true;

            if (OnCheckSaveDataResult != null)
                OnCheckSaveDataResult(bResult);
        }
        #endregion Game Methods
    }
}