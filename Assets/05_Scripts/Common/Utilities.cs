﻿using UnityEngine;
using System;
using AvoEx;

namespace Topping
{
    public class Utilities : MonoBehaviour
    {
        /// <summary>
        /// 흑백 쉐이더 적용. 
        /// </summary>
        public static void AdjustGrayScale(ManagedObject obj, bool isGray = false)
        {
            if (obj == null)
                return;

            SpriteRenderer[] arrRenderer = obj.GetComponentsInChildren<SpriteRenderer>();
            if (arrRenderer.Length < 1)
                return;

            for (int i = 0, cnt = arrRenderer.Length; i < cnt; i++)
            {
                AdjustGrayScale(arrRenderer[i], isGray);
            }

            //AdjustGrayScale(obj.GetComponentInChildren<SpriteRenderer>(), isGray);
        }

        /// <summary>
        /// 흑백 쉐이더 적용. 
        /// </summary>
        public static void AdjustGrayScale(Renderer renderer, bool isGray = false)
        {
            if (renderer == null)
                return;

            AdjustGrayScale(renderer.material, isGray);
        }

        /// <summary>
        /// 흑백 쉐이더 적용. 
        /// </summary>
        public static void AdjustGrayScale(Material mat, bool isGray = false)
        {
            if (mat == null)
                return;

            // 흑백.
            if (isGray)
                mat.SetFloat("_EffectAmount", 1f);
            // 컬러.
            else
                mat.SetFloat("_EffectAmount", 0f);
        }

        /// <summary>
        /// 초로 날짜 구하기. 
        /// </summary>
        public static DateTime ConvertTimeToDate(int iSeconds)
        {
            DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0).AddSeconds(iSeconds);
            return dt;
        }

        /// <summary>
        /// 날짜로 초 구하기. 
        /// </summary>
        public static int ConvertDateToTime(DateTime dateTime)
        {
            TimeSpan ts = dateTime - new DateTime(1970, 1, 1, 0, 0, 0);
            return Convert.ToInt32(ts.TotalSeconds);
        }
    }

    // 확장 메소드 클래스. 
    public static partial class ExtensionMethods
    {
        #region Extension Methods
        /// <summary>
        /// Transform 확장 메소드. 
        /// (Null 체크, 포지션 설정, 기본 Local)
        /// </summary>
        public static bool SetPosition(this Transform target, Vector3 value, bool isLocal = true)
        {
            bool bResult = false;

            if (target != null)
            {
                if (isLocal)
                    target.localPosition = value;
                else
                    target.position = value;

                bResult = true;
            }

            return bResult;
        }

        /// <summary>
        /// Transform 확장 메소드. 
        /// (Null 체크, 로테이션 설정, 기본 Local)
        /// </summary>
        public static bool SetRotation(this Transform target, Vector3 value, bool isLocal = true)
        {
            bool bResult = false;

            if (target != null)
            {
                if (isLocal)
                    target.localRotation = Quaternion.Euler(value);
                else
                    target.rotation = Quaternion.Euler(value);

                bResult = true;
            }

            return bResult;
        }

        /// <summary>
        /// Transform 확장 메소드. 
        /// (Null 체크, X축 조회, 기본 Local)
        /// </summary>
        public static float GetPositionX(this Transform target, bool isLocal = true)
        {
            float fResult = 0f;

            if (target != null)
            {
                if (isLocal)
                    fResult = target.localPosition.x;
                else
                    fResult = target.position.x;
            }

            return fResult;
        }

        /// <summary>
        /// Transform 확장 메소드. 
        /// (Null 체크, Y축 조회, 기본 Local)
        /// </summary>
        public static float GetPositionY(this Transform target, bool isLocal = true)
        {
            float fResult = 0f;

            if (target != null)
            {
                if (isLocal)
                    fResult = target.localPosition.y;
                else
                    fResult = target.position.y;
            }

            return fResult;
        }

        /// <summary>
        /// Transform 확장 메소드. 
        /// (Null 체크, Z축 조회, 기본 Local)
        /// </summary>
        public static float GetPositionZ(this Transform target, bool isLocal = true)
        {
            float fResult = 0f;

            if (target != null)
            {
                if (isLocal)
                    fResult = target.localPosition.z;
                else
                    fResult = target.position.z;
            }

            return fResult;
        }

        /// <summary>
        /// String 확장 메소드. 
        /// (Null 인지 확인)
        /// </summary>
        public static bool IsNull(this string target)
        {
            return string.IsNullOrEmpty(target);
        }

        /// <summary>
        /// String 확장 메소드. 
        /// (숫자인지 확인)
        /// </summary>
        public static bool IsNumeric(this string target)
        {
            float output;
            return float.TryParse(target, out output);
        }

        /// <summary>
        /// String 확장 메소드. 
        /// (포맷 텍스트)
        /// </summary>
        public static string SetFormat(this string target, params object[] args)
        {
            return string.Format(target, args);
        }

        /// <summary>
        /// GameObject 확장 메소드. 
        /// (Null 체크, 활성화 제어)
        /// </summary>
        public static bool SetActivate(this GameObject target, bool bActive)
        {
            bool bResult = false;

            if (target != null)
            {
                target.SetActive(bActive);
                bResult = true;
            }

            return bResult;
        }

        /// <summary>
        /// UILabel 확장 메소드. 
        /// (Null 체크, 텍스트 할당)
        /// </summary>
        public static bool SetText(this UILabel target, string szText = "")
        {
            bool bResult = false;

            if (target != null)
            {
                target.text = szText;
                bResult = true;
            }

            return bResult;
        }

        /// <summary>
        /// UISprite 확장 메소드. 
        /// (Null 체크, Fill Amount 할당)
        /// </summary>
        public static bool SetFillAmount(this UISprite target, float fValue = 0f)
        {
            bool bResult = false;

            if (target != null)
            {
                target.fillAmount = fValue;
                bResult = true;
            }

            return bResult;
        }

        /// <summary>
        /// UISprite 확장 메소드. 
        /// (Null 체크, SpriteName 할당)
        /// </summary>
        public static bool SetSprite(this UISprite target, string szSpriteName)
        {
            bool bResult = false;

            if (target != null)
            {
                target.spriteName = szSpriteName;
                bResult = true;
            }

            return bResult;
        }
        #endregion Extension Methods
    }
}