﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System;
using System.IO;

namespace Topping
{
    public static class GameDataEditor
    {
        public delegate void ParseFunc<TKey, TValue>(string szTableData, out Dictionary<TKey, TValue> outdata);

        public static string s_szTableConverter = Application.dataPath + "/00_3rdParty/Tool/ExcelTableConverter.exe";
        public static string s_szExcelForder = Application.dataPath + "/01_RawData/DataTable";
        public static string s_szTableForder = Application.dataPath + "/02_PlayData/DataTable";
        public static string s_szFilePath = "Assets/02_PlayData/DataTable/{0}.{1}";

        public static string s_szFixedTableForder = Application.dataPath + "/02_PlayData/FixedDataTable/Resources";
        public static string s_szFixedFilePath = "Assets/02_PlayData/FixedDataTable/Resources/{0}.{1}";


        public static void ConvertExcelTable<TKey, TValue>(ParseFunc<TKey, TValue> Parsing, string szTableName, string szTableExt)
        {
            DateTime timeStarted = DateTime.Now;
            Debug.LogFormat("<color=#ffff00ff>Started [{0}] table converting</color> : {1}\n{2}", szTableName, timeStarted, s_szExcelForder + "/" + szTableName + szTableExt);

            //< 시리얼라이즈화를 여기서 시킴
            //< 출력할 데이터 dic을만듬
            Dictionary<TKey, TValue> dicData = null;

            //< 1. 자기 자신의 경로를 보내서 외부프로그램을 거쳐서 txt화 함
            CsvSerializer.ConvertExcelToTxt(s_szTableConverter, s_szExcelForder + "/" + szTableName + szTableExt);

            //< txt화 한 파일을 s_szTableForder로 보낸다
            File.Move(s_szExcelForder + "/" + szTableName + ".txt", s_szTableForder + "/" + szTableName + ".txt");

            //< 2. 파싱함
            Parsing(string.Format(s_szFilePath, szTableName, "txt"), out dicData);

            //< 3. 시리얼라이저를 통해 시리얼라이즈 시킴            
            string szResultFilePath = string.Format(s_szFilePath, szTableName, "bytes");
            Serializer.EncryptSerialize<TKey, TValue>(szResultFilePath, ref dicData);

            //< 4. txt파일 정리
            File.Delete(string.Format(s_szFilePath, szTableName, "txt"));

            //
            AssetDatabase.ImportAsset(szResultFilePath);

            //
            DateTime timeFinished = DateTime.Now;
            Debug.LogFormat("<color=#00ff00ff>Finished [{2}] table converting</color> : {0}, duration = {1}", timeFinished, timeFinished - timeStarted, szTableName);
        }
    }

    #region Fixed String Data
    public partial class FixedStringData : TableData<string, StringData>
    {
        [MenuItem("Tools/Convert/CONTENT_FIXED_STRING")]
        public static void ConvertFixedTable()
        {
            StringData.ConvertTable(TABLE_NAME, GameDataEditor.s_szFixedTableForder, GameDataEditor.s_szFixedFilePath);
        }
    }
    #endregion Fixed String Data

    #region String Data
    public partial class StringData
    {
        static readonly string TABLE_EXT = ".xls";

        [MenuItem("Tools/Convert/CONTENT_STRING")]
        public static void ConvertTable()
        {
            ConvertTable(TABLE_NAME, GameDataEditor.s_szTableForder, GameDataEditor.s_szFilePath);
        }

        public static void ConvertTable(string szTableName, string szTableFolder, string szFilePath)
        {
            DateTime timeStarted = DateTime.Now;
            Debug.LogFormat("<color=#ffff00ff>Started [{1}] table converting</color> : {0}", timeStarted, szTableName);

            //< 1. 자기 자신의 경로를 보내서 외부프로그램을 거쳐서 txt화 함
            CsvSerializer.ConvertExcelToTxt(GameDataEditor.s_szTableConverter, GameDataEditor.s_szExcelForder + "/" + szTableName + TABLE_EXT);

            //< txt화 한 파일을 s_szTableForder로 보낸다
            File.Move(GameDataEditor.s_szExcelForder + "/" + szTableName + ".txt", szTableFolder + "/" + szTableName + ".txt");

            for (int i = 0; i < s_arLangPostfixs.Length; ++i)
            {
                //< 2. 파싱함
                Dictionary<string, StringData> dicData = null;
                ParseTable(string.Format(szFilePath, szTableName, "txt"), out dicData, s_arLangPostfixs[i]);

                //< 3. 시리얼라이저를 통해 시리얼라이즈 시킴            
                Serializer.EncryptSerialize(string.Format(szFilePath, szTableName + s_arLangPostfixs[i], "bytes"), ref dicData);
            }

            //< 4. txt파일 정리
            File.Delete(string.Format(szFilePath, szTableName, "txt"));

            //
            DateTime timeFinished = DateTime.Now;
            Debug.LogFormat("<color=#00ff00ff>Finished [{2}] table converting</color> : {0}, duration = {1}", timeFinished, timeFinished - timeStarted, szTableName);
        }

        public static void ParseTable(string szTableData, out Dictionary<string, StringData> outdata, string szColName)
        {
            outdata = new Dictionary<string, StringData>();

            CSVData csvData = new CSVData(szTableData);
            for (int iRow = 1; iRow < csvData.GetLineCount; ++iRow)
            {
                try
                {
                    string szTID = csvData.GetString(iRow, "TID");
                    if (szTID.IsNull() || outdata.ContainsKey(szTID))
                        continue;

                    string szText = ParseEscape(csvData.GetString(iRow, szColName));
                    if (szText.IsNull()) // TID는 있는데, TEXT가 없다. (이런 경우는 TID가 곧 TEXT)
                        szText = ParseEscape(szTID); // TID에 개행문자가 있다면, 변환해서 TEXT에 출력되어야 하므로, TEXT가 없지만, TID를 변환한 텍스트를 임의로 추가해준다.

                    if (szTID == szText) // TID와 TEXT가 같다면 굳이 추가할 필요 없다..
                        continue;

                    // TID와 다른 TEXT 데이터가 있었거나, 없었지만 개행문자 변환에 의해 임의로 추가 해주는 경우.
                    StringData tempData = new StringData();
                    tempData.TID = szTID;
                    tempData.UID = csvData.GetInt(iRow, "UID");
                    tempData.m_szText = szText;

                    outdata.Add(tempData.TID, tempData);
                }
                catch (Exception e)
                {
                    Debug.LogErrorFormat("load row failed. table = [{0}], row = [{1}]\nerror = {2}", szTableData, iRow, e);
                }
            }
        }
    }
    #endregion String Data

    #region Stage Data
    public partial class StageData
    {
        static readonly string TABLE_EXT = ".xls";

        static readonly string BG_RES_PATH = "01_RawData/Textures/Background/";
        static readonly string BG_RES_EXT = ".png";
        static readonly string TRAIN_RES_PATH = "01_RawData/Textures/Background/";
        static readonly string TRAIN_RES_EXT = ".png";

        [MenuItem("Tools/Convert/CONTENT_STAGE")]
        public static void ConvertTable()
        {
            //< 시리얼라이즈화를 여기서 시킴
            //< 출력할 데이터 dic을만듬
            GameDataEditor.ConvertExcelTable<int, StageData>(ParseTable, TABLE_NAME, TABLE_EXT);
        }

        public static void ParseTable(string szTableData, out Dictionary<int, StageData> outdata)
        {
            outdata = new Dictionary<int, StageData>();

            CSVData csvData = new CSVData(szTableData);
            for (int iRow = 1; iRow < csvData.GetLineCount; ++iRow)
            {
                try
                {
                    StageData tempData = new StageData();

                    tempData.UID = csvData.GetInt(iRow, "UID");
                    tempData.m_iStageNo = csvData.GetInt(iRow, "STAGE_NO");
                    tempData.m_szName = csvData.GetString(iRow, "NAME");
                    tempData.m_szDesc = csvData.GetString(iRow, "DESC");
                    tempData.m_fStartDistance = csvData.GetFloat(iRow, "START_DISTANCE");

                    tempData.m_szBgPath = csvData.GetString(iRow, "BG_PATH");
                    if (!tempData.m_szBgPath.IsNull())
                        tempData.m_szBgPath = string.Concat(BG_RES_PATH, tempData.m_szBgPath, BG_RES_EXT);
                    tempData.m_szTrainPath = csvData.GetString(iRow, "TRAIN_PATH");
                    if (!tempData.m_szTrainPath.IsNull())
                        tempData.m_szTrainPath = string.Concat(TRAIN_RES_PATH, tempData.m_szTrainPath, TRAIN_RES_EXT);

                    string szAppearNpc = csvData.GetString(iRow, "APPEAR_NPC_LIST");
                    if (!szAppearNpc.IsNull())
                    {
                        if (tempData.m_lstAppearNpc == null)
                            tempData.m_lstAppearNpc = new List<int>();

                        string[] npcIds = szAppearNpc.Split('|');
                        for (int i = 0, cnt = npcIds.Length; i < cnt; i++)
                        {
                            if (npcIds[i] != null)
                            {
                                if (!npcIds[i].IsNull())
                                    tempData.m_lstAppearNpc.Add(int.Parse(npcIds[i]));
                            }
                        }
                    }

                    string szAppearEnemy = csvData.GetString(iRow, "APPEAR_ENEMY_LIST");
                    if (!szAppearEnemy.IsNull())
                    {
                        if (tempData.m_lstAppearEnemy == null)
                            tempData.m_lstAppearEnemy = new List<int>();

                        string[] enemyIds = szAppearEnemy.Split('|');
                        for (int i = 0, cnt = enemyIds.Length; i < cnt; i++)
                        {
                            if (enemyIds[i] != null)
                            {
                                if (!enemyIds[i].IsNull())
                                    tempData.m_lstAppearEnemy.Add(int.Parse(enemyIds[i]));
                            }
                        }
                    }

                    tempData.m_fSpawnStartDelay = csvData.GetFloat(iRow, "SPAWN_START_DELAY");

                    outdata.Add(tempData.UID, tempData);
                }
                catch (Exception e)
                {
                    Debug.LogErrorFormat("load row failed. table = [{0}], row = [{1}]\nerror = {2}", szTableData, iRow, e);
                }
            }
        }
    }
    #endregion Stage Data

    #region Appearance Data
    public partial class AppearanceData
    {
        static readonly string TABLE_EXT = ".xlsx";

        [MenuItem("Tools/Convert/CONTENT_APPEARANCE")]
        public static void ConvertTable()
        {
            //< 시리얼라이즈화를 여기서 시킴
            //< 출력할 데이터 dic을만듬
            GameDataEditor.ConvertExcelTable<int, AppearanceData>(ParseTable, TABLE_NAME, TABLE_EXT);
        }

        public static void ParseTable(string szTableData, out Dictionary<int, AppearanceData> outdata)
        {
            outdata = new Dictionary<int, AppearanceData>();

            CSVData csvData = new CSVData(szTableData);
            for (int iRow = 1; iRow < csvData.GetLineCount; ++iRow)
            {
                try
                {
                    AppearanceData tempData = new AppearanceData();

                    tempData.UID = csvData.GetInt(iRow, "UID");
                    tempData.m_iStageId = csvData.GetInt(iRow, "STAGE_ID");
                    tempData.m_iSpeedUpDist = csvData.GetInt(iRow, "SPEED_UP_DIST");
                    tempData.m_fSpeedUpAdd = csvData.GetFloat(iRow, "SPEED_UP_ADD");
                    tempData.m_fEnemyMoveSpeedAdd = csvData.GetFloat(iRow, "ENEMY_MOVE_SPEED_ADD");

                    outdata.Add(tempData.UID, tempData);
                }
                catch (Exception e)
                {
                    Debug.LogErrorFormat("load row failed. table = [{0}], row = [{1}]\nerror = {2}", szTableData, iRow, e);
                }
            }
        }
    }
    #endregion Appearance Data

    #region Player Data
    public partial class PlayerData
    {
        static readonly string TABLE_EXT = ".xls";

        static readonly string PREFAB_PATH = "03_Prefabs/Character/Player/";
        static readonly string PREFAB_EXT = ".prefab";
        static readonly string ICON_PATH = "";  // NGUI 사용. Sprite Name 호출. 
        static readonly string EFFECT_PATH = "03_Prefabs/Effect/";

        [MenuItem("Tools/Convert/CHARACTER/CONTENT_PLAYER")]
        public static void ConvertTable()
        {
            //< 시리얼라이즈화를 여기서 시킴
            //< 출력할 데이터 dic을만듬
            GameDataEditor.ConvertExcelTable<int, PlayerData>(ParseTable, TABLE_NAME, TABLE_EXT);
        }

        public static void ParseTable(string szTableData, out Dictionary<int, PlayerData> outdata)
        {
            outdata = new Dictionary<int, PlayerData>();

            CSVData csvData = new CSVData(szTableData);
            for (int iRow = 1; iRow < csvData.GetLineCount; ++iRow)
            {
                try
                {
                    PlayerData tempData = new PlayerData();

                    tempData.UID = csvData.GetInt(iRow, "UID");
                    tempData.m_iGrade = csvData.GetInt(iRow, "GRADE");
                    tempData.m_szName = csvData.GetString(iRow, "NAME");
                    tempData.m_szDesc = csvData.GetString(iRow, "DESC");
                    tempData.m_szMemo = csvData.GetString(iRow, "MEMO");
                    tempData.m_szPrefab = csvData.GetString(iRow, "PREFAB_PATH");
                    if (!tempData.m_szPrefab.IsNull())
                        tempData.m_szPrefab = string.Concat(PREFAB_PATH, tempData.m_szPrefab, PREFAB_EXT);
                    tempData.m_szIcon = csvData.GetString(iRow, "ICON_PATH");
                    if (!tempData.m_szIcon.IsNull())
                        tempData.m_szIcon = string.Concat(ICON_PATH, tempData.m_szIcon);

                    tempData.m_fMaxHP = csvData.GetFloat(iRow, "TRAIN_HP");
                    tempData.m_fMaxFever = csvData.GetFloat(iRow, "FEVER_MAX");
                    tempData.m_fFeverDuration = csvData.GetFloat(iRow, "FEVER_DURATION");
                    tempData.m_fAttackDmg = csvData.GetFloat(iRow, "ATTACK");
                    tempData.m_fAttackDelay = csvData.GetFloat(iRow, "ATTACK_DELAY");
                    tempData.m_fDefense = csvData.GetFloat(iRow, "DEFENSE");
                    tempData.m_fMoveSpeed = csvData.GetFloat(iRow, "MOVE_SPEED");

                    tempData.m_szHitAnimation = csvData.GetString(iRow, "HIT_ANIMATION");

                    string szRunSound = csvData.GetString(iRow, "RUN_SOUND");
                    if (!szRunSound.IsNull())
                        tempData.m_arrRunSound = szRunSound.Split('|');
                    string szAttackSound = csvData.GetString(iRow, "ATTACK_SOUND");
                    if (!szAttackSound.IsNull())
                        tempData.m_arrAttackSound = szAttackSound.Split('|');
                    string szHitSound = csvData.GetString(iRow, "HIT_SOUND");
                    if (!szHitSound.IsNull())
                        tempData.m_arrHitSound = szHitSound.Split('|');
                    string szDieSound = csvData.GetString(iRow, "DIE_SOUND");
                    if (!szDieSound.IsNull())
                        tempData.m_arrDieSound = szDieSound.Split('|');

                    tempData.m_szAttackEffect = csvData.GetString(iRow, "ATTACK_EFFECT");
                    if (!tempData.m_szAttackEffect.IsNull())
                        tempData.m_szAttackEffect = string.Concat(EFFECT_PATH, tempData.m_szAttackEffect, PREFAB_EXT);
                    tempData.m_szHitEffect = csvData.GetString(iRow, "HIT_EFFECT");
                    if (!tempData.m_szHitEffect.IsNull())
                        tempData.m_szHitEffect = string.Concat(EFFECT_PATH, tempData.m_szHitEffect, PREFAB_EXT);
                    tempData.m_szDieEffect = csvData.GetString(iRow, "DIE_EFFECT");
                    if (!tempData.m_szDieEffect.IsNull())
                        tempData.m_szDieEffect = string.Concat(EFFECT_PATH, tempData.m_szDieEffect, PREFAB_EXT);

                    if (tempData.m_lstDamage == null) tempData.m_lstDamage = new List<DamageInfo>();
                    else tempData.m_lstDamage.Clear();

                    DamageInfo damageInfo = new DamageInfo();
                    damageInfo.SetDamageType(DamageInfo.ENUM_DAMAGE_TYPE.NORMAL);
                    damageInfo.SetDamageValue(tempData.m_fAttackDmg);

                    tempData.m_lstDamage.Add(damageInfo);

                    tempData.m_iSkillID = csvData.GetInt(iRow, "SKILL_ID");
                    string szSpeech = csvData.GetString(iRow, "SPEECH");
                    if (!szSpeech.IsNull())
                        tempData.m_arrSpeech = szSpeech.Split('|');

                    tempData.m_fDrainHpPerSec = csvData.GetFloat(iRow, "DRAIN_HP");
                    tempData.m_fRestoreFeverPerSec = csvData.GetFloat(iRow, "RESTORE_FEVER");

                    outdata.Add(tempData.UID, tempData);
                }
                catch (Exception e)
                {
                    Debug.LogErrorFormat("load row failed. table = [{0}], row = [{1}]\nerror = {2}", szTableData, iRow, e);
                }
            }
        }
    }
    #endregion Player Data

    #region Enemy Data
    public partial class EnemyData
    {
        static readonly string TABLE_EXT = ".xls";

        static readonly string PREFAB_PATH = "03_Prefabs/Character/Enemy/";
        static readonly string PREFAB_EXT = ".prefab";
        static readonly string ICON_PATH = "";
        static readonly string EFFECT_PATH = "03_Prefabs/Effect/";

        [MenuItem("Tools/Convert/CHARACTER/CONTENT_ENEMY")]
        public static void ConvertTable()
        {
            //< 시리얼라이즈화를 여기서 시킴
            //< 출력할 데이터 dic을만듬
            GameDataEditor.ConvertExcelTable<int, EnemyData>(ParseTable, TABLE_NAME, TABLE_EXT);
        }

        public static void ParseTable(string szTableData, out Dictionary<int, EnemyData> outdata)
        {
            outdata = new Dictionary<int, EnemyData>();

            CSVData csvData = new CSVData(szTableData);
            for (int iRow = 1; iRow < csvData.GetLineCount; ++iRow)
            {
                try
                {
                    EnemyData tempData = new EnemyData();

                    tempData.UID = csvData.GetInt(iRow, "UID");
                    tempData.m_iGrade = csvData.GetInt(iRow, "GRADE");
                    tempData.m_szName = csvData.GetString(iRow, "NAME");
                    tempData.m_szDesc = csvData.GetString(iRow, "DESC");
                    tempData.m_szMemo = csvData.GetString(iRow, "MEMO");
                    tempData.m_szPrefab = csvData.GetString(iRow, "PREFAB_PATH");
                    if (!tempData.m_szPrefab.IsNull())
                        tempData.m_szPrefab = string.Concat(PREFAB_PATH, tempData.m_szPrefab, PREFAB_EXT);
                    tempData.m_szIcon = csvData.GetString(iRow, "ICON_PATH");
                    if (!tempData.m_szIcon.IsNull())
                        tempData.m_szIcon = string.Concat(ICON_PATH, tempData.m_szIcon);

                    tempData.m_fMaxHP = csvData.GetFloat(iRow, "HP");
                    tempData.m_fMaxFever = csvData.GetFloat(iRow, "FEVER_MAX");
                    tempData.m_fAttackDmg = csvData.GetFloat(iRow, "ATTACK");
                    tempData.m_fAttackDelay = csvData.GetFloat(iRow, "ATTACK_DELAY");
                    tempData.m_fDefense = csvData.GetFloat(iRow, "DEFENSE");
                    tempData.m_fMoveSpeed = csvData.GetFloat(iRow, "MOVE_SPEED");

                    tempData.m_szHitAnimation = csvData.GetString(iRow, "HIT_ANIMATION");

                    string szRunSound = csvData.GetString(iRow, "RUN_SOUND");
                    if (!szRunSound.IsNull())
                        tempData.m_arrRunSound = szRunSound.Split('|');
                    string szAttackSound = csvData.GetString(iRow, "ATTACK_SOUND");
                    if (!szAttackSound.IsNull())
                        tempData.m_arrAttackSound = szAttackSound.Split('|');
                    string szHitSound = csvData.GetString(iRow, "HIT_SOUND");
                    if (!szHitSound.IsNull())
                        tempData.m_arrHitSound = szHitSound.Split('|');
                    string szDieSound = csvData.GetString(iRow, "DIE_SOUND");
                    if (!szDieSound.IsNull())
                        tempData.m_arrDieSound = szDieSound.Split('|');

                    tempData.m_szAttackEffect = csvData.GetString(iRow, "ATTACK_EFFECT");
                    if (!tempData.m_szAttackEffect.IsNull())
                        tempData.m_szAttackEffect = string.Concat(EFFECT_PATH, tempData.m_szAttackEffect, PREFAB_EXT);
                    tempData.m_szHitEffect = csvData.GetString(iRow, "HIT_EFFECT");
                    if (!tempData.m_szHitEffect.IsNull())
                        tempData.m_szHitEffect = string.Concat(EFFECT_PATH, tempData.m_szHitEffect, PREFAB_EXT);
                    tempData.m_szDieEffect = csvData.GetString(iRow, "DIE_EFFECT");
                    if (!tempData.m_szDieEffect.IsNull())
                        tempData.m_szDieEffect = string.Concat(EFFECT_PATH, tempData.m_szDieEffect, PREFAB_EXT);

                    if (tempData.m_lstDamage == null) tempData.m_lstDamage = new List<DamageInfo>();
                    else tempData.m_lstDamage.Clear();

                    DamageInfo damageInfo = new DamageInfo();
                    damageInfo.SetDamageType(DamageInfo.ENUM_DAMAGE_TYPE.NORMAL);
                    damageInfo.SetDamageValue(tempData.m_fAttackDmg);

                    tempData.m_lstDamage.Add(damageInfo);


                    tempData.m_eEnemyType = (ENUM_ENEMY_TYPE)csvData.GetInt(iRow, "TYPE");
                    tempData.m_fGetGas = csvData.GetFloat(iRow, "GET_GAS");
                    tempData.m_fLostGas = csvData.GetFloat(iRow, "LOST_GAS");
                    tempData.m_fGetFever = csvData.GetFloat(iRow, "GET_FEVER");
                    tempData.m_fLostFever = csvData.GetFloat(iRow, "LOST_FEVER");

                    tempData.m_fAppearDelayMin = csvData.GetFloat(iRow, "APPEAR_DELAY_MIN");
                    tempData.m_fAppearDelayMax = csvData.GetFloat(iRow, "APPEAR_DELAY_MAX");

                    outdata.Add(tempData.UID, tempData);
                }
                catch (Exception e)
                {
                    Debug.LogErrorFormat("load row failed. table = [{0}], row = [{1}]\nerror = {2}", szTableData, iRow, e);
                }
            }
        }
    }
    #endregion Enemy Data

    #region Npc Data
    public partial class NpcData
    {
        static readonly string TABLE_EXT = ".xls";

        static readonly string PREFAB_PATH = "03_Prefabs/Character/Npc/";
        static readonly string PREFAB_EXT = ".prefab";
        static readonly string ICON_PATH = "";
        static readonly string EFFECT_PATH = "03_Prefabs/Effect/";

        [MenuItem("Tools/Convert/CHARACTER/CONTENT_NPC")]
        public static void ConvertTable()
        {
            //< 시리얼라이즈화를 여기서 시킴
            //< 출력할 데이터 dic을만듬
            GameDataEditor.ConvertExcelTable<int, NpcData>(ParseTable, TABLE_NAME, TABLE_EXT);
        }

        public static void ParseTable(string szTableData, out Dictionary<int, NpcData> outdata)
        {
            outdata = new Dictionary<int, NpcData>();

            CSVData csvData = new CSVData(szTableData);
            for (int iRow = 1; iRow < csvData.GetLineCount; ++iRow)
            {
                try
                {
                    NpcData tempData = new NpcData();

                    tempData.UID = csvData.GetInt(iRow, "UID");
                    tempData.m_iGrade = csvData.GetInt(iRow, "GRADE");
                    tempData.m_szName = csvData.GetString(iRow, "NAME");
                    tempData.m_szDesc = csvData.GetString(iRow, "DESC");
                    tempData.m_szMemo = csvData.GetString(iRow, "MEMO");
                    tempData.m_szPrefab = csvData.GetString(iRow, "PREFAB_PATH");
                    if (!tempData.m_szPrefab.IsNull())
                        tempData.m_szPrefab = string.Concat(PREFAB_PATH, tempData.m_szPrefab, PREFAB_EXT);
                    tempData.m_szIcon = csvData.GetString(iRow, "ICON_PATH");
                    if (!tempData.m_szIcon.IsNull())
                        tempData.m_szIcon = string.Concat(ICON_PATH, tempData.m_szIcon);

                    tempData.m_fMaxHP = csvData.GetFloat(iRow, "HP");
                    tempData.m_fMaxFever = csvData.GetFloat(iRow, "FEVER_MAX");
                    tempData.m_fAttackDmg = csvData.GetFloat(iRow, "ATTACK");
                    tempData.m_fAttackDelay = csvData.GetFloat(iRow, "ATTACK_DELAY");
                    tempData.m_fDefense = csvData.GetFloat(iRow, "DEFENSE");
                    tempData.m_fMoveSpeed = csvData.GetFloat(iRow, "MOVE_SPEED");

                    tempData.m_szHitAnimation = csvData.GetString(iRow, "HIT_ANIMATION");

                    string szRunSound = csvData.GetString(iRow, "RUN_SOUND");
                    if (!szRunSound.IsNull())
                        tempData.m_arrRunSound = szRunSound.Split('|');
                    string szAttackSound = csvData.GetString(iRow, "ATTACK_SOUND");
                    if (!szAttackSound.IsNull())
                        tempData.m_arrAttackSound = szAttackSound.Split('|');
                    string szHitSound = csvData.GetString(iRow, "HIT_SOUND");
                    if (!szHitSound.IsNull())
                        tempData.m_arrHitSound = szHitSound.Split('|');
                    string szDieSound = csvData.GetString(iRow, "DIE_SOUND");
                    if (!szDieSound.IsNull())
                        tempData.m_arrDieSound = szDieSound.Split('|');

                    tempData.m_szAttackEffect = csvData.GetString(iRow, "ATTACK_EFFECT");
                    if (!tempData.m_szAttackEffect.IsNull())
                        tempData.m_szAttackEffect = string.Concat(EFFECT_PATH, tempData.m_szAttackEffect, PREFAB_EXT);
                    tempData.m_szHitEffect = csvData.GetString(iRow, "HIT_EFFECT");
                    if (!tempData.m_szHitEffect.IsNull())
                        tempData.m_szHitEffect = string.Concat(EFFECT_PATH, tempData.m_szHitEffect, PREFAB_EXT);
                    tempData.m_szDieEffect = csvData.GetString(iRow, "DIE_EFFECT");
                    if (!tempData.m_szDieEffect.IsNull())
                        tempData.m_szDieEffect = string.Concat(EFFECT_PATH, tempData.m_szDieEffect, PREFAB_EXT);

                    if (tempData.m_lstDamage == null) tempData.m_lstDamage = new List<DamageInfo>();
                    else tempData.m_lstDamage.Clear();

                    DamageInfo damageInfo = new DamageInfo();
                    damageInfo.SetDamageType(DamageInfo.ENUM_DAMAGE_TYPE.NORMAL);
                    damageInfo.SetDamageValue(tempData.m_fAttackDmg);

                    tempData.m_lstDamage.Add(damageInfo);


                    tempData.m_eNpcType = (ENUM_NPC_TYPE)csvData.GetInt(iRow, "TYPE");
                    tempData.m_fGetGas = csvData.GetFloat(iRow, "GET_GAS");
                    tempData.m_fLostGas = csvData.GetFloat(iRow, "LOST_GAS");
                    tempData.m_fGetFever = csvData.GetFloat(iRow, "GET_FEVER");
                    tempData.m_fLostFever = csvData.GetFloat(iRow, "LOST_FEVER");

                    tempData.m_fAppearDelayMin = csvData.GetFloat(iRow, "APPEAR_DELAY_MIN");
                    tempData.m_fAppearDelayMax = csvData.GetFloat(iRow, "APPEAR_DELAY_MAX");

                    tempData.m_fAliveGetGas = csvData.GetFloat(iRow, "ALIVE_GET_GAS");
                    tempData.m_fAliveGetFever = csvData.GetFloat(iRow, "ALIVE_GET_FEVER");

                    outdata.Add(tempData.UID, tempData);
                }
                catch (Exception e)
                {
                    Debug.LogErrorFormat("load row failed. table = [{0}], row = [{1}]\nerror = {2}", szTableData, iRow, e);
                }
            }
        }
    }
    #endregion Npc Data

    #region Skill Data
    public partial class SkillData
    {
        static readonly string TABLE_EXT = ".xls";

        static readonly string PREFAB_PATH = "03_Prefabs/Skill/";
        static readonly string PREFAB_EXT = ".prefab";

        [MenuItem("Tools/Convert/CHARACTER/CONTENT_SKILL")]
        public static void ConvertTable()
        {
            //< 시리얼라이즈화를 여기서 시킴
            //< 출력할 데이터 dic을만듬
            GameDataEditor.ConvertExcelTable<int, SkillData>(ParseTable, TABLE_NAME, TABLE_EXT);
        }

        public static void ParseTable(string szTableData, out Dictionary<int, SkillData> outdata)
        {
            outdata = new Dictionary<int, SkillData>();

            CSVData csvData = new CSVData(szTableData);
            for (int iRow = 1; iRow < csvData.GetLineCount; ++iRow)
            {
                try
                {
                    SkillData tempData = new SkillData();

                    tempData.UID = csvData.GetInt(iRow, "UID");
                    tempData.m_szName = csvData.GetString(iRow, "NAME");
                    tempData.m_szDesc = csvData.GetString(iRow, "DESC");
                    tempData.m_szMemo = csvData.GetString(iRow, "MEMO");

                    tempData.m_szStartPrefab = csvData.GetString(iRow, "START_PREFAB");
                    if (!tempData.m_szStartPrefab.IsNull())
                        tempData.m_szStartPrefab = string.Concat(PREFAB_PATH, tempData.m_szStartPrefab, PREFAB_EXT);
                    tempData.m_szEffectPrefab = csvData.GetString(iRow, "EFFECT_PREFAB");
                    if (!tempData.m_szEffectPrefab.IsNull())
                        tempData.m_szEffectPrefab = string.Concat(PREFAB_PATH, tempData.m_szEffectPrefab, PREFAB_EXT);
                    tempData.m_szEndPrefab = csvData.GetString(iRow, "END_PREFAB");
                    if (!tempData.m_szEndPrefab.IsNull())
                        tempData.m_szEndPrefab = string.Concat(PREFAB_PATH, tempData.m_szEndPrefab, PREFAB_EXT);

                    tempData.m_szStartSound = csvData.GetString(iRow, "START_SOUND");
                    tempData.m_szEffectSound = csvData.GetString(iRow, "EFFECT_SOUND");
                    tempData.m_iEffectSoundLoop = csvData.GetInt(iRow, "EFFECT_SOUND_LOOP");
                    tempData.m_szEndSound = csvData.GetString(iRow, "END_SOUND");
                    tempData.m_szHitSound = csvData.GetString(iRow, "HIT_SOUND");

                    tempData.m_fStartDelay = csvData.GetFloat(iRow, "START_DELAY");
                    tempData.m_fEndDelay = csvData.GetFloat(iRow, "END_DELAY");
                    tempData.m_fAttackDmg = csvData.GetFloat(iRow, "DAMAGE");

                    tempData.m_fSearchDelay = csvData.GetFloat(iRow, "TARGET_SEARCH_DELAY");
                    tempData.m_fSearchHeight = csvData.GetFloat(iRow, "TARGET_SEARCH_HEIGHT");

                    outdata.Add(tempData.UID, tempData);
                }
                catch (Exception e)
                {
                    Debug.LogErrorFormat("load row failed. table = [{0}], row = [{1}]\nerror = {2}", szTableData, iRow, e);
                }
            }
        }
    }
    #endregion Skill Data

    #region Condition Data
    public partial class ConditionData
    {
        static readonly string TABLE_EXT = ".xlsx";

        [MenuItem("Tools/Convert/CONTENT_CONDITION")]
        public static void ConvertTable()
        {
            //< 시리얼라이즈화를 여기서 시킴
            //< 출력할 데이터 dic을만듬
            GameDataEditor.ConvertExcelTable<int, ConditionData>(ParseTable, TABLE_NAME, TABLE_EXT);
        }

        public static void ParseTable(string szTableData, out Dictionary<int, ConditionData> outdata)
        {
            outdata = new Dictionary<int, ConditionData>();

            CSVData csvData = new CSVData(szTableData);
            for (int iRow = 1; iRow < csvData.GetLineCount; ++iRow)
            {
                try
                {
                    ConditionData tempData = new ConditionData();

                    tempData.UID = csvData.GetInt(iRow, "UID");
                    tempData.m_eCondition = (ConditionManager.ENUM_UNLOCK_CONDITION)csvData.GetInt(iRow, "UNLOCK_CONDITION");
                    tempData.m_szTitle = csvData.GetString(iRow, "TITLE");
                    tempData.m_iAmount = csvData.GetInt(iRow, "AMOUNT");
                    tempData.m_iCharId = csvData.GetInt(iRow, "UNLOCK_CHAR_ID");

                    outdata.Add(iRow, tempData);
                }
                catch (Exception e)
                {
                    Debug.LogErrorFormat("load row failed. table = [{0}], row = [{1}]\nerror = {2}", szTableData, iRow, e);
                }
            }
        }
    }
    #endregion Condition Data
}
#endif
