﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using AvoEx;

namespace Topping
{
    public class GameDataManager : DataManager<GameDataManager>
    {
        public bool IsInitialized = false;

        public static readonly string TABLE_PATH = "02_PlayData/DataTable/";
        public static readonly string TABLE_EXT = ".bytes";

        public static string GetTablePath(string tableName)
        {
            return string.Concat(TABLE_PATH, tableName, TABLE_EXT);
        }

        public override void InitOnApp()
        {
            dictTableLoaders.Clear();

            //dictTableLoaders.Add("StringData", StringData.LoadTable);
            dictTableLoaders.Add("StageData", StageData.LoadTable);
            dictTableLoaders.Add("AppearanceData", AppearanceData.LoadTable);
            dictTableLoaders.Add("PlayerData", PlayerData.LoadTable);
            dictTableLoaders.Add("EnemyData", EnemyData.LoadTable);
            dictTableLoaders.Add("NpcData", NpcData.LoadTable);
            dictTableLoaders.Add("SkillData", SkillData.LoadTable);
            dictTableLoaders.Add("ConditionData", ConditionData.LoadTable);

            IsInitialized = true;
        }

        /// <summary>
        /// 데이터 로드 후 처리 해야 할 부분들. 
        /// </summary>
        public override void PostProcessOnLoadTables()
        {
            PlayerData.PostProcessAfterTables();
        }

        void OnLogOut()
        {
            PlayerData.ClearTableData();

            LoadingScene.RequestSceneAsync(ProjectDefines.SCENE_NAME_TITLE);
        }

        protected override void OnQuit()
        {
            base.OnQuit();
            EngineManager.onLogOut -= OnLogOut;
        }

        protected override void OnAwake()
        {
            base.OnAwake();
            EngineManager.onLogOut += OnLogOut;
        }

        #region Client Save Data
        static readonly string KEY_SELECTED_LANGUAGE = "KEY_SELECTED_LANGUAGE";
        public static StringData.ENUM_LANGUAGE selectedLanguage
        {
            get
            {
                return (StringData.ENUM_LANGUAGE)EncryptPrefs.GetInt(KEY_SELECTED_LANGUAGE, (int)StringData.ENUM_LANGUAGE.NONE);
            }
            set
            {
                EncryptPrefs.SetInt(KEY_SELECTED_LANGUAGE, (int)value);
            }
        }
        #endregion Client Save Data
    }

    #region Fixed String Data
    [Serializable]
    public partial class FixedStringData : TableData<string, StringData>
    {
        private static Dictionary<string, StringData> s_dictFixedStrings = null;

        public static readonly string TABLE_NAME = "CONTENT_FIXED_STRING";

        public static bool LoadTable()
        {
            if (s_dictFixedStrings != null) // 이미 로딩 되어 있는가?
                return true; // 빌드에 포함된 스트링 테이블, 다시 로딩할 필요가 없다.

            s_dictFixedStrings = new Dictionary<string, StringData>();

            string szTableName = TABLE_NAME + StringData.languagePostfix;
            if (LoadTableFromResources(szTableName))
            {
                s_dictFixedStrings = s_dictTableData;
                return true;
            }

            return false;
        }

        public static string GetLocalizedString(string szTID)
        {
            StringData data = null;

            if (s_dictFixedStrings != null)
            {
                try
                {
                    s_dictFixedStrings.TryGetValue(szTID, out data);
                }
                catch
                {
                    return StringData.ParseEscape(szTID);
                }
            }

            if (data == null)
                return StringData.ParseEscape(szTID);
            return data.m_szText;
        }
    }
    #endregion Fixed String Data

    #region String Data
    [Serializable]
    public partial class StringData : TableData<string, StringData>
    {
        public int UID { get; private set; }
        public string m_szText { get; private set; }

        public static readonly string TABLE_NAME = "CONTENT_STRING";

        public static bool LoadTable()
        {
            return LoadTableFromBundle(GameDataManager.GetTablePath(TABLE_NAME + languagePostfix));
            //return LoadTableFromDirect(GameDataManager.GetTablePath(TABLE_NAME));
            //return LoadTableFromResources(GameDataManager.GetTablePath(TABLE_NAME));
        }

        public static string GetLocalizedString(string szTID)
        {
            if (szTID.IsNull())
                return "";

            StringData stringData = GetTableData(szTID);
            if (stringData == null)
                return ParseEscape(szTID);
            return stringData.m_szText;
        }

        public static string FormatLocalizedString(string szTID, params object[] args)
        {
            StringData stringData = GetTableData(szTID);
            try
            {
                if (stringData == null)
                    return string.Format(szTID, args);
                return string.Format(stringData.m_szText, args);
            }
            catch
            {
                return szTID;
            }
        }

        public static string ParseEscape(string szText)
        {
            return szText.Replace("\\n", Environment.NewLine);
        }

        #region LANGUAGE
        public enum ENUM_LANGUAGE
        {
            NONE = -1,
            KOREAN = 0,
            ENGLISH,
            FRENCH,
            GERMAN,
            SPANISH,
            SPANISH_CASTIYA,
            ITALIAN,
            PORTUGUESS,
            CHINESE_SIMPLIFIED,
            CHINESE_TRADITIONAL,
            JAPANESE,
        }

        public delegate void EventOnLanguage(ENUM_LANGUAGE eLanguage);
        public static EventOnLanguage s_onLanguage = null;

        public static ENUM_LANGUAGE currentLanguage
        {
            get
            {
                ENUM_LANGUAGE curLang = GameDataManager.selectedLanguage;
                if (curLang == ENUM_LANGUAGE.NONE) // 앱 설치 후 최초 실행..
                {
                    switch (Application.systemLanguage) // 초기 설정 언어 판단..
                    {
                        case SystemLanguage.Korean:
                            curLang = ENUM_LANGUAGE.KOREAN;
                            break;

                        case SystemLanguage.English:
                        default:
                            curLang = ENUM_LANGUAGE.ENGLISH;
                            break;
                    }
                    GameDataManager.selectedLanguage = curLang;
                }

                return curLang;
            }
            set
            {
                if (GameDataManager.selectedLanguage == value)
                    return;
                GameDataManager.selectedLanguage = value;
                LoadTable();

                if (s_onLanguage != null)
                    s_onLanguage(value);
            }
        }

        static readonly string KOREAN_COL_NAME = "TEXT_KOR";
        static readonly string ENGLISH_COL_NAME = "TEXT_ENG";
        static readonly string FRENCH_COL_NAME = "TEXT_FRA";
        static readonly string GERMAN_COL_NAME = "TEXT_DEU";
        static readonly string SPANISH_COL_NAME = "TEXT_ESP";
        static readonly string SPANISH_CASTIYA_COL_NAME = "TEXT_ESP_CASTIYA";
        static readonly string ITALIAN_COL_NAME = "TEXT_ITA";
        static readonly string PORTUGUESS_COL_NAME = "TEXT_POR";
        static readonly string CHINESE_SIMPLIFIED_COL_NAME = "TEXT_CHI_CAN";
        static readonly string CHINESE_TRADITIONAL_COL_NAME = "TEXT_CHI_BUN";
        static readonly string JAPANESE_COL_NAME = "TEXT_JAP";
        private static readonly string[] s_arLangPostfixs = {
            KOREAN_COL_NAME, ENGLISH_COL_NAME, FRENCH_COL_NAME, GERMAN_COL_NAME,
            SPANISH_COL_NAME, SPANISH_CASTIYA_COL_NAME, ITALIAN_COL_NAME, PORTUGUESS_COL_NAME,
            CHINESE_SIMPLIFIED_COL_NAME, CHINESE_TRADITIONAL_COL_NAME, JAPANESE_COL_NAME
        };

        public static string languagePostfix
        {
            get
            {
                int iIndex = (int)currentLanguage;
                if (iIndex < 0 || iIndex >= s_arLangPostfixs.Length)
                    return ENGLISH_COL_NAME;

                return s_arLangPostfixs[iIndex];
            }
        }
        #endregion LANGUAGE
    }
    #endregion String Data

    #region Stage Data
    [Serializable]
    public partial class StageData : TableData<int, StageData>
    {
        public int UID { get; private set; }
        public int m_iStageNo { get; private set; }
        public string m_szName { get; private set; }
        public string m_szDesc { get; private set; }
        public float m_fStartDistance { get; private set; }

        public string m_szBgPath { get; private set; }
        public string m_szTrainPath { get; private set; }

        public List<int> m_lstAppearNpc { get; private set; }
        public List<int> m_lstAppearEnemy { get; private set; }

        public float m_fSpawnStartDelay { get; private set; }

        // 빌드 에러를 피하기 위해 이 곳에 지정. 
        public static readonly string TABLE_NAME = "CONTENT_STAGE";

        /// <summary>
        /// 데이터 로드 후 처리 해야 할 부분들. 
        /// </summary>
        public static void PostProcessAfterTables()
        {

        }

        public static bool LoadTable()
        {
            return LoadTableFromBundle(GameDataManager.GetTablePath(TABLE_NAME));
            //return LoadTableFromDirect(GameDataManager.GetTablePath(TABLE_NAME));
            //return LoadTableFromResources(GameDataManager.GetTablePath(TABLE_NAME));
        }
    }
    #endregion Stage Data

    #region Appearance Data
    [Serializable]
    public partial class AppearanceData : TableData<int, AppearanceData>
    {
        public int UID { get; private set; }
        public int m_iStageId { get; private set; }
        public int m_iSpeedUpDist { get; private set; }
        public float m_fSpeedUpAdd { get; private set; }
        public float m_fEnemyMoveSpeedAdd { get; private set; }

        // 빌드 에러를 피하기 위해 이 곳에 지정. 
        public static readonly string TABLE_NAME = "CONTENT_APPEARANCE";

        /// <summary>
        /// 데이터 로드 후 처리 해야 할 부분들. 
        /// </summary>
        public static void PostProcessAfterTables()
        {

        }

        public static bool LoadTable()
        {
            return LoadTableFromBundle(GameDataManager.GetTablePath(TABLE_NAME));
            //return LoadTableFromDirect(GameDataManager.GetTablePath(TABLE_NAME));
            //return LoadTableFromResources(GameDataManager.GetTablePath(TABLE_NAME));
        }
    }
    #endregion Appearance Data

    #region Player Data
    /// <summary>
    /// 공통 데이터. 
    /// </summary>
    public interface IPlayerData
    {
        int UID { get; }
        int m_iGrade { get; }
        string m_szName { get; }
        string m_szDesc { get; }
        string m_szMemo { get; }
        string m_szPrefab { get; }
        string m_szIcon { get; }

        float m_fMaxHP { get; }
        float m_fMaxFever { get; }
        float m_fFeverDuration { get; }
        float m_fAttackDmg { get; }
        float m_fAttackDelay { get; }
        float m_fDefense { get; }
        float m_fMoveSpeed { get; }

        string m_szHitAnimation { get; }

        string[] m_arrRunSound { get; }
        string[] m_arrAttackSound { get; }
        string[] m_arrHitSound { get; }
        string[] m_arrDieSound { get; }

        string m_szAttackEffect { get; }
        string m_szHitEffect { get; }
        string m_szDieEffect { get; }

        List<DamageInfo> m_lstDamage { get; }
        List<BuffInfo> m_lstBuff { get; }
    }

    [Serializable]
    public partial class PlayerData : TableData<int, PlayerData>, IPlayerData
    {
        public int UID { get; private set; }
        public int m_iGrade { get; private set; }
        public string m_szName { get; private set; }
        public string m_szDesc { get; private set; }
        public string m_szMemo { get; private set; }
        public string m_szPrefab { get; private set; }
        public string m_szIcon { get; private set; }

        public float m_fMaxHP { get; private set; }
        public float m_fMaxFever { get; private set; }
        public float m_fFeverDuration { get; private set; }
        public float m_fAttackDmg { get; private set; }
        public float m_fAttackDelay { get; private set; }
        public float m_fDefense { get; private set; }
        public float m_fMoveSpeed { get; private set; }

        public string m_szHitAnimation { get; private set; }

        public string[] m_arrRunSound { get; private set; }
        public string[] m_arrAttackSound { get; private set; }
        public string[] m_arrHitSound { get; private set; }
        public string[] m_arrDieSound { get; private set; }

        public string m_szAttackEffect { get; private set; }
        public string m_szHitEffect { get; private set; }
        public string m_szDieEffect { get; private set; }

        public List<DamageInfo> m_lstDamage { get; private set; }
        public List<BuffInfo> m_lstBuff { get; private set; }

        public int m_iSkillID { get; private set; }
        public string[] m_arrSpeech { get; private set; }

        public float m_fDrainHpPerSec { get; private set; }
        public float m_fRestoreFeverPerSec { get; private set; }

        // 빌드 에러를 피하기 위해 이 곳에 지정. 
        public static readonly string TABLE_NAME = "CONTENT_PLAYER";

        /// <summary>
        /// 데이터 로드 후 처리 해야 할 부분들. 
        /// </summary>
        public static void PostProcessAfterTables()
        {

        }

        public static bool LoadTable()
        {
            return LoadTableFromBundle(GameDataManager.GetTablePath(TABLE_NAME));
            //return LoadTableFromDirect(GameDataManager.GetTablePath(TABLE_NAME));
            //return LoadTableFromResources(GameDataManager.GetTablePath(TABLE_NAME));
        }
    }
    #endregion Player Data

    #region Enemy Data
    [Serializable]
    public partial class EnemyData : TableData<int, EnemyData>, IPlayerData
    {
        public enum ENUM_ENEMY_TYPE
        {
            NORMAL,
        }

        public int UID { get; private set; }
        public int m_iGrade { get; private set; }
        public string m_szName { get; private set; }
        public string m_szDesc { get; private set; }
        public string m_szMemo { get; private set; }
        public string m_szPrefab { get; private set; }
        public string m_szIcon { get; private set; }

        public float m_fMaxHP { get; private set; }
        public float m_fMaxFever { get; private set; }
        public float m_fFeverDuration { get; private set; }
        public float m_fAttackDmg { get; private set; }
        public float m_fAttackDelay { get; private set; }
        public float m_fDefense { get; private set; }
        public float m_fMoveSpeed { get; private set; }

        public string m_szHitAnimation { get; private set; }

        public string[] m_arrRunSound { get; private set; }
        public string[] m_arrAttackSound { get; private set; }
        public string[] m_arrHitSound { get; private set; }
        public string[] m_arrDieSound { get; private set; }

        public string m_szAttackEffect { get; private set; }
        public string m_szHitEffect { get; private set; }
        public string m_szDieEffect { get; private set; }


        public ENUM_ENEMY_TYPE m_eEnemyType { get; private set; }
        public float m_fGetGas { get; private set; }
        public float m_fLostGas { get; private set; }
        public float m_fGetFever { get; private set; }
        public float m_fLostFever { get; private set; }

        public float m_fAppearDelayMin { get; private set; }
        public float m_fAppearDelayMax { get; private set; }

        public List<DamageInfo> m_lstDamage { get; private set; }
        public List<BuffInfo> m_lstBuff { get; private set; }

        // 빌드 에러를 피하기 위해 이 곳에 지정. 
        public static readonly string TABLE_NAME = "CONTENT_ENEMY";

        /// <summary>
        /// 데이터 로드 후 처리 해야 할 부분들. 
        /// </summary>
        public static void PostProcessAfterTables()
        {

        }

        public static bool LoadTable()
        {
            return LoadTableFromBundle(GameDataManager.GetTablePath(TABLE_NAME));
            //return LoadTableFromDirect(GameDataManager.GetTablePath(TABLE_NAME));
            //return LoadTableFromResources(GameDataManager.GetTablePath(TABLE_NAME));
        }
    }
    #endregion Enemy Data

    #region Npc Data
    [Serializable]
    public partial class NpcData : TableData<int, NpcData>, IPlayerData
    {
        public enum ENUM_NPC_TYPE
        {
            NORMAL,

            SHERIFF,
            CITIZEN,
        }

        public int UID { get; private set; }
        public int m_iGrade { get; private set; }
        public string m_szName { get; private set; }
        public string m_szDesc { get; private set; }
        public string m_szMemo { get; private set; }
        public string m_szPrefab { get; private set; }
        public string m_szIcon { get; private set; }

        public float m_fMaxHP { get; private set; }
        public float m_fMaxFever { get; private set; }
        public float m_fFeverDuration { get; private set; }
        public float m_fAttackDmg { get; private set; }
        public float m_fAttackDelay { get; private set; }
        public float m_fDefense { get; private set; }
        public float m_fMoveSpeed { get; private set; }

        public string m_szHitAnimation { get; private set; }

        public string[] m_arrRunSound { get; private set; }
        public string[] m_arrAttackSound { get; private set; }
        public string[] m_arrHitSound { get; private set; }
        public string[] m_arrDieSound { get; private set; }

        public string m_szAttackEffect { get; private set; }
        public string m_szHitEffect { get; private set; }
        public string m_szDieEffect { get; private set; }


        public ENUM_NPC_TYPE m_eNpcType { get; private set; }
        public float m_fGetGas { get; private set; }
        public float m_fLostGas { get; private set; }
        public float m_fGetFever { get; private set; }
        public float m_fLostFever { get; private set; }

        public float m_fAppearDelayMin { get; private set; }
        public float m_fAppearDelayMax { get; private set; }

        public float m_fAliveGetGas { get; private set; }
        public float m_fAliveGetFever { get; private set; }

        public List<DamageInfo> m_lstDamage { get; private set; }
        public List<BuffInfo> m_lstBuff { get; private set; }

        // 빌드 에러를 피하기 위해 이 곳에 지정. 
        public static readonly string TABLE_NAME = "CONTENT_NPC";

        /// <summary>
        /// 데이터 로드 후 처리 해야 할 부분들. 
        /// </summary>
        public static void PostProcessAfterTables()
        {

        }

        public static bool LoadTable()
        {
            return LoadTableFromBundle(GameDataManager.GetTablePath(TABLE_NAME));
            //return LoadTableFromDirect(GameDataManager.GetTablePath(TABLE_NAME));
            //return LoadTableFromResources(GameDataManager.GetTablePath(TABLE_NAME));
        }
    }
    #endregion Npc Data

    #region Skill Data
    [Serializable]
    public partial class SkillData : TableData<int, SkillData>
    {
        public int UID { get; private set; }
        public string m_szName { get; private set; }
        public string m_szDesc { get; private set; }
        public string m_szMemo { get; private set; }

        public string m_szStartPrefab { get; private set; }
        public string m_szEffectPrefab { get; private set; }
        public string m_szEndPrefab { get; private set; }

        public string m_szStartSound { get; private set; }
        public string m_szEffectSound { get; private set; }
        public int m_iEffectSoundLoop { get; private set; }
        public string m_szEndSound { get; private set; }
        public string m_szHitSound { get; private set; }

        public float m_fStartDelay { get; private set; }
        public float m_fEndDelay { get; private set; }
        public float m_fAttackDmg { get; private set; }

        public float m_fSearchDelay { get; private set; }
        public float m_fSearchHeight { get; private set; }

        // 빌드 에러를 피하기 위해 이 곳에 지정. 
        public static readonly string TABLE_NAME = "CONTENT_SKILL";

        /// <summary>
        /// 데이터 로드 후 처리 해야 할 부분들. 
        /// </summary>
        public static void PostProcessAfterTables()
        {

        }

        public static bool LoadTable()
        {
            return LoadTableFromBundle(GameDataManager.GetTablePath(TABLE_NAME));
            //return LoadTableFromDirect(GameDataManager.GetTablePath(TABLE_NAME));
            //return LoadTableFromResources(GameDataManager.GetTablePath(TABLE_NAME));
        }

        public bool IsHaveSkill()
        {
            return !m_szStartPrefab.IsNull() && !m_szEffectPrefab.IsNull();
        }
    }
    #endregion Skill Data

    #region Condition Data
    [Serializable]
    public partial class ConditionData : TableData<int, ConditionData>
    {
        public int UID { get; private set; }
        public ConditionManager.ENUM_UNLOCK_CONDITION m_eCondition { get; private set; }
        public string m_szTitle { get; private set; }
        public int m_iAmount { get; private set; }
        public int m_iCharId { get; private set; }

        // 빌드 에러를 피하기 위해 이 곳에 지정. 
        public static readonly string TABLE_NAME = "CONTENT_CONDITION";

        /// <summary>
        /// 데이터 로드 후 처리 해야 할 부분들. 
        /// </summary>
        public static void PostProcessAfterTables()
        {

        }

        public static bool LoadTable()
        {
            return LoadTableFromBundle(GameDataManager.GetTablePath(TABLE_NAME));
            //return LoadTableFromDirect(GameDataManager.GetTablePath(TABLE_NAME));
            //return LoadTableFromResources(GameDataManager.GetTablePath(TABLE_NAME));
        }

        public static ConditionData GetConditionData(int iCharId)
        {
            ConditionData data = null;

            var itorData = dataDictionary.GetEnumerator();
            while (itorData.MoveNext())
            {
                if (itorData.Current.Value.m_iCharId == iCharId)
                {
                    data = itorData.Current.Value;
                    break;
                }
            }

            return data;
        }

        public static ConditionData GetConditionData(ConditionManager.ENUM_UNLOCK_CONDITION eType)
        {
            ConditionData data = null;

            var itorData = dataDictionary.GetEnumerator();
            while (itorData.MoveNext())
            {
                if (itorData.Current.Value.m_eCondition == eType)
                {
                    data = itorData.Current.Value;
                    break;
                }
            }

            return data;
        }
    }
    #endregion Condition Data
}