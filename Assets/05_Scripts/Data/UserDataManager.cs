﻿using System;
using System.Text;
using System.Collections.Generic;
using UnityEngine;
using AvoEx.ObservableType;

namespace Topping
{
    #region UserData Class
    public static class UserData
    {
        #region Private Variables
        private static string _szPathDir = null;
        private static string _szFileName = null;
        private static string _szSavePath = null;


        // 플레이어 점수. (거리)
        private static ObEncFloat _fScore = new ObEncFloat();
        // 좀비 킬 카운트. 
        private static ObEncInt _iKillCount = new ObEncInt();
        // 콤보 수. 
        private static ObEncInt _iComboCount = new ObEncInt();
        // 단일 게임 맥스 콤보 수. 
        private static ObEncInt _iMaxComboCount = new ObEncInt();
        // 베스트 점수. 
        private static ObEncFloat _fBestScore = new ObEncFloat();
        // 베스트 킬 카운트. 
        private static ObEncInt _iBestKillCount = new ObEncInt();
        // 베스트 NPC 킬 카운트. 
        private static ObEncInt _iBestKillNpcCount = new ObEncInt();
        // 베스트 콤보 횟수. 
        private static ObEncInt _iBestComboCount = new ObEncInt();

        // 언락된 캐릭터 ID 리스트. 
        private static List<ObEncInt> _lstUnLockCharId = new List<ObEncInt>();
        // 현재 선택된 캐릭터 아이디. 
        private static int _iSelectedCharId = 0;

        ////////// 여기부터 통계용 //////////

        // 플레이어 점수. (누적)
        private static ObEncDouble _dScoreAccum = new ObEncDouble();
        // 좀비 킬 카운트. (누적)
        private static ObEncLong _lKillCountAccum = new ObEncLong();
        // 게임 오버 횟수. 
        private static ObEncInt _iGameOverCount = new ObEncInt();
        // 출석 체크 날짜. 
        private static ObEncInt _iAttendDateTime = new ObEncInt();
        // 출석 체크 횟수. 
        private static ObEncInt _iAttendCount = new ObEncInt();
        // 일요일 접속 횟수. 
        private static ObEncInt _iAttendSundayCount = new ObEncInt();
        // 플레이 시간. (초, 일시정지 노카운트)
        private static ObEncInt _iPlayTime = new ObEncInt();
        // 플레이 누적 시간. (초, 일시정지 노카운트)
        private static ObEncLong _lPlayTimeAccum = new ObEncLong();
        // 보안관 또는 시민 처치 수. 
        private static ObEncInt _iKillNpcCount = new ObEncInt();
        // 보안관 또는 시민 처치 수. (누적)
        private static ObEncLong _lKillNpcCountAccum = new ObEncLong();
        // 열차에 탑승한 좀비 수.
        private static ObEncInt _iZombieOnTrain = new ObEncInt();
        // 열차에 탑승한 좀비 수. (누적)
        private static ObEncLong _lZombieOnTrainAccum = new ObEncLong();

        // 공격 터치 횟수. 
        private static ObEncInt _iAttackCount = new ObEncInt();
        // 공격 터치 횟수. (누적)
        private static ObEncLong _lAttackCountAccum = new ObEncLong();
        // 피버 사용 횟수. 
        private static ObEncInt _iFeverCount = new ObEncInt();
        // 피버 사용 횟수. (누적)
        private static ObEncLong _lFeverCountAccum = new ObEncLong();

        // 광고 시청 횟수. 
        private static ObEncInt _iWatchAdCount = new ObEncInt();
        #endregion Private Variables

        #region Properties
        public static string PathDirectory
        {
            get
            {
                if (_szPathDir.IsNull())
                {
                    _szPathDir = string.Concat(Application.persistentDataPath, "/Mem/");
                }
                return _szPathDir;
            }
        }

        public static string FakeUserName
        {
            get
            {
                // SaveFile 네이밍. 
                UTF8Encoding encoder = new UTF8Encoding();
                // 디바이스 ID 에서 10자리 추출. 
                string szUid = SystemInfo.deviceUniqueIdentifier.Substring(3, Math.Min(10, SystemInfo.deviceUniqueIdentifier.Length));
                // Base64 인코딩. 
                string szEncUid = Convert.ToBase64String(encoder.GetBytes(szUid));
                // 문자열 변환. 
                string szSaveFileName = szEncUid.Replace("=", "").Replace("+", "");
                // 페이크 문자열 추가. 
                return string.Concat("0", szSaveFileName.ToLower());
            }
        }

        public static string FileName
        {
            get
            {
                if (_szFileName.IsNull())
                {
                    _szFileName = string.Concat(FakeUserName, ".dat");
                }
                return _szFileName;
            }
        }

        public static string SavePath
        {
            get
            {
                if (_szSavePath.IsNull())
                {
                    _szSavePath = string.Concat(PathDirectory, FileName);
                }

                return _szSavePath;
            }
        }


        public static float Score { get { float value = _fScore.Value; return value; } }
        public static int KillCount { get { int value = _iKillCount.Value; return value; } }
        public static float BestScore { get { float value = _fBestScore.Value; return value; } }
        public static int BestKillCount { get { int value = _iBestKillCount.Value; return value; } }
        public static int ComboCount { get { int value = _iComboCount.Value; return value; } }
        public static int MaxComboCount { get { int value = _iMaxComboCount.Value; return value; } }
        public static int BestComboCount { get { int value = _iBestComboCount.Value; return value; } }

        public static int ListUnLockCharCount { get { return _lstUnLockCharId.Count; } }
        public static List<int> ListUnLockCharId
        {
            get
            {
                List<int> list = null;

                if (_lstUnLockCharId != null)
                {
                    list = new List<int>();
                    for (int i = 0, cnt = _lstUnLockCharId.Count; i < cnt; i++)
                    {
                        list.Add(_lstUnLockCharId[i].Value);
                    }
                }

                return list;
            }
        }
        public static int SelectedCharId { get { return _iSelectedCharId; } }

        ////////// 여기부터 통계용 //////////

        public static double ScoreAccum { get { return _dScoreAccum.Value; } }
        public static long KillCountAccum { get { return _lKillCountAccum.Value; } }
        public static int GameOverCount { get { return _iGameOverCount.Value; } }
        public static int AttendDateTime { get { return _iAttendDateTime.Value; } }
        public static int AttendCount { get { return _iAttendCount.Value; } }
        public static int AttendSundayCount { get { return _iAttendSundayCount.Value; } }
        public static int PlayTime { get { return _iPlayTime.Value; } }
        public static long PlayTimeAccum { get { return _lPlayTimeAccum.Value; } }
        public static int KillNpcCount { get { return _iKillNpcCount.Value; } }
        public static long KillNpcCountAccum { get { return _lKillNpcCountAccum.Value; } }
        public static int ZombieOnTrain { get { return _iZombieOnTrain.Value; } }
        public static long ZombieOnTrainAccum { get { return _lZombieOnTrainAccum.Value; } }

        public static int AttackCount { get { return _iAttackCount.Value; } }
        public static long AttackCountAccum { get { return _lAttackCountAccum.Value; } }
        public static int FeverCount { get { return _iFeverCount.Value; } }
        public static long FeverCountAccum { get { return _lFeverCountAccum.Value; } }

        public static int WatchAdCount { get { return _iWatchAdCount.Value; } }
        #endregion Properties

        #region Game Methods
        static UserData()
        {
            _fScore = new ObEncFloat();
            _iKillCount = new ObEncInt();
            _fBestScore = new ObEncFloat();
            _iBestKillCount = new ObEncInt();
            _iComboCount = new ObEncInt();
            _iMaxComboCount = new ObEncInt();
            _iBestComboCount = new ObEncInt();

            if (_lstUnLockCharId == null) _lstUnLockCharId = new List<ObEncInt>();
            else _lstUnLockCharId.Clear();

            _dScoreAccum = new ObEncDouble();
            _lKillCountAccum = new ObEncLong();
            _iGameOverCount = new ObEncInt();
            _iAttendDateTime = new ObEncInt();
            _iAttendCount = new ObEncInt();
            _iAttendSundayCount = new ObEncInt();
            _iPlayTime = new ObEncInt();
            _lPlayTimeAccum = new ObEncLong();
            _iKillNpcCount = new ObEncInt();
            _lKillNpcCountAccum = new ObEncLong();
            _iZombieOnTrain = new ObEncInt();
            _lZombieOnTrainAccum = new ObEncLong();

            _iAttackCount = new ObEncInt();
            _lAttackCountAccum = new ObEncLong();
            _iFeverCount = new ObEncInt();
            _lFeverCountAccum = new ObEncLong();

            _iWatchAdCount = new ObEncInt();

            _CheckBasicData();
        }

        public static void CopyFrom(UserSaveData userSaveData)
        {
            if (userSaveData == null)
                return;

            _fScore.Value = userSaveData.m_fScore;
            _iKillCount.Value = userSaveData.m_iKillCount;
            _fBestScore.Value = userSaveData.m_fBestScore;
            _iBestKillCount.Value = userSaveData.m_iBestKillCount;
            _iBestKillNpcCount.Value = userSaveData.m_iBestKillNpcCount;
            _iComboCount.Value = userSaveData.m_iComboCount;
            _iMaxComboCount.Value = userSaveData.m_iMaxComboCount;
            _iBestComboCount.Value = userSaveData.m_iBestComboCount;

            if (_lstUnLockCharId == null) _lstUnLockCharId = new List<ObEncInt>();
            else _lstUnLockCharId.Clear();

            for (int i = 0, cnt = userSaveData.m_lstUnLockCharId.Count; i < cnt; i++)
            {
                _lstUnLockCharId.Add(new ObEncInt(userSaveData.m_lstUnLockCharId[i]));
            }

            _iSelectedCharId = userSaveData.m_iSelectedCharId;


            _dScoreAccum.Value = userSaveData.m_dScoreAccum;
            _lKillCountAccum.Value = userSaveData.m_lKillCountAccum;
            _iGameOverCount.Value = userSaveData.m_iGameOverCount;
            _iAttendDateTime.Value = userSaveData.m_iAttendDateTime;
            _iAttendCount.Value = userSaveData.m_iAttendCount;
            _iAttendSundayCount.Value = userSaveData.m_iAttendSundayCount;
            _iPlayTime.Value = userSaveData.m_iPlayTime;
            _lPlayTimeAccum.Value = userSaveData.m_lPlayTimeAccum;
            _iKillNpcCount.Value = userSaveData.m_iKillNpcCount;
            _lKillNpcCountAccum.Value = userSaveData.m_lKillNpcCountAccum;
            _iZombieOnTrain.Value = userSaveData.m_iZombieOnTrain;
            _lZombieOnTrainAccum.Value = userSaveData.m_lZombieOnTrainAccum;

            _iAttackCount.Value = userSaveData.m_iAttackCount;
            _lAttackCountAccum.Value = userSaveData.m_lAttackCountAccum;
            _iFeverCount.Value = userSaveData.m_iFeverCount;
            _lFeverCountAccum.Value = userSaveData.m_lFeverCountAccum;

            _iWatchAdCount.Value = userSaveData.m_iWatchAdCount;

            _CheckBasicData();
        }

        // 기본 데이터 체크. 
        private static void _CheckBasicData()
        {
            // 획득 캐릭터. 
            if (_lstUnLockCharId.Count < 1)
                _lstUnLockCharId.Add(new ObEncInt(ProjectDefines.BASE_PLAYER_ID));

            // 현재 사용중인 캐릭터. 
            if (_iSelectedCharId <= 0)
                _iSelectedCharId = ProjectDefines.BASE_PLAYER_ID;
        }

        /// <summary>
        /// 데이터 저장. 
        /// </summary>
        public static bool Save()
        {
            bool bResult = false;

            if (SavePath.IsNull())
            {
                Debug.LogError("Error!! Can't save user data. path is null..");
                return bResult;
            }

            try
            {
                // 시리얼라이즈. 
                UserSaveData saveData = new UserSaveData();
                saveData.CopyUserData();
                bResult = Serializer.EncryptSerialize(SavePath, ref saveData);
            }
            catch
            {
                bResult = false;
                Debug.LogError("Error!! Can't save user data..");
            }

            return bResult;
        }

        /// <summary>
        /// 데이터 로드. 
        /// </summary>
        /// <returns></returns>
        public static bool Load()
        {
            bool bResult = false;
            try
            {
                // 디시리얼라이즈. 
                UserSaveData saveData = Serializer.DecryptDeSerialize<UserSaveData>(SavePath);
                if (saveData != null)
                {
                    UserData.CopyFrom(saveData);
                    CheckBestScore();
                    CheckBestKillCount();
                    CheckBestKillNpcCount();
                    CheckBestComboCount();
                    bResult = true;
                }
            }
            catch
            {
                bResult = false;
                Debug.LogError("Error!! Can't load user data..");
            }

            return bResult;
        }


        /// <summary>
        /// 점수 설정. 
        /// </summary>
        public static void SetScore(float fValue) { _fScore.Value = fValue; }

        /// <summary>
        /// 점수 합산. 
        /// </summary>
        public static void AddScore(float fValue) { _fScore.Value += fValue; }

        /// <summary>
        /// 베스트 스코어 체크. 
        /// </summary>
        public static void CheckBestScore()
        {
            if (_fBestScore.Value < _fScore.Value)
            {
                _fBestScore.Value = _fScore.Value;
                GPGSManager.SendLeader(GPGSManager.ENUM_GPGS_TYPE.LEADER_HIGH_SCORE, Convert.ToInt64(_fBestScore.Value));
            }
        }

        /// <summary>
        /// 좀비 킬 설정. 
        /// </summary>
        public static void SetKillCount(int iValue) { _iKillCount.Value = iValue; }

        /// <summary>
        /// 좀비 킬 합산. 
        /// </summary>
        public static void AddKillCount(int iValue) { _iKillCount.Value += iValue; }

        /// <summary>
        /// 베스트 좀비 킬 체크. 
        /// </summary>
        public static void CheckBestKillCount()
        {
            if (_iBestKillCount.Value < _iKillCount.Value)
                _iBestKillCount.Value = _iKillCount.Value;
        }

        /// <summary>
        /// NPC 킬 설정. 
        /// </summary>
        public static void SetKillNpcCount(int iValue) { _iKillNpcCount.Value = iValue; }

        /// <summary>
        /// NPC 킬 합산. 
        /// </summary>
        public static void AddKillNpcCount(int iValue) { _iKillNpcCount.Value += iValue; }

        /// <summary>
        /// 베스트 NPC 킬 체크. 
        /// </summary>
        public static void CheckBestKillNpcCount()
        {
            if (_iBestKillNpcCount.Value < _iKillNpcCount.Value)
                _iBestKillNpcCount.Value = _iKillNpcCount.Value;
        }

        /// <summary>
        /// 콤보 수 설정. 
        /// </summary>
        public static void SetComboCount(int iValue, bool bSetMax = false)
        {
            _iComboCount.Value = iValue;

            if (bSetMax)
                _iMaxComboCount.Value = iValue;
        }

        /// <summary>
        /// 콤보 수 합산. 
        /// </summary>
        public static void AddComboCount(int iValue)
        {
            _iComboCount.Value += iValue;

            if (_iMaxComboCount.Value < _iComboCount.Value)
                _iMaxComboCount.Value = _iComboCount.Value;
        }

        /// <summary>
        /// 베스트 콤보 수 체크. 
        /// </summary>
        public static void CheckBestComboCount()
        {
            if (_iBestComboCount.Value < _iMaxComboCount.Value)
                _iBestComboCount.Value = _iMaxComboCount.Value;
        }

        /// <summary>
        /// 게임 오버 횟수 합산. 
        /// </summary>
        public static void AddGameOverCount(int iValue) { _iGameOverCount.Value += iValue; }

        /// <summary>
        /// 플레이 시간 합산. 
        /// </summary>
        public static void AddPlayTime(int iValue)
        {
            _lPlayTimeAccum.Value += iValue;

            if (_iPlayTime.Value < iValue)
                _iPlayTime.Value = iValue;
        }

        /// <summary>
        /// 좀비 탑승 합산. 
        /// </summary>
        public static void AddZombieOnTrain(int iValue)
        {
            _lZombieOnTrainAccum.Value += iValue;

            if (_iZombieOnTrain.Value < iValue)
                _iZombieOnTrain.Value = iValue;
        }

        /// <summary>
        /// 공격 횟수 합산. 
        /// </summary>
        public static void AddAttackCount(int iValue)
        {
            _lAttackCountAccum.Value += iValue;

            if (_iAttackCount.Value < iValue)
                _iAttackCount.Value = iValue;
        }

        /// <summary>
        /// 피버 사용 횟수 합산. 
        /// </summary>
        public static void AddFeverCount(int iValue)
        {
            _lFeverCountAccum.Value += iValue;

            if (_iFeverCount.Value < iValue)
                _iFeverCount.Value = iValue;
        }

        /// <summary>
        /// 광고 시청 횟수 합산. 
        /// </summary>
        public static void AddWatchAdCount(int iValue) { _iWatchAdCount.Value += iValue; }


        /// <summary>
        /// 출석 체크. 
        /// </summary>
        public static void CheckUserAttend()
        {
            DateTime dtLastAttendDate = Utilities.ConvertTimeToDate(_iAttendDateTime);
            DateTime dtNextDate = dtLastAttendDate.AddDays(1);
            DateTime dtNextDateStart = new DateTime(dtNextDate.Year, dtNextDate.Month, dtNextDate.Day, 0, 0, 0);

            // 마지막 출석일 다음날 이후라면 출석체크. 
            if (dtNextDateStart <= DateTime.Now)
            {
                _iAttendDateTime.Value = Utilities.ConvertDateToTime(DateTime.Now);
                _iAttendCount.Value++;

                // 일요일 출석 체크. 
                if (DateTime.Now.DayOfWeek.Equals(DayOfWeek.Sunday))
                    _iAttendSundayCount.Value += 1;

                Save();
            }

        }

        /// <summary>
        /// 누적 값 축적. (결과화면에서 호출)
        /// </summary>
        public static void AddAccumulationValue()
        {
            _dScoreAccum.Value += _fScore;
            _lKillCountAccum.Value += _iKillCount;
            _lKillNpcCountAccum.Value += _iKillNpcCount;
        }

        /// <summary>
        /// 캐릭터 추가. 
        /// </summary>
        public static bool AddCharacter(int iCharId, bool bSave = true)
        {
            bool bResult = false;

            if (iCharId > 0 && !ListUnLockCharId.Contains(iCharId))
            {
                _lstUnLockCharId.Add(new ObEncInt(iCharId));
                if (bSave) Save();
                bResult = true;
            }

            return bResult;
        }

        /// <summary>
        /// 캐릭터 삭제. 
        /// </summary>
        public static bool RemoveCharacter(int iCharId, bool bSave = true)
        {
            bool bResult = false;

            int iIdx = ListUnLockCharId.IndexOf(iCharId);
            if (iIdx > -1)
            {
                _lstUnLockCharId.RemoveAt(iIdx);
                if (bSave) Save();
                bResult = true;
            }

            return bResult;
        }

        /// <summary>
        /// 모든 캐릭터 해금. 
        /// </summary>
        public static void UnlockAllCharacter(bool bSave = true)
        {
            _lstUnLockCharId.Clear();

            var itorPlayerData = PlayerData.dataDictionary.GetEnumerator();
            while (itorPlayerData.MoveNext())
            {
                if (itorPlayerData.Current.Value == null)
                    return;

                _lstUnLockCharId.Add(new ObEncInt(itorPlayerData.Current.Value.UID));
            }

            if (bSave) Save();
        }

        /// <summary>
        /// 모든 캐릭터 금지. 
        /// </summary>
        public static void LockAllCharacter(bool bSave = true)
        {
            _lstUnLockCharId.Clear();
            _lstUnLockCharId.Add(new ObEncInt(ProjectDefines.BASE_PLAYER_ID));

            if (bSave) Save();
        }

        /// <summary>
        /// 캐릭터 선택. 
        /// </summary>
        public static void SelectCharacter(int iCharId, bool bSave = true)
        {
            if (iCharId <= 0)
                return;

            _iSelectedCharId = iCharId;
            if (bSave) Save();
        }

        /// <summary>
        /// 유저 데이터 초기화. 
        /// </summary>
        public static void ResetUserData(bool bSave = true)
        {
            UserSaveData newData = new UserSaveData();
            UserData.CopyFrom(newData);
            if (bSave) Save();
        }
        #endregion Game Methods

        #region Event
        public static void AddObservScore(Observable<ObEncFloat>.EventChanged callback, bool bCallbackNow = false)
        {
            _fScore.AddObserver(callback, bCallbackNow);
        }

        public static void DelObservScore(Observable<ObEncFloat>.EventChanged callback)
        {
            _fScore.DelObserver(callback);
        }

        public static void AddObservCombo(Observable<ObEncInt>.EventChanged callback, bool bCallbackNow = false)
        {
            _iComboCount.AddObserver(callback, bCallbackNow);
        }

        public static void DelObservCombo(Observable<ObEncInt>.EventChanged callback)
        {
            _iComboCount.DelObserver(callback);
        }
        #endregion Event
    }
    #endregion UserData Class

    #region UserData Save Class
    // UserData 에 쓰이는 값들이 이미 암호화된 값이므로, 저장파일에 암호화를 적용하기 위해 일반 변수로 변환해주는 클래스. 
    [Serializable]
    public class UserSaveData
    {
        public float m_fScore;
        public int m_iKillCount;
        public float m_fBestScore;
        public int m_iBestKillCount;
        public int m_iBestKillNpcCount;
        public int m_iComboCount;
        public int m_iMaxComboCount;
        public int m_iBestComboCount;

        public List<int> m_lstUnLockCharId;
        public int m_iSelectedCharId;

        public double m_dScoreAccum;
        public long m_lKillCountAccum;
        public int m_iGameOverCount;
        public int m_iAttendDateTime;
        public int m_iAttendCount;
        public int m_iAttendSundayCount;
        public int m_iPlayTime;
        public long m_lPlayTimeAccum;
        public int m_iKillNpcCount;
        public long m_lKillNpcCountAccum;
        public int m_iZombieOnTrain;
        public long m_lZombieOnTrainAccum;

        public int m_iAttackCount;
        public long m_lAttackCountAccum;
        public int m_iFeverCount;
        public long m_lFeverCountAccum;

        public int m_iWatchAdCount;


        public UserSaveData()
        {
            if (m_lstUnLockCharId == null)
                m_lstUnLockCharId = new List<int>();
        }

        public void CopyUserData()
        {
            m_fScore = UserData.Score;
            m_iKillCount = UserData.KillCount;
            m_fBestScore = UserData.BestScore;
            m_iBestKillCount = UserData.BestKillCount;
            m_iComboCount = UserData.ComboCount;
            m_iMaxComboCount = UserData.MaxComboCount;
            m_iBestComboCount = UserData.BestComboCount;

            m_lstUnLockCharId = UserData.ListUnLockCharId;
            m_iSelectedCharId = UserData.SelectedCharId;


            m_dScoreAccum = UserData.ScoreAccum;
            m_lKillCountAccum = UserData.KillCountAccum;
            m_iGameOverCount = UserData.GameOverCount;
            m_iAttendDateTime = UserData.AttendDateTime;
            m_iAttendCount = UserData.AttendCount;
            m_iAttendSundayCount = UserData.AttendSundayCount;
            m_iPlayTime = UserData.PlayTime;
            m_lPlayTimeAccum = UserData.PlayTimeAccum;
            m_iKillNpcCount = UserData.KillNpcCount;
            m_lKillNpcCountAccum = UserData.KillNpcCountAccum;
            m_iZombieOnTrain = UserData.ZombieOnTrain;
            m_lZombieOnTrainAccum = UserData.ZombieOnTrainAccum;

            m_iAttackCount = UserData.AttackCount;
            m_lAttackCountAccum = UserData.AttackCountAccum;
            m_iFeverCount = UserData.FeverCount;
            m_lFeverCountAccum = UserData.FeverCountAccum;

            m_iWatchAdCount = UserData.WatchAdCount;
        }
    }
    #endregion UserData Save Class
}