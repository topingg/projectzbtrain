﻿using UnityEngine;
using System.Collections.Generic;

namespace Topping
{
    public class SceneObject : MonoBehaviour
    {
        #region DELEGATES
        public delegate void EventOn();
        public delegate void EventOnBool(bool bValue);

        public static EventOnBool onToggleAutoPlay = null;
        #endregion DELEGATES

        public bool isManualDestroy = false;
        public GameObject[] arrPostScenePrefabs;
        /*public string pathBgmClip = "";
        public AudioClip bgmClip = null;*/

        static SceneObject s_curScene = null;
        protected static bool s_bAutoPlay = false;

        public static SceneObject currentScene { get { return s_curScene; } }
        public static bool IsAutoPlay { get { return s_bAutoPlay; } }

        #region AUTO_PLAY
        public static void ToggleAutoPlay()
        {
            if (currentScene == null)
                return;
            currentScene.SetAutoPlay(!IsAutoPlay);
        }

        public void LoadAutoPlay()
        {
            SetAutoPlay(LoadAutoPlaySetting());
        }

        public void SetAutoPlay(bool bAutoPlay)
        {
            if (s_bAutoPlay == bAutoPlay)
                return;
            s_bAutoPlay = bAutoPlay;

            Debug.LogFormat("SceneObject.SetAutoPlay('{0}')", bAutoPlay);
            SaveAutoPlaySetting();

            if (onToggleAutoPlay != null)
                onToggleAutoPlay(s_bAutoPlay);
        }

        protected virtual bool LoadAutoPlaySetting() { return false; }
        protected virtual void SaveAutoPlaySetting() { }
        #endregion AUTO_PLAY

        public static void DestroyScene()
        {
            if (s_curScene != null)
                Destroy(s_curScene.gameObject);
        }

        protected virtual void Awake()
        {
            if (isManualDestroy)
            {
                if (s_curScene != null)
                {
                    Destroy(gameObject);
                }
                else
                {
                    s_curScene = this;
                    DontDestroyOnLoad(gameObject);
                }
            }
        }

        protected virtual void OnDestroy()
        {
            if (isManualDestroy)
            {
                if (s_curScene == this)
                    s_curScene = null;
            }
        }

        // Use this for initialization
        protected virtual void Start()
        {
            if (arrPostScenePrefabs != null)
            {
                for (int i = 0; i < arrPostScenePrefabs.Length; ++i)
                {
                    if (arrPostScenePrefabs[i] == null)
                        continue;

                    GameObject instance = Instantiate<GameObject>(arrPostScenePrefabs[i]);
                    instance.transform.SetParent(transform, false);
                }
            }

            /*if (bgmClip != null)
                SoundManager.Instance.ChangeBGM(bgmClip);
            else if (!pathBgmClip.IsNull())
                SoundManager.Instance.ChangeBGM(pathBgmClip);*/
        }
    }

}