﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
using System.Security.Cryptography;
using AvoEx;

public class Serializer
{
    public static void Serialize<TKey, TValue>(string szFilePath, ref Dictionary<TKey, TValue> TableData)
    {
        StreamWriter sWriter = new StreamWriter(szFilePath);
        BinaryFormatter bin = new BinaryFormatter();

        bin.Serialize(sWriter.BaseStream, TableData);
        sWriter.Close();
    }

    public static bool Serialize<T>(string szFilePath, ref T Value)
    {
        try
        {
            StreamWriter sWriter = new StreamWriter(szFilePath);
            BinaryFormatter bin = new BinaryFormatter();

            bin.Serialize(sWriter.BaseStream, Value);
            sWriter.Close();

            return true;
        }
        catch(Exception e)
        {
#if UNITY_EDITOR
            Debug.LogErrorFormat("Serialize Error! Path : {0}, code : {1}", szFilePath, e);
#endif
            return false;
        }
    }

    public static T DeSerialize<T>(string szFilePath)
    {
        try
        {
            using (FileStream fs = File.Open(szFilePath, FileMode.Open))
            {
                BinaryFormatter bin = new BinaryFormatter();

                T value = (T)bin.Deserialize(fs);

                return value;
            }
        }
        catch(Exception e)
        {
#if UNITY_EDITOR
            Debug.LogErrorFormat("DeSerialize Error!! Path : {0}, code : {1}", szFilePath, e);
#endif
            return default(T);
        }
    }

    /// <summary>
    /// 암호화 시리얼라이즈. 
    /// </summary>
    public static bool EncryptSerialize<TKey, TValue>(string szFilePath, ref Dictionary<TKey, TValue> TableData)
    {
        try
        {
            BinaryFormatter bin = new BinaryFormatter();
            using (Stream innerStream = File.Open(szFilePath, FileMode.OpenOrCreate))
            {
                using (CryptoStream cryptoStream = new CryptoStream(innerStream, AesEncryptor.Aes.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    bin.Serialize(cryptoStream, TableData);
                    cryptoStream.Close();
                }
                innerStream.Close();
            }

            return true;
        }
        catch (Exception e)
        {
#if UNITY_EDITOR
            Debug.LogErrorFormat("EncryptSerialize Error! Path : {0}, code : {1}", szFilePath, e);
#endif
            return false;
        }
    }

    /// <summary>
    /// 복호화 디시리얼라이즈. 
    /// </summary>
    public static Dictionary<TKey, TValue> DecryptDeSerialize<TKey, TValue>(string szFilePath)
    {
        try
        {
            Dictionary<TKey, TValue> returnDic = null;

            BinaryFormatter bin = new BinaryFormatter();
            using (Stream innerStream = File.Open(szFilePath, FileMode.Open))
            {
                using (CryptoStream cryptoStream = new CryptoStream(innerStream, AesEncryptor.Aes.CreateDecryptor(), CryptoStreamMode.Read))
                {
                    returnDic = (Dictionary<TKey, TValue>)bin.Deserialize(cryptoStream);
                    cryptoStream.Close();
                }
                innerStream.Close();
            }

            return returnDic;
        }
        catch (Exception e)
        {
#if UNITY_EDITOR
            Debug.LogErrorFormat("DecryptDeSerialize Error!! Path : {0}, code : {1}", szFilePath, e);
#endif
            return null;
        }
    }

    /// <summary>
    /// 암호화 시리얼라이즈. 
    /// </summary>
    public static bool EncryptSerialize<T>(string szFilePath, ref T Value)
    {
        Stream innerStream = null;
        CryptoStream cryptoStream = null;

        try
        {
            BinaryFormatter bin = new BinaryFormatter();
            using (innerStream = File.Open(szFilePath, FileMode.OpenOrCreate))
            {
                using (cryptoStream = new CryptoStream(innerStream, AesEncryptor.Aes.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    bin.Serialize(cryptoStream, Value);
                    cryptoStream.Close();
                }
                innerStream.Close();
            }

            return true;
        }
        catch (Exception e)
        {
#if UNITY_EDITOR
            Debug.LogErrorFormat("EncryptSerialize Error! Path : {0}, code : {1}", szFilePath, e);
#endif
            return false;
        }
        finally
        {
            if (innerStream != null)
                innerStream.Close();

            if (cryptoStream != null)
                cryptoStream.Close();
        }
    }

    /// <summary>
    /// 복호화 디시리얼라이즈. 
    /// </summary>
    public static T DecryptDeSerialize<T>(string szFilePath)
    {
        Stream innerStream = null;
        CryptoStream cryptoStream = null;

        try
        {
            T returnValue = default(T);

            BinaryFormatter bin = new BinaryFormatter();
            using (innerStream = File.Open(szFilePath, FileMode.Open))
            {
                using (cryptoStream = new CryptoStream(innerStream, AesEncryptor.Aes.CreateDecryptor(), CryptoStreamMode.Read))
                {
                    returnValue = (T)bin.Deserialize(cryptoStream);
                    cryptoStream.Close();
                }
                innerStream.Close();
            }

            return returnValue;
        }
        catch (Exception e)
        {

#if UNITY_EDITOR
            Debug.LogErrorFormat("DecryptDeSerialize Error!! Path : {0}, code : {1}", szFilePath, e);
#endif
            return default(T);
        }
        finally
        {
            if (innerStream != null)
                innerStream.Close();

            if (cryptoStream != null)
                cryptoStream.Close();
        }
    }
}