﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class CSVData
{
    private Dictionary<string, int> m_dicColIndex = new Dictionary<string, int>();
    private string[,] m_szGrid;
    private string m_szTablePath;
    private int m_iLineCount;

    public int GetLineCount
    {
        get
        {
            return m_iLineCount;
        }
    }

    public CSVData(string szTablePath)
    {
        try
        {
            string szTableData = System.IO.File.ReadAllText(szTablePath);

            m_szGrid = CSVReader.SplitCsvGrid(szTableData);

            // 컬럼 명으로 읽게 하기 위해 dic에 저장합니다.
            int iColLength = m_szGrid.GetUpperBound(0);
            for (int i = 0; i < iColLength; ++i)
            {
                try
                {
                    m_dicColIndex.Add(m_szGrid[i, 0], i);
                }
                catch (Exception e)
                {
                    Debug.LogErrorFormat("load column failed. table = [{0}], col({3}) = [{1}]\nerror = {2}", szTablePath, m_szGrid[i, 0], e, i);
                }
            }

            //< 중간에 오류가 없으면 로그 찍는 용으로 테이블 주소를 저장해줍니다.
            m_szTablePath = szTablePath;
            //< 라인 수 저장
            m_iLineCount = m_szGrid.GetUpperBound(1);
        }
        catch (Exception e)
        {
            Debug.LogErrorFormat("Error :{0}, {1}", szTablePath, e);
        }
    }

    public int GetColumnIndex(string szColName)
    {
        int iColIdx = 0;
        if (m_dicColIndex.TryGetValue(szColName, out iColIdx) == false)
        {
            Debug.LogErrorFormat("CSVData GetColumnIndex failed. table = [{0}], ColName = [{1}]", m_szTablePath, szColName);
        }

        return iColIdx;
    }

    public string GetString(int iLineIndex, string szColName)
    {
        int iColIdx = GetColumnIndex(szColName);

        return m_szGrid[iColIdx, iLineIndex];
    }

    public int GetInt(int iLineIndex, string szColName, int iDefault = 0, bool bPassEmpty = false)
    {
        int iColIdx = GetColumnIndex(szColName);

        if (bPassEmpty && string.IsNullOrEmpty(m_szGrid[iColIdx, iLineIndex]))
            return iDefault;

        int iValue = iDefault;
        if (int.TryParse(m_szGrid[iColIdx, iLineIndex], out iValue) == false)
        {
            Debug.LogErrorFormat("CSVData GetInt failed. table = [{0}], ColName = [{1}], LineIndex = [{2}]", m_szTablePath, szColName, iLineIndex);
            return iDefault;
        }

        return iValue;
    }

    public long GetLong(int iLineIndex, string szColName)
    {
        int iColIdx = GetColumnIndex(szColName);

        long nValue = 0;
        if (long.TryParse(m_szGrid[iColIdx, iLineIndex], out nValue) == false)
        {
            Debug.LogErrorFormat("CSVData GetLong failed. table = [{0}], ColName = [{1}], LineIndex = [{2}]", m_szTablePath, szColName, iLineIndex);
        }

        return nValue;
    }

    public double GetDouble(int iLineIndex, string szColName)
    {
        int iColIdx = GetColumnIndex(szColName);

        double dValue = 0.0f;
        if (double.TryParse(m_szGrid[iColIdx, iLineIndex], out dValue) == false)
        {
            Debug.LogErrorFormat("CSVData GetDouble failed. table = [{0}], ColName = [{1}], LineIndex = [{2}]", m_szTablePath, szColName, iLineIndex);
        }

        return dValue;
    }

    public float GetFloat(int iLineIndex, string szColName)
    {
        int iColIdx = GetColumnIndex(szColName);

        float fValue = 0.0f;
        if (float.TryParse(m_szGrid[iColIdx, iLineIndex], out fValue) == false)
        {
            Debug.LogErrorFormat("CSVData GetFloat failed. table = [{0}], ColName = [{1}], LineIndex = [{2}]", m_szTablePath, szColName, iLineIndex);
        }

        return fValue;
    }

    public bool GetBool(int iLineIndex, string szColName)
    {
        int iColIdx = GetColumnIndex(szColName);

        bool bValue = false;
        if (bool.TryParse(m_szGrid[iColIdx, iLineIndex], out bValue) == false)
        {
            Debug.LogErrorFormat("CSVData GetBool failed. table = [{0}], ColName = [{1}], LineIndex = [{2}]", m_szTablePath, szColName, iLineIndex);
        }

        return bValue;
    }
}