﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
using System.Text;
using System.Diagnostics;

using Debug = UnityEngine.Debug;

namespace Topping
{
    public class CsvSerializer
    {
        //< 외부프로그램을 만들어서 이 쪽에 연결하세요
        public static void ConvertExcelToTxt(string szExePath, string szArg)
        {
            Process pHandle = Process.Start(szExePath, szArg);

            pHandle.WaitForExit();
        }

        //< txt파일을 읽어서 utf8로 변환해서 반환하는 함수작성
        static string GetTxtFile(string szFileName)
        {
            return System.IO.File.ReadAllText(szFileName);
        }

        static void DeSerializeSlz<T>(string szReadFilePath, string szSaveFileName)
        {
            StreamReader sr = new StreamReader(szReadFilePath);
            BinaryFormatter bin = new BinaryFormatter();

            object obResult = bin.Deserialize(sr.BaseStream);
            sr.Close();

            List<T> lstTmp = (List<T>)obResult;
            StreamWriter sWriter = new StreamWriter(string.Format(GameDataEditor.s_szFilePath, szSaveFileName, "txt"));

            int iLoopCount = lstTmp.Count;

            for (int i = 0; i < iLoopCount; ++i)
            {
                sWriter.WriteLine(lstTmp[i]);
            }

            sWriter.Close();
        }

        #region //--나중에 쓸거 모음--//
        // 
        //         public static void ExcelToTxt()
        //         {
        //             string[] szFiles = Directory.GetFiles(KRPGTableConverter.s_szTableForder);
        // 
        //             int iLoopCount = szFiles.Length;
        // 
        //             StringBuilder sbArg = new StringBuilder();
        // 
        //             sbArg.Append(KRPGTableConverter.s_szTableForder);
        // 
        // 
        //             for (int i = 0; i < iLoopCount; ++i)
        //             {
        //                 if (szFiles[i].EndsWith(".xlsx") || szFiles[i].EndsWith(".xls"))
        //                 {
        //                     sbArg.Append(",");
        //                     sbArg.Append(szFiles[i]);
        //                 }
        //             }
        //             File.WriteAllText(KRPGTableConverter.s_szFileListPath, sbArg.ToString(), Encoding.UTF8);
        // 
        //             Process pHandle = Process.Start(KRPGTableConverter.s_szTableConverter, KRPGTableConverter.s_szFileListPath);
        // 
        //             pHandle.WaitForExit();
        //         }

        //< 테이블 이름에 따라 종류를 나눠서 파싱한다.
        //< 테이블이 추가될 때 마다 이 쪽에 추가시키면 된다.
        //         static void SerializeTableType(string szFilePath)
        //         {
        //             //< 테이블 정보를 얻어옵니다.
        //             string szTable = GetTxtFile(szFilePath);
        // 
        //             //             if (szFilePath.Contains(s_szCharacterTable))
        //             //             {
        //             //                 Dictionary<int, CharacterData> outdata = null;
        //             //                 
        //             //                 //< 얻어온 테이블 정보로 파싱합니다.
        //             //                 CharacterData.ParseTable(szTable, out outdata);
        //             //                 //< 직렬화 시켜서 bytes파일로 저장합니다.
        //             //                 SerializeCsv<int, CharacterData>(s_szCharacterTable, ref outdata);
        //             //             }
        //             // 
        //             //             if (szFilePath.Contains(s_szStringTable))
        //             //             {
        //             //                 Dictionary<int, StringData> outdata = null;
        //             //                 
        //             //                 //< 얻어온 테이블 정보로 파싱합니다.
        //             //                 StringData.ParseTable(szTable, out outdata);
        //             //                 //< 직렬화 시켜서 bytes파일로 저장합니다.
        //             //                 SerializeCsv<int, StringData>(s_szStringTable, ref outdata);
        //             //             }            
        //         }
        #endregion
    }
}
#endif