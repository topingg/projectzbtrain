﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using AvoEx;
using System.Security.Cryptography;

namespace Topping
{
    public class BinaryLoader<TKey, TValue>
    {
        public static Dictionary<TKey, TValue> Load(StreamReader sr)
        {
            try
            {                
                BinaryFormatter bin = new BinaryFormatter();

                object obResult = bin.Deserialize(sr.BaseStream);
                sr.Close();

                return (Dictionary<TKey, TValue>)obResult;
            }
            catch (System.Exception e)
            {
                Debug.LogError("TableLoader Error! : " + e.ToString());
                return null;
            }
        }

        public static Dictionary<TKey, TValue> EncryptLoad(StreamReader sr)
        {
            try
            {
                object obResult = null;

                BinaryFormatter bin = new BinaryFormatter();
                using (CryptoStream cryptoStream = new CryptoStream(sr.BaseStream, AesEncryptor.Aes.CreateDecryptor(), CryptoStreamMode.Read))
                {
                    obResult = bin.Deserialize(cryptoStream);
                    cryptoStream.Close();
                }
                sr.Close();

                return (Dictionary<TKey, TValue>)obResult;
            }
            catch (System.Exception e)
            {
                Debug.LogError("Encrypt TableLoader Error! : " + e.ToString());
                return null;
            }
        }
    }

    public class BinaryLoader<T>
    {
        public static List<T> Load(StreamReader sr)
        {
            try
            {
                BinaryFormatter bin = new BinaryFormatter();

                object obResult = bin.Deserialize(sr.BaseStream);
                sr.Close();

                return (List<T>)obResult;
            }
            catch (System.Exception e)
            {
                Debug.LogError("TableLoader Error! : " + e.ToString());
                return null;
            }
        }

        public static List<T> EncryptLoad(StreamReader sr)
        {
            try
            {
                object obResult = null;

                BinaryFormatter bin = new BinaryFormatter();
                using (CryptoStream cryptoStream = new CryptoStream(sr.BaseStream, AesEncryptor.Aes.CreateDecryptor(), CryptoStreamMode.Read))
                {
                    obResult = bin.Deserialize(cryptoStream);
                    cryptoStream.Close();
                }
                sr.Close();

                return (List<T>)obResult;
            }
            catch (System.Exception e)
            {
                Debug.LogError("Encrypt TableLoader Error! : " + e.ToString());
                return null;
            }
        }
    }

    [Serializable]
    public abstract class TableData<TKey, TValue> where TValue : TableData<TKey, TValue>
    {
        public TKey TID { get; protected set; }

        #region STATICS
        protected static Dictionary<TKey, TValue> s_dictTableData = new Dictionary<TKey, TValue>();

        public static Dictionary<TKey, TValue> dataDictionary { get { return s_dictTableData; } }

        #region //< 종류별로 테이블 로드 할 함수들을 이 안에 작성하세요
        //< 에셋매니저에서부터 불러오기
        public static bool LoadTableFromBundle(string szTablePath)
        {
            TextAsset taTableContent = AssetManager.LoadAsset<TextAsset>(szTablePath);

            if(taTableContent == null)
            {
                Debug.LogErrorFormat("Not Found Table : {0}", szTablePath);
                return false;
            }

            Dictionary<TKey, TValue> dicTmp = null;

            using (StreamReader sr = new StreamReader(new MemoryStream(taTableContent.bytes)))
            {
                dicTmp = BinaryLoader<TKey, TValue>.EncryptLoad(sr);
            }

            AssetManager.Unload(szTablePath);

            //< 예외처리
            if (dicTmp == null)
            {
                Debug.LogErrorFormat("Error Table : {0}", szTablePath);
                return false;
            }
            else
            {
                s_dictTableData = dicTmp;
                return true;
            }
        }

        //< 파일경로로 부터 바로 불러오기
        public static bool LoadTableFromDirect(string szTablePath)
        {
            Dictionary<TKey, TValue> dicTmp = null;

            using (StreamReader sr = new StreamReader(Application.dataPath + "/" + szTablePath))
            {
                dicTmp = BinaryLoader<TKey, TValue>.EncryptLoad(sr);
            }

            //< 예외처리
            if (dicTmp == null)
            {
                Debug.LogErrorFormat("Error Table : {0}", szTablePath);
                return false;
            }
            else
            {
                s_dictTableData = dicTmp;

                return true;
            }
        }

        //< resouces로 부터 불러오기 매개변수에 파일 확장자가 들어가면 안된다.
        public static bool LoadTableFromResources(string szTablePath)
        {            
            Dictionary<TKey, TValue> dicTmp = null;
            TextAsset taTableContent = (TextAsset)Resources.Load(szTablePath);

            if (taTableContent == null)
            {
                Debug.LogErrorFormat("Not Found Table {0}", szTablePath);
                return false;
            }

            using (StreamReader sr = new StreamReader(new MemoryStream(taTableContent.bytes)))
            {
                dicTmp = BinaryLoader<TKey, TValue>.EncryptLoad(sr);
            }

            Resources.UnloadAsset(taTableContent);

            //< 예외처리
            if (dicTmp == null)
            {
                Debug.LogErrorFormat("Error Table : {0}", szTablePath);
                return false;
            }
            else
            {
                s_dictTableData = dicTmp;
                return true;
            }
        }
        #endregion     

        protected static void AddTableData(TKey key, TValue data)
        {
            try
            {
                s_dictTableData.Add(key, data);
            }
            catch
            {
                Debug.LogErrorFormat("{0}.AddTableData('{1}', '{0}') is failed. the key '{1}' is already exist.", typeof(TValue), key);
            }
        }

        public static TValue GetTableData(TKey key)
        {
            TValue data = null;
            try
            {
                s_dictTableData.TryGetValue(key, out data);
            }
            catch(Exception e)
            {
                Debug.LogErrorFormat("{0}.GetTableData('{1}') is failed. the key '{1}' is not exist : {3}", typeof(TValue), key, e);
            }            
            
            return data;
        }

        public static void ClearTableData()
        {
            s_dictTableData.Clear();
        }
        #endregion STATICS
    }

    public abstract class DataManager<T> : MonoSingleton<T> where T : DataManager<T>
    {
        public enum STATUS : byte
        {
            NONE,
            LOADING,
            FINISHED,
            ERROR,
        }

        static STATUS m_currentStatus;
        public static STATUS currentStatus
        {
            get { return m_currentStatus; }
            private set
            {
#if DEBUG_DATAMANAGER
                Debug.LogFormat("DataManager STATUS changed <color=#ffff00ff>{0}</color> from {1}", value, m_currentStatus);
#endif
                m_currentStatus = value;
            }
        }
        public static string loadingTableName { get; private set; }

        public delegate bool TableLoader(); // loader must be clear loaded data before load the table.
        protected Dictionary<string, TableLoader> dictTableLoaders = new Dictionary<string, TableLoader>();

        public bool loadTablesOnStart = false;

        public int curLoadCount { get; private set; }
        public int maxLoadCount
        {
            get { return dictTableLoaders.Count; }
        }

        protected override void OnAwake()
        {
            InitOnApp();
        }

        // Use this for initialization
        protected virtual void Start()
        {
            if (loadTablesOnStart)
                LoadTables();
        }

        //// Update is called once per frame
        //void Update()
        //{

        //}

        /// <summary>
        /// register TableData. just called once on application running.
        /// </summary>
        public abstract void InitOnApp();

        public abstract void PostProcessOnLoadTables();

        public void LoadTables(bool bAsync = true)
        {
            StopAllCoroutines();
            if (bAsync)
            {
                StartCoroutine(CoLoadTables());
            }
            else
            {
                curLoadCount = 0;
                var itorTableLoaders = dictTableLoaders.GetEnumerator();
                while (itorTableLoaders.MoveNext())
                {
                    if (itorTableLoaders.Current.Value == null)
                    {
                        ++curLoadCount;
                        continue;
                    }

                    loadingTableName = itorTableLoaders.Current.Key;

                    if (itorTableLoaders.Current.Value() == false)
                    {
                        currentStatus = STATUS.ERROR;
                        return;
                    }

                    ++curLoadCount;
                }

                PostProcessOnLoadTables();

                currentStatus = STATUS.FINISHED;
            }
        }

        IEnumerator CoLoadTables()
        {
#if DEBUG_DATAMANAGER
            System.Diagnostics.Stopwatch swTotal = System.Diagnostics.Stopwatch.StartNew();
            System.Diagnostics.Stopwatch swTable = System.Diagnostics.Stopwatch.StartNew();
#endif
            currentStatus = STATUS.LOADING;
            loadingTableName = "";

            curLoadCount = 0;
            var itorTableLoaders = dictTableLoaders.GetEnumerator();
            while (itorTableLoaders.MoveNext())
            {
                if (itorTableLoaders.Current.Value == null)
                {
                    ++curLoadCount;
                    continue;
                }

                loadingTableName = itorTableLoaders.Current.Key;

                if (itorTableLoaders.Current.Value() == false)
                {
                    currentStatus = STATUS.ERROR;
                    yield break;
                }

#if DEBUG_DATAMANAGER
                swTable.Stop();
                Debug.LogFormat("table index {0} load finished. elapsed time = {1}", curLoadCount, swTable.Elapsed);
                swTable.Reset();
                swTable.Start();
#endif
                ++curLoadCount;
                yield return null;
            }

            if (currentStatus == STATUS.LOADING)
            {
                loadingTableName = "Post Processing..";
                yield return null;

                PostProcessOnLoadTables();
                currentStatus = STATUS.FINISHED;
                loadingTableName = "";
            }

#if DEBUG_DATAMANAGER
            swTable.Stop();
            swTotal.Stop();
            Debug.LogFormat("all table load finished. elapsed time = {0}", swTotal.Elapsed);
#endif
        }
    }

}