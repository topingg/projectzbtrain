﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using AvoEx;
using AvoEx.ObservableType;

namespace Topping
{
    public class EngineManager : MonoSingleton<EngineManager>
    {
        public delegate void EventEngine();
        public delegate void EventInt(int iValue);
        public delegate void EventSceneRequest(string curScene, string nextScene);
        public delegate void EventSceneLoaded(string curScene);

        //
        public static EventEngine onLogOut;
        private const float RESUME_TIME_OUT = 300f;
        private float m_fApplicationPausedRealTime = 0f;

        // scene loading.
        public static EventSceneRequest onSceneRequest; // called before request scene loading.
        public static EventSceneLoaded onSceneLoaded; // called after completed scene loading.
        public static EventSceneLoaded onSceneFailed; // failed scene loading.

        static bool s_isSceneLoading = false;
        static float s_sceneLoadingProgress = 0f;

        // game speed.
        public static EventEngine onPauseGame = null;
        public static EventEngine onResumeGame = null;
        public static EventEngine onGameSpeed = null;

        public static ObEncFloat baseTimeScale = new ObEncFloat(1.0f);
        public static ObEncFloat playTimeScale = new ObEncFloat(1.0f);

        // graphic quality.
        const string KEY_GRAPHIC_QUALITY = "GRAPHIC_QUALITY";
        public static EventInt onGraphicQuality = null;

        private float _fElapsedTime = 0;

        #region MonoBehaviour
        void Update()
        {
            _fElapsedTime += Time.deltaTime;
        }

        public static void LogOut()
        {
            if (onLogOut != null)
                onLogOut();
        }

        void OnApplicationPause(bool pauseStatus)
        {
            if (pauseStatus)
            {
                m_fApplicationPausedRealTime = Time.realtimeSinceStartup;
            }
            else
            {
                if ((Time.realtimeSinceStartup - m_fApplicationPausedRealTime) > RESUME_TIME_OUT)
                    LogOut();
            }
        }
        #endregion MonoBehaviour

        #region LOADING_SCENE
        /// <summary>
        /// 씬을 로딩중인지 여부
        /// </summary>
        /// <returns>로딩중이면 return true</returns>
        public static bool isSceneLoading
        {
            get { return s_isSceneLoading; }
        }

        public static float sceneLoadingProgress
        {
            get { return s_sceneLoadingProgress; }
        }

        void LoadSceneAsync(string sceneName, string bundleName = null, bool additive = false)
        {
#if DEBUG_ENGINEMANAGER
            Debug.LogFormat("EngineManager.LoadSceneAsync('{0}', '{1}') : previous scene name is '{2}", sceneName, bundleName, SceneManager.GetActiveScene().name);
#endif
            if (additive)
                StartCoroutine(CoLoadSceneAdditiveAsync(sceneName, bundleName));
            else
                StartCoroutine(CoLoadSceneAsync(sceneName, bundleName));
        }

        IEnumerator CoLoadSceneAsync(string sceneName, string bundleName = null)
        {

#if DEBUG_ENGINEMANAGER
            System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
            Debug.LogFormat("EngineManager.CoLoadSceneAsync('{0}', '{1}') started.", sceneName, bundleName);
#endif
            if (string.IsNullOrEmpty(sceneName))
            {
                Debug.LogErrorFormat("EngineManager.CoLoadSceneAsync('{0}', '{1}') is failed.", sceneName, bundleName);
                yield break;
            }
            if (s_isSceneLoading)
            {
                Debug.LogErrorFormat("EngineManager.CoLoadSceneAsync('{0}', '{1}') is failed. loading is in progress now.", sceneName, bundleName);
                yield break;
            }

            // initializing process
            s_isSceneLoading = true;
            s_sceneLoadingProgress = 0f;

            //GlobalTopUI.IncWaiting(); // block user interface and show loading indicator
            ResumeGame(); // restore game speed

            //ObjectPoolManager.PushAllInstaces();
            if (onSceneRequest != null)
                onSceneRequest(SceneManager.GetActiveScene().name, sceneName);

            // bundle loading process
            float downloadProgressRate = 0f;
            if (string.IsNullOrEmpty(bundleName) == false) // load scene from bundle.
            {
                downloadProgressRate = 0.5f;
                AsyncBundleRequest bundleScene = BundleManager.LoadBundleAsync(bundleName);
                while (bundleScene.isDone == false)
                {
                    s_sceneLoadingProgress = BundleManager.ManagedBundle.downloadingProgress * downloadProgressRate;
                    yield return null;
                }

                if (string.IsNullOrEmpty(bundleScene.error) == false) // failed load bundle
                {
                    string szError = string.Format("EngineManager.CoLoadSceneAsync('{0}', '{1}') is failed. '{2}'", sceneName, bundleName, bundleScene.error);
                    Debug.LogError(szError);
                    s_isSceneLoading = false;
                    s_sceneLoadingProgress = 0f;
                    //GlobalTopUI.DecWaiting(); // hide loading indicator
                    if (onSceneFailed != null)
                        onSceneFailed(szError);
                    yield break;
                }
#if DEBUG_ENGINEMANAGER
                Debug.LogFormat("EngineManager.CoLoadSceneAsync('{0}', '{1}') load bundle completed. elapsed time = '{2}'", sceneName, bundleName, sw.Elapsed);
#endif
            }

            // scene loading process
            AsyncOperation asyncOpScene = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Single);
            if (asyncOpScene == null) // failed load scene
            {
                string szError = string.Format("Application.LoadLevelAsync('{0}') is failed. check scene name('{0}') or bundle name('{1}').", sceneName, bundleName);
                Debug.LogError(szError);
                s_isSceneLoading = false;
                s_sceneLoadingProgress = 0f;
                //GlobalTopUI.DecWaiting(); // hide loading indicator
                if (onSceneFailed != null)
                    onSceneFailed(szError);
                yield break;
            }

            while (asyncOpScene.isDone == false)
            {
                s_sceneLoadingProgress = downloadProgressRate + (asyncOpScene.progress * (1f - downloadProgressRate));
                yield return null;
            }
            // OnLevelWasLoaded() called on this timing.

            // finishing process
            s_sceneLoadingProgress = 1f;
            s_isSceneLoading = false;
            //GlobalTopUI.DecWaiting(); // hide loading indicator

            if (onSceneLoaded != null)
                onSceneLoaded(sceneName);

#if DEBUG_ENGINEMANAGER
            sw.Stop();
            Debug.LogFormat("EngineManager.CoLoadSceneAsync('{0}', '{1}') finished. elapsed time = '{2}'", sceneName, bundleName, sw.Elapsed);
#endif
        }

        IEnumerator CoLoadSceneAdditiveAsync(string sceneName, string bundleName = null)
        {

#if DEBUG_ENGINEMANAGER
            System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
            Debug.LogFormat("EngineManager.CoLoadSceneAdditiveAsync('{0}', '{1}') started.", sceneName, bundleName);
#endif
            if (string.IsNullOrEmpty(sceneName))
            {
                string szError = string.Format("EngineManager.CoLoadSceneAdditiveAsync('{0}', '{1}') is failed.", sceneName, bundleName);
                Debug.LogError(szError);
                if (onSceneFailed != null)
                    onSceneFailed(szError);
                yield break;
            }

            // initializing process
            if (onSceneRequest != null)
                onSceneRequest(SceneManager.GetActiveScene().name, sceneName);

            // bundle loading process
            if (string.IsNullOrEmpty(bundleName) == false) // load scene from bundle.
            {
                AsyncBundleRequest bundleScene = BundleManager.LoadBundleAsync(bundleName);
                while (bundleScene.isDone == false)
                {
                    yield return null;
                }

                if (string.IsNullOrEmpty(bundleScene.error) == false) // failed load bundle
                {
                    string szError = string.Format("EngineManager.CoLoadSceneAdditiveAsync('{0}', '{1}') is failed. '{2}'", sceneName, bundleName, bundleScene.error);
                    Debug.LogError(szError);
                    if (onSceneFailed != null)
                        onSceneFailed(szError);
                    yield break;
                }
#if DEBUG_ENGINEMANAGER
                Debug.LogFormat("EngineManager.CoLoadSceneAdditiveAsync('{0}', '{1}') load bundle completed. elapsed time = '{2}'", sceneName, bundleName, sw.Elapsed);
#endif
            }

            // scene loading process
            AsyncOperation asyncOpScene = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
            if (asyncOpScene == null) // failed load scene
            {
                string szError = string.Format("Application.LoadLevelAsync('{0}') is failed. check scene name('{0}') or bundle name('{1}').", sceneName, bundleName);
                Debug.LogError(szError);
                if (onSceneFailed != null)
                    onSceneFailed(szError);
                yield break;
            }
            while (asyncOpScene.isDone == false)
            {
                yield return null;
            }
            // OnLevelWasLoaded() called on this timing.

            // finishing process
            if (onSceneLoaded != null)
                onSceneLoaded(sceneName);

#if DEBUG_ENGINEMANAGER
            sw.Stop();
            Debug.LogFormat("EngineManager.CoLoadSceneAsync('{0}', '{1}') finished. elapsed time = '{2}'", sceneName, bundleName, sw.Elapsed);
#endif
        }

        /// <summary>
        /// SceneManager.sceneLoaded += 로 사용. 
        /// </summary>
        /// <param name="level"></param>
        /*void OnLevelWasLoaded(int level)
        {
#if DEBUG_ENGINEMANAGER
            Debug.LogFormat("EngineManager.OnLevelWasLoaded('{0}') : loadedLevelName = '{1}'", level, SceneManager.GetActiveScene().name);
#endif
        }*/

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sceneName"></param>
        /// <param name="bundleName"></param>
        public static void RequestSceneAsync(string sceneName, string bundleName = null, bool additive = false)
        {
            Instance.LoadSceneAsync(sceneName, bundleName, additive);
        }
        #endregion LOADING_SCENE

        #region GAME_SPEED
        public static void SetGameBaseSpeed(float fSpeed)
        {
            baseTimeScale.Value = fSpeed;
            ApplyGameSpeed();

#if DEBUG_ENGINEMANAGER
            Debug.LogFormat("EngineManager.SetGameBaseSpeed('{0}') - previous baseTimeScale = '{1}'", baseTimeScale.Value, baseTimeScale.PreValue);
#endif
        }

        /// <summary>
        /// set play speed. do not use Time.timeScale directly.
        /// </summary>
        /// <param name="timeScale">play speed</param>
        public static void SetGameSpeed(float timeScale)
        {
            playTimeScale.Value = timeScale;
            ApplyGameSpeed();

#if DEBUG_ENGINEMANAGER
            Debug.LogFormat("EngineManager.SetGameSpeed('{0}') - previous playTimeScale = '{1}'", playTimeScale.Value, playTimeScale.PreValue);
#endif

            if (onGameSpeed != null)
                onGameSpeed();
        }

        /// <summary>
        /// pause game play. do not use Time.timeScale directly.
        /// </summary>
        public static void PauseGame(bool bSilently = false)
        {
            Time.timeScale = 0f;

#if DEBUG_ENGINEMANAGER
            Debug.Log("EngineManager.PauseGame()");
#endif

            if (bSilently)
                return;

            if (onPauseGame != null)
                onPauseGame();
        }

        /// <summary>
        /// resume game play. do not use Time.timeScale directly.
        /// </summary>
        public static void ResumeGame()
        {
            ApplyGameSpeed();

#if DEBUG_ENGINEMANAGER
            Debug.LogFormat("EngineManager.ResumeGame() - restored timeScale to '{0}'", Time.timeScale);
#endif

            if (onResumeGame != null)
                onResumeGame();
        }

        static void ApplyGameSpeed()
        {
            Time.timeScale = baseTimeScale * playTimeScale;
        }
        #endregion GAME_SPEED

        #region GRAPHIC_QUALITY
        public static void SetScreenResolution(bool isHighQuality, bool portrait = false, bool bForce = false)
        {
            int defaultWidth = 1280;
            int defaultHeight = 720;

            if (isHighQuality == false)
            {
                defaultWidth = 864;
                defaultHeight = 486;
            }

            bool changeResolution = portrait == false ? (Screen.width > defaultWidth) : (Screen.height > defaultHeight);

            // 1280 보다 큰 해상도는 필요 없다
            if (changeResolution == true || bForce == true)
            {
                float fWidth = defaultWidth;
                float fHeight = defaultWidth * Screen.height / Screen.width;

                if (portrait == true)
                {
                    fWidth = defaultWidth * Screen.width / Screen.height;
                    fHeight = defaultWidth;
                }
                
                Screen.SetResolution((int)fWidth, (int)fHeight, true);
            }
        }

        public static void LoadGraphicQuality()
        {
            int iCurLevel = QualitySettings.GetQualityLevel();
            int iSavedLevel = EncryptPrefs.GetInt(KEY_GRAPHIC_QUALITY, iCurLevel);
            if (iCurLevel == iSavedLevel)
                return;

            QualitySettings.SetQualityLevel(iSavedLevel, true);
            //ShadowProjector.SetQualityLevel(iSavedLevel);

            // TODO : 화면 해상도 변경 옵션
            SetScreenResolution(true, false, false);
        }

        public static void SetGraphicQuality(int iLevel)
        {
            int iCurLevel = QualitySettings.GetQualityLevel();
            if (iCurLevel == iLevel)
                return;

            QualitySettings.SetQualityLevel(iLevel, true);
            //ShadowProjector.SetQualityLevel(iLevel);

            EncryptPrefs.SetInt(KEY_GRAPHIC_QUALITY, iLevel);

            if (onGraphicQuality != null)
                onGraphicQuality(iLevel);
        }

        /*public static void LoadFrameRate()
        {
            s_iFrameRateLevel = EncryptPrefs.GetInt(KEY_FRAME_RATE, DEFAULT_FRAME_RATE_LEVEL);
            ApplyFrameRate(s_iFrameRateLevel);
        }*/

        /*private static bool ApplyFrameRate(int iLevel)
        {
            if (iLevel < 0 || iLevel >= GRAPHIC_QUALITY_FRAME_RATES.Length)
                return false;

            // TargetFRameRate에 맞춰 나오게 하기 위해 설정
            QualitySettings.vSyncCount = 0;

            Application.targetFrameRate = GRAPHIC_QUALITY_FRAME_RATES[iLevel];
#if DEBUG_ENGINEMANAGER
            Debug.LogFormat("EngineManager.ApplyFrameRate({0}) : frame rate set to {1}", iLevel, Application.targetFrameRate);
#endif
            return true;
        }*/

        /*public static void SetFrameRate(int iLevel)
        {
            if (ApplyFrameRate(iLevel) == false)
            {
                iLevel = DEFAULT_FRAME_RATE_LEVEL;
                ApplyFrameRate(iLevel);
            }

            s_iFrameRateLevel = iLevel;
            EncryptPrefs.SetInt(KEY_FRAME_RATE, iLevel);

            if (onFrameRate != null)
                onFrameRate(iLevel);
        }*/
        #endregion GRAPHIC_QUALITY

#if UNITY_EDITOR
        /*public static void InitTestMode()
        {
            if (BundleEditorSettings.SavedSettings.testMode != BundleEditorSettings.TEST_MODE.AssetDatabase)
            {
                Debug.LogErrorFormat("InitTestMode() is works only AssetDatabase mode. current mode is '{0}'.", BundleEditorSettings.SavedSettings.testMode);
                return;
            }

            if (BundleManager.isInitialized == false)
                BundleManager.InitManager();

            if (AssetManager.isInitialized == false)
                AssetManager.InitManager();

            if (KRPG5DataManager.currentStatus != DataManager<KRPG5DataManager>.STATUS.FINISHED)
            {
                KRPG5DataManager.Instance.LoadTables(false);
            }
        }

        public static IEnumerator CoInitTestMode()
        {
            BundleManager.InitManager();
            while (BundleManager.isInitialized == false)
            {
                if (BundleManager.isError)
                {
                    Debug.LogErrorFormat("CoInitTestMode() is failed. BundleManager throw error : '{0}'.", BundleManager.error);
                    yield break;
                }
                yield return null;
            }

            AssetManager.InitManager();
            while (AssetManager.isInitialized == false)
            {
                if (AssetManager.isError)
                {
                    Debug.LogErrorFormat("CoInitTestMode() is failed. AssetManager throw error : '{0}'.", AssetManager.error);
                    yield break;
                }
                yield return null;
            }

            KRPG5DataManager.Instance.LoadTables();

            while (KRPG5DataManager.currentStatus == KRPG5DataManager.STATUS.LOADING)
                yield return null;

            if (KRPG5DataManager.currentStatus == KRPG5DataManager.STATUS.ERROR)
            {
                Debug.LogError("CoInitTestMode() is failed. KRPG5DataManager throw error.");
                yield break;
            }
        }*/
#endif

        public static void Quit()
        {
            Debug.LogError("Engine QUIT");
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }
    }

}