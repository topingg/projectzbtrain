﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using AvoEx;

namespace Topping
{

    public partial class LoadingScene : SceneObject
    {
        static string s_loadingSceneName;
        static string s_loadingSceneBundle;
        static string s_targetSceneName;
        static string s_targetBundleName;
        static bool s_isPreLoadingCompleted;

        public static bool isPreLoadingCompleted { get { return s_isPreLoadingCompleted; } }

        public const float sceneLoadingRate = 0.5f;
        public static float sceneLoadedRate = 0f;

        // Use this for initialization
        protected override void Start()
        {
            s_isPreLoadingCompleted = false;
            maxPrecachingCount = s_lstPrecacheObjects.Count;
            maxPreloadingCount = s_lstPreloadAssets.Count;
            currentPrecachingCount = 0;
            currentPreloadingCount = 0;

            base.Start();
            StartCoroutine(CoLoadingProcess());
        }

        #region LOADING_PROCESS
        /// <summary>
        /// override this
        /// </summary>
        /// <returns></returns>
        IEnumerator CoLoadingProcess()
        {
#if DEBUG_ENGINEMANAGER
            System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
            Debug.LogFormat("LoadingScene.CoLoadingProcess() started. targetSceneName = '{0}', targetBundleName = '{1}'", s_targetSceneName, s_targetBundleName);
#endif
            //GlobalTopUI.IncWaiting(); // block user interface and show loading indicator

            MemoryScope.BeginNextScope(s_targetSceneName);

            yield return StartCoroutine(CoPreLoading()); // 목표하는 씬으로 가기 전에 해야할 것. 프리캐싱 등..

            MemoryScope.EndPrevScope();
            Resources.UnloadUnusedAssets();

#if DEBUG_ENGINEMANAGER
            //yield return new WaitForSeconds(1f); // delete this
            Debug.LogFormat("LoadingScene.CoLoadingProcess() passed CoPreLoading(). elapsed time = '{0}'", sw.Elapsed);
#endif
            s_isPreLoadingCompleted = true;
            ClearAllList();
            sceneLoadedRate = 0f;
            EngineManager.RequestSceneAsync(s_targetSceneName, s_targetBundleName);
            sceneLoadedRate = 0.5f;

            //GlobalTopUI.DecWaiting(); // hide loading indicator

#if DEBUG_ENGINEMANAGER
            sw.Stop();
            Debug.LogFormat("LoadingScene.CoLoadingProcess() finished. elapsed time = '{0}'", sw.Elapsed);
#endif

            while (EngineManager.isSceneLoading)
                yield return null;

            sceneLoadedRate = 1f;
            yield return null;

            Destroy(gameObject);
        }
        #endregion LOADING_PROCESS

        #region STATIC_INTERFACES
        static LoadingScene()
        {
            SetLoadingSceneName(ProjectDefines.SCENE_NAME_LOADING, ProjectDefines.BUNDLE_NAME_SCENE);
        }

        public static void SetLoadingSceneName(string sceneName, string bundleName)
        {
            s_loadingSceneName = sceneName;
            s_loadingSceneBundle = bundleName;
        }

        public static void RequestSceneAsync(string sceneName, string bundleName = null)
        {
            ObjectPoolManager.PushAllInstaces();
            s_targetSceneName = sceneName;
            s_targetBundleName = bundleName;
            EngineManager.RequestSceneAsync(s_loadingSceneName, s_loadingSceneBundle, false);
        }
        #endregion STATIC_INTERFACES
    }

    public partial class LoadingScene : SceneObject // preloading
    {
        [SerializeField]
        class PrecacheInfo
        {
            private string m_szObjectName;
            private int m_iCount = 0;
            private int m_iResetCount = 0;

            public string Name { get { return m_szObjectName; } }
            public int Count { get { return Mathf.Max(m_iResetCount, m_iCount); } }

            public PrecacheInfo(string szName, int iCount)
            {
                m_szObjectName = szName;
                m_iCount = iCount;
                m_iResetCount = 0;
            }

            public void IncreaseCount(int iCount)
            {
                m_iCount += iCount;
            }

            public void ResetCount()
            {
                m_iResetCount = Count;
                m_iCount = 0;
            }
        }

        static Dictionary<string, PrecacheInfo> s_lstPrecacheObjects = new Dictionary<string, PrecacheInfo>();
        static List<string> s_lstPreloadAssets = new List<string>();

        static public int maxPrecachingCount { get; private set; }
        static public int maxPreloadingCount { get; private set; }
        static public int currentPrecachingCount { get; private set; }
        static public int currentPreloadingCount { get; private set; }

        static public int maxTotalCount { get { return maxPrecachingCount + maxPreloadingCount; } }
        static public int currentTotalCount { get { return currentPrecachingCount + currentPreloadingCount; } }
        static public float currentLoadingRate { get { return maxTotalCount > 0 ? (float)currentTotalCount / maxTotalCount : 1f; } }

        /// <summary>
        /// ObjectPoolManager 에 미리 생성해 놓을 인스턴스 갯수.
        /// </summary>
        /// <param name="szName"></param>
        /// <param name="iCount"></param>
        public static void AddPrecacheList(string szName, int iCount = 1)
        {
            if (szName.IsNull() || iCount < 1)
                return;

            PrecacheInfo curInfo = null;
            s_lstPrecacheObjects.TryGetValue(szName, out curInfo);
            if (curInfo == null)
            {
                curInfo = new PrecacheInfo(szName, iCount);
                s_lstPrecacheObjects[szName] = curInfo;
            }
            else
            {
                curInfo.IncreaseCount(iCount);
            }
        }

        public static void ResetPrecacheCount()
        {
            var itorPrecaches = s_lstPrecacheObjects.GetEnumerator();
            while (itorPrecaches.MoveNext())
            {
                itorPrecaches.Current.Value.ResetCount();
            }
        }

        /// <summary>
        /// AssetManager 에 미리 로딩 해놓을 리소스
        /// </summary>
        /// <param name="szName"></param>
        public static void AddPreloadList(string szName)
        {
            if (szName.IsNull())
                return;

            if (s_lstPreloadAssets.Contains(szName))
                return;
            s_lstPreloadAssets.Add(szName);
        }

        public static void ClearAllList()
        {
            s_lstPrecacheObjects.Clear();
            s_lstPreloadAssets.Clear();
            maxPrecachingCount = 0;
            maxPreloadingCount = 0;
            currentPrecachingCount = 0;
            currentPreloadingCount = 0;
        }

        /// <summary>
        /// 목표하는 씬으로 가기 전에 해야할 것. 프리캐싱 등..
        /// </summary>
        /// <returns></returns>
        protected virtual IEnumerator CoPreLoading()
        {
            var itorPrecaches = s_lstPrecacheObjects.GetEnumerator();
            while (itorPrecaches.MoveNext())
            {
#if DEBUG_ENGINEMANAGER
                Debug.LogFormat("LoadingScene.CoPreLoading() : instantiate name = {0}, count = {1}", itorPrecaches.Current.Value.Name, itorPrecaches.Current.Value.Count);
#endif
                yield return StartCoroutine(ObjectPoolManager.Instance.CoAdjustPoolSize(itorPrecaches.Current.Value.Name, itorPrecaches.Current.Value.Count));

                ++currentPrecachingCount;
            }
            s_lstPrecacheObjects.Clear();

            var itorPreloads = s_lstPreloadAssets.GetEnumerator();
            while (itorPreloads.MoveNext())
            {
#if DEBUG_ENGINEMANAGER
                Debug.LogFormat("LoadingScene.CoPreLoading() : load asset name = {0}", itorPreloads.Current);
#endif
                AssetRequest<Object> request = AssetManager.LoadAssetAsync<Object>(itorPreloads.Current);
                while (request.isDone == false)
                    yield return null;

                ++currentPreloadingCount;
                yield return null;
            }
            s_lstPreloadAssets.Clear();

            yield return new WaitForSeconds(1f);
        }
    }

}