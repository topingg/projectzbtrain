﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AvoEx;

namespace Topping
{
    public class TitleScene : SceneObject
    {
        public enum ENUM_INIT_PROGRESS
        {
            NONE = 0,

            UPDATE_BUNDLE,
            ASK_DOWNLOAD,
            LOAD_TABLE,

            UPDATE_BUNDLE_ERROR = 100,
            ASSET_INFO_ERROR,
            LOAD_TABLE_ERROR,

            FINISHED = 999,
            START_LOGIN_PROCESS,
            LOGIN_FAILED,
        }

        public enum ENUM_LOGIN_PROCESS
        {
            NONE,

            REQUEST_SERVER_LIST,
            RESPONSE_SERVER_LIST,

            WAIT_LOGIN_ID,
            WAIT_NICKNAME,

            REQUEST_LOGIN,
            RESPONSE_LOGIN,

            FINISHED = 999,
            ERROR,
        }

        public delegate void DelegateInitProgress(ENUM_INIT_PROGRESS initProgress);
        public delegate void DelegateLoginProgress(ENUM_LOGIN_PROCESS loginProgress);

        public static event DelegateInitProgress onInitProgress = null;
        public static event DelegateLoginProgress onLoginProgress = null;

#if DEBUG_TITLESCENE
        private static System.Diagnostics.Stopwatch swInit = new System.Diagnostics.Stopwatch();
#endif
#if DEBUG_LOGINSCENE
        private static System.Diagnostics.Stopwatch swLogin = new System.Diagnostics.Stopwatch();
#endif

        private static ENUM_INIT_PROGRESS _initProgress = ENUM_INIT_PROGRESS.NONE;
        public static ENUM_INIT_PROGRESS initProgress
        {
            get { return _initProgress; }
            private set
            {
                if (_initProgress == value)
                    return;

#if DEBUG_TITLESCENE
                if (_initProgress == ENUM_INIT_PROGRESS.UPDATE_BUNDLE)
                    swInit.Start();
                else if (_initProgress == ENUM_INIT_PROGRESS.FINISHED)
                    swInit.Stop();
                else if (_initProgress == ENUM_INIT_PROGRESS.NONE)
                    swInit.Reset();
                Debug.LogFormat("LoginScene.initProgress is changed <color=#00ffffff>{0}</color> from {1}. elapsed time = {2}", value, _initProgress, swInit.Elapsed);
#endif
                _initProgress = value;
                if (onInitProgress != null)
                    onInitProgress(_initProgress);
            }
        }

        private static ENUM_LOGIN_PROCESS _loginProgress = ENUM_LOGIN_PROCESS.NONE;
        public static ENUM_LOGIN_PROCESS loginProgress
        {
            get { return _loginProgress; }
            private set
            {
                if (_loginProgress == value)
                    return;
#if DEBUG_LOGINSCENE
                if (_loginProgress == ENUM_LOGIN_PROCESS.REQUEST_LOGIN)
                    swLogin.Start();
                else if (_loginProgress == ENUM_LOGIN_PROCESS.FINISHED)
                    swLogin.Stop();
                else if (_loginProgress == ENUM_LOGIN_PROCESS.NONE)
                    swLogin.Reset();
                Debug.LogFormat("LoginScene.loginProgress is changed <color=#ff00ffff>{0}</color> from {1}. elapsed time = {2}", value, _loginProgress, swLogin.Elapsed);
#endif
                _loginProgress = value;
                if (onLoginProgress != null)
                    onLoginProgress(_loginProgress);
            }
        }

        #region Initialize Game
        /// <summary>
        /// 초기화 시작. 
        /// </summary>
        private void _StartInitialize()
        {
            StopAllCoroutines();
            StartCoroutine(_CrtGameInitialize());
        }

        /// <summary>
        /// 게임 초기화. 
        /// </summary>
        private IEnumerator _CrtGameInitialize()
        {
            initProgress = ENUM_INIT_PROGRESS.NONE;
            loginProgress = ENUM_LOGIN_PROCESS.NONE;

            yield return StartCoroutine(_CrtUpdateBundle());
            if (initProgress == ENUM_INIT_PROGRESS.UPDATE_BUNDLE_ERROR)
                yield break;

            yield return StartCoroutine(_CrtLoadTable());
            if (initProgress == ENUM_INIT_PROGRESS.LOAD_TABLE_ERROR)
                yield break;

            initProgress = ENUM_INIT_PROGRESS.FINISHED;

            StartCoroutine(_CrtLoginProcess());
        }

        /// <summary>
        /// 번들 업데이트 체크 및 다운로드. 
        /// </summary>
        private IEnumerator _CrtUpdateBundle()
        {
            initProgress = ENUM_INIT_PROGRESS.UPDATE_BUNDLE;

            BundleManager.InitManager(false);

            while (BundleManager.isInitialized == false && BundleManager.isReadyPredownload == false)
            {
                if (BundleManager.isError)
                {
                    initProgress = ENUM_INIT_PROGRESS.UPDATE_BUNDLE_ERROR;
                    yield break;
                }
                yield return null;
            }

            if (BundleManager.isInitialized == false)
            {
                if (BundleManager.isReadyPredownload)
                {
                    //initProgress = ENUM_INIT_PROGRESS.ASK_DOWNLOAD;

                    // 서버가 없으므로.. 준비가 되었다면 바로 다운로드. 
                    StartPredownload();
                }

                while (BundleManager.isInitialized == false)
                {
                    if (BundleManager.isError)
                    {
                        initProgress = ENUM_INIT_PROGRESS.UPDATE_BUNDLE_ERROR;
                        yield break;
                    }
                    yield return null;
                }
            }

            AssetManager.InitManager();

            while (AssetManager.isInitialized == false)
            {
                if (AssetManager.isError)
                {
                    initProgress = ENUM_INIT_PROGRESS.ASSET_INFO_ERROR;
                    yield break;
                }
                yield return null;
            }
        }

        /// <summary>
        /// 테이블 데이터 로드. 
        /// </summary>
        private IEnumerator _CrtLoadTable()
        {
            initProgress = ENUM_INIT_PROGRESS.LOAD_TABLE;

            GameDataManager.Instance.LoadTables();

            while (GameDataManager.currentStatus == GameDataManager.STATUS.LOADING)
                yield return null;

            if (GameDataManager.currentStatus == GameDataManager.STATUS.ERROR)
            {
                initProgress = ENUM_INIT_PROGRESS.LOAD_TABLE_ERROR;
                yield break;
            }
        }

        // UI 팝업에서 호출할 수도 있음. 
        public static void StartPredownload()
        {
            BundleManager.Instance.StartPredownload();
        }
        #endregion Initialize Game

        #region Login
        public static void StartLoginProgress()
        {
            initProgress = ENUM_INIT_PROGRESS.START_LOGIN_PROCESS;
        }

        private IEnumerator _CrtLoginProcess()
        {
            while (initProgress != ENUM_INIT_PROGRESS.START_LOGIN_PROCESS)
                yield return null;

            yield return StartCoroutine(_CrtServerList());
            if (loginProgress == ENUM_LOGIN_PROCESS.ERROR)
                yield break;

            yield return StartCoroutine(_CrtRequestLogin());
            if (loginProgress == ENUM_LOGIN_PROCESS.ERROR)
                yield break;

            Debug.Log("<color=#FFFF00FF>Login Process Finished..</color>");
        }

        /// <summary>
        /// 서버 리스트 요청. 
        /// </summary>
        private IEnumerator _CrtServerList()
        {
            loginProgress = ENUM_LOGIN_PROCESS.REQUEST_SERVER_LIST;

            //NetworkGateway.RequestServerList();

            while (loginProgress == ENUM_LOGIN_PROCESS.REQUEST_SERVER_LIST)
                yield return null;
        }

        /// <summary>
        /// 로그인 요청. 
        /// </summary>
        private IEnumerator _CrtRequestLogin()
        {
            //string savedUserID = GameDataManager.savedUserID;
            string savedUserID = "bds_user";

            if (savedUserID.IsNull())
            {
                loginProgress = ENUM_LOGIN_PROCESS.WAIT_LOGIN_ID;
            }
            else
            {
                loginProgress = ENUM_LOGIN_PROCESS.REQUEST_LOGIN;
                //NetworkGateway.RequestLogin(savedUserID);
            }

            while (loginProgress == ENUM_LOGIN_PROCESS.WAIT_LOGIN_ID || loginProgress == ENUM_LOGIN_PROCESS.REQUEST_LOGIN)
                yield return null;
        }
        #endregion Login

        #region Mono
        protected override void Awake()
        {
            if (Time.timeScale < 1f)
                Time.timeScale = 1f;

            base.Awake();
            EngineManager.LoadGraphicQuality();
            FixedStringData.LoadTable();
        }

        protected override void Start()
        {
            base.Start();
            BundleManager.ClearManager();
            AssetManager.ClearManager();

            /*NetworkManager.onServerList += _OnResponseServerList;
            NetworkManager.onLogin += _OnResponseLogin;
            NetworkManager.onLoginComplete += _OnLoginComplete;*/

            _StartInitialize();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            /*NetworkManager.onServerList -= _OnResponseServerList;
            NetworkManager.onLogin -= _OnResponseLogin;
            NetworkManager.onLoginComplete -= _OnLoginComplete;*/
        }
        #endregion Mono

        #region Game Methods
        public static void MoveNextScene()
        {
            LoadingScene.RequestSceneAsync(ProjectDefines.SCENE_NAME_GAME, ProjectDefines.BUNDLE_NAME_SCENE);
        }
        #endregion Game Methods

        #region OnEvent
        /// <summary>
        /// 서버 리스트 요청 결과. 
        /// </summary>
        private void _OnResponseServerList(byte byResult)
        {
            /*switch (byResult)
            {
                case (byte)Net_ResponseServerList.RESULT.SUCCESS:
                    loginProgress = ENUM_LOGIN_PROCESS.RESPONSE_SERVER_LIST;
                    break;

                case (byte)Net_ResponseServerList.RESULT.ERROR:
                    {
                        loginProgress = ENUM_LOGIN_PROCESS.ERROR;

                        // 로그인 프로세스 재시작. 
                        initProgress = ENUM_INIT_PROGRESS.LOGIN_FAILED;
                        StartCoroutine(_CrtLoginProcess());
                    }
                    break;
            }*/
        }

        /// <summary>
        /// 로그인 요청 결과. 
        /// </summary>
        private void _OnResponseLogin(byte byResult)
        {
            /*switch (byResult)
            {
                case (byte)Net_ResponseLogin.RESULT.SUCCESS:
                    loginProgress = ENUM_LOGIN_PROCESS.RESPONSE_LOGIN;
                    break;

                case (byte)Net_ResponseLogin.RESULT.ERROR:
                    {
                        loginProgress = ENUM_LOGIN_PROCESS.ERROR;

                        // 로그인 프로세스 재시작. 
                        initProgress = ENUM_INIT_PROGRESS.LOGIN_FAILED;
                        StartCoroutine(_CrtLoginProcess());
                    }
                    break;
            }*/
        }

        /// <summary>
        /// 로그인 성공. 
        /// </summary>
        private void _OnLoginComplete()
        {
            loginProgress = ENUM_LOGIN_PROCESS.FINISHED;
        }
        #endregion OnEvent
    }
}